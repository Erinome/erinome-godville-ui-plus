from waflib.TaskGen import before, feature

import subst_all
import taskgen_utils


class dyncp(subst_all.cp):
	"""
	Delete `outdated_outputs` (silently ignoring errors if `suppress_errors` is True), then copy
	`inputs` to `quiet_outputs` without imposing a dependency on targets. It is meant to be used
	when target names differ between runs.
	"""

	@property
	def _outputs(self):
		return self.quiet_outputs

	def run(self):
		for node in self.outdated_outputs:
			try:
				node.delete()
			except OSError:
				if not self.suppress_errors:
					raise
		super().run()


@feature("dyncp")
@before("process_source")
def process_dyncp(tgen):
	sources = tgen.to_nodes(tgen.source)
	if len(sources) != 1:
		raise ValueError("Only a single source is allowed: %r" % tgen)

	task = tgen.create_task("dyncp", sources, [ ])
	task.quiet_outputs = tgen.to_out_nodes(tgen.target)
	task.outdated_outputs = tgen.to_out_nodes(getattr(tgen, "outdated", [ ]))
	task.suppress_errors = getattr(tgen, "suppress_errors", False)
	tgen.source = [ ]
