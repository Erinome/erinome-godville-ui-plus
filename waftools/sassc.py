from waflib.Task    import Task
from waflib.TaskGen import extension


def configure(cnf):
	try:
		import sass
	except ImportError:
		cnf.find_program(["sassc", "sass"], var="SASS")
		cnf.env.SASS_CLASS = "sassc"
	else:
		cnf.env.SASS_CLASS = "libsass"


class libsass(Task):
	color = "CYAN"

	def run(self):
		import sass

		self.outputs[0].write(
			sass.compile(filename=self.inputs[0].abspath(), **self.options),
			encoding="utf-8",
		)


class sassc(Task):
	run_str = "${SASS} ${SASS_FLAGS} ${SRC} ${TGT}"
	color = "CYAN"


@extension(".sass", ".scss")
def compile_sass(tgen, source):
	task = tgen.create_task(tgen.env.SASS_CLASS, source, source.change_ext(".css"))
	task.options = getattr(tgen, "options", { })
