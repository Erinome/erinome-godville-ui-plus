import contextlib

from waflib.Task    import Task
from waflib.TaskGen import before, feature


class post(Task):
	always_run = True
	color = "CYAN"

	def keyword(self):
		return "Sending"

	def __str__(self):
		if len(self.inputs) == 1:
			return self.inputs[0].srcpath()
		else:
			return "{%s}" % ", ".join([node.srcpath() for node in self.inputs])

	def run(self):
		# Avoid importing at global level to keep other commands usable without forcing to install
		# third-party libraries.
		import requests

		with contextlib.ExitStack() as stack:
			files = [
				(field, (node.name, stack.enter_context(open(node.abspath(), "rb")), mime))
				for node, (field, path, mime) in zip(self.inputs, self.files)
			]
			with requests.post(self.url, files=files, data=self.data, stream=True) as response:
				response.raise_for_status() # Handle 4xx and 5xx.


@feature("http_post")
@before("process_source")
def process_http_post(tgen):
	files = getattr(tgen, "files", [ ])
	task = tgen.create_task("post", tgen.to_nodes([path for field, path, mime in files]), [ ])
	task.url = tgen.url
	task.data = getattr(tgen, "data", None)
	task.files = files
	tgen.source = [ ]
