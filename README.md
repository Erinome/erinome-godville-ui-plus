# По-русски (in Russian)

## Структура репозитория

    .
    |-build/ (упакованные расширения для браузеров и артефакты сборки)
    |-fonts/ (шрифт с редкими символами, а также всё необходимое для его сборки)
    |-help_guide/ (картинки для окошка помощи)
    |-images/ (картинки: фон и лопатка)
    |-source/ (весь исходный код)
        |-chrome/ (специфичные для Chrome файлы)
        |-edge/ (специфичные для MS Edge файлы)
        |-firefox/ (специфичные для Firefox файлы)
        |-firefox-we/ (специфичные для Firefox/WebExtensions файлы)
        |-opera/ (специфичные для Opera 12 файлы)
        |-vendor/ (сторонние библиотеки)
        |-mixins/ (фрагменты, подключаемые в несколько файлов)
        |-loader/ (скрипты, подгружающие ресурсы расширения на страницы сайта)
        |-common/ (файлы, касающиеся нескольких разделов сайта)
        |-superhero/ (файлы, касающиеся страницы героя /superhero)
        |-log/ (файлы, касающиеся хроник /duels/log/*)
        |-forum/ (файлы, касающиеся форума /forums/*)
        |-*.js (разные скрипты)
        |-*.sass (разные файлы стилей)
    |-waftools/ (модули для сборки дополнения)
    |-* (разные файлы сборки дополнения, номера версии, файл обновления для FF и т. п.)

Обычно код редактируется только в `source/*` и `source/{common,superhero,log,forum}/**`.

*Примечание:* внесение обновлений в репозиторий может происходить с задержкой до нескольких дней
относительно даты релиза новых версий.


## Сборка из исходных текстов

Скрипты сборки написаны на [Python][] (с использованием [Waf Build System][]), так что вам нужно
установить интерпретатор версии **>=3.4**. Далее предполагается, что он прописан в `PATH`.

Прежде всего выполните `pip install -Ur requirements.txt`, чтобы установить зависимости.

* `python waf configure` — подготавливает проект к сборке. Должна быть вызвана в начале работы и при
  добавлении новых модулей в `waftools/`.
* `python waf build` (или просто `python waf`) — (пере)собирает проект.
* `python waf clean` — удаляет артефакты, позволяя сделать чистую сборку.
* `python waf distclean` — удаляет артефакты и конфигурацию. После этого нужно снова вызвать
  `configure`.
* `python waf upload` — загружает релиз на сайт. *Вряд ли вам понадобится.*

Команды можно записывать подряд друг за другом: например, `python waf clean build` или
`python waf distclean configure build`.

Кроме того, есть `waf_gui.pyw` — надстройка над `waf` с графическим интерфейсом.


### Сборка шрифта

`fonts/eGUIp.otf` — шрифт, в котором собраны некоторые редкие символы, чтобы они корректно
отображались, даже если в системных шрифтах на компьютере пользователя таких символов нет. Этот файл
включен в репозиторий и не перестраивается при стандартной сборке, поэтому обычно вам не нужно
о нём беспокоиться.

Если же вы хотите его изменить (например, добавить новые символы), вам потребуется проделать
следующее:

1. Установите программу [FontForge][].
2. Переконфигурируйте проект.
    * Если утилита `fontforge` прописана в `PATH`, просто выполните `python waf configure`;
    * Если нет, запишите путь к ней (включая имя файла) в переменную окружения `FONTFORGE`, после
      чего выполните `python waf configure`.
3. Скачайте набор шрифтов [DejaVu][].
4. Скачайте шрифт [Symbola][UFAS].
5. Поместите файлы `DejaVuSans.ttf`, `DejaVuSerif.ttf` и `Symbola.otf` в директорию `fonts/`.

Теперь вам доступна команда `python waf --font`, собирающая шрифт согласно инструкциям в `wscript`.


# In English (по-английски)

## Repository tree structure

    .
    |-build/ (packed browser add-ons and build artifacts)
    |-fonts/ (a font with rare characters and everything needed to build it)
    |-help_guide/ (images for help dialog)
    |-images/ (images: default background and shovel)
    |-source/ (all the source code)
        |-chrome/ (Chrome-specific files)
        |-edge/ (MS Edge-specific files)
        |-firefox/ (Firefox-specific files)
        |-firefox-we/ (Firefox/WebExtensions-specific files)
        |-opera/ (Opera 12-specific files)
        |-vendor/ (third-party libraries)
        |-mixins/ (code fragments included into several files)
        |-loader/ (scripts injecting add-on’s resources into the pages)
        |-common/ (files concerning several sections of the site)
        |-superhero/ (files concerning /superhero page)
        |-log/ (files concerning /duels/log/* pages)
        |-forum/ (files concerning /forums/* pages)
        |-*.js (various scripts)
        |-*.sass (various style sheets)
    |-waftools/ (modules for project building)
    |-* (other files of project building, version number, update file for Firefox add-on, etc.)

Usually, only the code from `source/*` and `source/{common,superhero,log,forum}/**` should
be edited.

*Note:* repository updates might be delayed for a few days after release of every new extension
version.


## Building from source

Build scripts are in [Python][] (and use the [Waf Build System][]) so you’ll need an interpreter of
version **>=3.4**. Then, assuming it is in your `PATH`:

First, run `pip install -Ur requirements.txt` to install dependencies.

* `python waf configure` — (re)configure the project. Must be run at the start or when a new module
  is added under `waftools/`.
* `python waf build` (or just `python waf`) — (re)build the project.
* `python waf clean` — remove build artifacts (for performing a full rebuild).
* `python waf distclean` — remove artifacts and reset the configuration. After that, `configure`
  has to be called again.
* `python waf upload` — upload a release to the website. *It’s a maintainer’s command.*

Commands can be chained, e.g., `python waf clean build` or `python waf distclean configure build`.

There is also `waf_gui.pyw` — a wrapper around `waf` with a graphical interface.


### Building the font

`fonts/eGUIp.otf` is a font consisting of several not widely supported characters. It is included
so that those characters can be displayed correctly even if they are missing from user’s system
fonts. This file is checked into the version control and is not rebuilt during standard build
process so usually you don’t need to worry about it.

If, however, you’d like to change it (e.g., add more characters), you’ll need to perform some
preparation steps:

1. Install [FontForge][].
2. Reconfigure the project.
    * If `fontforge` executable is accessible through the `PATH`, just run `python waf configure`;
    * Otherwise, set `FONTFORGE` environment variable to the path to it (including the file’s name),
      then run `python waf configure`.
3. Download [DejaVu][] font set.
4. Download [Symbola][UFAS] font.
5. Put `DejaVuSans.ttf`, `DejaVuSerif.ttf`, and `Symbola.otf` into `fonts/`.

Now you should be able to run `python waf --font`, and it will compose the font according to the
rules in `wscript`.


[Python]: https://www.python.org
[Waf Build System]: https://waf.io
[FontForge]: https://fontforge.org
[DejaVu]: https://dejavu-fonts.github.io
[UFAS]: https://dn-works.com/ufas/
