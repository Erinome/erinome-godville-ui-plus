var lastFights = {};
(function() {

/**
 * @private
 * @param {!HTMLCollection} rows
 * @returns {!Array<!GUIp.common.activities.LastFightsEntry>}
 */
var _getLastFights = function(rows) {
	var result = [],
		tFormat = localStorage.getItem('tampm'),
		parsedDate, date, ftype, link, href;
	for (var i = 1, len = rows.length; i < len; i++) {
		date = rows[i].firstElementChild;
		ftype = date.nextElementSibling;
		link = ftype.getElementsByTagName('a')[0];
		href = link.href;
		parsedDate = GUIp.common.parseDateTime(date.textContent)
		result[i - 1] = {
			date: +parsedDate,
			type: GUIp.common.activities.parseFightType(link.textContent),
			logID: href.slice(href.lastIndexOf('/') + 1),
			success: ftype.textContent.includes('✓')
		};
		// fix formatting from AM/PM to 24H when it is set in page settings
		if (tFormat === '24h' && /AM|PM/.test(date.textContent)) {
			date.textContent = GUIp.common.formatTime(parsedDate,'westerndate') + ' ' + GUIp.common.formatTime(parsedDate,'simpletime')
		}
	}
	// replicate souls from dungeons
	if (+storage.get('Logger:Souls'))
	for (var i = 0, len = result.length; i < len; i++) {
		if (result[i].type !== 'dungeon') {
			continue;
		}
		result.push({
			date: result[i].date,
			type: 'souls',
			logID: result[i].logID,
			success: result[i].success
		});
	}
	return result.sort(function(a, b) { return a.date - b.date; });
};

lastFights.init = function() {
	var table = document.getElementsByTagName('table')[0],
		rows = table.getElementsByTagName('tr'),
		activities = GUIp.common.activities.load(storage),
		byID = Object.create(null),
		soulsCombined = Object.create(null),
		desc = null,
		i, len, act, cell, href, id, content;
	GUIp.common.activities.updateLastFights(
		storage,
		GUIp.common.activities.readThirdEyeFromLS(storage.god_name),
		GUIp.common.parseJSON(storage.get('ThirdEye:Gaps')) || [],
		activities,
		GUIp.common.activities.loadStatuses(storage),
		_getLastFights(rows)
	);
	for (i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
			byID[act.logID] = act;
		} else if (act.type === 'souls') {
			id = act.logID || act.inID;
			if (!soulsCombined[id]) {
				soulsCombined[id] = act;
				soulsCombined[id].src = 1;
			} else {
				if (soulsCombined[id].result < 0) {
					soulsCombined[id].result = act.result; // actually this never should happen
				} else if (act.result >= 0) {
					soulsCombined[id].result += act.result;
				}
				soulsCombined[id].src++;
			}
		}
	}
	table.classList.add('e_last_fights');
	for (i = 1, len = rows.length; i < len; i++) {
		cell = rows[i].firstElementChild.nextElementSibling;
		href = cell.getElementsByTagName('a')[0].href;
		id = href.slice(href.lastIndexOf('/') + 1);
		content = '';
		if ((act = soulsCombined[id]) && (act.result || act.src < 2)) { // todo: don't show when souls unavailable
			desc = GUIp.common.activities.describe(act, desc);
			content += '<span class="' + desc.class + '" title="' + desc.title + '">' + desc.content + '</span>';
		}
		if ((act = byID[id]) && act.result) {
			desc = GUIp.common.activities.describe(act, desc);
			content += '<span class="' + desc.class + '" title="' + desc.title + '">' + desc.content + '</span>';
		}
		if (content) {
			cell.insertAdjacentHTML('beforeend',
				'<div class="e_fight_result">' + content + '</div>'
			);
		}
	}
};

})(); // lastFights
