(function(worker) {
'use strict';

//! include './init.js';
//! include './topic_formatting.js';
//! include './topic_other.js';
//! include './newspaper/package.js';
//! include './god_page.js';
//! include './last_fight.js';

// main code
var i, len, topic, isForum, isTopic, isSubs, queryString, topics;

var setInitVariables = function() {
	GUIp.common.forceDesktopPage();
	GUIp.common.exposeThemeName(localStorage.ui_s || 'th_classic');
	isForum = /^\/+forums\/+show\//.test(location.pathname);
	isTopic = /^\/+forums\/+show_topic\//.test(location.pathname);
	isSubs = /^\/+forums\/+show\/+1(?:\/|$)/.test(location.pathname) && location.hash.includes('#guip_subscriptions');
	queryString = GUIp.common.parseQueryString(location.search);
	storage.god_name = GUIp.common.getCurrentGodname();
	if (!storage.get('ForumSubscriptions')) {
		storage.set('ForumSubscriptions', '{}');
		storage.set('ForumInformers', '{}');
	}
	topics = JSON.parse(storage.get('ForumSubscriptions')) || {};
};

//! include 'mixins/module.js';

registerModule('forum', function() {
	try {
		setInitVariables();
		GUIp.common.addCSSFromString(storage.get('UserCss'));

		if (/^\/+news(?:\/|$)/.test(location.pathname)) {
			newspaper.init();
			return;
		}

		if (/^\/+gods\//.test(location.pathname)) {
			godPage.init();
			return;
		}

		if (/^\/+hero\/+last_fight(?:\/|$)/.test(location.pathname)) {
			lastFights.init();
			return;
		}

		if (/^\/+forums\/+subs(?:\/|$)/.test(location.pathname)) {
			rearrangeUnreadSubs();
			return;
		}

		if (isSubs) {
			prepareSubscriptionsList();
		}

		if (isForum) {
			addSmallElements();
		}

		if (!isSubs && (isForum || isTopic)) {
			addLinks();
			addTooltipEmulation();
		}

		document.body.classList.add('forum');
		GUIp.common.setPageBackground(storage.get('Option:useBackground'));

		if (isTopic) {
			addFormattingButtons(true);
			fixGodnamePaste();
			improveTopic();
		}

		if (topics && !isSubs && (isForum || isTopic)) {
			addSubscriptionsLink();
		}
	} catch (e) {
		GUIp.common.error(e);
	}
});

})(this);
