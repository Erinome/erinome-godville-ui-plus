var newspaper = {};
(function() {

//! include './gpc.js';

var _annotateExpensiveBingoItem = function(html) {
	// there is an artifact named "heart of the matter" so this regex must be case-sensitive
	return /^(?:золотой кирпич|бесценный дар|инвайт на Годвилль|старую шмотку|шкуру разыскиваемого |(?:шкуру|рога|ухо|глаз|(?:влюбл.нное )?сердце|ребро|лапу|копыта) босса |зубастого триббла|бонус за подряд|призовой сундук|пасхал(?:ку|ьное яйцо)|(?:светящуюся |кран)тыкву|тыкву скорой помощи|юбилейный золотой|заморск.. |морскую (?:джемчужину|златоустрицу|суперзвезду)|морской приз|(?:ларец|сундучок|ящик) из моря|босс(?:коин|бух|оножку|ье)|(?:купон на|coupon for) .*©|golden brick|(?:really )?priceless gift|quest completion certificate|Godville invite|invite to Godville|old piece of equipment|[Hh]ide of a wanted |(?:fur|horn|ear|eye|heart|rib|lucky paw|hoof) of the [A-Z]|hungry tribble|side job bonus|(?:prize|goodies) chest|easter egg$|glowing pumpkin|best boss (?:award|pin|statuette)|🦃|outlandish |overseas |chained crate|donation box|pirate coffer|exotic seashell|bosscoin)/.test(html.replace(/<[^]*?>/g, '')) ? (
		'<span class="e_bingo_expensive" title="' + GUIp_i18n.bingo_expensive_artifact + '">' + html + '</span>'
	) : html;
};

var _highlightExpensiveInBingo = function() {
	var list = $id('b_inv'),
		html = list.innerHTML,
		replaced = html.replace(/[^\s:,.][^:,.]*/g, _annotateExpensiveBingoItem);
	if (replaced !== html) {
		list.innerHTML = replaced;
	}
};

var _prepareBingo = function() {
	var bingoItems = $Q('#bgn td span:not(.bgnk)'),
		bingoPattern = '',
		bingoTries = 0,
		node;
	if (bingoItems.length) {
		bingoPattern = Array.from(bingoItems, function(a) { return a.textContent; }).join('|');
		node = $id('l_clicks');
		if (node && node.style.display !== 'none') { // when filling the last time, #b_cnt is not updated
			node = $id('b_cnt');
			bingoTries = (node && parseInt(node.textContent)) || 0;
		}
	}
	storage.set('Newspaper:bingoItems', bingoPattern);
	storage.set('Newspaper:bingoTries', bingoTries);
	_highlightExpensiveInBingo();
};

var _updateCoupon = function() {
	var button = $id('coupon_b'),
		prize = button.disabled ? '' : button.previousElementSibling.previousSibling.nodeValue.trim();
	storage.set('Newspaper:couponPrize:raw', prize);
	storage.set('Newspaper:couponPrize', prize && prize.replace(/^(?:an?|the|some) /, ''));
};

var _updateAdvert = function() {
	var advert, button = document.querySelector('input#ad_b'); // there's an error in the newspaper: same `ad_b` id is assigned to both input and its nearest div, so we can't use $id() here
	if (!button) {
		return;
	}
	advert = button.disabled ? '' : button.parentNode.parentNode.firstChild.textContent.trim();
	storage.set('Newspaper:activeAdvert', advert);
	storage.set('Newspaper:activeAdvert:button', button.disabled ? '' : button.value);
};

var _updateForecast = function() {
	var fc = '', fcText = '', fcSB = '', forecasts = Array.from(document.querySelectorAll('.fc > p'), function(a) { return a.textContent; }).join('\n');
	if (forecasts) {
		fc = GUIp.common.parseForecasts(forecasts);
		fcText = forecasts;
		if (fc.includes('specbosses')) {
			var key, input = fcText.toLowerCase();
			for (var key in GUIp.common.bossAbilitiesList) {
				if (input.includes(key) || input.includes(GUIp.common.bossAbilitiesList[key])) {
					fcSB = (worker.GUIp_locale === 'ru' ? key : GUIp.common.bossAbilitiesList[key]).replace(/^.{1}/, function(a) { return a.toUpperCase(); });
				}
			}
		}
	}
	storage.set('Newspaper:dailyForecast', fc);
	storage.set('Newspaper:dailyForecastText', fcText);
	storage.set('Newspaper:dailyForecast:specBoss', fcSB);
};

var _fixExoticCharacters = function() {
	var fc = $q('.fc > p'),
		html = fc.innerHTML;
	if (html.includes('🗳')) {
		fc.innerHTML = html.replace(/🗳[\uFE0E\uFE0F]?/,
			'<span class="e_emoji e_emoji_ballot' + (GUIp.common.renderTester.testChar('🗳') ? '' : ' eguip_font') +
			'">$&</span>'
		);
	}
};

newspaper.init = function() {
	var node, observer;
	if ((node = $id('bgn_t'))) {
		observer = GUIp.common.newMutationObserver(_prepareBingo);
		observer.observe(node, {childList: true});
		observer.observe($id('l_clicks'), {attributes: true, attributeFilter: ['style']});
		_prepareBingo();
	}
	if ((node = $id('coupon_b'))) {
		GUIp.common.newMutationObserver(_updateCoupon).observe(node, {attributes: true, attributeFilter: ['disabled']});
		_updateCoupon();
	}
	if ((node = document.querySelector('input#ad_b'))) {
		GUIp.common.newMutationObserver(_updateAdvert).observe(node, {attributes: true, attributeFilter: ['disabled']});
		_updateCoupon();
	}
	GUIp.common.try2(gpc.init);
	_updateForecast();
	_fixExoticCharacters();
	GUIp.common.renderTester.deinit();
};

})(); // newspaper
