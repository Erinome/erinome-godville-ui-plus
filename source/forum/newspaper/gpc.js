var gpc = newspaper.gpc = {};
(function() {

gpc._available = false;
gpc._valueNode = null;
gpc._input = null;
gpc._threshold = 0;

var _scrollIntoView = function() {
	var header = $id('gp_bat').parentNode.firstElementChild;
	if (header.getBoundingClientRect().top < 0 ||
		gpc._input.getBoundingClientRect().bottom > document.documentElement.clientHeight
	) {
		try {
			gpc._input.scrollIntoView({block: 'center'});
		} catch (e) {
			header.scrollIntoView();
		}
	}
};

var _check = function() {
	var value = 0;
	if (!gpc._available || !gpc._threshold || (value = parseInt(gpc._valueNode.textContent)) < gpc._threshold) {
		GUIp.common.notif.hide('gpc');
	} else if (!GUIp.common.notif.enabled) {
		GUIp.common.playSound(storage.get('Option:informerCustomSound') || 'arena');
	} else {
		GUIp.common.notif.show(
			'[%] ' + storage.god_name,
			GUIp_i18n.fmt('gpc_filled', value),
			parseFloat(storage.get('Option:informerAlertsTimeout')) * 1e3,
			_scrollIntoView,
			'gpc'
		);
	}
};

var _updateThreshold = function() {
	gpc._threshold = gpc._input.checkValidity() ? gpc._input.valueAsNumber : 0;
	storage.set('Newspaper:godpowerCap:userThreshold', gpc._threshold);
	_check();
};

var _initNotifier = function() {
	var node = $id('gpc_err');
	gpc._threshold = +storage.get('Newspaper:godpowerCap:userThreshold');
	gpc._valueNode = $id('gpc_val');
	node.insertAdjacentHTML('beforebegin',
		'<div class="e_gpc_settings">' +
			GUIp_i18n.fmt('gpc_remind_when', '<input type="number" min="1" max="200" />') +
		'</div>'
	);
	gpc._input = node.previousSibling.lastElementChild;
	if (gpc._threshold) {
		// we set it from JS and not from HTML because this way the cursor will be placed after the number
		gpc._input.value = gpc._threshold;
	}
	GUIp.common.addListener(gpc._input, 'keydown', function(ev) {
		if (ev.keyCode === 13) {
			this.blur();
		}
	});
	GUIp.common.addListener(gpc._input, 'input', GUIp.common.debounce(1e3, _updateThreshold));
	GUIp.common.addListener(gpc._input, 'change', function() {
		GUIp.common.notif.initialize();
		_updateThreshold();
	});
	GUIp.common.newMutationObserver(_check).observe(gpc._valueNode, {childList: true});
	if (gpc._threshold) {
		_check();
	}
};

gpc.init = function() {
	var dischargeBtn = $id('gp_cap_use');
	storage.set('Newspaper:godpowerCap', (gpc._available = !dischargeBtn.disabled));
	if (!gpc._available) return; // already discharged today, not interested anymore
	GUIp.common.addListener(dischargeBtn, 'click', function() {
		storage.set('Newspaper:godpowerCap', (gpc._available = false));
		storage.set('Newspaper:godpowerCap:userThreshold', (gpc._threshold = 0));
		GUIp.common.notif.hide('gpc');
	});
	GUIp.common.newMutationObserver(function() {
		// the button gets disabled each time someone discharges the cap.
		// avoid accessing `localStorage` whenever possible.
		if (!gpc._available && !dischargeBtn.disabled) {
			// it got re-enabled after we tried to discharge it
			storage.set('Newspaper:godpowerCap', (gpc._available = true));
			_updateThreshold(); // read it from the input
		}
	}).observe(dischargeBtn, {attributes: true, attributeFilter: ['disabled']});
	_initNotifier();
};

})(); // newspaper.gpc
