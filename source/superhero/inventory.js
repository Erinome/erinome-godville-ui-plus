// ui_inventory
var ui_inventory = worker.GUIp.inventory = {};

ui_inventory.observer = {
	config: {
		childList: true,
		attributes: true,
		subtree: true,
		attributeFilter: ['style']
	},
	func: function(mutations) {
		var losingItem = false,
			mutation, target;
		for (var i = 0, len = mutations.length; i < len; i++) {
			mutation = mutations[i];
			target = mutation.target;
			switch (target.tagName) {
				case 'LI':
					// see `ui_inventory.getInventory` for an explanation
					if (mutation.type === 'attributes' && target.style.overflow === 'hidden' && target.parentNode) {
						losingItem = true;
					}
					break;
				case 'UL':
					if (mutation.addedNodes.length || mutation.removedNodes.length) {
						return ui_inventory._update();
					}
			}
		}
		if (losingItem) ui_inventory._updateThrottled();
	},
	target: ['#inv_block_content ul']
};
ui_inventory.bossPartRE = /[]/;
ui_inventory.model = null;
ui_inventory._updateThrottled = null;
ui_inventory.init = function() {
	if (ui_data.isFight) {
		ui_inventory.model = new ui_inventory.Model;
		return;
	}
	this.forbiddenCraft = ui_storage.createVar('Option:forbiddenCraft', function(text) { return text || ''; });
	this.bossPartRE = new RegExp('^(?:влюблённое )?(' + GUIp_i18n.bp_types.join('|') + ')' + GUIp_i18n.bp_prefix, 'i');
	this._updateThrottled = GUIp.common.throttle(500, this._update);
	this._createCraftButtons();
	this._update();
	ui_observers.start(ui_inventory.observer);
};
ui_inventory.rebuildCustomCraft = function() {
	var groupType, rebuiltGroup, craftGroups = document.getElementsByClassName('craft_group');
	for (var i = 0, len = craftGroups.length; i < len; i++) {
		while (craftGroups[i].firstChild) {
			craftGroups[i].removeChild(craftGroups[i].firstChild);
		}
		if ((groupType = /b_b|b_r|r_r/.exec(craftGroups[i].className))) {
			rebuiltGroup = ui_inventory._createCraftSubGroup(groupType[0]);
			rebuiltGroup.className = craftGroups[i].className;
			craftGroups[i].parentNode.replaceChild(rebuiltGroup,craftGroups[i]);
		}
	}
	ui_inventory._update();
};
ui_inventory._createCraftButtons = function() {
	var invContent = document.getElementById('inv_block_content'),
		craftContent = document.createElement('span');
	if (!invContent) {
		return;
	}
	craftContent.className = 'craft_button span';
	craftContent.appendChild(ui_inventory._createCraftButton(worker.GUIp_i18n.b_b, 'b_b', worker.GUIp_i18n.b_b_hint));
	craftContent.appendChild(ui_inventory._createCraftSubGroup('b_b'));
	craftContent.appendChild(ui_inventory._createCraftButton(worker.GUIp_i18n.b_r, 'b_r', worker.GUIp_i18n.b_r_hint));
	craftContent.appendChild(ui_inventory._createCraftSubGroup('b_r'));
	craftContent.appendChild(ui_inventory._createCraftButton(worker.GUIp_i18n.r_r, 'r_r', worker.GUIp_i18n.r_r_hint));
	craftContent.appendChild(ui_inventory._createCraftSubGroup('r_r'));
	ui_storage.addImmediateListener('Option:relocateCraftButtons', function(relocate) {
		invContent.insertAdjacentElement(relocate === 'true' ? 'beforebegin' : 'afterend', craftContent);
	});
};
ui_inventory._createInspectButton = function(item_name) {
	var a = document.createElement('a');
	a.className = 'inspect_button' + (ui_data.inShop ? ' crb_inactive' : '');
	a.title = worker.GUIp_i18n.fmt('ask_to_inspect', ui_data.char_sex[0], item_name);
	a.textContent = '?';
	GUIp.common.addListener(a, 'click', ui_inventory._inspectButtonClick.bind(null, item_name));
	return a;
};
ui_inventory._inspectButtonClick = function(item_name, ev) {
	ev.preventDefault();
	ui_utils.setVoice(ui_words.inspectPhrase(worker.GUIp_i18n.trophy + item_name));
};
ui_inventory._createCraftButton = function(combo, combo_list, hint) {
	var a = document.createElement('a');
	if (!combo_list.includes('customCraft-')) {
		a.className = 'craft_button ' + combo_list + (ui_data.inShop ? ' crb_inactive' : '');
		a.title = worker.GUIp_i18n.fmt('ask_to_craft', ui_data.char_sex[0], hint);
	} else {
		a.className = 'craft_button' + (ui_data.inShop ? ' crb_inactive' : '');
		a.id = combo_list;
		a.title = worker.GUIp_i18n.fmt('ask_to_craft_a', ui_data.char_sex[0], hint);
	}
	a.innerHTML = combo;
	GUIp.common.addListener(a, 'click', ui_inventory._craftButtonClick.bind(a, combo_list));
	return a;
};
ui_inventory._craftButtonClick = function(combo_list, ev) {
	ev.preventDefault();
	var rand = Math.floor(Math.random()*ui_inventory[combo_list].length),
		items = ui_inventory[combo_list][rand];
	if (Math.random() < 0.5) {
		items.reverse();
	}
	ui_utils.setVoice(ui_words.craftPhrase(items[0] + worker.GUIp_i18n.and + items[1]));
};
ui_inventory._createCraftSubGroup = function(combo_group) {
	var span = document.createElement('span'),
		customCraftGroups = ui_inventory._customCraftCheckGrp(combo_group);
	span.className = 'craft_group ' + combo_group + ' hidden';
	span.appendChild(document.createElement('wbr'));
	span.appendChild(document.createTextNode('('));
	for (var i = 0, len = customCraftGroups.length; i < len; i++) {
		span.appendChild(document.createElement('wbr'));
		span.appendChild(ui_inventory._createCraftButton(customCraftGroups[i].t, 'customCraft-'+combo_group+'-'+customCraftGroups[i].i, customCraftGroups[i].d));
	}
	span.appendChild(document.createTextNode(')'));
	span.appendChild(document.createElement('wbr'));
	return span;
};
ui_inventory._customCraftCheckGrp = function(grp) {
	var grpm, result = [];
	switch (grp) {
		case 'b_b': grpm = /[жb]/i; break;
		case 'b_r': grpm = /[сm]/i; break;
		default: grpm = /[нr]/i;
	}
	for (var i = 0, len = ui_words.base.custom_craft.length; i < len; i++) {
		var customCombo = ui_words.base.custom_craft[i];
		if (customCombo.q) {
			continue;
		}
		if (grpm.test(customCombo.g)) {
			result.push(customCombo);
		}
	}
	return result;
};
ui_inventory._getFirstLetter = function(name) {
	return name.charAt(name.search(/[a-zа-яё]/i)).toLowerCase(); // may return ''
};
ui_inventory._update = function() {
	var i, len, inv, li, item, count, ilink, types,
		trophyType = {},
		forbiddenCraft = '';
	if (ui_data.isSail || ui_data.isMining) {
		return;
	}
	forbiddenCraft = ui_inventory.forbiddenCraft.get();
	// Parse items
	inv = ui_inventory.getInventory();
	ui_inventory.model = inv.md;
	for (i = 0, len = inv.lis.length; i < len; i++) {
		li = inv.lis[i];
		item = inv.md.getName(i);
		count = inv.md.getCount(i);
		// color items and add buttons
		if ((ilink = inv.useLinks[i])) { // usable item
			if (inv.usableTypes[i]) {
				li.classList.add('type-' + inv.usableTypes[i].replace(/ /g,'-'));
			} else if (!ui_utils.hasShownInfoMessage) {
				ui_utils.hasShownInfoMessage = true;
				ui_utils.showMessage('info', {
					title: worker.GUIp_i18n.unknown_item_type_title,
					content: '<div>' + worker.GUIp_i18n.unknown_item_type_content + '<b>"' + inv.descriptions[i] + '</b>"</div>'
				});
			}
			if (!(forbiddenCraft.includes('usable') || (forbiddenCraft.includes('b_b') && forbiddenCraft.includes('b_r'))) &&
				!ui_words.usableItemTypeMatch(inv.descriptions[i], 'to arena box') && !ui_words.usableItemTypeMatch(inv.descriptions[i], 'temper box')) {
				trophyType[item] = {a:true, b:inv.md.isBold(i), c:count};
			}
			// confirm dialog on item activation for firefox on Android. note that official mobile version has its own confirm dialog doing the same thing
			if (GUIp.common.isAndroid && !ui_data.isMobile) {
				var ilink2;
				if (!li.getElementsByTagName('a')[1]) {
					ilink2 = document.createElement('a');
					GUIp.common.addListener(ilink2, 'click', function(link) {
						if (!link.classList.contains('div_link') || worker.confirm(link.title + '. ' + worker.GUIp_i18n.confirm_item_activate)) {
							link.click();
						}
					}.bind(null, ilink));
					ilink.insertAdjacentElement('afterend', ilink2);
					ilink.style.display = 'none';
				} else {
					ilink2 = li.getElementsByTagName('a')[1];
				}
				if (ilink2) {
					ilink2.title = ilink.title;
					ilink2.className = ilink.className;
					ilink2.textContent = '★';
				}
			} else if (!li.classList.contains('improved') && inv.gpCosts[i]) {
				ui_utils.addGPConfirmation(worker.$(ilink), inv.gpCosts[i],
					worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n.to_activate + item));
			}
			if (ui_improver.dailyForecast.get().includes('cheapactivatables')) {
				li.classList.add('usable_item_fcc');
				li.classList.remove('usable_item_fce');
			} else if (ui_improver.dailyForecast.get().includes('expensiveactivatables')) {
				li.classList.add('usable_item_fce');
				li.classList.remove('usable_item_fcc');
			} else {
				li.classList.remove('usable_item_fcc', 'usable_item_fce');
			}
		} else if (inv.md.isHealing(i)) { // healing item
			// if item quantity has increased, it seems that class needs to be re-added again
			li.classList.add('heal_item');
			if (!(forbiddenCraft.includes('heal') || (forbiddenCraft.includes('b_r') && forbiddenCraft.includes('r_r')))) {
				trophyType[item] = {b:false,c:count};
			}
		} else {
			if (inv.md.isBold(i)) { // bold item
				if (!(forbiddenCraft.includes('b_b') && forbiddenCraft.includes('b_r')) && inv.md.isCraftable(i)) {
					trophyType[item] = {b:true,c:count};
				}
			} else {
				if (!(forbiddenCraft.includes('b_r') && forbiddenCraft.includes('r_r')) && inv.md.isCraftable(i)) {
					trophyType[item] = {b:false,c:count};
				}
			}
		}
		if (!li.classList.contains('improved') && !ui_inventory._uninspectables.test(item)) {
			li.appendChild(ui_inventory._createInspectButton(item));
		}
		li.classList.toggle('wanted_item', ui_improver.wantedItems.get() && ui_improver.wantedItems.get().test(item));
		li.classList.toggle('bingo_item', ui_improver.bingoTries.get() && (item === '❄' || (ui_improver.bingoItems.get() && ui_improver.bingoItems.get().test(item))));
		li.classList.toggle('e_craft_impossible',
			!inv.md.isCraftable(i) || inv.craftableForPrefix[ui_inventory._getFirstLetter(item)] === 1
		);
		li.classList.add('improved');
	}

	types = Object.keys(ui_words.base.usable_items);
	for (i = 0, len = types.length; i < len; i++) {
		ui_informer.update(types[i], inv.md.hasType(types[i]));
	}
	ui_informer.update('transform!', inv.boldCount >= 2 && inv.md.hasType('transformer'));
	ui_informer.update('smelt!', inv.md.hasType('smelter') && ui_stats.Gold() >= 3000);

	ui_inventory._updateCraftCombos(trophyType);
};
ui_inventory.tryUpdate = function() {
	if (document.querySelector('#inv_block_content li.improved')) {
		ui_inventory._update();
	}
};
ui_inventory._updateCraftCombos = function(trophy_type) {
	// Склейка трофеев, формирование списков
	ui_inventory.b_b = [];
	ui_inventory.b_r = [];
	ui_inventory.r_r = [];
	var item_names = worker.Object.keys(trophy_type).sort(),
		ccraft_enabled = ui_storage.getFlag('Option:enableCustomCraft');
	if (item_names.length) {
		for (var i = 0, len = item_names.length; i < len; i++) {
			for (var j = i + 1; j < len && i < len - 1; j++) {
				if (item_names[i][0] === item_names[j][0]) {
					if (trophy_type[item_names[i]].b && trophy_type[item_names[j]].b) {
						if (!this.forbiddenCraft.get().includes('b_b')) {
							ui_inventory.b_b.push([item_names[i], item_names[j]]);
						}
					} else if (!trophy_type[item_names[i]].b && !trophy_type[item_names[j]].b) {
						if (!this.forbiddenCraft.get().includes('r_r')) {
							ui_inventory.r_r.push([item_names[i], item_names[j]]);
						}
					} else {
						if (!this.forbiddenCraft.get().includes('b_r')) {
							ui_inventory.b_r.push([item_names[i], item_names[j]]);
						}
					}
				} else {
					break;
				}
			}
			if (trophy_type[item_names[i]].c > 1) {
				if (trophy_type[item_names[i]].b) {
					ui_inventory.b_b.push([item_names[i], item_names[i]]);
				} else {
					ui_inventory.r_r.push([item_names[i], item_names[i]]);
				}
			}
		}
		for (var i = 0, len = ui_words.base.custom_craft.length; i < len; i++) {
			var customCombo = ui_words.base.custom_craft[i];
			if (/[жb]/i.test(customCombo.g)) {
				ui_inventory['customCraft-b_b-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.b_b.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory._getFirstLetter(ui_inventory.b_b[j][0]))) {
							if (/[аa]/i.test(customCombo.g) && !(trophy_type[ui_inventory.b_b[j][0]].a || trophy_type[ui_inventory.b_b[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-b_b-'+customCombo.i].push(ui_inventory.b_b[j]);
						}
					}
				}
			}
			if (/[сm]/i.test(customCombo.g)) {
				ui_inventory['customCraft-b_r-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.b_r.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory._getFirstLetter(ui_inventory.b_r[j][0]))) {
							if (/[аa]/i.test(customCombo.g) && !(trophy_type[ui_inventory.b_r[j][0]].a || trophy_type[ui_inventory.b_r[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-b_r-'+customCombo.i].push(ui_inventory.b_r[j]);
						}
					}
				}
			}
			if (/[нr]/i.test(customCombo.g)) {
				ui_inventory['customCraft-r_r-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.r_r.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory._getFirstLetter(ui_inventory.r_r[j][0]))) {
							if (/[аa]/i.test(customCombo.g) && !(trophy_type[ui_inventory.r_r[j][0]].a || trophy_type[ui_inventory.r_r[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-r_r-'+customCombo.i].push(ui_inventory.r_r[j]);
						}
					}
				}
			}
		}
	}
	// fixme! this shouldn't be called until everything in improver has finished loading
	if (!ui_improver.isFirstTime) {
		ui_improver.calculateButtonsVisibility();
	}
};

ui_inventory._uncraftables = /ошмёток босса |триббла|шкуру разыскиваемого |старую шмотку|золотой кирпич|ареналин|подземелин|заплывин|билет в провал|путевку в круиз|движок по фазе|слезинку бога в янтаре|^заморск.. |(?:ларец|сундучок|ящик) из моря|босскоин|shred of the |tribble$|hide of a wanted |old piece of equipment|^golden brick|arena(?:lin| ticket)|ticket to sail|invite to dungeon|sail setter|dungeon key|hammer of realignment|holy powercell|^outlandish |^overseas |chained crate|donation box|pirate coffer|bosscoin/i;
ui_inventory._unsellables = /триббла|шкуру разыскиваемого |(?:купон на|coupon for) .*©|tribble$|hide of a wanted /i;
ui_inventory._uninspectables = /триббла|шкуру разыскиваемого |tribble$|hide of a wanted /i;
ui_inventory._coupons = /^(купон на|coupon for) .*©/i

ui_inventory._areBossPartsUnsellable = function() {
	// boss parts' sellability is dynamic: they cannot be sold to a roadside trader
	return ui_stats.heroState() === 'trading' && ui_stats.sProgress() === 1 && !ui_stats.townName();
};

ui_inventory.Model = function() {
	this._names = [];
	this._namesDict = null; // lazy
	this._typesSet = Object.create(null);
	this._counts = [];
	this._useLinks = [];
	// healing:   0x1
	// bold:      0x2
	// boss part: 0x4
	// craftable: 0x8
	// sellable:  0x10 (lazy)
	this._props = [];
	this._healingCount = 0;
	this._bossPartsCount = 0;
	this._unsellableCount = -1; // lazy
};

ui_inventory.Model.prototype = {
	constructor: ui_inventory.Model,

	// lazy initialization
	_calcNamesDict: function() {
		var result = Object.create(null);
		for (var i = 0, len = this._names.length; i < len; i++) {
			result[this._names[i].toLowerCase()] = i;
		}
		return (this._namesDict = result);
	},
	_calcUnsellable: function() {
		var count = 0;
		for (var i = 0, len = this._names.length; i < len; i++) {
			if (ui_inventory._unsellables.test(this._names[i])) {
				count += this._counts[i];
			} else {
				this._props[i] += 0x10;
			}
		}
		return (this._unsellableCount = count);
	},

	get length() { return this._names.length; },
	// querying properties of a single item
	getName: function(i) { return this._names[i]; },
	getCount: function(i) { return this._counts[i]; },
	isUsable: function(i) { return !!this._useLinks[i]; },
	isHealing: function(i) { return !!(this._props[i] & 0x1); },
	isBold: function(i) { return !!(this._props[i] & 0x2); },
	isBossPart: function(i) { return !!(this._props[i] & 0x4); },
	isCraftable: function(i) { return !!(this._props[i] & 0x8); },
	isSellable: function(i) {
		var p = 0x0;
		if (this._unsellableCount < 0) { // not initialized yet
			this._calcUnsellable();
		}
		p = this._props[i];
		// healing items (0x1) are excluded as well
		return (p & 0x15) === 0x10 || !(!(p & 0x4) || ui_inventory._areBossPartsUnsellable());
	},

	// detecting presence of particular items
	getItemIndex: function(name) { return (this._namesDict || this._calcNamesDict())[name.toLowerCase()]; },
	hasType: function(type) { return type in this._typesSet; },

	// aggregate functions
	countHealing: function() { return this._healingCount; },
	countUnsellable: function() {
		var u = this._unsellableCount,
			bp = this._bossPartsCount;
		return (
			this._healingCount + (u >= 0 ? u : this._calcUnsellable()) +
			// avoid calling `_areBossPartsUnsellable` if we don't need it
			(bp && ui_inventory._areBossPartsUnsellable() && bp)
		);
	}
};

ui_inventory.getInventory = function() {
	var allLis = document.querySelectorAll('#inv_block_content li'),
		md = new ui_inventory.Model,
		boldCount = 0,
		lis = [],
		craftableForPrefix = {},
		usableTypes = [],
		descriptions = [],
		gpCosts = [],
		i = 0,
		name = '',
		count = 0,
		healing = false,
		bold = false,
		bossPart = false,
		craftable = false,
		s = '',
		li, node, m;
	for (var j = 0, jlen = allLis.length; j < jlen; j++) {
		li = allLis[j];
		// when an item is being removed from the inventory, its `overflow` is set to 'hidden' and its `height`,
		// `padding-top`, and `padding-bottom` are being decreased rapidly. when they reach 0, `display` is set to
		// 'none' and all other attributes are cleared.
		// on the other hand, when an item is added to the inventory, it appears instantly (and glows, but that is
		// irrelevant to us). so we just need to check `overflow` and `display` to exclude lost items.
		if (li.style.overflow === 'hidden' || li.style.display === 'none') {
			continue;
		}
		lis[i] = li;
		name = md._names[i] = li.firstChild.textContent;
		count = md._counts[i] =
			((node = li.childNodes[1]) && (m = /\((\d+)\s*(?:шт|pcs)\.?\)$/.exec(node.nodeValue)) && +m[1]) || 1;
		// usable items
		if ((node = md._useLinks[i] = li.querySelector('a.item_act_link_div'))) {
			// extract item's description, godpower cost, and type
			m = /^(.*?)\s*\((.*?)\)$/.exec(node.title) || [, node.title, node.title];
			s = descriptions[i] = m[1];
			if ((s = usableTypes[i] = ui_words.usableItemType(s, !(
				gpCosts[i] = (m = /\d+\s*%/.exec(m[2])) ? parseInt(m[0]) : 0
			)) || '')) {
				md._typesSet[s] = true;
			}
		} else {
			usableTypes[i] = descriptions[i] = '';
			gpCosts[i] = 0;
		}
		// various properties
		if ((healing = li.style.fontStyle === 'italic')) {
			md._healingCount += count;
		}
		if ((bold = (s = li.style.fontWeight) === 'bold' || !!+s)) {
			boldCount += count;
		}
		if ((bossPart =
			bold && (m = ui_inventory.bossPartRE.exec(name)) && (s = name[m[0].length]) && s === s.toUpperCase()
		)) {
			md._bossPartsCount += count;
		}
		if ((craftable =
			!bossPart && (!ui_inventory._uncraftables.test(name) || ui_inventory._coupons.test(name)) && !!(s = ui_inventory._getFirstLetter(name))
		)) {
			craftableForPrefix[s] = (craftableForPrefix[s] || 0) + count;
		}
		md._props[i] = healing | bold << 1 | bossPart << 2 | craftable << 3;
		i++;
	}
	return {
		md: md,
		boldCount: boldCount,
		lis: lis,
		craftableForPrefix: craftableForPrefix,
		usableTypes: usableTypes,
		descriptions: descriptions,
		gpCosts: gpCosts,
		useLinks: md._useLinks
	};
};

/**
 * Split boss part into its components. Be sure to check that the item is a boss part first.
 *
 * @param {string} item
 * @returns {{type: string, bossAndLevel: string}} There will be no level if we haven't gathered the pairs.
 */
ui_inventory.splitBossPart = function(item) {
	var m = ui_inventory.bossPartRE.exec(item);
	return {type: m[1].toLowerCase(), bossAndLevel: item.slice(m[0].length)};
};

/**
 * @returns {!Array<string>}
 */
ui_inventory.getBingoSlots = function() {
	var regex = ui_improver.bingoItems.get();
	return regex ? regex.source.split('|') : [];
};

/**
 * @returns {number}
 */
ui_inventory.countBingoItems = function() {
	var regex = ui_improver.bingoItems.get(),
		bonus = 0,
		graph = [],
		inv = ui_inventory.model,
		slotsCount = 0,
		itemName = '',
		itemCount = 0,
		slots, filteredSlots;
	if (!regex) return 0;
	slots = regex.source.split('|');
	slotsCount = slots.length;
	for (var i = 0, len = inv.length; i < len; i++) {
		itemName = inv.getName(i).toLowerCase();
		if (itemName === '❄') {
			// snowflakes are always at the end of the list so they don't ban slots
			bonus += inv.getCount(i);
			continue;
		}
		if (!regex.test(itemName)) {
			continue; // fast path
		}
		filteredSlots = [];
		for (var j = 0; j < slotsCount; j++) {
			if (itemName.includes(slots[j])) {
				filteredSlots.push(j);
			}
		}
		itemCount = inv.getCount(i);
		while (itemCount-- > 0) {
			graph.push(filteredSlots);
		}
	}
	return Math.min(GUIp.common.findBipartiteMatching(graph, slotsCount) + bonus, slotsCount);
};

/**
 * @private
 * @param {?RegExp} regex
 * @param {number} constraints
 * @returns {number}
 */
ui_inventory._countLike = function(regex, constraints) {
	var result = 0,
		inv = ui_inventory.model,
		bingoRegex = ui_improver.bingoItems.get(),
		name = '',
		mask = 0x0;
	for (var i = 0, len = inv.length; i < len; i++) {
		name = inv.getName(i);
		if (regex && !regex.test(name)) continue;
		if (constraints) {
			mask = constraints & 0x3 ? inv.isHealing(i) + 1 : 0x0;
			if (constraints & 0xC) {
				mask += inv.isBold(i) ? 0x8 : 0x4;
			}
			if (constraints & 0x30) {
				mask += inv.isUsable(i) ? 0x20 : 0x10;
			}
			if (constraints & 0xC0) {
				mask += inv.isCraftable(i) ? 0x80 : 0x40;
			}
			if (constraints & 0x300) {
				mask += inv.isSellable(i) ? 0x200 : 0x100;
			}
			if (constraints & 0xC00) {
				mask += bingoRegex && bingoRegex.test(name) ? 0x800 : 0x400;
			}
			if (mask !== constraints) continue;
		}
		result += inv.getCount(i);
	}
	return result;
};

/**
 * Count how many items match the regex (case-insensitive) and satisfy the constraints.
 * `constraints` are a string composed of the following letters:
 *
 * + 'h' - Healing.
 * + 'H' - Non-healing.
 * + 'b' - Any bold.
 * + 'B' - Non-bold.
 * + 'a' - Any activatable.
 * + 'A' - Non-activatable.
 * + 'c' - Can be used for crafting.
 * + 'C' - Cannot be used for crafting.
 * + 's' - Sellable.
 * + 'S' - Unsellable.
 * + 'g' - Can be used in Bingo.
 * + 'G' - Cannot be used in Bingo.
 *
 * @param {string}  regex
 * @param {(string|number)} [constraints]
 * @returns {number}
 */
ui_inventory.countLike = function(regex, constraints) {
	var mask = 0x0,
		unrecognized = '';
	if (typeof constraints === 'string') {
		for (var i = 0, len = constraints.length; i < len; i++) {
			switch (constraints.charCodeAt(i)) {
				case 0x48: /*H*/ mask |= 0x1; break;
				case 0x68: /*h*/ mask |= 0x2; break;
				case 0x42: /*B*/ mask |= 0x4; break;
				case 0x62: /*b*/ mask |= 0x8; break;
				case 0x41: /*A*/ mask |= 0x10; break;
				case 0x61: /*a*/ mask |= 0x20; break;
				case 0x43: /*C*/ mask |= 0x40; break;
				case 0x63: /*c*/ mask |= 0x80; break;
				case 0x53: /*S*/ mask |= 0x100; break;
				case 0x73: /*s*/ mask |= 0x200; break;
				// codes above once were public API and must not be changed
				case 0x47: /*G*/ mask |= 0x400; break;
				case 0x67: /*g*/ mask |= 0x800; break;
				default: unrecognized += constraints[i];
			}
		}
		if (unrecognized) {
			throw new Error('unknown constraints in gv.inventoryCountLike: "' + unrecognized + '"');
		}
		constraints = mask;
	} else {
		constraints >>>= 0;
		if (constraints >= 0x400) {
			throw new Error('unknown constraints in gv.inventoryCountLike: ' + constraints);
		}
	}
	// sadly, the regex is recompiled each time this function is invoked
	return ui_inventory._countLike(regex !== '' ? new RegExp(regex, 'i') : null, constraints);
};
