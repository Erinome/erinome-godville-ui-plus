// ui_infdebug
var ui_infdebug = GUIp.infdebug = {};

/**
 * @private
 * @param {*} value
 * @param {boolean} [verbose]
 * @returns {string}
 */
ui_infdebug._formatValue = function(value, verbose) {
	var s = '',
		type = typeof value,
		len = 0,
		keys;
	if (type === 'string') {
		type = GUIp.common.escapeHTML(value);
		return verbose || value.length <= 25 ? (
			'&#34;' + type + '&#34;'
		) : '<abbr title="&#34;' + type + '&#34;">&#34;' + GUIp.common.escapeHTML(value.slice(0, 24)) + '…&#34;</abbr>';
	}
	if (value) {
		if (Array.isArray(value)) {
			s = value.map(ui_infdebug._formatValueVerbose).join(', ');
			return verbose || !s ? (
				'[' + s + ']'
			) : '<abbr title="[' + s.replace(/<[^>]*>/g, '') + ']">array(' + value.length + ')</abbr>';
		}
		if (type === 'function') {
			return value.name ? GUIp.common.escapeHTML(value.name) : 'function';
		}
		if (type === 'object') {
			if (value instanceof RegExp) {
				type = value.toString();
				return verbose || value.source.length <= 25 ? GUIp.common.escapeHTML(type) : (
					'<abbr title="' + GUIp.common.escapeHTML(type) + '">/' +
					GUIp.common.escapeHTML(value.source.slice(0, 24)) + '…' + type.slice(type.lastIndexOf('/')) +
					'</abbr>'
				);
			}
			keys = Object.keys(value).sort();
			len = keys.length;
			for (var i = 0; i < len; i++) {
				if (i === 25) {
					s += '    …\n';
					break;
				}
				type = keys[i];
				s += /^\w+$/.test(type) ? (
					'    ' + type + ',\n'
				) : '    &#34;' + GUIp.common.escapeHTML(type) + '&#34;,\n';
			}
			type = Object.prototype.toString.call(value);
			return '<abbr title="{\n' + s + (
				type === '[object Object]' ? '}">object(' : '}">' + GUIp.common.escapeHTML(type.slice(1, -1)) + '('
			) + len + ')</abbr>';
		}
	} else if (type === 'undefined' || value !== value) {
		return '<span style="color: red;">' + value + '</span>';
	}
	return String(value); // no need to escape: the value is either boolean, number, or null
};

/**
 * @private
 * @param {*} value
 * @returns {string}
 */
ui_infdebug._formatValueVerbose = function(value) {
	return ui_infdebug._formatValue(value, true);
};

/**
 * @private
 * @param {*} value
 * @returns {string}
 */
ui_infdebug._formatProperty = function(value) {
	var s = String(value);
	if (/^[A-Za-z_]\w*$/.test(s)) {
		return '.' + s;
	} else {
		return '[</strong>' + ui_infdebug._formatValue(value) + '<strong>]';
	}
};

/**
 * @private
 * @param {{eResult: *, computed: boolean, object: {type: string, eResult: *}, property: {type: string, eResult: *}}} n
 * @returns {string}
 */
ui_infdebug._formatMember = function(n) {
	var result, valueType = typeof n.object.value;
	if (/*n.object.type === 'Literal' &&*/ (valueType === 'object' || valueType === 'function')) {
		result = GUIp.common.escapeHTML(n.object.raw);
	} else {
		result = ui_infdebug._formatValue(n.object.eResult);
	}
	return result + ui_infdebug._formatProperty(n.property.name || n.property.eResult);
};

/**
 * @param {!Array<{type: string, eResult: *}>} trace
 * @returns {string}
 */
ui_infdebug.formatTraceHTML = function(trace) {
	var s = '', line;
	for (var i = 0, len = trace.length; i < len; i++) {
		var n = trace[i];

		switch (n.type) {
		case 'Literal':
		case 'E_GVLiteral':
		case 'ArrayExpression':
			continue;

		case 'ConditionalExpression':
			line =
				ui_infdebug._formatValue(n.test.eResult) + ' <strong>?</strong>\xA0' +
				ui_infdebug._formatValue(n.consequent.eResult) + ' <strong>:</strong>\xA0' +
				ui_infdebug._formatValue(n.alternate.eResult);
			break;

		case 'UnaryExpression':
			if (typeof n.argument.eResult === 'number' && (n.operator === '-' || n.operator === '+')) {
				continue;
			}
			line =
				'<strong>' + GUIp.common.escapeHTML(n.operator) + '</strong>' +
				ui_infdebug._formatValue(n.argument.eResult);
			break;

		case 'BinaryExpression':
		case 'LogicalExpression':
			line =
				ui_infdebug._formatValue(n.left.eResult) +
				' <strong>' + GUIp.common.escapeHTML(n.operator) + '</strong>\xA0' +
				ui_infdebug._formatValue(n.right.eResult);
			break;

		case 'MemberExpression':
			line = '<strong>' + ui_infdebug._formatMember(n) + '</strong>';
			break;

		case 'E_GVExpression':
			line = '<strong>gv' + ui_infdebug._formatProperty(n.property.eResult) + '</strong>';
			break;

		case 'CallExpression':
			line = '<strong>';
			if (n.callee.type === 'MemberExpression') {
				line += ui_infdebug._formatMember(n.callee);
			} else {
				line += ui_infdebug._formatValue(n.callee.eResult);
			}
			line += '(</strong>';
			for (var j = 0, jlen = n.arguments.length; j < jlen; j++) {
				if (j) line += ', ';
				line += ui_infdebug._formatValue(n.arguments[j].eResult);
			}
			line += '<strong>)</strong>';
			break;

		case 'E_MatchExpression':
			line =
				ui_infdebug._formatValue(n.text.eResult) +
				' <strong>' + (n.negated ? '!' : '') + (n.insensitive ? '~*' : '~') + '</strong>\xA0' +
				ui_infdebug._formatValue(n.pattern.value !== undefined ? n.pattern.value : n.pattern.eResult);
			break;

		default:
			throw new Error('expression type "' + n.type + '" is unsupported in custom informers');
		}

		s += '<li>' + line + ' →\xA0' + ui_infdebug._formatValue(n.eResult, true) + '</li>';
	}
	return s;
};

/**
 * @private
 * @param {string} msg
 * @returns {string}
 */
ui_infdebug._formatError = function(msg) {
	return '<div>' + GUIp_i18n.custom_informers_error + ': ' + GUIp.common.escapeHTML(msg) + '.</div>';
};

/**
 * @param {!GUIp.expr.DebugInfo} debug
 * @returns {string}
 */
ui_infdebug.formatDebugHTML = function(debug) {
	var s = '<ol>' + ui_infdebug.formatTraceHTML(debug.trace) + '</ol><br />';
	if (debug.error != null) {
		s += ui_infdebug._formatError(debug.error);
	} else {
		s += '<div>' + GUIp_i18n.custom_informers_check_result + '<strong>' + !!debug.result + '</strong></div>';
	}
	return s;
};

/**
 * @param {string} text
 * @param {!Object} gv
 * @param {function(string): *} gvCache
 * @returns {string}
 */
ui_infdebug.formatExprHTML = function(text, gv, gvCache) {
	var ast;
	try {
		ast = ui_expr.compile(text, true);
	} catch (e) {
		return ui_infdebug._formatError(e.message);
	}
	return ui_infdebug.formatDebugHTML(ui_expr.debug(ast, gv, gvCache));
};
