var actions = ui_mining.actions = {};
(function() {

/**
 * @private
 * @type {?GUIp.storage.Var<!Array<!GUIp.mining.Frame>>}
 */
actions._frames = null;

/**
 * @private
 * @type {?GUIp.mining.FrameEx}
 */
actions._curFrx = null;

/**
 * @private
 * @type {!Array<string>}
 */
actions._bossTitles = [];

/**
 * @private
 * @type {?Element}
 */
actions._summaryNode = null;

actions.init = function() {
	var bossName = '',
		conditionNode;
	if (!ui_data.isMining) {
		return;
	}
	ui_storage.set('Log:current', ui_data.logId);
	ui_mining.bitsPerByte = ui_stats.Bits_Per_Byte();
	if ((conditionNode = document.getElementById('bps_ra'))) {
		ui_mining.conditions = GUIp.common.mining.parseConditions(conditionNode.textContent);
	}
	actions._frames = ui_storage.createVar('Log:' + ui_data.logId + ':data',
		function(text) { return GUIp.common.parseJSON(text) || []; },
		JSON.stringify
	);
	for (var i = 0; i < 4; i++) {
		bossName = ui_stats.EnemySingle_Name(i + 1);
		actions._bossTitles[i] = bossName[0] === '@' ? (
			ui_mining.ownBoss = i,
			'$&: '
		) : (
			bossName.slice(4, -1) + ' (' + (ui_stats.EnemySingle_Godname(i + 1) || '?') + '):\xA0'
		).replace(/\$/g, '$$$$');
	}
	GUIp.common.tooltips.watchSubtree(document.getElementsByClassName('wrmap')[0]);
};

/**
 * @private
 * @param {!GUIp.mining.VisibleFrame} vfrm
 * @returns {!GUIp.mining.FrameEx}
 */
var _updateModel = function(vfrm) {
	var frames = actions._frames.get(),
		f = analysis.findOrInsertFrame(frames, vfrm);
	if (f.index === frames.length - 1) {
		analysis.limitFrameSequenceLength(frames, 4); // keep all frames of the current step
	}
	actions._frames.save();
	return f.frx;
};

/**
 * @private
 * @param {!GUIp.mining.FrameEx} frx
 * @param {!Element} mapBlock
 */
var _showBitsSummary = function(frx, mapBlock) {
	var frm = frx.frm,
		collected = frm.collected,
		totalCollected = collected[0] + collected[1] + collected[2] + collected[3],
		s = GUIp_i18n.bits_summary +
			'<abbr title="' + GUIp_i18n.fmt('bits_free', frx.unlockedBits + ' + ' + frx.lockedBits) + '">' +
			(frx.unlockedBits + frx.lockedBits) +
			'</abbr> + <abbr title="' + GUIp_i18n.fmt('bits_collected', collected.join('\xA0+\xA0')) + '">' +
			totalCollected +
			'</abbr> = <abbr title="' + GUIp_i18n.bits_total + '">' +
			(frx.unlockedBits + frx.lockedBits + totalCollected + frm.destroyed) +
			'</abbr>';
	if (frm.destroyed) {
		s += ' − <abbr title="' + GUIp_i18n.bits_destroyed + '">' + frm.destroyed + '</abbr>';
	}
	if (actions._summaryNode) {
		actions._summaryNode.innerHTML = s;
	} else {
		mapBlock = mapBlock.getElementsByClassName('wrmap')[0].parentNode;
		mapBlock.insertAdjacentHTML('beforeend', '<div class="e_bits_summary' + (GUIp.common.isAndroid ? ' e_select_disabled' : '') + '">' + s + '</div>');
		actions._summaryNode = mapBlock.lastChild;
		GUIp.common.tooltips.watchSubtree(actions._summaryNode);
	}
};

/**
 * @private
 * @returns {?HTMLCollection}
 */
var _getStepMarks = function() {
	var bar = document.getElementById('turn_pbar');
	return bar && bar.getElementsByClassName('st_div');
};

/**
 * You should perform any desired manipulations with step marks prior to calling this function.
 *
 * @private
 * @param {!GUIp.mining.FrameEx} frx
 * @param {number} step
 * @param {?HTMLCollection} stepMarks
 */
var _highlightImportantStep = function(frx, step, stepMarks) {
	var shouldHighlight = !!frx.lockedBits && (
			ui_mining.conditions.clarity ? step <= 4 || !(step % 3) : step >= 6 && !((step - 1) % 5)
		),
		a, b;
	if (stepMarks && (a = stepMarks[3 - ui_mining.ownBoss])) {
		if (shouldHighlight) {
			a.classList.add('e_important_step_mark');
			a.classList.remove('hidden');
		} else {
			a.classList.remove('e_important_step_mark');
		}
	}
	a = ui_data.isMobile ? document.getElementsByClassName('e_mt_fight_log')[0] : document.querySelector('#r_map .block_title');
	if (a && (b = a.getElementsByClassName('e_step')[0])) {
		b.classList.toggle('e_important_step', shouldHighlight);
		b.title = shouldHighlight ? GUIp_i18n.rmap_important_step : '';
	} else if (shouldHighlight) {
		a.innerHTML = a.innerHTML.replace(/\(((?:шаг|step) \d+)\)/,
			'<span class="e_step e_important_step" title="' + GUIp_i18n.rmap_important_step +
			'">(<span class="e_step_inner">$1</span>)</span>'
		);
	}
};

/**
 * The main entry point for datamine-related code.
 */
actions.processMap = function() {
	var settings = ui_storage.getList('Option:rangeMapSettings'),
		replaceWithAlpha = settings.includes('balph'),
		mapBlock = ui_data.isMobile ? document.getElementsByClassName('wrmap')[0].parentNode : document.getElementById('r_map'),
		stepMarks = _getStepMarks(),
		i = 0,
		pos = 0,
		len = 0,
		cell, cells, vfrm;
	if (!mapBlock) {
		GUIp.common.error('no map block for mining found');
		return;
	}
	cells = mapBlock.getElementsByClassName('rmc');
	vfrm = analysis.createVisibleFrame(cells);
	actions._curFrx = _updateModel(vfrm);
	mapBlock.classList.toggle('e_rmap_grid', settings.includes('grid'));
	for (i = 0; i < 4; i++) {
		if ((pos = actions._curFrx.pos[i]) < 0) continue;
		len = actions._curFrx.frm.collected[i];
		cell = cells[pos].firstElementChild;
		if (!cell) continue;
		cell.title = cell.title.replace(/(?:ваш|чужой) босс|(?:Your|Other) Boss|$/,
			actions._bossTitles[i] +
			(len >= ui_mining.bitsPerByte ? Math.floor(len / ui_mining.bitsPerByte) + '+[' : '[') +
			len % ui_mining.bitsPerByte + ']'
		);
		if (replaceWithAlpha && i === ui_mining.ownBoss) {
			cell.textContent = String.fromCharCode(i + 65); // 'A'
		}
	}
	_showBitsSummary(actions._curFrx, mapBlock);
	if (stepMarks) {
		// hide marks for bosses that will not move anymore (e.g., because they are dead)
		for (pos = 0, len = stepMarks.length; pos < len; pos++) {
			i = (pos + ui_mining.ownBoss + 1) & 0x3;
			stepMarks[pos].classList.toggle('hidden',
				actions._curFrx.pos[i] < 0 || (vfrm.step === 40 && i < ui_mining.ownBoss)
			);
		}
	}
	_highlightImportantStep(actions._curFrx, vfrm.step, stepMarks);
};

/**
 * An alternative entry point used when step number has changed but map hasn't. This function exists not only
 * as optimization but to prevent double-improving as well.
 */
actions.processStepChange = function() {
	var step = ui_stats.currentStep(),
		stepMarks = _getStepMarks();
	if (step === 40 && stepMarks) {
		for (var i = 3 - ui_mining.ownBoss, len = stepMarks.length; i < len; i++) {
			stepMarks[i].classList.add('hidden');
		}
	}
	_highlightImportantStep(actions._curFrx, step, stepMarks);
};

})(); // ui_mining.actions
