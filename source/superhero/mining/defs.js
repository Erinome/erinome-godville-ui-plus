/**
 * @typedef {Object} GUIp.mining.VisibleFrame
 * @property {number} step
 * @property {!GUIp.common.mining.Map} map
 * @property {number} collected - Number of bits our own boss has.
 */

/**
 * Visible and inferred data related to one datamine frame (a frame is a quarter of a step).
 *
 * @typedef {Object} GUIp.mining.Frame
 * @property {number} step
 * @property {!GUIp.common.mining.Map} map
 * @property {!Array<number>} collected
 * @property {number} destroyed
 */

/**
 * A `Frame` augmented with several useful computed properties. Does not provide any additional information besides
 * what is contained in the wrapped `Frame`; thus it is always possible to construct a `FrameEx` out of `Frame`.
 *
 * @typedef {Object} GUIp.mining.FrameEx
 * @property {!GUIp.mining.Frame} frm
 * @property {!Array<number>} pos - Positions of bosses on the map (-1 if dead).
 * @property {number} unlockedBits
 * @property {number} lockedBits
 */

// map legend can be found in common/mining.js

/**
 * @readonly
 * @type {number}
 */
ui_mining.ownBoss = -1;

/**
 * @readonly
 * @type {number}
 */
ui_mining.bitsPerByte = 8;

/**
 * @readonly
 * @type {!Object<string, boolean>}
 */
ui_mining.conditions = {};
