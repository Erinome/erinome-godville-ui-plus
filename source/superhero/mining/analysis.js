var analysis = ui_mining.analysis = {};
(function() {

/**
 * @param {!HTMLCollection} cells
 * @returns {!GUIp.common.mining.Map}
 */
analysis.parseMap = function(cells) {
	return Array.from(cells, GUIp.common.mining.parseMapCell, ui_mining.ownBoss);
};

/**
 * @param {!HTMLCollection} cells
 * @returns {!GUIp.mining.VisibleFrame}
 */
analysis.createVisibleFrame = function(cells) {
	return {
		step: ui_stats.currentStep(),
		map: analysis.parseMap(cells),
		collected: ui_stats.Bits()
	};
};

/**
 * Create a frame with what we can see on the page. This is called only for the very first frame in the datamine.
 *
 * @private
 * @param {!GUIp.mining.VisibleFrame} vfrm
 * @returns {!GUIp.mining.Frame}
 */
var _createFrame = function(vfrm) {
	var collected = [0, 0, 0, 0];
	collected[ui_mining.ownBoss] = vfrm.collected;
	return {step: vfrm.step, map: vfrm.map, collected: collected, destroyed: 0};
};

/**
 * @param {!GUIp.mining.Frame} frm
 * @returns {!GUIp.mining.FrameEx}
 */
analysis.createFrameEx = function(frm) {
	var map = frm.map,
		pos = [-1, -1, -1, -1],
		unlockedBits = 0,
		lockedBits = 0;
	for (var p = 0, plen = map.length; p < plen; p++) {
		switch (map[p] & 0xF) {
			case 0x3: lockedBits += 2; break;
			case 0x4: unlockedBits++; break;
			case 0x5: unlockedBits += 2; break;
			case 0x8: case 0x9: case 0xA: case 0xB: // a boss
				pos[map[p] & 0x3] = p;
				break;
		}
	}
	return {frm: frm, pos: pos, unlockedBits: unlockedBits, lockedBits: lockedBits};
};

analysis._tmpArr = new Float64Array(4);

/**
 * Compare two frames and calculate all the properties we want.
 *
 * @private
 * @param {!GUIp.mining.Frame} oldFrm
 * @param {!GUIp.mining.VisibleFrame} vfrm
 * @returns {!GUIp.mining.FrameEx}
 */
var _createFrameExWithOld = function(oldFrm, vfrm) {
	var oldMap = oldFrm.map,
		newMap = vfrm.map,
		collected = oldFrm.collected.slice(),
		destroyed = oldFrm.destroyed,
		pos = [-1, -1, -1, -1],
		unlockedBits = 0,
		lockedBits = 0,
		cur = 0x0,
		tmp = 0x0,
		prev = 0x0,
		pickedUp = analysis._tmpArr;
	pickedUp[0] = pickedUp[1] = pickedUp[2] = pickedUp[3] = 0;
	// process dropped bits first
	for (var p = 0, plen = newMap.length; p < plen; p++) {
		cur = newMap[p];
		if (cur <= 0x2) continue; // fast path
		// check for destroyed bits
		tmp = cur & 0x4F;
		if (tmp >= 0x3 && tmp <= 0x5) { // bits without a skull
			if ((oldMap[p] & 0x4F) === 0x40 && destroyed) { // nothing except a skull
				// there was a skull, and now there are bits. time flowed backwards :(
				destroyed -= tmp === 0x4 ? 1 : 2;
			}
		} else if (tmp === 0x40) { // nothing except a skull
			prev = oldMap[p] & 0x4F;
			if (prev >= 0x3 && prev <= 0x5) { // bits without a skull
				// there were bits, and now there is a skull
				destroyed += prev === 0x4 ? 1 : 2;
			}
		}
		// check for dropped / picked up bits
		if ((cur & 0xF) === 0x4) { // a bit
			prev = oldMap[p];
			unlockedBits++;
			if ((prev & 0xC) === 0x8 && collected[prev &= 0x3] % ui_mining.bitsPerByte) { // a boss
				// there was a boss, and now there is a bit
				collected[prev]--;
			}
		} else if ((cur & 0xC) === 0x8) { // a boss
			prev = oldMap[p] & 0xF;
			pos[cur &= 0x3] = p;
			if (prev >= 0x3 && prev <= 0x5) { // bits
				// there were bits, and now there is a boss
				pickedUp[cur] = prev === 0x4 ? 1 : 2;
			}
		} else if ((cur &= 0xF) === 0x3 || cur === 0x5) { // bits
			prev = oldMap[p];
			if (cur === 0x3) {
				lockedBits += 2;
			} else {
				unlockedBits += 2;
			}
			if ((prev & 0xC) === 0x8) { // a boss
				// there was a boss, and now there are bits. time flowed backwards :(
				collected[prev & 0x3] -= 2; // can break a byte
			}
		}
	}
	// then process picked up bits
	collected[0] += pickedUp[0];
	collected[1] += pickedUp[1];
	collected[2] += pickedUp[2];
	collected[3] += pickedUp[3];
	// luckily, at least for our own boss we have a reliable info source
	collected[ui_mining.ownBoss] = vfrm.collected;
	return {
		frm: {step: vfrm.step, map: newMap, collected: collected, destroyed: destroyed},
		pos: pos,
		unlockedBits: unlockedBits,
		lockedBits: lockedBits
	};
};

/**
 * @param {!Array<!GUIp.mining.Frame>} frames
 * @param {!GUIp.mining.VisibleFrame} vfrm
 * @returns {{index: number, frx: !GUIp.mining.FrameEx}}
 */
analysis.findOrInsertFrame = function(frames, vfrm) {
	var len = frames.length,
		i = len,
		insIndex = 0,
		existingFrm, frx;
	while (i--) { // we are likely to receive a recent frame so search backwards
		existingFrm = frames[i];
		if (vfrm.step >= existingFrm.step) {
			if (!insIndex) insIndex = i + 1; // naive, but works in most cases
			if (vfrm.step !== existingFrm.step) {
				break;
			}
			if (GUIp.common.areArraysEqual(vfrm.map, existingFrm.map)) {
				return {index: i, frx: analysis.createFrameEx(existingFrm)};
			}
		}
	}
	if (insIndex) {
		frx = _createFrameExWithOld(frames[insIndex - 1], vfrm);
		frames[len] = null;
		frames.copyWithin(insIndex + 1, insIndex);
		frames[insIndex] = frx.frm;
	} else {
		// we have no previous frame
		frx = len ? _createFrameExWithOld(frames[0], vfrm) : analysis.createFrameEx(_createFrame(vfrm));
		frames.unshift(frx.frm);
	}
	return {index: insIndex, frx: frx};
};

/**
 * @param {!Array<!GUIp.mining.Frame>} frames
 * @param {number} len
 */
analysis.limitFrameSequenceLength = function(frames, len) {
	if (frames.length > len) {
		frames.copyWithin(0, frames.length - len);
		frames.length = len;
	}
};

})(); // ui_mining.analysis
