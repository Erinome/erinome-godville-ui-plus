// ui_expr
var ui_expr = GUIp.expr = {};

ui_expr.init = function() {
	GUIp.common.expr.init(jsep.noConflict());
};

ui_expr._unaryOps = {
	'!': function(x) { return !x; },
	'+': function(x) { return +x; },
	'-': function(x) { return -x; },
	'~': function(x) { return ~x; }
};

ui_expr._binaryOps = {
	'*':   function(x, y) { return x * y; },
	'/':   function(x, y) { return x / y; },
	'%':   function(x, y) { return x % y; },
	'+':   function(x, y) { return x + y; },
	'-':   function(x, y) { return x - y; },
	'<<':  function(x, y) { return x << y; },
	'>>':  function(x, y) { return x >> y; },
	'>>>': function(x, y) { return x >>> y; },
	'<':   function(x, y) { return x < y; },
	'>':   function(x, y) { return x > y; },
	'<=':  function(x, y) { return x <= y; },
	'>=':  function(x, y) { return x >= y; },
	'===': function(x, y) { return x === y; },
	'!==': function(x, y) { return x !== y; },
	'==':  function(x, y) { return x == y; },
	'!=':  function(x, y) { return x != y; },
	'&':   function(x, y) { return x & y; },
	'^':   function(x, y) { return x ^ y; },
	'|':   function(x, y) { return x | y; }
};

/**
 * @private
 * @param {*} text
 * @returns {boolean}
 */
ui_expr._testRegex = function(text) {
	return this.eRegex.test(text);
};

/**
 * @private
 * @param {*} text
 * @returns {?Array<string>}
 */
ui_expr._execRegex = function(text) {
	return this.eRegex.exec(text);
};

/**
 * @private
 * @const
 * @type {!Object<string, function({type: string})>}
 */
ui_expr._processors = {
	Literal: function() { },
	E_GVLiteral: function() { },

	ArrayExpression: function(node) {
		node.elements.forEach(ui_expr._processNode);
	},

	ConditionalExpression: function(node) {
		ui_expr._processNode(node.test);
		ui_expr._processNode(node.consequent);
		ui_expr._processNode(node.alternate);
	},

	UnaryExpression: function(node) {
		if (!node.prefix) {
			throw new Error('unsupported postfix unary operator "' + node.operator + '"');
		}
		if (!(node.eFunc = ui_expr._unaryOps[node.operator])) {
			throw new Error('unsupported prefix unary operator "' + node.operator + '"');
		}
		ui_expr._processNode(node.argument);
	},

	BinaryExpression: function(node) {
		if (!(node.eFunc = ui_expr._binaryOps[node.operator])) {
			throw new Error('unsupported binary operator "' + node.operator + '"');
		}
		ui_expr._processNode(node.left);
		ui_expr._processNode(node.right);
	},

	LogicalExpression: function(node) {
		if (!(node.eAnd = node.operator === '&&') && node.operator !== '||') {
			throw new Error('unsupported logical operator "' + node.operator + '"');
		}
		ui_expr._processNode(node.left);
		ui_expr._processNode(node.right);
	},

	MemberExpression: function(node) {
		ui_expr._processNode(node.object);
		if (node.computed) {
			ui_expr._processNode(node.property);
		}
	},

	E_GVExpression: function(node) {
		ui_expr._processNode(node.property);
	},

	CallExpression: function(node) {
		ui_expr._processNode(node.callee);
		node.arguments.forEach(ui_expr._processNode);
	},

	E_MatchExpression: function(node) {
		ui_expr._processNode(node.text);
		ui_expr._processNode(node.pattern);
		if (/*node.pattern.type !== 'Literal' ||*/ typeof node.pattern.value !== 'string') {
			return;
		}
		// pattern is a literal string
		var pattern = node.pattern.value,
			insensitive = node.insensitive;
		if (!node.testOnly || /[\\^$*+?.()|[{]/.test(pattern)) {
			node.eRegex = new RegExp(pattern, insensitive ? 'i' : '');
			node.eFunc = node.testOnly ? ui_expr._testRegex : ui_expr._execRegex;
		} else {
			// no metacharacters here, can optimize into plain substring search
			if (insensitive) {
				pattern = pattern.toLowerCase();
				node.eFunc = function(text) {
					return String(text).toLowerCase().includes(pattern);
				};
			} else {
				node.eFunc = function(text) {
					return String(text).includes(pattern);
				};
			}
			// though we must keep the regex available for debugging purpose
			GUIp.common.defineCachedProperty(node, 'eRegex', function() {
				return new RegExp(pattern, insensitive ? 'i' : '');
			});
		}
	}
};

/**
 * @private
 * @param {{type: string}} node
 */
ui_expr._processNode = function(node) {
	var type = node.type;
	ui_expr._processors[type](node);
	node.eEval = ui_expr._evaluators[type];
};

/**
 * @typedef {Object} GUIp.expr.CompiledExpr
 * @property {string} type
 * @property {function(!GUIp.expr.Runtime): *} eEval
 */

/**
 * @typedef {Object} GUIp.expr.Runtime
 * @property {!Object} gv
 * @property {function(string): *} gvCache
 * @property {?function(!GUIp.expr.CompiledExpr): *} eval
 */

/**
 * @param {string} text
 * @param {boolean} [inBooleanContext]
 * @returns {!GUIp.expr.CompiledExpr}
 */
ui_expr.compile = function(text, inBooleanContext) {
	var ast = GUIp.common.expr.parse(text, inBooleanContext);
	ui_expr._processNode(ast);
	return ast;
};

/**
 * @param {string} text
 * @returns {!Array<(string|!GUIp.expr.CompiledExpr)>}
 */
ui_expr.compileEmbedded = function(text) {
	var fragments = GUIp.common.expr.parseEmbedded(text);
	for (var i = 0, len = fragments.length; i < len; i++) {
		var fr = fragments[i];
		if (typeof fr === 'object') {
			ui_expr._processNode(fr);
		}
	}
	return fragments;
};

/**
 * @private
 * @param {!GUIp.expr.Runtime} rt
 * @param {!Array<!GUIp.expr.CompiledExpr>} items
 */
ui_expr._evaluateArray = function(rt, items) {
	if (rt.eval) {
		return items.map(rt.eval, rt);
	}
	var result = [];
	for (var i = 0, len = items.length; i < len; i++) {
		result[i] = items[i].eEval(rt);
	}
	return result;
};

/**
 * @private
 * @const
 * @type {!Object<string, function(this: GUIp.expr.CompiledExpr, !GUIp.expr.Runtime): *>}
 */
ui_expr._evaluators = {
	Literal: function() { return this.value; },
	E_GVLiteral: function(rt) { return rt.gv; },

	ArrayExpression: function(rt) {
		return ui_expr._evaluateArray(rt, this.elements);
	},

	ConditionalExpression: function(rt) {
		if (!rt.eval) {
			return this.test.eEval(rt) ? this.consequent.eEval(rt) : this.alternate.eEval(rt);
		}
		var a = rt.eval(this.test),
			b = rt.eval(this.consequent),
			c = rt.eval(this.alternate);
		return a ? b : c;
	},

	UnaryExpression: function(rt) {
		return this.eFunc(rt.eval ? rt.eval(this.argument) : this.argument.eEval(rt));
	},

	BinaryExpression: function(rt) {
		if (rt.eval) {
			return this.eFunc(rt.eval(this.left), rt.eval(this.right));
		} else {
			return this.eFunc(this.left.eEval(rt), this.right.eEval(rt));
		}
	},

	LogicalExpression: function(rt) {
		var x, y;
		if (rt.eval) {
			x = rt.eval(this.left);
			y = rt.eval(this.right);
		} else {
			x = this.left.eEval(rt);
		}
		return this.eAnd ? x && (rt.eval ? y : this.right.eEval(rt)) : x || (rt.eval ? y : this.right.eEval(rt));
	},

	MemberExpression: function(rt) {
		if (!this.computed) {
			return (rt.eval ? rt.eval(this.object) : this.object.eEval(rt))[this.property.name];
		}
		if (rt.eval) {
			return rt.eval(this.object)[rt.eval(this.property)];
		} else {
			return this.object.eEval(rt)[this.property.eEval(rt)];
		}
	},

	E_GVExpression: function(rt) {
		return rt.gvCache(rt.eval ? rt.eval(this.property) : this.property.eEval(rt));
	},

	CallExpression: function(rt) {
		var callee = this.callee, obj = null, methodName = '', func;
		if (callee.type === 'MemberExpression') {
			if (!callee.computed) {
				obj = rt.eval ? rt.eval(callee.object) : callee.object.eEval(rt);
				methodName = callee.property.name;
			} else if (rt.eval) {
				obj = rt.eval(callee.object);
				methodName = rt.eval(callee.property);
			} else {
				obj = calee.object.eEval(rt);
				methodName = callee.property.eEval(rt);
			}
			func = obj[methodName];
			if (typeof func !== 'function') {
				throw new TypeError(
					'<' + Object.prototype.toString.call(obj).slice(8, -1) + '>.' + methodName +
					' is ' + (func === null ? 'null' : typeof func)
				);
			}
		} else {
			func = rt.eval ? rt.eval(callee) : callee.eEval(rt);
			if (typeof func !== 'function') {
				throw new TypeError(
					(func == null ? func : '<' + Object.prototype.toString.call(func).slice(8, -1) + '>') +
					' cannot be used as a function'
				);
			}
		}
		return func.apply(obj, ui_expr._evaluateArray(rt, this.arguments));
	},

	E_MatchExpression: function(rt) {
		var text = rt.eval ? rt.eval(this.text) : this.text.eEval(rt);
		if (this.eFunc) {
			// we have got optimized code for matching, lucky us
			if (this.negated) {
				return !this.eFunc(text);
			} else if (rt.eval) {
				// ...but we're not going to use it
				return this.eRegex.exec(text);
			} else {
				return this.eFunc(text);
			}
		}
		// common case: compile and match
		var regex = new RegExp(rt.eval ? rt.eval(this.pattern) : this.pattern.eEval(rt), this.insensitive ? 'i' : '');
		if (this.negated) {
			return !regex.test(text);
		} else if (this.testOnly && !rt.eval) {
			return regex.test(text);
		} else {
			return regex.exec(text);
		}
	}
};

/**
 * @param {!Object} obj
 * @returns {function(string): *}
 */
ui_expr.makeCache = function(obj) {
	var cache = Object.create(null);
	return function(prop) {
		var x = cache[prop];
		return x !== undefined ? x : (cache[prop] = obj[prop]);
	};
};

/**
 * @param {!GUIp.expr.CompiledExpr} ast
 * @param {!Object} gv
 * @param {function(string): *} gvCache
 * @returns {*}
 */
ui_expr.eval = function(ast, gv, gvCache) {
	return ast.eEval({gv: gv, gvCache: gvCache, eval: null});
};

/**
 * @param {!Array<(string|!GUIp.expr.CompiledExpr)>} fragments
 * @param {!Object} gv
 * @param {function(string): *} gvCache
 * @returns {string}
 */
ui_expr.evalEmbedded = function(fragments, gv, gvCache) {
	var result = '',
		replacer = function(m0, m1) { return gvCache(m1); },
		rt = {gv: gv, gvCache: gvCache, eval: null};
	for (var i = 0, len = fragments.length; i < len; i++) {
		var fr = fragments[i];
		result += typeof fr === 'string' ? fr.replace(/\bgv\.(\w+)/g, replacer) : fr.eEval(rt);
	}
	return result;
};

/**
 * @typedef {Object} GUIp.expr.DebugInfo
 * @property {*} result
 * @property {(string|undefined)} error
 * @property {!Array<{type: string, eResult: *}>} trace
 */

/**
 * @param {!GUIp.expr.CompiledExpr} ast
 * @param {!Object} gv
 * @param {function(string): *} gvCache
 * @returns {!GUIp.expr.DebugInfo}
 */
ui_expr.debug = function(ast, gv, gvCache) {
	var trace = [];
	var rt = {
		gv: gv,
		gvCache: gvCache,
		eval: function(node) {
			var result = node.eResult = node.eEval(this);
			trace.push(node);
			return result;
		}
	};

	try {
		return {trace: trace, result: rt.eval(ast)};
	} catch (e) {
		return {trace: trace, error: e.toString()};
	}
};
