// ui_stats
var ui_stats = worker.GUIp.stats = {};

ui_stats.Ark_F = function() {
	var a = parseInt(ui_utils.getStat('#hk_ark_m .l_val','pointed',2));
	return !isNaN(a) ? a : null;
};
ui_stats.Ark_M = function() {
	var a = parseInt(ui_utils.getStat('#hk_ark_m .l_val','pointed',1));
	return !isNaN(a) ? a : null;
};
ui_stats.Bricks = function() {
	var a = ui_utils.getStat('#hk_bricks_cnt .l_val','dec');
	return parseInt(a === 1000 ? 1000 : a * 10) || 0;
};
ui_stats.Bits = function() {
	var a, b = ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_boss_bits' : '#hk_bl .l_val','text') || '';
	if ((a = /(?:(\d+)\+)?\[(.*?)\]/.exec(b))) {
		return (+a[1] || 0) * a[2].length + (a[2].match(/1/g) || []).length;
	}
	return parseInt(b) * ui_mining.bitsPerByte || 0;
};
ui_stats.Bytes = function() {
	var a = /\d+\+/.exec(ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_boss_bits' : '#hk_bl .l_val','text') || '');
	return a && parseInt(a[0]) || 0;
};
ui_stats.Bits_Per_Byte = function() {
	var a, b = ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_boss_bits' : '#hk_bl .l_val', 'text') || '';
	return (a = /\[.*?\]/.exec(b)) ? a[0].length - 2 : 8;
};
ui_stats.Book_Bytes = function() {
	var a, b = ui_utils.getStat('#hk_wrd .l_val','text') || '';
	if (a = b.match(/^(?:(.*?)% \+ )?\[(.*?)\]$/)) {
		return (+a[1]*10 || 0)*4 + 4 - (a[2] && a[2].match(/\./g) || []).length;
	}
	return 0;
};
ui_stats.Book_Words = function() {
	var a = (ui_utils.getStat('#hk_wrd .l_val','text') || '').match(/^(.*?)% \+ \[(.*?)\]/);
	return a && +a[1]*10 || 0;
};
ui_stats.Forge_Rank = function() {
	var a = (ui_utils.getStat('.dm_map_header .dl_val','text') || '').match(/^(.*?)(?: \+ (.*?)%)?$/);
	return a && +a[1] || 0;
};
ui_stats.Forge_Exp = function() {
	var a = (ui_utils.getStat('.dm_map_header .dl_val','text') || '').match(/^(.*?)(?: \+ (.*?)%)?$/);
	return a && +a[2] || 0;
};
ui_stats.Forge_Bytes = function() {
	var a, b = ui_utils.getStat('.dm_map_header + .dm_map_header .dl_val','text') || '';
	if (a = b.match(/^(\d+) ?(?: \+ \[(.*?)\])? (?:из|out of)/)) {
		return (+a[1] || 0)*4 + (a[2] && a[2].match(/[^\.]/g) || []).length;
	}
	return 0;
};
ui_stats.Forge_Words = function() {
	var a = (ui_utils.getStat('.dm_map_header + .dm_map_header .dl_val','text') || '').match(/^(\d+) ?(?: \+ \[(.*?)\])? (?:из|out of)/);
	return a && +a[1] || 0;
};
ui_stats.Charges =
ui_stats.Map_Charges =
ui_stats.Hero_Charges = function() {
	return ui_utils.getStat('#cntrl .acc_val','num') || 0;
};
ui_stats.Death = function() {
	return ui_utils.getStat('#hk_death_count .l_val','num') || 0;
};
ui_stats.Enemy_Bossname = function() {
	return ui_utils.getStat(ui_data.isMobile ? '.e_m_opps .opp_n' : '#o_hk_name .l_val a','text') || '';
};
ui_stats.Enemy_Godname = function() {
	if (ui_data.isMobile) {
		return ''; // in mobile version enemy godname is printed exactly the same as boss abilities, so probably better to always return empty string
	}
	return ui_utils.getStat('#o_hk_godname .l_val a','text') || '';
};
ui_stats.Enemy_Gold = function() {
	return ui_utils.getStat('#o_hk_gold_we .l_val','numre') || 0;
};
ui_stats.Enemy_HP = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h'),
		hp = 0;
	if (!b.length) {
		return +ui_utils.getStat('#o_hl1 .l_val','slashed',1) || 0;
	}
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			hp += +a[1] || 0;
		}
	}
	return hp;
};
ui_stats.Enemy_MaxHP = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h'),
		mhp = 0;
	if (!b.length) {
		return +ui_utils.getStat('#o_hl1 .l_val','slashed',2) || 0;
	}
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Enemy_Inv = function() {
	return +ui_utils.getStat('#o_hk_inventory_num .l_val','slashed',1) || 0;
};
ui_stats.EnemySingle_HP = function(enemy) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h, #bosses .opp_h');
	if (b[enemy-1] && (a = b[enemy-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[1] || 0;
	}
	return 0;
};
ui_stats.EnemySingle_MaxHP = function(enemy) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h, #bosses .opp_h');
	if (b[enemy-1] && (a = b[enemy-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[2] || 0;
	}
	return 0;
};
ui_stats.EnemySingle_Name = function(enemy) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_n:not(.opp_ng), #bosses .opp_n:not(.opp_ng)');
	return b[enemy-1] && b[enemy-1].textContent || '';
};
ui_stats.EnemySingle_Godname = function(enemy) {
	var a = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#bosses') + ' .opp_ng')[enemy - 1];
	return a ? a.textContent.replace(/\s*➤/, '').slice(1, -1) : ''; // enemies can be friends as well
};
ui_stats.Enemy_AbilitiesText = function() {
	var a = document.querySelector(ui_data.isMobile ? '.e_m_opps .opp_n.opp_ng span' : '#o_hk_gold_we + .line:not(#o_hk_death_count) .l_val');
	return a && a.textContent || '';
};
ui_stats.Enemy_AbilitiesCount = function() {
	var a = this.Enemy_AbilitiesText();
	return a && a.split(',').length || 0;
};
ui_stats.Enemy_HasAbility = function(ability) {
	return !!this.Enemy_AbilitiesText().match(new worker.RegExp(ability,'i')) || false;
};
ui_stats.Enemy_HasAbilityLoc = function(ability) {
	return this.Enemy_HasAbility(ability);
};
ui_stats.Enemy_Count = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h, #bosses .opp_h'),
		count = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (!(a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) || +a[2] > 1) {
			count++;
		}
	}
	return count;
};
ui_stats.Enemy_AliveCount = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_opps' : '#opps') + ' .opp_h, #bosses .opp_h'),
		alive = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if ((a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) && +a[2] > 1) {
			alive++;
		}
	}
	return alive;
};
ui_stats.Equip1 = function() {
	return ui_utils.getStat('#eq_0 .eq_level','num') || 0;
};
ui_stats.Equip2 = function() {
	return ui_utils.getStat('#eq_1 .eq_level','num') || 0;
};
ui_stats.Equip3 = function() {
	return ui_utils.getStat('#eq_2 .eq_level','num') || 0;
};
ui_stats.Equip4 = function() {
	return ui_utils.getStat('#eq_3 .eq_level','num') || 0;
};
ui_stats.Equip5 = function() {
	return ui_utils.getStat('#eq_4 .eq_level','num') || 0;
};
ui_stats.Equip6 = function() {
	return ui_utils.getStat('#eq_5 .eq_level','num') || 0;
};
ui_stats.Equip7 = function() {
	return ui_utils.getStat('#eq_6 .eq_level','num') || 0;
};
ui_stats._isEquipBold = function(selector) {
	var a = document.querySelector(selector);
	return !!(a && a.classList.contains('eq_b'));
};
ui_stats.Equip1_IsBold = ui_stats._isEquipBold.bind(null, '#eq_0 .eq_name');
ui_stats.Equip2_IsBold = ui_stats._isEquipBold.bind(null, '#eq_1 .eq_name');
ui_stats.Equip3_IsBold = ui_stats._isEquipBold.bind(null, '#eq_2 .eq_name');
ui_stats.Equip4_IsBold = ui_stats._isEquipBold.bind(null, '#eq_3 .eq_name');
ui_stats.Equip5_IsBold = ui_stats._isEquipBold.bind(null, '#eq_4 .eq_name');
ui_stats.Equip6_IsBold = ui_stats._isEquipBold.bind(null, '#eq_5 .eq_name');
ui_stats.Equip7_IsBold = ui_stats._isEquipBold.bind(null, '#eq_6 .eq_name');
ui_stats.Exp =
ui_stats.Map_Exp =
ui_stats.Hero_Exp = function() {
	return ui_utils.getStat('#hk_level .p_bar','titlenumre') || 0;
};
ui_stats.Godpower = function() {
	return ui_utils.getStat('#cntrl .gp_val','num') || 0;
};
ui_stats.Gold =
ui_stats.Map_Gold =
ui_stats.Hero_Gold = function() {
	return ui_utils.getStat('#hk_gold_we .l_val','numre') || 0;
};
ui_stats.Map_Alls_HP =
ui_stats.Hero_Alls_HP = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h'),
		hp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			hp += +a[1] || 0;
		}
	}
	return hp;
};
ui_stats.Map_Ally_HP =
ui_stats.Hero_Ally_HP = function(ally) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h');
	if (b[ally-1]) {
		if (a = b[ally-1].textContent.match(/^(.+?) \/ (.+?)$/)) {
			return +a[1] || 0;
		}
		if (a = b[ally-1].textContent.match(/(defeated|повержен)/)) {
			return 1;
		}
		if (a = b[ally-1].textContent.match(/(exited|evacuated|вышел|отбуксирован)/)) {
			return null;
		}
	}
	return 0;
};
ui_stats.Hero_Alls_MaxHP = function(notBosses) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h'),
		mhp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (notBosses && ui_stats.Hero_Ally_Name(i)[0] === '+') continue;
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Map_Ally_MaxHP =
ui_stats.Hero_Ally_MaxHP = function(ally) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h');
	if (b[ally-1] && (a = b[ally-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[2] || 0;
	}
	return 0;
};
ui_stats.Hero_Alls_Count = function(notBosses) {
	var a = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_n:not(.opp_ng)');
	if (notBosses) {
		return Array.from(a).filter(function(b) { return b.textContent[0] !== '+'; }).length;
	}
	return a.length;
};
ui_stats.Hero_Alls_AliveCount = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h'),
		alive = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			alive++;
		}
	}
	return alive;
};
ui_stats.Hero_Alls_AliveMaxHP = function() {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_h'),
		mhp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Hero_Ally_Name = function(ally) {
	var a, b = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_n:not(.opp_ng)');
	if (ui_data.isSail) {
		return b[ally-1] && b[ally-1].textContent.slice(3) || '';
	} else {
		return b[ally-1] && b[ally-1].textContent || '';
	}
};
ui_stats.Hero_Ally_Names = function() {
	return Array.from(document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_n:not(.opp_ng)'),
		ui_data.isSail ? function(a) { return a.textContent.slice(3); } : function(a) { return a.textContent; }
	);
};
ui_stats._HP_selector = function() {
	var selector = (ui_data.isMining ? '#hk_bhp' : '#hk_health')+' .l_val';
	if (ui_data.isMobile) {
		if (ui_data.isMining) {
			selector = '#statusbar .e_sb_boss_hp';
		} else if (ui_data.isSail) {
			selector = '#statusbar .e_sb_ark_hp';
		}
	}
	return selector;
}
ui_stats.HP =
ui_stats.Map_HP =
ui_stats.Hero_HP = function() {
	return +ui_utils.getStat(this._HP_selector(),'slashed',1) || 0;
};
ui_stats.Inv =
ui_stats.Map_Inv =
ui_stats.Hero_Inv = function() {
	return +ui_utils.getStat('#hk_inventory_num .l_val','slashed',1) || 0;
};
ui_stats.Invites = function() {
	return ui_utils.getStat(ui_data.isMobile ? '.e_m_friends .fsearchbar + .line' : '#invites .block_title','numre') || 0;
};
ui_stats.Map_Supplies = function() {
	return +ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_ark_supplies' : '#hk_water .l_val','slashed',1) || 0;
};
ui_stats.Map_MaxSupplies = function() {
	return +ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_ark_supplies' : '#hk_water .l_val','slashed',2) || 0;
};
ui_stats.Lab_F = function() {
	var a = parseInt(ui_utils.getStat('.bps_mf .l_val','pointed',2));
	return !isNaN(a) ? a : null;
};
ui_stats.Lab_M = function() {
	var a = parseInt(ui_utils.getStat('.bps_mf .l_val','pointed',1));
	return !isNaN(a) ? a : null;
};
ui_stats.Level =
ui_stats.Map_Level = function() {
	return ui_utils.getStat('#hk_level .l_val','num') || 0;
};
ui_stats.Logs = function() {
	return ui_utils.getStat('#hk_wood .l_val','dec') * 10 || 0;
};
ui_stats.Max_Godpower = function() {
	return ui_data.hasShop ? 200 : 100;
};
ui_stats.Max_HP = function() {
	return +ui_utils.getStat(this._HP_selector(),'slashed',2) || 0;
};
ui_stats.Max_Inv = function() {
	return +ui_utils.getStat('#hk_inventory_num .l_val','slashed',2) || 0;
};
ui_stats.Monster = function() {
	return ui_utils.getStat('#hk_monsters_killed .l_val','num') || 0;
};
ui_stats.Pet_Level = function() {
	var a = document.querySelector('#hk_pet_class + div .l_val');
	return (a && a.parentNode.style.display !== 'none' && parseInt(a.textContent)) || 0;
};
ui_stats.Pet_NameType = function() {
	var b = document.querySelector('#hk_pet_class .l_val');
	if (!b || b.parentNode.style.display === 'none') {
		return ':';
	}
	var pName = ui_utils.getStat('#hk_pet_name .l_val','text') || '',
		pType = b.textContent;
	return (pName.match(/^(.*?)(\ «.+»)?(\ “.+”)?(\ ❌)?$/) || [null,''])[1] + ':' + pType;
};
ui_stats.Push_Readiness = function() {
	return ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_boss_push' : '#hk_bls .l_val','num') || 0;
};
ui_stats.Souls = function() {
	return Math.round(ui_utils.getStat('#hk_sl .l_val','dec') * 50) || 0;
};
ui_stats.Souls_Processed = function() {
	return ui_utils.getStat('#sl_popover_c .bps_mf .l_val','num') || 0;
};
ui_stats.Souls_Collected = function() {
	return ui_utils.getStat('#sl_popover_c .sl_cnt .l_val','num') || 0;
};
ui_stats.Task = function() {
	return ui_utils.getStat('#hk_quests_completed .p_bar','titlenumre') || 0;
};
ui_stats.Task_Name = function() {
	return ui_utils.getStat('#hk_quests_completed .q_name','text') || '';
};
ui_stats.Task_Number = function() {
	return +(ui_utils.getStat('#hk_quests_completed .l_val','text') || '').slice(1) || 0;
};
ui_stats.Side_Job = function() {
	var a = document.querySelector('#hk_quests_completed + .line .p_bar');
	return (a && (!ui_utils.isHidden(a) || ui_data.isMobile && a.parentNode.style.display !== 'none') && ui_utils.parseStat(a, 'titlenumre')) || 0;
};
ui_stats.Side_Job_Name = function() {
	var a = document.querySelector('#hk_quests_completed + .line .q_name');
	return (a && (!ui_utils.isHidden(a) || ui_data.isMobile && a.parentNode.style.display !== 'none') && a.textContent) || '';
};
ui_stats.Side_Job_Duration = function() {
	var m = /(\d+):(\d+)/.exec(ui_utils.getStat('#hk_quests_completed + .line .l_val','text') || '');
	return m ? +m[1] * 3600 + +m[2] * 60 : 0;
};
ui_stats.Side_Job_Requirements = function(title) {
	var a = title || this.Side_Job_Name();
	return +(/\d+/.exec(a) || '')[0] || (/дважды|twice/.test(a) ? 2 : 0);
};
ui_stats.Trader_Exp = function() {
	return ui_utils.getStat('#hk_t_level .p_bar','titlenumre') || 0;
};
ui_stats.Trader_Level = function() {
	return ui_utils.getStat('#hk_t_level .l_val','num') || 0;
};
ui_stats.Savings = function() {
	var a, b = ui_utils.getStat('#hk_retirement .l_val','text') || '';
	if (a = b.match(/^((\d+)M,? ?)?(\d+)?k?$/i)) {
		return 1000 * (a[2] || 0) + 1 * (a[3] || 0)
	} else {
		return parseInt(b) || 0;
	}
};
ui_stats._upProgressParser = function(idx) {
	return ui_utils.getStat('#upgrades > div > div:nth-of-type(' + idx + ') .up_line .upg_p_bar','titlenumre') || 0;
}
ui_stats.Up_Retroscope = function() {
	return this._upProgressParser(1);
};
ui_stats.Up_Cryochamber = function() {
	return this._upProgressParser(2);
};
ui_stats.Up_Temple_Tower = function() {
	return this._upProgressParser(3);
};
ui_stats.Up_Temple_Tower_2 = function() {
	return this._upProgressParser(4);
};
ui_stats.Up_Temple_Tower_3 = function() {
	return this._upProgressParser(5);
};
ui_stats.Up_Temple_Tower_4 = function() {
	return this._upProgressParser(6);
};
ui_stats.Up_Temple_Tower_5 = function() {
	return this._upProgressParser(7);
};

ui_stats.auraName = function() {
	var a, b = document.getElementById('hk_distance');
	if (b && (b = b.previousSibling) && (!ui_utils.isHidden(b) || ui_data.isMobile && b.style.display !== 'none') && (a = /^(?:Аура|Aura)(.*?) \(.*?\)$/.exec(b.textContent))) {
		return a[1] || ''
	}
	return '';
};
ui_stats.auraDuration = function() {
	var a, b = document.getElementById('hk_distance');
	if (b && b.previousSibling && (a = b.previousSibling.textContent.match(/^(Аура|Aura).*?\((\d+):(\d+)\)$/))) {
		return (parseInt(a[2]) * 3600 + parseInt(a[3]) * 60) || 0;
	}
	return 0;
};
ui_stats.currentStep = function() {
	var a, b = ui_data.isMobile ? document.getElementsByClassName('e_mt_fight_log')[0] : (document.getElementById('m_fight_log') || document.getElementById('r_map'));
	return b && (b = (ui_data.isMobile ? (ui_data.isMining ? b : b.getElementsByClassName('l_header')[0]) : b.getElementsByTagName('h2')[0])) && (a = /\((?:шаг|step) (\d+)\)$/.exec(b.textContent)) ? +a[1] : 0;
};
ui_stats.currentStepText = function() {
	var i, len, a = Array.from(document.querySelectorAll((ui_data.isMobile ? '.e_m_fight_log' : '#m_fight_log') + ' .d_msg')),
		b = document.querySelector('#m_fight_log .sort_ch');
	if (b && b.textContent === '▲') {
		b = true;
		a.reverse();
	} else {
		b = false;
	}
	for (i = 0, len = a.length; i < len; i++) {
		if (a[i].parentNode.classList.contains('turn_separator' + (b ? '_inv' : ''))) break;
	}
	return a.slice(0,i+1).map(function(c) { return c.textContent; }).join('\n');
}
ui_stats.cargo = function() {
	return ui_utils.getStat(ui_data.isMobile ? '#statusbar .e_sb_ark_cargo' : '#hk_cargo .l_val', 'text') || '';
};
ui_stats.charName = function() {
	if (ui_data.isSail && !ui_data.isMobile) {
		if (this._sailCharName) {
			return this._sailCharName;
		}
		var n, pt = document.querySelector('.dir_arrow');
		if (pt && (n = pt.classList.toString().match(/pl(\d)/))) {
			var a = document.querySelectorAll((ui_data.isMobile ? '.e_m_alls' : '#alls') + ' .opp_n:not(.opp_ng)');
			if (a[+n[1]-1] && a[+n[1]-1].textContent.slice(3)) {
				this._sailCharName = a[+n[1]-1].textContent.slice(3);
				return this._sailCharName;
			}
		}
	}
	// note: if the user opens a datamine when their localStorage is empty (i.e., they've just installed
	// the extension or reset its settings), we will not be able to detect hero's name
	return ui_utils.getStat('#hk_name .l_val','text') || ui_storage.get('charName') || '';
};
ui_stats.inShop = function() {
	var inShop = document.querySelectorAll((ui_data.isMobile ? '.e_m_shop' : '#trader') + ' .line a');
	return inShop[1] && inShop[1].parentNode.style.display !== 'none' && /Покинуть|Leave/.test(inShop[1].textContent) || false;
};
ui_stats.checkShop = function() {
	var a = document.querySelector(ui_data.isMobile ? '.e_m_shop' : '#trader > .block_content');
	return !!(a && a.firstChild);
};
ui_stats.pendingShop = function() {
	var a = document.querySelector('#hk_t_level + div');
	return !!(a && /применения команды|will be processed/.test(a.textContent));
};
ui_stats.checkLab = function() {
	return ['Лаборатория','Lab'].includes((document.querySelector('#hk_distance ~ .line') || '').textContent);
};
ui_stats.checkDungeonForge = function() {
	return ['Творительная','Dungeon Forge'].includes((document.querySelector('#hk_distance ~ .line ~ .line') || '').textContent);
};
ui_stats.fightType = function() {
	if (this.isFight()) {
		var mfl = document.querySelector(ui_data.isMobile ? '.e_mt_fight_log .l_header' : '#m_fight_log h2');
		if (document.getElementById('map') && document.getElementById('map').style.display !== 'none' || ui_data.isMobile && document.getElementById('dmap_icon')) {
			return 'dungeon';
		} else if (document.getElementById('s_map') && document.getElementById('s_map').style.display !== 'none' || ui_data.isMobile && document.getElementById('map_wrap')) {
			return 'sail';
		} else if (document.getElementById('r_map') && document.getElementById('r_map').style.display !== 'none' || ui_data.isMobile && document.getElementsByClassName('wrmap')[0]) {
			return 'mining';
		} else if (mfl && /Тренировочный бой|Sparring Chronicle/.test(mfl.textContent)) {
			return 'spar';
		} else if (mfl && /Вести с арены|Arena Journal/.test(mfl.textContent)) {
			return 'arena';
		} else if (this.Enemy_Godname().length > 0) {
			return 'player';
		} else if (this.Hero_Alls_Count() === 0 && this.Enemy_Count() > 1) {
			return 'multi_monster';
		} else {
			return 'monster';
		}
	}
	return '';
};
ui_stats.godName = function() {
	var result, a, b = ui_utils.getStat('#hk_name a, #hk_godname a','href') || '';
	if (a = b.match(/\/([^\/]+)$/)) {
		result = decodeURIComponent(a[1]) || '';
	}
	if (!result) {
		if (a = ui_data.docTitle.match(/(?:\((?:\d+)?[!@~\+]\) )?(.*?)(?: и е(го|ё) геро| and h(is|er) hero)/)) {
			return a[1].trim();
		}
		return GUIp.common.getCurrentGodname();
	}
	return result;
};
ui_stats.guildName = function() {
	return ui_utils.getStat('#hk_clan a','text') || '';
};
ui_stats.goldTextLength = function() {
	return (ui_utils.getStat('#hk_gold_we .l_val','text') || '').length;
};
ui_stats.hasArk = function() {
	return this.Logs() >= 1000;
};
ui_stats.hasTemple = function() {
	return this.Bricks() === 1000;
}
ui_stats.heroHasPet = function() {
	var a = document.querySelector('#hk_pet_class .l_val');
	return !!(a && a.parentNode.style.display !== 'none' && a.textContent);
};
ui_stats._isActivityAvailable = function(selector) {
	var a = document.querySelector(selector);
	return !!a && a.classList.contains('div_link') && a.parentNode.style.display !== 'none';
};
ui_stats.isArenaAvailable   = ui_stats._isActivityAvailable.bind(ui_stats, '.arena_link_wrap a');
ui_stats.isSparAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_challenge_button a');
ui_stats.isDungeonAvailable = ui_stats._isActivityAvailable.bind(ui_stats, '.e_dungeon_button a');
ui_stats.isSailAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_sail_button a');
ui_stats.isMiningAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_mining_button a');
ui_stats.isFight = function() {
	return /^\((\d+)?[!@~\+]\) /.test(ui_data.docTitle);
};
ui_stats.isBossFight = function() {
	return this.fightType() === 'monster';
};
ui_stats.isDungeon = function() {
	return this.fightType() === 'dungeon';
};
ui_stats.isSail = function() {
	return this.fightType() === 'sail';
};
ui_stats.isMining = function() {
	return this.fightType() === 'mining';
};
ui_stats.isMale = function() {
	return !/героиня|heroine/i.test((document.querySelector('#stats h2, #m_info h2') || '').textContent + ui_data.docTitle);
};
ui_stats.isBlessed = function() {
	var a = document.querySelector('.e_bless > .line');
	return a && !!a.textContent.length;
};
ui_stats.isGoingBack = function() {
	return /до столицы|to Pass/i.test(ui_utils.getStat('#hk_distance .l_capt','text') || '');
};
ui_stats.lastDiaryItems = function() {
	var a = Array.from(document.querySelectorAll('#diary .d_msg, .e_m_diary .d_msg')),
		b = document.querySelector('#diary .sort_ch');
	if (b && b.textContent === '▲') {
		a.reverse();
	}
	return a;
};
ui_stats.lastDiaryRealEntry = function(skip, skipInfl) {
	var a = ui_stats.lastDiaryItems();
	skip >>= 0;
	for (var i = 0, j = 0, len = a.length; i < len; i++) {
		if (a[i].textContent.includes('☣')) {
			this._lastDiaryVoice = a[i].textContent.replace(/ \([^(]+\)$/,'');
			continue;
		}
		if (this._lastDiaryVoice === a[i].textContent || (skipInfl && a[i].classList.contains('m_infl'))) {
			continue;
		}
		if (skip === j) {
			return a[i];
		}
		j++;
	}
	return null;
};
ui_stats.lastDiaryIsInfl = function(skip) {
	var a = ui_stats.lastDiaryRealEntry(skip);
	return a && a.classList.contains('m_infl') || false;
};
ui_stats.lastDiary = function() {
	return ui_stats.lastDiaryItems().map(function(a) { return a.textContent; });
};
ui_stats.lastNews = function() {
	return ui_utils.getStat('.f_news.line','text') || '';
};
ui_stats.logId = function() {
	var a, b = ui_utils.getStat('#fbclink','href') || '';
	if (!b.endsWith('/superhero') && (a = b.match(/\/([^\/\?]+)(\?.+)?$/))) {
		return decodeURIComponent(a[1]) || '';
	}
	return '';
};
ui_stats.mileStones = function() {
	return ui_utils.getStat('#hk_distance .l_val','num') || ui_utils.getStat('#hmap_svg g.loc_dir + title, #hmap_svg g.sl title','numre') || 0;
};
ui_stats.monsterName = function() {
	var a = document.querySelector('#news_pb .line .l_val');
	return a && a.parentNode.style.display !== 'none' && a.textContent || '';
};
ui_stats.canonicalMonsterName = function(name) {
	return (name != null ? name : ui_stats.monsterName()).replace(/^(?:(?:\uD83E\uDDB4|☠|WANTED) )+/, ''); // bone, skull
};
ui_stats.monsterLoot = function() {
	var a = document.querySelector('#news_pb .monster_d');
	return a && a.parentNode.style.display !== 'none' && a.textContent && (a = a.textContent.match(/^\((?:has|carries|тащит|несёт) (?:(?:(?:an?|the|some) )?(.*?))(?:(?: and | и )?(\d+ (?:coins|монет.?)?))?\)$/)) && a[1] !== 'мертвечиной' && [a[1],a[2]] || [];
};
ui_stats.monsterTrophy = function() {
	return this.monsterLoot()[0] || '';
};
ui_stats.monsterGold = function() {
	return parseInt(this.monsterLoot()[1]) || 0;
};
ui_stats.nearbyTown = function() {
	var a = (ui_utils.getStat('#hk_distance .l_val','title') || '').match(/: (.+?)(?: \(.*?\))?$/);
	return a && a[1] || '';
};
ui_stats.poiMileStones = function(dir) {
	var a = document.querySelectorAll('#hmap_svg g.poi title'),
		c = ui_stats.mileStones();
	if (a.length) {
		a = Array.from(a, function(b) { return +(b.textContent.match(/(\d+)/) || '')[1]; });
		this._poiMileStonesCached = a;
	} else if (this._poiMileStonesCached) {
		if (!document.querySelector('#hmap_svg g:not(.poi):not(.tl):not([style$="none;"]) title')) {
			a = this._poiMileStonesCached;
		} else {
			delete this._poiMileStonesCached;
		}
	}
	if (a.length > 1) {
		a.sort(function(d,e) { return Math.abs(c - d) - Math.abs(c - e);});
	}
	if (a.length && dir) {
		a = a.filter(function(b) {
			return (dir > 0) ? (b > c) : (b < c);
		});
	}
	return a[0] || Infinity;
};
ui_stats.poiDistance = function() {
	return ui_stats.poiMileStones() - ui_stats.mileStones();
};
ui_stats.selTownMileStones = function() {
	return document.querySelector('#hmap_svg g.e_selected_town') ? ui_utils.getStat('#hmap_svg g.e_selected_town title','numre') : Infinity;
};
ui_stats.selTownDistance = function() {
	return ui_stats.selTownMileStones() - ui_stats.mileStones();
};
ui_stats.petIsKnockedOut = function() {
	return (ui_utils.getStat('#hk_pet_name .l_val','text') || '').endsWith(' ❌');
};
ui_stats.heroState = function() {
	var a = document.getElementsByClassName('e_hero_state')[0];
	return a && a.dataset.state || 'unknown';
};
ui_stats.heroStateText = function() {
	var a = document.getElementsByClassName('e_hero_state')[0];
	return a && a.textContent || '';
};
ui_stats.mProgress = function() {
	var a = ui_utils.getStat('#news_pb .p_bar.monster_pb','title');
	return a && parseInt((a.match(/ (?:—|-) (\d+)%/) || [])[1]) || 0;

};
ui_stats.sProgress = function() {
	var a = ui_utils.getStat('#news_pb .p_bar.n_pbar','title');
	return a && parseInt((a.match(/ (?:—|-) (\d+)%/) || [])[1]) || 0;
};
ui_stats.progressDiff = function() {
	return Math.abs(ui_stats.sProgress() - ui_stats.Task());
};
ui_stats.sendDelay = function(text) {
	var nodes = document.querySelectorAll('#cntrl2 div.arena_msg'),
		sendToDesc, sendToStr, m;
	for (var i = 0, len = nodes.length; i < len; i++) {
		sendToDesc = nodes[i];
		sendToStr = sendToDesc.textContent;
		if (text.test(sendToStr)) {
			if (sendToDesc.style.display !== 'none' && (m = /(?:(\d+)\s*[чh]\s+)?(\d+)\s*(?:мин|m)/.exec(sendToStr))) {
				return ((+m[1] || 0) * 60 + +m[2]) * 60;
			}
			return 0;
		}
	}
	return 0;
};
ui_stats.sendDelayArena = function() { return this.sendDelay(/Арена откроется через|Arena available in/); };
ui_stats.sendDelayDungeon = function() { return this.sendDelay(/Подземелье откроется через|Dungeon available in/); };
ui_stats.sendDelaySail = function() { return this.sendDelay(/Отплыть можно через|Sail available in/); };
ui_stats.sendDelaySpar = function() { return this.sendDelay(/Тренировка через|Sparring available in/); };
ui_stats.sendDelayMine = function() { return this.sendDelay(/Босс освободится через|Boss ready in/); };
ui_stats.storedPets = function() {
	return ui_data.storedPets || [];
};
ui_stats.townName = function() {
	var a = ui_utils.getStat('#hk_distance .l_val','text') || '';
	return isNaN(parseInt(a)) && a || '';
};
ui_stats.chosenMonster = function() {
	var monster = ui_stats.monsterName();
	if (monster && ui_words.base.chosen_monsters.length && (new worker.RegExp(ui_words.base.chosen_monsters.join('|'),'i')).test(monster)) {
		return true;
	}
	return false;
};
ui_stats.soulMonster = function() {
	var a = document.querySelector('#news_pb .line .l_val');
	return !!a && a.parentNode.style.display !== 'none' && a.textContent.startsWith('☥ ');
};
ui_stats.specialMonster = function() {
	var monster = ui_stats.monsterName();
	if (monster && ui_words.base.special_monsters.length && (new worker.RegExp(ui_words.base.special_monsters.join('|'),'i')).test(monster)) {
		return true;
	}
	return false;
};
ui_stats.strongMonster = function() {
	var a = document.querySelector('#news_pb .line .l_val');
	return !!a && a.parentNode.style.display !== 'none' && a.classList.contains('bold');
};
ui_stats.tamableMonster = function() {
	var monster = ui_stats.monsterName();
	return (
		monster.includes('\uD83E\uDDB4') && // bone
		!ui_stats.heroHasPet() &&
		!ui_stats.storedPets().includes(ui_stats.canonicalMonsterName(monster).toLowerCase())
	);
};
ui_stats.wantedMonster = function() {
	return /(?:☠|WANTED) /.test(ui_stats.monsterName());
};
ui_stats._calcBossMaxDamage = function(abilitiesCount, teamSize, teamHP) {
	var lim = 102,
		k = 16.5;
	if (teamSize !== 5) {
		if (teamSize === 4) {
			// lim = 102;
			k = 13.5;
		} else if (teamSize >= 6) {
			lim = 111;
			k = 18.5;
		} else {
			lim = 91;
			k = 10.5;
		}
	}
	teamHP -= (408 + (abilitiesCount << 4)) * teamSize;
	return teamHP >= 0 ? lim : lim + teamHP / k;
};
ui_stats._applyAbilityModifiers = function(dmg, abilities, heroMaxHP) {
	var miniDmg = 0;
	if (/зовущий|summoning/i.test(abilities)) {
		if (ui_stats.EnemySingle_HP(2) > 0) { // in case secondary boss is present and alive
			miniDmg = dmg * 0.3;
		}
		if (ui_stats.EnemySingle_HP(1) < 1) { // in case primary boss is dead
			dmg = 0;
		}
	}
	if (/бойкий|nimble/i.test(abilities)) {
		dmg *= 1.9; // nimble boss can cause almost double damage, but it would be very rarely exactly doubled
	}
	if (/спешащий|scurrying/i.test(abilities)) {
		dmg += heroMaxHP * 0.05; // heroes can hurt themselves
	}
	return dmg + miniDmg;
};
ui_stats.lowHealth = function(location) {
	var step, hp = ui_stats.HP();
	switch (location) {
		case 'dungeon':
			return hp < 130 && hp > 1;
		case 'sail':
			// ark health level should be actually checked only when the ark is on the map since it is getting changed
			// to hero's health level when exiting sailing mode, possibly leading to false triggering
			if (ui_improver.islandsMap.manager && ui_improver.islandsMap.manager.model.arks[ui_improver.islandsMap.ownArk] !== 0x8080) {
				return hp < 25 && hp > 0;
			}
			return false;
		case 'fight':
			if (ui_stats.Enemy_HP() <= 0) {
				return false; // the fight is over, no need to worry any more
			}
			var healthLim = 0,
				heroMaxHP = ui_stats.Max_HP(),
				enemyHP = ui_stats.EnemySingle_HP(1),
				alliesCount = ui_stats.Hero_Alls_Count(true),
				bossAbilities = '';
			if (ui_stats.fightType() === 'multi_monster') { // corovan
				healthLim = heroMaxHP * 0.05 * ui_stats.Enemy_AliveCount();
			} else if (!alliesCount) { // single enemy
				healthLim = heroMaxHP * 0.15;
			} else { // raid boss or dungeon boss
				bossAbilities = ui_stats.Enemy_AbilitiesText();
				healthLim = ui_stats._applyAbilityModifiers(
					ui_stats._calcBossMaxDamage(
						bossAbilities.split(',').length,
						alliesCount + 1,
						(ui_improver.allsHP.sum || ui_stats.Hero_Alls_MaxHP(true)) + heroMaxHP
					),
					bossAbilities,
					heroMaxHP
				);
			}
			if ((step = this.currentStep()) > 60) { // damage increases in long fights
				healthLim *= (step - 60) * 0.02 + 1;
			}
			if (/взрывной|explosive/i.test(bossAbilities) && enemyHP > 0 && enemyHP < ui_stats.EnemySingle_MaxHP(1) * 0.3) {
				// however, damage from the explosion does not increase
				healthLim = Math.max(healthLim, heroMaxHP * 0.29);
			}
			return hp < healthLim + 2 && hp > 1;
	}
	return false;
};
