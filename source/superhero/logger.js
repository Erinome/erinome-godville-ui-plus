// ui_logger
var ui_logger = worker.GUIp.logger = {};

ui_logger.create = function() {
	var menuBar = document.getElementById(ui_data.isMobile ? 'statusbar' : 'menu_bar'), html;
	this.updating = false;
	menuBar.insertAdjacentHTML(ui_data.isMobile ? 'beforeend' : 'afterend',
		'<ul id="logger"' + (worker.GUIp_browser === 'Firefox' ? ' style="mask: url(#fader_masking);"' : '') +
		'>\u200B</ul>' // zero-width space
	);
	this.bar = document.getElementById('logger');
	ui_storage.addImmediateListener('Option:disableLogger', function(newValue) {
		ui_logger.disabled = newValue === 'true';
		ui_utils.hideElem(ui_logger.bar, ui_logger.disabled);
	});
	GUIp.common.tooltips.watchSubtree(this.bar);
	this.need_separator = false;
	// arguments to ui_logger._watchStatsValue
	this.dungeonWatchers = [
		['Map_HP', 'hp', worker.GUIp_i18n.hero_health, 'hp'],
		['Map_Alls_HP', 'a:hp', worker.GUIp_i18n.allies_health, 'allies'],
		['Map_Exp', 'exp', worker.GUIp_i18n.exp, 'exp'],
		['Map_Level', 'lvl', worker.GUIp_i18n.level, 'level'],
		['Map_Inv', 'inv', worker.GUIp_i18n.inventory, 'inv'],
		['Map_Gold', 'gld', worker.GUIp_i18n.gold, 'gold'],
		['Map_Charges', 'ch', worker.GUIp_i18n.charges, 'charges']
	];
	this.battleWatchers = [
		['Hero_HP', 'h:hp', worker.GUIp_i18n.hero_health, 'hp'],
		['Enemy_HP', 'e:hp', worker.GUIp_i18n.enemy_health, 'enemies'],
		['Hero_Alls_HP', 'a:hp', worker.GUIp_i18n.allies_health, 'allies'],
		['Hero_Inv', 'h:inv', worker.GUIp_i18n.inventory, 'inv'],
		['Hero_Gold', 'h:gld', worker.GUIp_i18n.gold, 'gold'],
		['Hero_Charges', 'ch', worker.GUIp_i18n.charges, 'charges'],
		['Enemy_Gold', 'e:gld', worker.GUIp_i18n.gold, 'monster'],
		['Enemy_Inv', 'e:inv', worker.GUIp_i18n.inventory, 'monster']
	];
	this.sailWatchers = [
		['Map_HP', 'hp', worker.GUIp_i18n.hero_health, 'hp'],
		['Enemy_HP', 'e:hp', worker.GUIp_i18n.enemy_health, 'enemies'],
		['Map_Alls_HP', 'a:hp', worker.GUIp_i18n.allies_health, 'allies'],
		['Map_Supplies', 'spl', worker.GUIp_i18n.supplies, 'supplies'],
		['Map_Charges', 'ch', worker.GUIp_i18n.charges, 'charges']
	];
	this.miningWatchers = [
		['Hero_HP', 'hp', worker.GUIp_i18n.boss_health, 'hp'],
		['Enemy_HP', 'e:hp', worker.GUIp_i18n.enemy_health, 'enemies'],
		['Map_Charges', 'ch', worker.GUIp_i18n.charges, 'charges'],
		['Bits', 'bits', worker.GUIp_i18n.bits, 'bits'],
		['Bytes', 'bytes', worker.GUIp_i18n.bytes, 'bytes'],
		['Push_Readiness', 'psh', worker.GUIp_i18n.push_ready, 'push']
	];
	this.shopWatchers = [
		['HP', 'hp', worker.GUIp_i18n.health],
		['Charges', 'ch', worker.GUIp_i18n.charges],
		['Logs', 'wd', worker.GUIp_i18n.logs],
		['Side_Job', 'x:tsk', worker.GUIp_i18n.side_job],
		['Hero_Inv', 'tr:inv', worker.GUIp_i18n.inventory, 'inv'],
		['Hero_Gold', 'tr:gld', worker.GUIp_i18n.gold, 'gold'],
		['Trader_Exp', 'tr:exp', worker.GUIp_i18n.trader_exp, 'trader_exp'],
		['Trader_Level', 'tr:lvl', worker.GUIp_i18n.trader_level, 'trader_level']
	];
	this.labWatchers = [
		['Lab_M', 'lab:m', worker.GUIp_i18n.ark_creatures, 'toek_m'],
		['Lab_F', 'lab:f', worker.GUIp_i18n.ark_creatures, 'toek_f'],
	];
	this.forgeWatchers = [
		['Forge_Exp', 'df:exp', worker.GUIp_i18n.forge_exp, 'forge_exp'],
		['Forge_Rank', 'df:rank', worker.GUIp_i18n.forge_rank, 'forge_rank'],
		['Forge_Bytes', 'df:bytes', worker.GUIp_i18n.forge_bytes, 'forge_b'],
		['Forge_Words', 'df:words', worker.GUIp_i18n.forge_words, 'forge_w'],
	];
	this.refineryWatchers = [
		['Souls_Processed', 'sl:prc', worker.GUIp_i18n.souls_prc, 'souls_prc'],
		['Souls_Collected', 'sl:cnt', worker.GUIp_i18n.souls_cnt, 'souls_cnt']
	];
	this.upgraderWatchers = [
		['Up_Retroscope', 'up:rsc', worker.GUIp_i18n.up_rsc, 'up_rsc'],
		['Up_Cryochamber', 'up:cch', worker.GUIp_i18n.up_cch, 'up_cch'],
		['Up_Temple_Tower', 'up:ttw', worker.GUIp_i18n.up_ttw, 'up_ttw'],
		['Up_Temple_Tower_2', 'up:ttw:2', worker.GUIp_i18n.up_ttw_2, 'up_ttw'],
		['Up_Temple_Tower_3', 'up:ttw:3', worker.GUIp_i18n.up_ttw_3, 'up_ttw'],
		['Up_Temple_Tower_4', 'up:ttw:4', worker.GUIp_i18n.up_ttw_4, 'up_ttw'],
		['Up_Temple_Tower_5', 'up:ttw:5', worker.GUIp_i18n.up_ttw_5, 'up_ttw']
	];
	this.fieldWatchers = [
		['Exp', 'exp', worker.GUIp_i18n.exp],
		['Level', 'lvl', worker.GUIp_i18n.level],
		['HP', 'hp', worker.GUIp_i18n.health],
		['Charges', 'ch', worker.GUIp_i18n.charges],
		['Task', 'tsk', worker.GUIp_i18n.task],
		['Side_Job', 'x:tsk', worker.GUIp_i18n.side_job],
		['Monster', 'mns', worker.GUIp_i18n.monsters],
		['Inv', 'inv', worker.GUIp_i18n.inventory],
		['Gold', 'gld', worker.GUIp_i18n.gold],
		['Bricks', 'br', worker.GUIp_i18n.bricks],
		['Logs', 'wd', worker.GUIp_i18n.logs],
		['Savings', 'rtr', worker.GUIp_i18n.savings],
		['Equip1', 'eq1', worker.GUIp_i18n.weapon, 'equip'],
		['Equip2', 'eq2', worker.GUIp_i18n.shield, 'equip'],
		['Equip3', 'eq3', worker.GUIp_i18n.head, 'equip'],
		['Equip4', 'eq4', worker.GUIp_i18n.body, 'equip'],
		['Equip5', 'eq5', worker.GUIp_i18n.arms, 'equip'],
		['Equip6', 'eq6', worker.GUIp_i18n.legs, 'equip'],
		['Equip7', 'eq7', worker.GUIp_i18n.talisman, 'equip'],
		['Death', 'death', worker.GUIp_i18n.death_count],
		['Pet_Level', 'pet:lvl', worker.GUIp_i18n.pet_level, 'pet'],
		['Ark_M', 'ark:m', worker.GUIp_i18n.ark_creatures, 'toek_m'],
		['Ark_F', 'ark:f', worker.GUIp_i18n.ark_creatures, 'toek_f'],
		['Book_Bytes', 'b:bytes', worker.GUIp_i18n.book_bytes, 'book_b'],
		['Book_Words', 'b:words', worker.GUIp_i18n.book_words, 'book_w'],
		['Souls', 'sl', worker.GUIp_i18n.souls, 'souls'],
		['Trader_Exp', 'tr:exp', worker.GUIp_i18n.trader_exp, 'exp'],
		['Trader_Level', 'tr:lvl', worker.GUIp_i18n.trader_level, 'level'],
		['Invites', 'invts']
	];
	this.commonWatchers = [
		['Godpower', 'gp', worker.GUIp_i18n.godpower]
	];

	this._htmlStorageKey = 'Logger:HTML:' + (
		ui_data.isDungeon ? 'Dungeon' : ui_data.isSail ? 'Sail' : ui_data.isMining ? 'Mining' : ui_data.isFight ? 'Fight' : 'Field'
	);
	// forget data when leaving fight mode
	if (this._htmlStorageKey !== 'Logger:HTML:Fight') {
		ui_storage.remove('Logger:HTML:Fight');
		if (this._htmlStorageKey !== 'Logger:HTML:Dungeon') {
			ui_storage.remove('Logger:HTML:Dungeon');
		}
	}
	if (this._htmlStorageKey !== 'Logger:HTML:Sail') {
		ui_storage.remove('Logger:HTML:Sail');
	}
	if (this._htmlStorageKey !== 'Logger:HTML:Mining') {
		ui_storage.remove('Logger:HTML:Mining');
	}

	// save html state only when page becomes hidden, not at every batch
	GUIp.common.addListener(document, 'visibilitychange', function() {
		if (document.visibilityState === 'hidden') {
			ui_storage.set(this._htmlStorageKey, this.bar.innerHTML);
		}
	}.bind(this));

	if (this.suppressOldStats()) {
		this.update();
	} else if ((html = ui_storage.get(this._htmlStorageKey))) {
		this.bar.innerHTML = html;
		this.need_separator = true;
		this.bar.scrollLeft = 21474836;
		GUIp.common.setTimeout(function(bar) { bar.scrollLeft = 21474836; }, 0, this.bar);
	}
};
ui_logger._appendStr = function(id, klass, str, descr) {
	if (this.disabled) return;
	// append separator if needed
	if (this.need_separator) {
		this.need_separator = false;
		if (this.bar.lastElementChild) {
			this.bar.insertAdjacentHTML('beforeend',
				'<li class="separator" title="' + GUIp.common.formatTime(new Date(), 'fulltime') + '"> |</li>'
			);
		}
	}
	// append string
	this.bar.insertAdjacentHTML('beforeend', '<li class="' + klass + '" title="' + descr + '"> ' + str + '</li>');
};

ui_logger._formatNumber = function(x) {
	return x >= 0 ? '+' + x : '\u2212' + -x; // minus sign
};

ui_logger.appendLogs = function(diff) {
	ui_logger._appendStr('Logs', 'logs', 'wd' + ui_logger._formatNumber(diff), worker.GUIp_i18n.logs);
};

ui_logger.finishBatch = function() {
	if (this.disabled) return;
	var bar = this.bar, li, classes;
	while ((li = bar.firstChild) && (bar.scrollWidth > bar.getBoundingClientRect().width + 100 || ((classes = li.classList) && classes.contains('separator')))) {
		bar.removeChild(li);
	}
	this.need_separator = true;
	bar.scrollLeft = 21474836;
};

/**
 * @private
 * @type {!Object<string, function(number, number, string): (number|string|undefined)>}
 */
ui_logger._diffOverriders = {
	generic_exp: function(diff, level, oldLevel) {
		if (diff === null) return null;
		oldLevel = +ui_storage.get('Logger:' + (oldLevel || level));
		level = ui_stats[level]();
		if (oldLevel !== level) {
			return (level - oldLevel) * 100 + diff;
		}
	},
	exp: function(diff, exp, id) {
		return this.generic_exp(diff,'Level',(id === 'Exp' ? 'Level' : 'Map_Level'));
	},

	'tr:exp': function(diff) {
		return this.generic_exp(diff,'Trader_Level');
	},

	'df:exp': function(diff) {
		return this.generic_exp(diff,'Forge_Rank');
	},

	'df:bytes': function(diff, progress) {
		if (diff < 0) {
			if (progress === 0) {
				return '';
			} else {
				return '→' + progress;
			}
		}
	},

	'df:words': function(diff, progress) {
		if (diff < 0) {
			return '→' + progress;
		}
	},

	tsk: function(diff, progress) {
		var quest = ui_stats.Task_Name().replace(/ \((?:выполнено|отменено|эпик|completed|cancelled|epic)\)/g, '');
		if (ui_storage.get('Logger:Task_Name') !== quest) {
			if (!diff && progress === 100) return ''; // workaround a Godville bug when quest progress is not reset to 0
			ui_storage.set('Logger:Task_Name', quest);
			if (diff === null) return null;
			return '→' + progress;
		}
	},

	'x:tsk': function(diff, progress) {
		var sideJob = ui_stats.Side_Job_Name().replace(/ \((?:готов к сдаче|провален|reward pending|expired)\)/g, '');
		if (ui_storage.get('Logger:Side_Job_Name') !== sideJob) {
			ui_storage.set('Logger:Side_Job_Name', sideJob);
			if (diff === null) return null;
			return sideJob && '→' + progress;
		}
	},

	'pet:lvl': function(diff, level) {
		var pet = ui_stats.Pet_NameType();
		if (ui_storage.get('Logger:Pet_NameType') !== pet) {
			ui_storage.set('Logger:Pet_NameType', pet);
			if (diff === null) return null;
			return '→' + level;
		}
	},

	spl: function(diff) {
		if (diff === -1) return '';
	},

	'sl:prc': function(diff, progress) {
		if (diff < 0) {
			if (diff <= -100) {
				return '→' + progress;
			}
		}
	},

	'up:rsc': function(diff, progress) {
		if (diff < 0 && progress === 0) { return ''; }
	},

	invts: function() { return ''; },

	eq1: function(diff, value, id) {
		var bold = ui_stats[id + '_IsBold']();
		if (bold !== ui_storage.getFlag('Logger:' + id + '_IsBold')) {
			ui_storage.set('Logger:' + id + '_IsBold', bold);
			if (diff === null) return null;
			return (diff ? ui_logger._formatNumber(diff) : '') + (bold ? '+<b>b</b>' : '\u2212<b>b</b>');
		}
	}
};
ui_logger._diffOverriders.eq2 =
ui_logger._diffOverriders.eq3 =
ui_logger._diffOverriders.eq4 =
ui_logger._diffOverriders.eq5 =
ui_logger._diffOverriders.eq6 =
ui_logger._diffOverriders.eq7 =
	ui_logger._diffOverriders.eq1;

ui_logger._diffOverriders['up:cch'] =
ui_logger._diffOverriders['up:ttw'] =
ui_logger._diffOverriders['up:ttw:2'] =
ui_logger._diffOverriders['up:ttw:3'] =
	ui_logger._diffOverriders['up:rsc'];

ui_logger._watchStatsValue = function(id, name, descr, klass) {
	klass = (klass || id).toLowerCase();
	var i, j, len, len2, diff, value, result,
		q = '',
		alliesHPCallback = function(i) { return ui_stats.Map_Ally_MaxHP(i) || ui_improver.allsHP.a[i-1] || 0; },
		enemiesHPCallback = function(i) { return ui_stats.EnemySingle_MaxHP(i) || ui_improver.allsHP.e[i-1] || 0; };
	if (name === 'a:hp' && (!ui_storage.getFlag('Option:sumAlliesHp') || ui_data.isSail)) {
		var damageData = [];
		a_loop:
		for (i = 1, len = ui_stats.Hero_Alls_Count(); i <= len; i++) {
			diff = ui_storage.set_with_diff('Logger:'+(id === 'Hero_Alls_HP' ? 'Hero' : 'Map')+'_Ally'+i+'_HP', ui_stats.Hero_Ally_HP(i));
			if (diff) {
				if (!ui_data.isSail) {
					damageData.push({ num: i, diff: diff, cnt: 0, fuzz: 0, cntf: 0 });
				} else {
					// don't display our own hp in allies block
					if (ui_stats.Hero_Ally_Name(i) === ui_data.char_name) {
						continue a_loop;
					}
					// don't display ally hp when we're in fight with that agressive so-called ally (or maybe it's we who are agressive, but anyway)
					for (j = 1, len2 = ui_stats.Enemy_Count(); j <= len2; j++) {
						if (ui_stats.Hero_Ally_Name(i) === ui_stats.EnemySingle_Name(j)) {
							continue a_loop;
						}
					}
					ui_logger._appendStr(id, klass, 'a' + i + ':hp' + ui_logger._formatNumber(diff), descr + ui_utils.loggerPrc(diff,alliesHPCallback(i)));
				}
			}
		}
		if (!damageData.length) {
			return;
		}
		for (i = 0, len = damageData.length; i < len; i++) {
			for (j = (i + 1); j < damageData.length; j++) {
				if (damageData[j].processed) {
					continue;
				}
				if (damageData[i].diff === damageData[j].diff) {
					damageData[i].cnt++;
					damageData[j].processed = true;
					if (damageData[i].parts) {
						damageData[i].parts.push(damageData[j].num);
					} else {
						damageData[i].parts = [damageData[i].num, damageData[j].num];
					}
				} else if (Math.abs(damageData[i].diff - damageData[j].diff) < 3) {
					damageData[i].cntf++;
					damageData[i].fuzz = (damageData[i].fuzz || damageData[i].diff) + damageData[j].diff;
					damageData[j].processed = true;
					if (damageData[i].parts) {
						damageData[i].parts.push(damageData[j].num);
					} else {
						damageData[i].parts = [damageData[i].num, damageData[j].num];
					}
				}
			}
		}
		damageData.sort(function(a,b) {return a.cnt === b.cnt ? a.num - b.num : b.cnt - a.cnt;});
		for (i = 0, len = damageData.length; i < len; i++) {
			if (damageData[i].processed) {
				continue;
			}
			if (damageData[i].fuzz) {
				ui_logger._appendStr(id, klass, 'a:hp' + ui_logger._formatNumber(Math.round((damageData[i].fuzz + damageData[i].diff * damageData[i].cnt)/(damageData[i].cnt + damageData[i].cntf + 1))) + 'x' + (damageData[i].cnt + damageData[i].cntf + 1), descr + ui_utils.loggerPrcMulti(damageData[i],alliesHPCallback));
			} else if (damageData[i].cnt > 0) {
				ui_logger._appendStr(id, klass, 'a:hp' + ui_logger._formatNumber(damageData[i].diff) + 'x' + (damageData[i].cnt + 1), descr + ui_utils.loggerPrcMulti(damageData[i],alliesHPCallback));
			} else {
				ui_logger._appendStr(id, klass, 'a' + damageData[i].num + ':hp' + ui_logger._formatNumber(damageData[i].diff), descr + ui_utils.loggerPrc(damageData[i].diff,alliesHPCallback(damageData[i].num)));
			}
		}
		return;
	}
	if (name === 'e:hp' && (!ui_storage.getFlag('Option:sumAlliesHp') || ui_data.isSail || ui_data.isMining)) {
		for (i = 1, j = 1, len = ui_stats.Enemy_Count(); i <= len; i++) {
			diff = ui_storage.set_with_diff('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			if (diff) {
				if (ui_data.isSail) {
					// in sail, we're not only ally to ourselves, but also an enemy too! cool, right?
					if (ui_stats.EnemySingle_Name(i) === ui_data.char_name) {
						continue;
					}
					// in sail we do have different enemies without page reloading, so as a workaround we'll just silently update saved hp
					// when the name of current enemy[i] doesn't match with previously saved enemy[i]
					if (ui_stats.EnemySingle_Name(i) !== ui_storage.get('Logger:Enemy'+i+'_Name')) {
						ui_storage.set('Logger:Enemy'+i+'_Name', ui_stats.EnemySingle_Name(i));
						continue;
					}
					ui_logger._appendStr(id, klass, 'e' + j++ + ':hp' + ui_logger._formatNumber(diff), descr + ui_utils.loggerPrc(diff,enemiesHPCallback(i)));
				} else if (ui_data.isMining) {
					// player's boss name is prefixes with an ugly '@' char, check for it and continue as we're not an enemy to ourselves (at least here)
					if (ui_stats.EnemySingle_Name(i).includes('@')) {
						continue;
					}
					ui_logger._appendStr(id, klass, 'e' + i + ':hp' + ui_logger._formatNumber(diff), descr + ui_utils.loggerPrc(diff,enemiesHPCallback(i)));
				} else {
					ui_logger._appendStr(id, klass, 'e' + (len > 1 ? i : '') + ':hp' + ui_logger._formatNumber(diff), descr + ui_utils.loggerPrc(diff,enemiesHPCallback(i)));
				}
			}
		}
		return;
	}

	value = ui_stats[id]();
	diff = ui_storage.set_with_diff('Logger:' + id, value);
	if (ui_logger._diffOverriders[name]) {
		result = ui_logger._diffOverriders[name](diff, value, id);
	}
	if (result == null) {
		if (!diff) return; // do not show zero diffs unless explicitly requested by the overrider
		result = diff;
	}
	if (typeof result === 'number') {
		result = ui_logger._formatNumber(result);
	}
	if (result) {
		if (name === 'hp' || name === 'h:hp') {
			q = ui_utils.loggerPrc(diff, ui_stats.Max_HP());
		}
		ui_logger._appendStr(id, klass, name + result, descr + q);
	}
};
ui_logger._updateWatchers = function(watchersList) {
	var args;
	for (var i = 0, len = watchersList.length; i < len; i++) {
		args = watchersList[i];
		ui_logger._watchStatsValue(args[0], args[1], args[2], args[3]);
	}
};
ui_logger.suppressOldStats = function() {
	var lastFieldInit;
	if ((lastFieldInit = ui_data.lastFieldInit)) {
		delete ui_data.lastFieldInit;
		// last init in more than 2 days ago
		if (lastFieldInit > 172800000) {
			// suppress some values so digest won't get overflown
			var supList = [
				'HP', 'Godpower', 'Gold', 'Inv', 'Task', 'Task_Name', 'Side_Job', 'Side_Job_Name', 'Equip1', 'Equip2',
				'Equip3', 'Equip4', 'Equip5', 'Equip6', 'Equip7', 'Equip1_IsBold', 'Equip2_IsBold', 'Equip3_IsBold',
				'Equip4_IsBold', 'Equip5_IsBold', 'Equip6_IsBold', 'Equip7_IsBold', 'Pet_NameType'
			];
			for (var i = 0, len = supList.length; i < len; i++) {
				ui_storage.set_with_diff('Logger:' + supList[i], ui_stats[supList[i]]());
			}
			// show ref date
			ui_logger._appendStr(null, 'refdate', GUIp.common.formatTime(new Date(Date.now() - lastFieldInit),'logger'), '');
			ui_logger.need_separator = true;
			return true;
		}
	}
	return false;
};
ui_logger.update = function(ltarget) {
	if (document.getElementById('search_block') && !ui_utils.isHidden(document.getElementById('search_block'))) {
		return;
	}
	if (ltarget !== undefined) {
		ui_logger._updateWatchers(ltarget);
	} else if (ui_data.isDungeon) {
		ui_logger._updateWatchers(this.dungeonWatchers);
	} else if (ui_data.isSail) {
		ui_logger._updateWatchers(this.sailWatchers);
	} else if (ui_data.isMining) {
		ui_logger._updateWatchers(this.miningWatchers);
	} else if (ui_data.isFight) {
		ui_logger._updateWatchers(this.battleWatchers);
	} else if (ui_data.inShop) {
		ui_logger._updateWatchers(this.shopWatchers);
	} else {
		ui_logger.suppressOldStats();
		ui_logger._updateWatchers(this.fieldWatchers);
	}
	this._updateWatchers(this.commonWatchers);
	this.finishBatch();
};
