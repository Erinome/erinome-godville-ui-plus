// ui_starter
var ui_starter = worker.GUIp.starter = {};

ui_starter._init = function() {
	var try2 = GUIp.common.try2;
	ui_data.init();
	ui_storage.init();
	try2.call(ui_utils,     ui_utils.jqueryExtInit);
	try2.call(ui_utils,     ui_utils.addCSS);
	try2.call(ui_expr,      ui_expr.init);
	try2.call(ui_words,     ui_words.init);
	try2.call(ui_logger,    ui_logger.create);
	try2.call(ui_timeout,   ui_timeout.create);
	try2.call(ui_help,      ui_help.init);
	try2.call(ui_informer,  ui_informer.init);
	try2.call(ui_forum,     ui_forum.init);
	try2.call(ui_inventory, ui_inventory.init);
	try2.call(ui_mining,    ui_mining.init);
	try2.call(ui_dragger,   ui_dragger.init);
	try2.call(ui_improver,  ui_improver.init);
	try2.call(ui_improver,  ui_improver.improve);
	try2.call(ui_timers,    ui_timers.init);
	try2.call(ui_observers, ui_observers.init);
};

ui_starter.start = function() {
	if (document.body.classList.contains('mbg') && !/^\((\d+)?[!@~\+]\) /.test(document.title)) {
		// a faulty mobile page in non-fight mode may request town_c update after the page is fully renreded,
		// but then will throw a devastating unrecoverable error if we try to open #hmap_svg before data is fetched.
		// so we'll try to delay our initialization until town_c is updated
		var lsTowns, ct = Date.now()/1e3;
		if (!(lsTowns = localStorage.getItem('town_c')) || !(lsTowns = JSON.parse(lsTowns)) || (ct - lsTowns.at > 180) || !lsTowns.mrt || (ct - lsTowns.at > lsTowns.mrt) /* town_c updates when these conditions are met */
			&& (!worker.e_start_delayed || worker.e_start_delayed < 10) /* also safeguard ourselves from waiting forever in case something goes wrong */) {
			GUIp.common.setTimeout(function() {
				if (!worker.e_start_delayed) {
					worker.e_start_delayed = 0;
				}
				worker.e_start_delayed++;
				ui_starter.start();
			}, 200);
			return;
		}
	}
	worker.console.time('Godville UI+ initialized in');

	GUIp.common.onUnhandledException = ui_utils.processErrorOnce;
	ui_starter._init();

	if (!ui_data.isFight) {
		GUIp.common.addListener(worker, 'mousemove', ui_improver.activity);
		GUIp.common.addListener(worker, 'scroll', ui_improver.activity);
		GUIp.common.addListener(worker, 'touchmove', ui_improver.activity);
	}

	// svg for #logger fade-out in FF
	if (worker.GUIp_browser === 'Firefox') {
		var is5c = document.getElementsByClassName('page_wrapper_5c').length;
		document.body.insertAdjacentHTML('beforeend',
			'<svg id="fader">' +
				'<defs>' +
					'<linearGradient id="gradient" x1="0" y1="0" x2 ="100%" y2="0">' +
						'<stop stop-color="black" offset="0"></stop>' +
						'<stop stop-color="white" offset="0.0' + (is5c ? '2' : '3') + '"></stop>' +
					'</linearGradient>' +
					'<mask id="fader_masking" maskUnits="objectBoundingBox" maskContentUnits="objectBoundingBox">' +
						'<rect x="0.0' + (is5c ? '2' : '3') + '" width="0.9' + (is5c ? '8' : '7') + '" height="1" fill="url(#gradient)" />' +
					'</mask>' +
				'</defs>' +
			'</svg>'
		);
	}

	worker.console.timeEnd('Godville UI+ initialized in');
};

ui_starter.waitForTitle = function(callback) {
	var titleRE = /(\(\d*[!@~+]\) )?.*? (и е(го|ё) геро|and h(is|er) hero)/;
	if (titleRE.test(document.title)) {
		ui_starter.start();
		callback();
	} else {
		GUIp.common.newMutationObserver(function(mutations, observer) {
			if (mutations.some(function(mutation) {
				return Array.prototype.some.call(mutation.addedNodes, function(node) {
					return titleRE.test(node.nodeValue);
				});
			})) {
				observer.disconnect();
				ui_starter.start();
				callback();
			}
		}).observe(document.querySelector('title'),{ childList: true });
	}
};

ui_starter.waitForJQuery = function(callback) {
	if (worker.$) {
		ui_starter.waitForTitle(callback);
	} else {
		// no better way
		// fortunately, we almost surely won't get there, as jQuery would either be cached or already loaded
		var timer = GUIp.common.setInterval(function() {
			if (!worker.$) return;
			worker.clearInterval(timer);
			ui_starter.waitForTitle(callback);
		}, 100);
	}
};

ui_starter.waitForContents = function(callback) {
	if (document.getElementById('stats') || document.getElementById('m_info') || document.getElementById('b_info') || document.getElementById('tabbar')) {
		GUIp.common.try2(this.waitForJQuery, callback);
	} else {
		GUIp.common.newMutationObserver(function(mutations, observer) {
			if (document.getElementById('stats') || document.getElementById('m_info') || document.getElementById('b_info') || document.getElementById('tabbar')) {
				observer.disconnect();
				ui_starter.waitForJQuery(callback);
			}
		}).observe(document.body, {childList: true, subtree: true});
	}
};
