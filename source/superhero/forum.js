// ui_forum
var ui_forum = worker.GUIp.forum = {};

/** @type {!Array<string>} */
ui_forum.processed = []; // list of already updated topics

/** @type {boolean} */
ui_forum.altCounter = false;

ui_forum.init = function() {
	if (ui_storage.getFlag('Option:forumInformerAltCounter')) {
		ui_forum.altCounter = true;
	}
	document.body.insertAdjacentHTML('afterbegin', '<div id="forum_informer_bar"' + (ui_forum.altCounter ? ' class="fi_alt_counter"' : '') + '></div>');
	// check for possible missed notifications
	var notifications = ui_storage.list('ForumInformersNotify');
	if (notifications.length) {
		var tids = [];
		notifications = notifications.map(function(a) { return {key:a,data:JSON.parse(ui_storage.get(a))}; })
			.sort(function(a,b) { return b.data.ndate - a.data.ndate; })
			.filter(function(a) { if (tids.includes(a.data.tid)) { ui_storage.remove(a.key); return false; }; tids.push(a.data.tid); return true; });
		ui_forum._informersNotify(notifications);
	}
	ui_storage.addFallbackListener(function onTopicOpen(key, value) {
		if (key.startsWith('ForumInformersNotify')) {
			ui_forum._informersNotify([{key: key, data: JSON.parse(value)}]);
		}
	});
	var subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
		informers = JSON.parse(ui_storage.get('ForumInformers')) || {},
		topics = worker.Object.keys(informers),
		tid;
	// update data from cache at first since online check might be unavailable due to new insane request limit restrictions
	for (var i = 0, len = topics.length; i < len; i++) {
		tid = topics[i];
		if (!subscriptions[tid] || informers[tid].obsolete) {
			delete informers[tid];
			continue;
		}
		ui_forum._setInformer(tid, informers[tid], subscriptions[tid]);
	}
	ui_storage.set('ForumInformers', JSON.stringify(informers));
	// first attempt to check for new posts
	ui_forum._check();
	GUIp.common.setInterval(ui_forum._check.bind(ui_forum), 200*1000);
};
ui_forum._processPending = function(subscriptions, informers, pending) {
	var topic = pending.tid,
		sub = subscriptions[topic],
		inf = informers[topic],
		quiet = true,
		node;
	if (sub && pending.posts && pending.date > sub.date) {
		sub.posts = pending.posts;
		sub.date = pending.date;
		sub.by = pending.by;
		quiet = false;
		GUIp.common.debug('updating subscriptions data for tid=' + topic);
	}
	if (pending.obsolete) {
		delete informers[topic];
		ui_forum._unsetInformer(topic);
		GUIp.common.debug('removing obsolete informer for tid=' + topic);
	} else if (inf && pending.iposts) {
		inf.posts = pending.iposts;
		inf.diff = Math.max(pending.tposts - inf.posts, 1);
		GUIp.common.debug('updating informer for tid=' + topic + ' with posts=' + inf.posts + ', diff=' + inf.diff);
		if (!(node = document.getElementById('topic' + topic)) || node.lastChild.textContent != inf.diff) {
			ui_forum._setInformer(topic, inf, sub, quiet);
		}
	}
};
ui_forum._informersNotify = function(pending_notifications) {
	var subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
		informers = JSON.parse(ui_storage.get('ForumInformers')) || {};
	for (var i = 0, len = pending_notifications.length; i < len; i++) {
		var pending = pending_notifications[i].data;
		if (pending) { // can be null, e. g., when there are more than one /superhero tab open
			GUIp.common.debug('processing forum notification: ' + JSON.stringify(pending_notifications[i]));
			ui_forum._processPending(subscriptions, informers, pending);
		}
		ui_storage.remove(pending_notifications[i].key);
	}
	ui_storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
	ui_storage.set('ForumInformers', JSON.stringify(informers));
};
ui_forum._check = function() {
	var i, len, topics, subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
		prepared = [],
		available = [],
		requests = 0;
	topics = worker.Object.keys(subscriptions);
	if (topics.length) {
		ui_improver.showSubsLink();
	}
	if (topics.length <= 20) {
		prepared = topics;
	} else {
		topics.sort();
		// process prioritized topics
		for (i = 0, len = topics.length; i < len; i++) {
			if (subscriptions[topics[i]].rapid) {
				if (prepared.length < 19) {
					prepared.push(topics[i]);
				}
			}
		}
		// prepare a list of available topics
		for (i = 0, len = topics.length; i < len; i++) {
			if (!prepared.includes(topics[i]) && !ui_forum.processed.includes(topics[i])) {
				available.push(topics[i]);
			}
		}
		if ((prepared.length + available.length) < 20) {
			prepared = prepared.concat(available);
			ui_forum.processed = [];
			available = [];
			for (i = 0, len = topics.length; i < len; i++) {
				if (!prepared.includes(topics[i])) {
					available.push(topics[i]);
				}
			}
		}
		// append random available topics to list of prepared ones up to 20
		prepared = prepared.concat(GUIp.common.shuffleArray(available).slice(0,Math.max(20 - prepared.length,0)));
	}
	// request update if there's anything we're interested in
	if (prepared.length) {
		for (i = 0, len = prepared.length; i < len; i++) {
			prepared[i] = 'topic_ids[]=' + prepared[i];
		}
		this._request(prepared, requests++);
	}
};
ui_forum._request = function(postdata, requests) {
	GUIp.common.setTimeout(GUIp.common.postXHR.bind(null, worker.location.protocol + '//' + worker.location.host + '/forums/last_in_topics', postdata.join('&'), 'url', ui_forum._parse.bind(ui_forum)), 500*requests);
};
ui_forum._setInformer = function(tid, inf, sub, quiet) {
	var nodeID = 'topic' + tid,
		notifID = nodeID,
		informer = document.getElementById(nodeID);
	if (!informer) {
		if (quiet) return;
		if (ui_data.isMobile) {
			var node = document.getElementsByClassName('e_mt_forum_informers')[0];
			ui_utils.hideElem(node, false);
			node.insertAdjacentHTML('afterend',
				'<div class="line e_m_forum_informers_line" id="' + nodeID + '"><div class="l_text"><span class="topic"></span><span class="date"></span><div class="fr_new_badge"></div></div></div>'
			);
		} else {
			document.getElementById('forum_informer_bar').insertAdjacentHTML('beforeend',
				'<a id="' + nodeID + '" target="_blank"><span></span><div class="fr_new_badge"></div></a>'
			);
		}
		informer = document.getElementById(nodeID);
		if (ui_data.isMobile) {
			GUIp.common.addListener(informer, 'click', function(ev) {
				// just open the forum link in a new tab
				worker.open(informer.dataset.href);
				// hide the desktop notification
				ui_utils.hideNotification(notifID);
			});
			GUIp.common.addListener(informer.firstChild.lastChild, 'click', function(ev) {
				// do not open the link
				ev.stopPropagation();
				// mark informer as read
				var informers = JSON.parse(ui_storage.get('ForumInformers'));
				delete informers[tid];
				ui_storage.set('ForumInformers', JSON.stringify(informers));
				ui_forum._unsetInformer(tid);
				// hide the desktop notification
				ui_utils.hideNotification(notifID);
			});
		} else {
			GUIp.common.addListener(informer, 'click', function(ev) {
				// prevent closing Godville's pop-ups
				ev.stopPropagation();
				if (ev.button === 0 /*left*/ && !ev.ctrlKey && !ev.shiftKey && !ev.metaKey /*⌘*/) {
					// prevent following the link
					ev.preventDefault();
					// hide the informer and desktop notification
					var informers = JSON.parse(ui_storage.get('ForumInformers'));
					delete informers[tid];
					ui_storage.set('ForumInformers', JSON.stringify(informers));
					ui_forum._unsetInformer(tid);
				} else if (ev.button !== 2 /*right*/) {
					// hide the desktop notification
					ui_utils.hideNotification(notifID);
				}
			});
			GUIp.common.addListener(informer.lastChild, 'click', function(ev) {
				if (ev.button === 0 /*left*/) {
					// prevent preventing following the link
					ev.stopPropagation();
					// hide the desktop notification
					ui_utils.hideNotification(notifID);
				}
			});
		}
	}
	var page = Math.floor((sub.posts - inf.diff) / 25) + 1;
	if (ui_data.isMobile) {
		informer.dataset.href = '/forums/show_topic/' + tid + '?page=' + page + '&epost=' + (sub.posts - inf.diff + 25 - page*25 + 1);
		informer.firstChild.style.paddingLeft = (16 + String(inf.diff).length*6) + 'px';
		informer.firstChild.firstChild.textContent = sub.name;
		informer.firstChild.firstChild.nextSibling.textContent = '(' + GUIp.common.formatTime(new Date(sub.date),'forum') + ', ' + sub.by +')'; 
		informer.firstChild.lastChild.textContent = inf.diff;
	} else {
		informer.style[ui_forum.altCounter ? 'paddingLeft' : 'paddingRight'] = (16 + String(inf.diff).length*6) + 'px';
		informer.title = worker.GUIp_i18n.forum_subs_info + sub.by + ' (' + GUIp.common.formatTime(new Date(sub.date)) + ')';
		informer.href = '/forums/show_topic/' + tid + '?page=' + page + '&epost=' + (sub.posts - inf.diff + 25 - page*25 + 1);
		informer.firstChild.textContent = sub.name;
		informer.lastChild.textContent = inf.diff;
	}

	if (quiet) return;

	// desktop notification
	if (sub.notifications & 0x1) {
		ui_utils.showNotification(
			'[F] ' + ui_data.god_name,
			GUIp_i18n.plfmt('forum_subs_new_posts_', inf.diff, sub.by, sub.name),
			function() { informer.lastChild.click(); return false; },
			notifID
		);
	}
	// sound notification
	if (sub.notifications & 0x2) {
		GUIp.common.playSound(ui_storage.get('Option:forumInformerCustomSound') || 'msg');
	}
};
ui_forum._unsetInformer = function(tid) {
	var informer = worker.$('#topic' + tid);
	if (informer.length) {
		informer.slideToggle("fast", function() {
			if (this.parentElement) {
				this.parentElement.removeChild(this);
				ui_informer.clearTitle();
				if (ui_data.isMobile) {
					ui_utils.hideElem(document.getElementsByClassName('e_mt_forum_informers')[0], !document.querySelectorAll('.e_m_forum_informers_line').length);
				}
			}
		});
	}
	ui_utils.hideNotification('topic' + tid);
};
ui_forum._parse = function(xhr) {
	var response;
	try {
		response = JSON.parse(xhr.responseText);
		if (response.status !== 'success' || !response.topics) {
			return;
		}
	} catch (e) {
		GUIp.common.error('unexpected response to forum subscriptions request:', xhr.responseText);
		return;
	}
	var topic, topicDate, sub, inf, diff, new_diff, last_read,
		subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
		informers = JSON.parse(ui_storage.get('ForumInformers')) || {},
		topics = response.topics;
	for (var tid in topics) {
		topic = topics[tid];
		topicDate = +new Date(topic.last_post_at);
		sub = subscriptions[tid];
		if (!sub) continue;

		if (!ui_forum.processed.includes(tid)) {
			ui_forum.processed.push(tid);
		}
		diff = topic.cnt - sub.posts;
		if (diff <= 0 && sub.date < topicDate) {
			diff = 1;
		}
		sub.posts = topic.cnt;
		sub.date = topicDate;
		sub.name = topic.title;
		sub.by = topic.last_post_by;
		inf = informers[tid];
		if (diff > 0) {
			if (topic.last_post_by !== ui_data.god_name) {
				new_diff = inf ? (topic.cnt - inf.posts) : diff;
				last_read = inf ? inf.posts : (topic.cnt - diff);
				inf = informers[tid] = {diff: new_diff < 1 ? 1 : new_diff, posts: last_read};
			} else {
				inf = null;
				delete informers[tid];
				ui_forum._unsetInformer(tid);
			}
		} else if (diff < 0) {
			if (topic.last_post_by !== ui_data.god_name && inf && (inf.diff + diff) > 0) {
				last_read = topic.cnt > inf.posts ? inf.posts : topic.cnt;
				inf = informers[tid] = {diff: inf.diff + diff, posts: last_read};
			} else {
				inf = null;
				delete informers[tid];
				ui_forum._unsetInformer(tid);
			}
		}
		if (inf) {
			ui_forum._setInformer(tid, inf, sub, diff <= 0);
		}
	}
	ui_storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
	ui_storage.set('ForumInformers', JSON.stringify(informers));
	ui_informer.clearTitle();
};
