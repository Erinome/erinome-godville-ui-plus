// ui_utils
var ui_utils = worker.GUIp.utils = {};

ui_utils.voiceInput = null;
ui_utils.voiceDisabled = false; // only set to true when we disable it ourselves
ui_utils.hasShownErrorMessage = false;
ui_utils.hasShownInfoMessage = false;

if (document.hasFocus) {
	Object.defineProperty(ui_utils, 'windowFocused', {get: document.hasFocus.bind(document)});
} else {
	ui_utils.windowFocused = !document.hidden; // might give false positives
	worker.addEventListener('blur',  function() { ui_utils.windowFocused = false; });
	worker.addEventListener('focus', function() { ui_utils.windowFocused = true; });
}

// base phrase say algorithm
ui_utils.setVoice = function(voice, force) {
	var postv = this.voiceInput.value.match(/\/\/\ .+$/);
	if (postv && !force) {
		this.voiceInput.value = voice + ' ' + postv[0];
	} else {
		this.voiceInput.value = voice;
	}
	ui_utils.triggerChangeOnVoiceInput();
};
ui_utils.triggerChangeOnVoiceInput = function() {
	//worker.$(this.voiceInput).change();
	worker.$(this.voiceInput).trigger(worker.$.Event( "change", { originalEvent: {} } ));
};
// finds a label with given name
ui_utils.findLabel = function($base_elem, label_name) {
	return worker.$('.l_capt', $base_elem).filter(function(index) {
		return this.textContent === label_name;
	});
};
// checks if $elem already improved
ui_utils.isAlreadyImproved = function(elem) {
	if (!elem) {
		return true; // if elem is missing we'll assume it's already improved so the code won't try to re-improve it again
	}
	// opera 12 does not implement dataset interface on svg elements
	if (elem.getAttribute('data-improved') === '1') {
		return true;
	} else {
		elem.setAttribute('data-improved','1');
		return false;
	}
};
// generic voice generator
ui_utils.getGenericVoicegenButton = function(text, section, title) {
	var voicegen = document.createElement('a');
	voicegen.title = title;
	voicegen.textContent = text;
	voicegen.className = 'voice_generator ' + (ui_data.isDungeon ? 'dungeon' : ui_data.isFight ? 'battle' : 'field') + ' ' + section;
	GUIp.common.addListener(voicegen, 'click', function(ev) {
		ev.preventDefault();
		if (ui_utils.voiceInput.getAttribute('disabled') === 'disabled') {
			return;
		}
		if (section !== 'mnemonics') {
			ui_utils.setVoice(ui_words.longPhrase(section));
		} else {
			ui_words.mnemoVoice();
		}
		ui_words.currentPhrase = "";
	});
	return voicegen;
};
ui_utils.addVoicegen = function(elem, voicegen_name, section, title) {
	elem.insertAdjacentElement('afterend', ui_utils.getGenericVoicegenButton(voicegen_name, section, title));
};
ui_utils.mapVoicegen = function(e) {
	var x, y, X, Y, K, curPos = GUIp.common.getOwnCell();

	if (!curPos || ui_utils.isHidden(document.getElementById('godvoice') || {}) || ui_storage.getFlag('Option:disableVoiceGenerators') || ui_utils.voiceInput.getAttribute('disabled') === 'disabled') {
		return;
	}

	curPos = curPos.getBoundingClientRect();
	X = e.clientX - curPos.left - curPos.width / 2;
	Y = e.clientY - curPos.top - curPos.height / 2;

	K = 1 / Math.sqrt(2);
	x = X * K - Y * K;
	y = X * K + Y * K;

	if (x > 0) {
		if (y > 0) {
			ui_utils.setVoice(ui_words.longPhrase('go_east'));
		} else {
			ui_utils.setVoice(ui_words.longPhrase('go_north'));
		}
	} else {
		if (y > 0) {
			ui_utils.setVoice(ui_words.longPhrase('go_south'));
		} else {
			ui_utils.setVoice(ui_words.longPhrase('go_west'));
		}
	}
	ui_words.currentPhrase = "";
	return;
};
ui_utils.addGPConfirmation = function($buttons, requiredGP, msg) {
	var onClick = function(target, ev) {
		if (ui_storage.getFlag('Option:confirmInfluences') && ui_stats.Godpower() < requiredGP && target.classList.contains('div_link') && !worker.confirm(msg)) {
			ev.preventDefault();
			ev.stopImmediatePropagation();
		}
	};
	$buttons.click(function(ev) { GUIp.common.try2(onClick, this, ev); });
	// prioritize
	for (var i = 0, len = $buttons.length; i < len; i++) {
		var handlers = worker.$._data($buttons[i], 'events').click;
		handlers.unshift(handlers.pop());
	}
};
// Случайный индекс в массиве
ui_utils.getRandomIndex = function(arr) {
	return Math.floor(Math.random()*arr.length);
};
// Случайный элемент массива
ui_utils.getRandomItem = function(arr) {
	return arr[ui_utils.getRandomIndex(arr)];
};
// Вытаскивает случайный элемент из массива
ui_utils.popRandomItem = function(arr) {
	var ind = ui_utils.getRandomIndex(arr);
	var res = arr[ind];
	arr.copyWithin(ind, ind + 1);
	arr.length--;
	return res;
};

ui_utils.parseDiaryDate = function(time) {
	var m, hours, date,
		ampm = worker.ampm === '12h',
		today = new Date,
		nearFuture = +today + 300e3; // 5m
	if (!time || !(m = /(\d+):(\d+)/.exec(time))) {
		return null;
	}
	if (ampm) {
		// there are no AM or PM marks. we have to guess
		hours = +m[1];
		date = today.setHours(hours >= 12 ? hours - 12 : hours, +m[2], 0, 0);
		if (date > nearFuture) {
			date -= 86400e3; // 24h
		}
		if (date + 43200e3 <= nearFuture) { // 12h
			date += 43200e3; // 12h
		}
	} else {
		date = today.setHours(+m[1], +m[2], 0, 0);
		if (date > nearFuture) {
			date -= 86400e3; // 24h
		}
	}
	return date;
};

/**
 * @param {{date: (number|!Date)}} a
 * @param {{date: (number|!Date)}} b
 * @returns {number}
 */
ui_utils.byDate = function(a, b) { return a.date - b.date; };

/**
 * @param {?string} text
 * @returns {!Array<string>}
 */
ui_utils.splitList = function(text) {
	return text ? text.split(',') : [];
};

/**
 * @template T
 * @param {!Element} node
 * @param {!MutationObserverInit} options
 * @param {function(!Array<!MutationRecord>): (T | undefined)} check
 * @returns {!Promise<T>}
 */
ui_utils.observeUntil = function(node, options, check) {
	var x = check([]);
	return x !== undefined ? Promise.resolve(x) : new Promise(function(resolve) {
		GUIp.common.newMutationObserver(function(mutations, observer) {
			var x = check(mutations);
			if (x !== undefined) {
				observer.disconnect();
				resolve(x);
			}
		}).observe(node, options);
	});
};

ui_utils._clock = {promise: null, diff: 0, failures: 0};

ui_utils._failSyncingClock = function(resolve, xhr) {
	GUIp.common.warn('could not sync with Akamai Time Server (' + xhr.status + '):', xhr.responseText);
	ui_utils._clock.promise = null;
	ui_utils._clock.failures++;
	resolve(false);
};

ui_utils._syncClock = function(resolve) {
	GUIp.common.getXHR('https://time.akamai.com', function(xhr) {
		var received = +xhr.responseText;
		if (received !== received) {
			ui_utils._failSyncingClock(resolve, xhr);
		} else {
			ui_utils._clock.promise = null;
			ui_utils._clock.diff = received * 1e3 - Date.now();
			resolve(true);
		}
	}, ui_utils._failSyncingClock.bind(null, resolve));
};

/**
 * @returns {!Promise}
 */
ui_utils.syncClock = function() {
	return ui_utils._clock.promise || (ui_utils._clock.promise = new Promise(ui_utils._syncClock));
};

/**
 * @param {number} [offset]
 * @param {boolean} [bypassSyncing]
 * @returns {!Date}
 */
ui_utils.getPreciseTime = function(offset, bypassSyncing) {
	if (!ui_utils._clock.diff && !ui_utils._clock.promise && !bypassSyncing && ui_utils._clock.failures < 5) {
		ui_utils.syncClock().catch(GUIp.common.onUnhandledException);
	}
	return new Date(Date.now() + (offset || 0) + ui_utils._clock.diff);
};

// Форматирование времени
ui_utils.formatClock = function(godvilleTime) {
	return ('0' + godvilleTime.getUTCHours()).slice(-2) + ':' + ('0' + godvilleTime.getUTCMinutes()).slice(-2) + ':' + ('0' + godvilleTime.getUTCSeconds()).slice(-2);
};

/**
 * @param {string} text
 * @returns {string}
 */
ui_utils.formatSailingDistInfoAbbr = function(text) {
	var first = text[0].toUpperCase();
	return '<abbr title="' + first + text.slice(1) + '">' + first + ':</abbr>';
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {!Object<string, boolean>} conditions
 * @param {!Array<string>} mapSettings
 * @returns {!Element}
 */
ui_utils.formatSailingDistInfo = function(model, pos, conditions, mapSettings) {
	if (!mapSettings.includes('tcrd')) {
		var imap = GUIp.common.islandsMap,
			stepsLeft = (conditions.small ? 50 : 100) - ui_stats.currentStep(),
			supplies = ui_stats.Map_Supplies(),
			predictor = imap.defaults[mapSettings.includes('pdpm') ? 'predictMaxBorderRadius' : 'predictBorderRadius'],
			distToPort = imap.vec.dist(pos, model.port !== 0x8080 ? model.port : 0x0),
			distToRim = predictor(model, conditions) - distToPort;

		var formatPart = function(dist, stepsLeft, abbrText) {
			var cls = stepsLeft < dist ? 'e_low_steps' : supplies < dist ? 'e_low_supplies' : '';
			return (
				'<span' + (cls && ' class="' + cls + '"') + '>' +
					ui_utils.formatSailingDistInfoAbbr(abbrText) + dist +
				'</span>'
			);
		};
		return (
			'(' +
			formatPart(distToPort, stepsLeft, worker.GUIp_i18n.sail_port) +
			', ' +
			formatPart(distToRim, conditions.locked ? 0 : stepsLeft, worker.GUIp_i18n.sail_rim) +
			')'
		);
	} else {
		var result = '(',
			q = pos << 24 >> 24,
			r = pos << 16 >> 24,
			y = r,
			x = q + r * 0.5;
		if (y) {
			result += ui_utils.formatSailingDistInfoAbbr(worker.GUIp_i18n[y > 0 ? 'south' : 'north']);
		}
		result += Math.abs(y) + ', ';
		if (x) {
			result += ui_utils.formatSailingDistInfoAbbr(worker.GUIp_i18n[x > 0 ? 'east' : 'west']);
		}
		return result + Math.ceil(Math.abs(x)) + ')';
	}
};

ui_utils.addCSS = function () {
	if (worker.GUIp_browser === 'Opera') {
		// this should make minimap in dungeons a bit better
		if (document.getElementsByClassName('dml').length > 0) {
			document.getElementsByClassName('dml')[0].parentNode.style.paddingBottom = '21px';
		}
	}
};
ui_utils.showMessage = function(msg_no, msg) {
	var id = 'msg' + msg_no, msgElem, closeButton, onClick;
	if ((msgElem = document.getElementById(id + '_close'))) {
		msgElem.click();
		msgElem.id = document.getElementById(id).id = '';
	}
	document.getElementById('menu_bar').insertAdjacentHTML('afterend',
		'<div id="' + id + '" class="hint_bar ui_msg">'+
			'<div class="hint_bar_capt"><b>' + msg.title + '</b></div>'+
			'<div class="hint_bar_content">' + msg.content + '</div>'+
			'<div class="hint_bar_close"><a id="' + id + '_close">' + worker.GUIp_i18n.close + '</a></div>' +
		'</div>'
	);
	msgElem = document.getElementById(id);
	closeButton = document.getElementById(id + '_close');
	onClick = GUIp.common.try2.bind(null, function(ev) {
		ev.preventDefault();
		closeButton.removeEventListener('click', onClick);
		worker.$(msgElem).fadeToggle(GUIp.common.try2.bind(null, function() {
			msgElem.parentNode.removeChild(msgElem);
			if (!isNaN(msg_no)) {
				ui_storage.set('lastShownMessage', msg_no);
			}
		}));
	});
	closeButton.addEventListener('click', onClick);

	GUIp.common.setTimeout(function() {
		worker.$(msgElem).fadeToggle(500, msg.callback && GUIp.common.try2.bind(null, msg.callback));
	}, 500);
};
ui_utils.getNodeIndex = function(node) {
	var i = 0;
	while ((node = node.previousElementSibling)) {
		i++;
	}
	return i;
};
ui_utils.openChatWith = function(friend, e) {
	if (e) {
		e.preventDefault();
		e.stopPropagation();
	}
	var found = Array.from(document.querySelectorAll((ui_data.isMobile ? '.e_m_friends' : '.msgDockPopupW') + ' .frname')).find(function(a) { return a.textContent === friend; });
	if (found) {
		found.click();
	}
};
ui_utils.dateToMoscowTimeZone = function(date) {
	var temp = new Date(date);
	temp.setTime(temp.getTime() + (temp.getTimezoneOffset() + (worker.GUIp_locale === 'en' ? 115 : 175))*60*1000);
	return temp.getFullYear() + '/' +
		  (temp.getMonth() + 1 < 10 ? '0' : '') + (temp.getMonth() + 1) + '/' +
		  (temp.getDate() < 10 ? '0' : '') + temp.getDate();
};
ui_utils.updateVoiceSubmitState = function() {
	if (ui_data.isFight) return;
	var submit = document.getElementById('voice_submit');
	if (!submit) return;

	var dis = false, conds = ui_improver.freezeVoiceButton.get();
	dis = dis || (conds.includes('when_empty') && !!this.voiceInput && !this.voiceInput.value);
	dis = dis || (conds.includes('after_voice') && ui_timeout.running);
	// check not to re-enable when it was blocked by Godville
	if (dis !== submit.disabled && (dis || this.voiceDisabled)) {
		submit.disabled = this.voiceDisabled = dis;
	}
};
ui_utils.hideElem = function(elem, hide) {
	if (elem) elem.classList.toggle('hidden', !!hide);
};
ui_utils.isHidden = function(elem) {
	return !elem.offsetParent;
};
ui_utils._parseVersion = function(isNewestCallback, isNotNewestCallback, failCallback, xhr) {
	var m = /Godville UI\+ (\d+\.\d+\.\d+\.\d+)/.exec(xhr.responseText);
	if (m) {
		var cur = ui_data.currentVersion.split('.'),
			latest = m[1].split('.');
		if ((cur[0] - latest[0] || cur[1] - latest[1] || cur[2] - latest[2] || cur[3] - latest[3]) >= 0) {
			if (isNewestCallback) {
				isNewestCallback();
			}
		} else if (isNotNewestCallback) {
			isNotNewestCallback(m[1]);
		}
	} else if (failCallback) {
		failCallback();
	}
};
ui_utils.checkVersion = function(isNewestCallback, isNotNewestCallback, failCallback) {
	GUIp.common.getXHR(GUIp.common.erinome_url+'/checkversion?ver=' + ui_data.currentVersion, ui_utils._parseVersion.bind(null, isNewestCallback, isNotNewestCallback, failCallback), failCallback);
};

ui_utils.processError = function(error, isDebugMode) {
	var description = error.name + ': ' + error.message,
		htmlDescription = GUIp.common.escapeHTML(description),
		stack = error.stack
			.replace(description, '')
			.trim()
			.replace(/^\s*at\s+|\(\[arguments not available\]\)|@$/gm, '')
			.replace(/^Object\.(?=\w+\.)/gm, '')
			.replace(/<anonymous function: (.*?)>/g, '$1')
			.replace(/@https?:.*?(:[^:\n]*)?$/gm, ' (<embedded>$1)')
			.replace(/@(?:(?:\w+-extension|resource):.*\/)?(.*)/g, ' ($1)')
			.replace(/\w+-extension:.*\//g, ''),
		browserVersion = (new RegExp(worker.GUIp_browser + '/([\\d.]+)').exec(navigator.userAgent) || [, '&lt;unknown version&gt;'])[1];
	worker.console.error('Godville UI+ error log:\n' + description + '\n' + worker.GUIp_i18n.error_message_stack_trace + '\n' + stack);
	ui_utils.showMessage('error', {
		title: worker.GUIp_i18n.error_message_title,
		content:
			(isDebugMode ? '<div><strong class="debug_mode_warning">' + worker.GUIp_i18n.debug_mode_warning + '</strong></div>' : '') +
			'<div id="possible_actions" ' + (isDebugMode ? ' class="hidden"' : '') + '>' +
				'<div>' + worker.GUIp_i18n.error_message_text + ' <strong>' + htmlDescription + '</strong>.</div>' +
				'<div>' + worker.GUIp_i18n.possible_actions + '</div>' +
				'<ol>' +
					'<li>' + worker.GUIp_i18n.if_first_time + '<a id="press_here_to_reload">' + worker.GUIp_i18n.press_here_to_reload + '</a>.</li>' +
					'<li>' + worker.GUIp_i18n.if_repeats + '<a id="press_here_to_show_details">' + worker.GUIp_i18n.press_here_to_show_details + '</a>.</li>' +
				'</ol>' +
			'</div>' +
			'<div id="error_details"' + (isDebugMode ? '' : ' class="hidden"') + '>' +
				'<div>' + worker.GUIp_i18n.error_message_subtitle + '</div>' +
					'<div>' + worker.GUIp_i18n.browser + ' <strong>' + worker.GUIp_browser + ' ' + browserVersion + '</strong>.</div>' +
				'<div>' + worker.GUIp_i18n.version + ' <strong>' + ui_data.currentVersion + '</strong>.</div>' +
				'<div>' + worker.GUIp_i18n.error_message_text + ' <strong>' + htmlDescription + '</strong>.</div>' +
				'<div>' + worker.GUIp_i18n.error_message_stack_trace +
					'<div><strong>' + GUIp.common.escapeHTML(stack).replace(/\n/g, '<br />') + '</strong></div>' +
				'</div>' +
			'</div>',
		callback: function() {
			GUIp.common.addListener(document.getElementById('press_here_to_reload'), 'click', location.reload.bind(location));
			GUIp.common.addListener(document.getElementById('press_here_to_show_details'), 'click', function() {
				ui_utils.hideElem(document.getElementById('possible_actions'), true);
				ui_utils.hideElem(document.getElementById('error_details'), false);
				if (!ui_storage.getFlag('helpDialogVisible')) {
					ui_help.toggleDialog();
				}
			});
		}
	});
};

ui_utils.informAboutOldVersion = function() {
	ui_utils.showMessage('update_required', {
		title: worker.GUIp_i18n.error_message_title,
		content: '<div>' + worker.GUIp_i18n.error_message_in_old_version + '</div>'
	});
};

ui_utils.processErrorOnce = function(error) {
	try {
		if (ui_utils.hasShownErrorMessage) {
			GUIp.common.error(error);
			return;
		}
		ui_utils.hasShownErrorMessage = true;
		if (ui_storage.getFlag('Option:enableDebugMode')) {
			ui_utils.processError(error, true);
		} else {
			ui_utils.checkVersion(ui_utils.processError.bind(null, error, false), ui_utils.informAboutOldVersion);
		}
	} catch (e) {
		try {
			GUIp.common.error('got an error while processing an error:', e);
			console.error(error);
		} catch (e1) {
			try {
				console.error("[eGUI+] error: seems that you've intruded into window.GUIp;", e);
				console.error(error);
			} catch (e2) { /* wow, they tried really hard */ }
		}
	}
};

ui_utils.informNewVersionAvailable = function(newVersion) {
	ui_utils.showMessage('update_required', {
		title: worker.GUIp_i18n.new_version_available,
		content: '<div>' + worker.GUIp_i18n.fmt('is_not_last_version', newVersion) + '</div>',
		callback: function() {
			if (!ui_storage.getFlag('helpDialogVisible')) {
				ui_help.toggleDialog();
			}
		}
	});
};

ui_utils.informNoNewVersionAvailable = function() {
	ui_utils.showMessage('update_not_required', {
		title: worker.GUIp_i18n.is_last_version,
		content: '<div>' + worker.GUIp_i18n.is_last_version_desc + '</div>'
	});
};

ui_utils.informVersionCheckFailed = function() {
	ui_utils.showMessage('update_check_failed', {
		title: worker.GUIp_i18n.getting_version_failed,
		content: '<div>' + worker.GUIp_i18n.getting_version_failed_desc + '</div>'
	});
};

ui_utils.showNotification = function(title, text, callback, notid) {
	GUIp.common.notif.show(title, text, parseFloat(ui_storage.get('Option:informerAlertsTimeout')) * 1e3, callback, notid);
};

ui_utils.hideNotification = function(notid) {
	GUIp.common.notif.hide(notid);
};

ui_utils.pmNotificationInit = function() {
	GUIp.common.setTimeout(function() {
		try {
			var lastMsgs = JSON.parse(localStorage['fr_arr' + ui_data.god_name]);
			ui_utils.pmNotificationLastMsg = 0;
			// let's assume 'fr_arr' is always sorted, but we need the newest msgid, which might be put behind all pinned chats
			for (var i = 0, len = lastMsgs.length; i < len; i++) {
				if (lastMsgs[i].msg.id > ui_utils.pmNotificationLastMsg) {
					ui_utils.pmNotificationLastMsg = lastMsgs[i].msg.id;
				}
				if (!lastMsgs[i].p) {
					break;
				}
			}
		} catch (e) {
			ui_utils.pmNotificationLastMsg = 0;
		}
	},5e3);
};

ui_utils.pmNotification = function() {
	var pmSounds = ui_storage.getFlag('Option:enablePmSounds'),
		pmAlerts = ui_storage.getFlag('Option:enablePmAlerts');
	if (!this.pmNotificationLastMsg || !pmAlerts && !pmSounds) {
		return;
	}
	var contacts, maxid = 0,
		playSound = false;
	try {
		contacts = JSON.parse(localStorage['fr_arr' + ui_data.god_name]);
		for (var i = 0, j = 0, len = contacts.length; i < len; i++) {
			var contact = contacts[i].name, msg = contacts[i].msg;
			if (msg && msg.id) {
				if (msg.id > this.pmNotificationLastMsg) {
					if (contact === msg.from && j < 3) {
						if (ui_utils.getCurrentChat() !== contact || !document.hasFocus()) {
							if (pmAlerts && GUIp.common.notif.enabled) {
								var title = '[PM] ' + contact,
									text = msg.msg.slice(0,200) + (msg.msg.length > 200 ? '...' : ''),
									callback = function() {
										if (ui_utils.getCurrentChat() !== this) {
											ui_utils.openChatWith(this);
										}
									}.bind(contact);
								ui_utils.showNotification(title,text,callback,'pm_' + contact);
							}
							playSound = true;
						}
						j++;
					}
					if (msg.id > maxid) {
						maxid = msg.id;
					}
				} else if (!contacts[i].p) { // iterate up to the first old non-pinned chat
					break;
				}
			}
		}
		if (maxid) {
			this.pmNotificationLastMsg = maxid;
		}
		// the 'localStorage.sds' being set means that all sounds are disabled in global godville's settings,
		// and when 'window.isActive' is false the sound is already played
		if (playSound && !localStorage.getItem('sds') && worker.isActive) {
			GUIp.common.playSound('msg');
		}
	} catch (e) {}
};

ui_utils.getCurrentChat = function() {
	if (ui_data.isMobile) {
		return (document.querySelector('#hero_block .mpage:not(#tabbar) .ui-title') || '').textContent || null;
	}
	var docktitle = window.$('.frbutton_pressed .dockfrname_w .dockfrname').text().replace(/\.+/g,''),
		headtitle = window.$('.frbutton_pressed .frMsgBlock .fr_chat_header').text().match(/^(.*?)(?: и е| and h)/);
	if (docktitle && headtitle && headtitle[1].startsWith(docktitle)) {
		return headtitle[1];
	}
	return null;
};

ui_utils.getLastGCM = function() {
	var gc_tab = document.querySelector('.frbutton_pressed .dockfrname');
	if (!gc_tab || !gc_tab.textContent.match(/Гильдсовет|Guild Council/)) {
		return [];
	}
	var meta, message, messages = [], messageLines = gc_tab.parentNode.parentNode.getElementsByClassName('fr_msg_l');
	for (var i = 0, len = messageLines.length; i < len; i++) {
		if (!messageLines[i].firstChild) {
			continue;
		}
		message = {};
		if (messageLines[i].firstChild.nodeType === 3) {
			message.c = messageLines[i].firstChild.textContent
		}
		meta = messageLines[i].getElementsByClassName('gc_fr_el');
		for (var j = 0, len2 = meta.length; j < len2; j++) {
			if (meta[j].classList.contains('gc_fr_god')) {
				message.a = meta[j].textContent;
			} else if (meta[j].title) {
				message.t = meta[j].title;
			}
		}
		if (message.c && message.a && message.t) {
			messages.push(message);
		}
	}
	return messages;
};

ui_utils.saveMobileUnsentMessage = function(node) {
	if (!node) {
		return;
	}
	var textarea = node.getElementsByTagName('textarea'),
		name = node.getElementsByClassName('ui-title');
	// when chat page closes, textarea gets detached from the page but is still reachable via previously saved reference
	if (!textarea.length && ui_improver._openedChatContents) {
		textarea = ui_improver._openedChatContents.getElementsByTagName('textarea');
	}
	// if we have a name and a textarea, we can save typed text
	if (name[0] && name[0].textContent && textarea[0]) {
		if (textarea[0].value) {
			ui_tmpstorage.set('PM:' + name[0].textContent, textarea[0].value);
		} else {
			ui_tmpstorage.remove('PM:' + name[0].textContent);
		}
	}
};

ui_utils.switchTheme = function(theme,override) {
	var currentTheme = localStorage.ui_s,
		stylesheet;
	theme = theme || currentTheme || 'th_classic';
	GUIp.common.exposeThemeName(theme);
	if (ui_storage.get('ui_s') !== theme) {
		ui_storage.set('ui_s',theme);
	}
	if (override && currentTheme !== theme) {
		worker.jQuery.fx.off = theme === 'th_retro';
		stylesheet = document.querySelector('link[href*="' + currentTheme + '.css"]');
		GUIp.common.addCSSFromURL('/stylesheets/' + theme + '.css','guip_theme_override');
		if (stylesheet) {
			stylesheet.parentNode.insertBefore(document.getElementById('guip_theme_override'),stylesheet);
			stylesheet.parentNode.removeChild(stylesheet);
		}
		localStorage.ui_s = theme;
	}
};

ui_utils.loggerWidthChanger = function() {
	var heroBlockWidth = document.getElementById('hero_block').clientWidth;
	document.getElementById('logger').style.width = (ui_data.isMobile ? Math.floor(heroBlockWidth*0.96) : Math.max(919,(heroBlockWidth - 55))) + 'px';
};

ui_utils.parseStat = function(b, type, pos) {
	var a;
	switch (type) {
		case 'text':
			return b && b.textContent || null;
		case 'title':
			return b && b.title || null;
		case 'href':
			return b && b.href || null;
		case 'num':
			return b && parseInt(b.textContent) || 0;
		case 'dec':
			return b && parseFloat(b.textContent) || 0;
		case 'numre':
			if (b && (a = b.textContent.match(/(\d+)/))) {
				return +a[0] || 0;
			}
			return 0;
		case 'slashed':
			if (b && (a = b.textContent.match(/^(.+?) ?\/ ?(.+?)$/))) {
				return a[pos];
			}
			return null;
		case 'pointed':
			if (b && (a = b.textContent.match(/^(.+?), (.+?)( .+?)?$/))) {
				return a[pos];
			}
			return null;
		case 'width':
			return b && b.style.display !== 'none' && parseInt(b.style.width) || 0;
		case 'titlenumre':
			if (b && (a = b.title.match(/(\d+)/))) {
				return +a[0] || 0;
			}
			return 0;
	}
};

ui_utils.getStat = function(selector,type,pos) {
	return ui_utils.parseStat(document.querySelector(selector), type, pos);
};

ui_utils._formatPrc = function(diff, max) {
	var pos = diff >= 0;
	return (pos ? '+' : '\u2212') + ((pos ? 100 : -100) * diff / max).toFixed(1);
};

ui_utils.loggerPrc = function(diff, max) {
	if (max === 0) {
		return '';
	}
	return ' (' + ui_utils._formatPrc(diff, max) + '%)';
};

ui_utils.hpPrc = function(diff, max, sign) {
	if (max === 0) {
		return '';
	}
	return ui_utils._formatPrc(diff, max).slice(sign ? 0 : 1) + '%';
};

ui_utils.loggerPrcMulti = function(damage,callback) {
	var max, prcs = [];
	for (var i = 0, len = damage.parts.length; i < len; i++) {
		max = callback(damage.parts[i]);
		if (max === 0) {
			continue;
		}
		prcs.push(damage.parts[i] + ':' + ui_utils._formatPrc(damage.diff, max) + '%')
	}
	if (prcs.length > 0) {
		return ' (' + prcs.join(', ') + ')';
	}
	return '';
};

ui_utils.generateLightboxSetup = function(type,target,callback) {
	var span = document.createElement('span'),
		target = document.querySelector(target);
	if (!target) return;
	span.id = 'e_' + type + '_setup';
	span.className = 'em_font e_t_icon t_icon';
	span.textContent = '⚙';
	span.title = worker.GUIp_i18n['lb_' + type + '_title'];
	GUIp.common.addListener(span, 'click', GUIp.common.createLightbox.bind(null,type,ui_storage,worker.GUIp_words(),callback));
	target.appendChild(span);
};

ui_utils.capitalizeFirstLetter = function(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
};

ui_utils.decapitalizeFirstLetter = function(str) {
	return str.charAt(0).toLowerCase() + str.slice(1);
};

ui_utils.updateBroadcastLink = function(target) {
	if (target) {
		target.href = '/duels/log/' + ui_data.logId + ui_utils.generateBroadcastLinkXtra();
	}
};

ui_utils.generateBroadcastLinkXtra = function() {
	var corrs, items, xhref = [];
	if ((corrs = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dlMoves'))) && Object.keys(corrs).length) {
		items = [];
		Object.keys(corrs).forEach(function(a) {
			items.push(a+':'+corrs[a]);
		});
		xhref.push('edm=' + items.join(','));
	}
	if ((corrs = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':whMoves'))) && Object.keys(corrs).length) {
		items = [];
		Object.keys(corrs).forEach(function(a) {
			items.push(a+':'+corrs[a][1]+':'+-corrs[a][0]);
		});
		xhref.push('ewh=' + items.join(','));
	}
	ui_improver.dungeonExtras = Object.keys(ui_improver.dungeonExtras).length ? ui_improver.dungeonExtras: JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':extras') || '{}');
	if (ui_improver.dungeonExtras.nookOffset && ui_improver.dungeonExtras.nookOffset.x !== null) {
		xhref.push('eno=' + ui_improver.dungeonExtras.nookOffset.x + ':' + -ui_improver.dungeonExtras.nookOffset.y);
	}
	if (ui_improver.dungeonExtras.stairsOffset && ui_improver.dungeonExtras.stairsOffset.x !== null) {
		xhref.push('eso=' + ui_improver.dungeonExtras.stairsOffset.x + ':' + -ui_improver.dungeonExtras.stairsOffset.y);
	}
	return xhref.length ? '?' + xhref.join('&') : '';
};

ui_utils.setTitle = function(text) {
	document.querySelector('title').childNodes[0].nodeValue = text;
};

ui_utils.jqueryExtInit = function() {
	if (!worker.jQuery) {
		return;
	}
	(function($) {
		// workaround for preventing unnecessary calls to glow() since godville devs are ignoring this issue
		if (typeof $.fn.glow === "function" && ui_storage.getFlag('Option:enableGlowfix')) {
			var shouldGlow = function() {
				// block glowing for the side job duration time
				var node;
				if (this.classList.contains('l_val') &&
					(node = document.getElementById('hk_quests_completed')) && this.parentNode === node.nextSibling) {
					return false;
				}
				var $this = $(this);
				// block extra glowing only for health labels
				if ($this.closest('.opp_h').length || this.classList.contains('hp_diff')) {
					// for hp_diff block only when text matches and step number is the same
					if ($this.data('e_text') === this.textContent && (!this.classList.contains('hp_diff') || $this.data('e_step') === ui_stats.currentStep())) {
						return false;
					}
					$this.data('e_step', ui_stats.currentStep());
					$this.data('e_text', this.textContent);
				}
				return true;
			};
			$.fn.e_glow = $.fn.glow;
			$.fn.glow = function() {
				$.fn.e_glow.apply(this.filter(shouldGlow), arguments);
			};
		}
		// workaround for issue with closing chats when selecting texts and releasing mouse button outside of chat blocks, notably in Chrome-based browsers.
		// this happens due to the fact that target of click event in Chrome is defined when button was released and godville code assumes it's defined when
		// button was pressed. thus chats can be accidentally closed way too easy. this was reported to godville devs but they ignored it as they usually do.
		if (worker.GUIp_browser === 'Chrome' && ui_storage.getFlag('Option:enableChromeChatfix')) {
			try {
				var eClickInsideChats = false;
				$(document).bind('mousedown', function(e) {
					if ($(e.target).parents('.msgDockWrapper').length || $(e.target).parents('.fr_msg_l').length) {
						eClickInsideChats = true;
					}
				});
				$(document).bind('mouseup', function(e) {
					setTimeout(function() { eClickInsideChats = false },50);
				});
				$(document).bind('click', function(e) {
					if (eClickInsideChats && !$(e.target).parents('.msgDockWrapper').length && !$(e.target).parents('.fr_msg_l').length) {
						e.stopImmediatePropagation();
					}
					eClickInsideChats = false;
				});
				if ($._data(document,'events') && $._data(document,'events').click) {
					$._data(document,'events').click.unshift($._data(document,'events').click.pop());
				}
			} catch (e) { console.log(e); }
		}
	})(worker.jQuery);
};
