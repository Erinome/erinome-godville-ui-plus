// ui_improver
var ui_improver = worker.GUIp.improver = {};

ui_improver.isFirstTime = true;
ui_improver.clockToggling = false;
ui_improver.clock = null;
ui_improver.wantedItems = null;
ui_improver.wantedMonsterRewards = null;
ui_improver.dailyForecast = null;
ui_improver.dailyForecastText = null;
ui_improver.dailyForecastSpecBoss = null;
ui_improver.bingoTries = null;
ui_improver.bingoItems = null;
ui_improver.improvementDebounce = null;
// dungeon & sailing
ui_improver.chronicles = {};
ui_improver.directionlessMoves = null;
ui_improver.wormholeMoves = null;
ui_improver.dungeonGuidedSteps = null;
ui_improver.allsHP = {a:[], e:[], sum: 0};
ui_improver.corrections = {n: 'north', e: 'east', s: 'south', w: 'west'};
ui_improver.dungeonXHRCount = 0;
ui_improver.dungeonExtras = {};
ui_improver.needLog = true;
// resresher
ui_improver.softRefreshInt = 0;
ui_improver.hardRefreshInt = 0;

ui_improver.init = function() {
	ui_improver.improvementDebounce = GUIp.common.debounce(250, function improvementDebounce() {
		ui_improver.improve();
		if (ui_data.isFight) {
			ui_logger.update();
		}
	});
};
ui_improver.softRefresh = function() {
	GUIp.common.info('soft reloading...');
	document.getElementById('d_refresh') && document.getElementById('d_refresh').click();
};
ui_improver.hardRefresh = function() {
	GUIp.common.warn('hard reloading...');
	location.reload();
};
ui_improver.improve = function() {
	this.improveInProcess = true;

	if (this.isFirstTime) {
		if (GUIp_browser === 'Opera') {
			document.body.classList.add('e_opera');
		}
		if (!ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail) {
			ui_improver.improveDiary();
			ui_improver.distanceInformerInit();
			ui_improver.improveShop();
		}
		if (ui_data.isDungeon) {
			ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content, .e_m_fight_log'));
			var heroNames = ui_stats.Hero_Ally_Names();
			heroNames.push(ui_data.char_name);
			GUIp.common.setExtraDiscardData(heroNames);
			this.dungeonExtras = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':extras') || '{}'); // even after this we need to reparse again in case we missed something
			if (!ui_data.isMobile) {
				this.parseDungeonExtras(document.querySelectorAll('#m_fight_log .d_imp .d_msg'), document.getElementById('map'), ui_stats.currentStep());
			}
			GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
			GUIp.common.dmapExclCache = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':excl') || '{}');
			GUIp.common.dmapDisabledPointersCache = JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dptr') || '[]');
		}
		if (ui_data.isSail) {
			var mapSettings = ui_storage.getList('Option:islandsMapSettings');
			if (mapSettings.includes('widen')) {
				ui_improver.sailPageResize = true;
				ui_improver.whenWindowResize();
			}
			ui_improver.initSailing();
		}
		if (ui_data.isMining) {
			ui_mining.processMap();
		}
		if (ui_data.isFight) {
			GUIp.common.cleanupLogStorage();
			GUIp.common.setTimeout(ui_improver.improvePlayers, 250);
			ui_improver.addLastFightLink();
		}
		ui_improver.initTouchDragging();
	}
	ui_improver.improveStats();
	ui_improver.improveVoiceDialog();
	if (!ui_data.isFight) {
		ui_improver.improveNews();
		ui_improver.improveEquip();
		ui_improver.improveSkills();
		ui_improver.improvePet();
		ui_improver.improveSendButtons();
		ui_improver.detectors.detectField();
		if (ui_data.hasShop) {
			ui_improver.detectors.detectLS();
		}
		if (ui_data.isMobile) {
			// this is mostly for hp in #statusbar
			ui_improver.improveHP();
		}
	} else {
		if (ui_improver.isFirstTime) {
			ui_logger.update();
		} else {
			// Godville makes a tiny delay before assigning inline styles to players. we have to wait
			// until they're completed.
			ui_improver.improvePlayers();
		}
		ui_improver.improveHP();
		ui_improver.improveColumnWithMap();
	}
	if (ui_data.isDungeon) {
		ui_improver.improveDungeon();
	}
	ui_improver.improveInterface();
	ui_improver.improveChat();
	ui_improver.calculateButtonsVisibility();
	this.isFirstTime = false;
	this.improveInProcess = false;

	ui_informer.update('fight', ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail);
	ui_informer.update('arena available', ui_stats.isArenaAvailable());
	ui_informer.update('dungeon available', ui_stats.isDungeonAvailable());
	ui_informer.update('sail available', ui_stats.isSailAvailable());

	ui_informer.updateCustomInformers();
};
ui_improver.improveVoiceDialog = function() {
	var map;
	// If playing in pure ZPG mode there won't be present voice input block at all;
	if (!document.getElementById('ve_wrap')) {
		return;
	}
	// Add voicegens and show timeout bar after saying
	if (this.isFirstTime) {
		this.freezeVoiceButton = ui_storage.createVar('Option:freezeVoiceButton', function(text) { return text || ''; });
		ui_utils.updateVoiceSubmitState();

		var voiceSubmit = document.getElementById('voice_submit');
		document.getElementById('ve_wrap').insertAdjacentHTML('afterbegin',
			'<div id="clear_voice_input" class="div_link_nu gvl_popover hidden" title="' + worker.GUIp_i18n.clear_voice_input + '">×</div>'
		);
		GUIp.common.addListener(document.getElementById('clear_voice_input'), 'click', function() {
			ui_utils.setVoice('');
		});
		worker.$(document).on('change keypress paste focus textInput input', '#godvoice, #god_phrase', function() {
			GUIp.common.try2(function(target) {
				ui_utils.updateVoiceSubmitState();
				ui_utils.hideElem(document.getElementById('clear_voice_input'), !target.value);
			}, this);
		});
		GUIp.common.addListener(document.getElementById('ve_wrap'), 'keypress', function(e) {
			if (e.which !== 13) return;
			if (voiceSubmit.disabled) {
				e.preventDefault();
				e.stopPropagation();
			}
		}, true);
		GUIp.common.addListener(voiceSubmit, 'click', function() {
			voiceSubmit.blur();
		});
		// prevent Godville from re-enabling the button when we want to keep it disabled
		GUIp.common.newMutationObserver(ui_utils.updateVoiceSubmitState.bind(ui_utils))
			.observe(voiceSubmit, {attributes: true, attributeFilter: ['disabled']});

		if (!ui_utils.isAlreadyImproved(document.getElementById('cntrl'))) {
			var gp_label = document.getElementsByClassName('gp_label')[0];
			gp_label.classList.add('l_capt');
			document.getElementsByClassName('gp_val')[0].classList.add('l_val');
			if (ui_words.base.phrases.mnemonics.length) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.mnemo_button, 'mnemonics', worker.GUIp_i18n.mnemo_title);
			}
			if (ui_data.isDungeon) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.upstairs, 'go_upstairs', worker.GUIp_i18n.fmt('ask_to_go_upstairs', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.downstairs, 'go_downstairs', worker.GUIp_i18n.fmt('ask_to_go_downstairs', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.east, 'go_east', worker.GUIp_i18n.fmt('ask_to_go_east', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.west, 'go_west', worker.GUIp_i18n.fmt('ask_to_go_west', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.south, 'go_south', worker.GUIp_i18n.fmt('ask_to_go_south', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.north, 'go_north', worker.GUIp_i18n.fmt('ask_to_go_north', ui_data.char_sex[0]));
				if ((map = document.getElementById('map'))) {
					if (GUIp.common.isAndroid) {
						GUIp.common.addListener(map, 'click', ui_utils.mapVoicegen);
					}
					GUIp.common.tooltips.watchSubtree(map.getElementsByClassName('block_content')[0]);
				}
			} else if (!ui_data.isSail) {
				if (ui_data.isFight) {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.defend, 'defend', worker.GUIp_i18n.fmt('ask_to_defend', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
				} else {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.sacrifice, 'sacrifice', worker.GUIp_i18n.fmt('ask_to_sacrifice', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
				}
			}
		}
	}
	//hide_charge_button
	var charge_button = document.querySelector('#cntrl .hch_link');
	if (charge_button) {
		charge_button.style.visibility = ui_storage.getFlag('Option:hideChargeButton') ? 'hidden' : '';
	}
	ui_informer.update('full godpower', ui_stats.Godpower() === ui_stats.Max_Godpower() && !ui_data.isFight);
};
ui_improver._formatPetFeature = function(feature) {
	return GUIp_i18n['pet_feature_' + feature];
};
ui_improver._formatMonsterTitle = function(monster, isTamable) {
	var result = ui_improver.wantedMonsterRewards.get()[monster] || '',
		pet;
	if (isTamable && (pet = ui_words.base.pets[monster])) {
		if (result) result += '\n\n';
		result += GUIp_i18n.fmt('tamable_monster', Array.from(pet.features, ui_improver._formatPetFeature).join(', '));
	}
	return result;
};
ui_improver._improveMonsterNameHTML = function(html, putPetLink) {
	// must not be called twice
	return html.replace(/\uD83E\uDDB4 /g, // bone
		putPetLink ? (
			// we use `white-space: nowrap` instead of `&nbsp;` since the latter might cause unexpected results
			// in custom informers (`gv.lastDiary`, etc.) and user scripts
			'<span class="e_nowrap"><a class="div_link_nu e_emoji e_emoji_bone eguip_font" href="' +
				GUIp_i18n.wiki_pets_table +
			'" title="' + GUIp_i18n.open_wiki_pets_table +
			'" target="_blank">\uD83E\uDDB4</a> </span>'
		) : '<span class="e_nowrap"><span class="e_emoji e_emoji_bone eguip_font">\uD83E\uDDB4</span> </span>'
	).replace(/☠ /g,
		'<span class="e_nowrap"><span class="e_emoji e_emoji_skull' +
			(GUIp.common.renderTester.testChar('☠') ? '' : ' eguip_font') +
		'">☠</span> </span>'
	);
};
ui_improver.improveMonsterName = function() {
	var div = document.querySelector('#news_pb .line .l_val'),
		title = '';
	if (div && !div.getElementsByClassName('improved')[0]) {
		title = div.textContent;
		if ((title = ui_improver._formatMonsterTitle(
			ui_stats.canonicalMonsterName(title).toLowerCase(), title.includes('\uD83E\uDDB4') // bone
		))) {
			GUIp.common.tooltips.watchSubtree(div);
		}
		div.innerHTML = '<span class="improved' + (title && '" title="' + title) + '">' +
			ui_improver._improveMonsterNameHTML(div.innerHTML, true) +
		'</span>';
	}
	div = document.getElementsByClassName('f_news')[0];
	if (div && !div.getElementsByClassName('improved')[0]) {
		div.innerHTML = '<span class="improved">' +
			ui_improver._improveMonsterNameHTML(div.innerHTML, false) +
		'</span>';
	}
};
ui_improver.improveNews = function() {
	var node, news = document.getElementById('news_pb');
	if (!ui_utils.isAlreadyImproved(news)) {
		ui_utils.addVoicegen(news.getElementsByClassName('l_capt')[0], worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
		Array.prototype.forEach.call(news.getElementsByClassName('p_bar'), GUIp.common.tooltips.watchSubtree);
		// make a way to easier refer to news block title
		// and add an observer for timely reparsing its contents
		if (ui_data.isMobile) {
			node = news.previousElementSibling;
		} else {
			node = document.querySelector('#news .block_title');
		}
		if (node && node.textContent) {
			var parseState = function() {
				var state, text = node.textContent.split(/[. ]/)[0];
				if (['Бой','Fighting'].includes(text)) {
					state = 'battling';
				} else if (['Дорога','Questing'].includes(text)) {
					state = 'walking';
				} else if (['Возврат','Returning'].includes(text)) {
					state = 'returning';
				} else if (['Лечение','Healing'].includes(text)) {
					state = 'healing';
				} else if (['Торговля','Trading'].includes(text)) {
					state = 'trading';
				} else if (['Отдых','Partying'].includes(text)) {
					state = 'partying';
				} else if (['Сон','Sleeping'].includes(text)) {
					state = 'sleeping';
				} else if (['Молитва','Praying'].includes(text)) {
					state = 'praying';
				} else if (['Рыбалка','Fishing'].includes(text)) {
					state = 'fishing';
				} else if (['Авантюра','Waiting'].includes(text)) {
					state = 'preAdventure';
				} else if (['Геройство','Бытиё','Adventuring','Being'].includes(text)) {
					state = 'postAdventure';
				} else if (['Спекуляция','Shopkeeping'].includes(text)) {
					state = 'profiteering';
				} else if (['Смерть','Decomposing'].includes(text)) {
					state = 'dying';
				} else {
					state = 'unparsed';
				}
				node.dataset.state = state;
			};
			GUIp.common.newMutationObserver(parseState).observe(node, {childList: true});
			parseState();
			node.classList.add('e_hero_state');
		}
	}
	ui_improver.improveMonsterName();
	ui_informer.update('wanted monster', ui_stats.wantedMonster());
	ui_informer.update('special monster', ui_stats.specialMonster());
	ui_informer.update('tamable monster', ui_stats.tamableMonster());
	ui_informer.update('chosen monster', ui_stats.chosenMonster());
};
ui_improver.improveColumnWithMap = function() {
	if (ui_storage.getFlag('Option:disableMapColumnImprover')) {
		return;
	}
	var oldColumn = document.getElementsByClassName('e_map_column')[0],
		node = document.getElementById('s_map') || document.getElementById('r_map') || document.getElementById('map'),
		pageClasses;
	for (; node; node = node.parentElement) {
		if (node.classList.contains('group_wrapper')) {
			if (node === oldColumn) return; // no changes
			node.classList.add('e_map_column');
			pageClasses = document.getElementById('main_wrapper').classList;
			if (node.classList.contains('c_col')) {
				pageClasses.add('e_map_in_c_col');
				pageClasses.remove('e_map_in_s_col');
			} else {
				pageClasses.add('e_map_in_s_col');
				pageClasses.remove('e_map_in_c_col');
			}
			break;
		}
	}
	if (oldColumn) {
		oldColumn.classList.remove('e_map_column');
	}
};
ui_improver.improveDungeon = function() {
	if (this.isFirstTime) {
		ui_storage.addListener('Option:dungeonMapSettings', function(newValue) {
			ui_utils.hideElem(document.querySelector('.dmapDimensions'), !(newValue || '').includes('dims'));
		});
	}
	this.updateDungeonVoicegen(); // todo: place this into dungeon colorization? also run this from here but only at first time?
};
ui_improver.updateDungeonVoicegen = function() {
	var map = ui_data.isMobile ? '.e_m_dmap' : '#map';
	if (document.querySelectorAll(map + ' .dml').length) {
		var i, j, chronolen = +worker.Object.keys(this.chronicles).reverse()[0],
			$box = worker.$('#cntrl .voice_generator'),
			$boxML = worker.$(map + ' .dml'),
			$boxMC = worker.$(map + ' .dmc'),
			kRow = $boxML.length,
			kColumn = $boxML[0].textContent.length,
			isJumping = this.checkParsedDungeonType('jumping');
		if (!$box.length) {
			return;
		}
		for (i = 0; i < 6; i++) {
			if (i < 4) {
				$box[i].style.visibility = 'hidden';
			} else {
				$box[i].style.display = 'none';
			}
		}
		var isCellar = GUIp.common.isDungeonCellar();
		for (var si = 0; si < kRow; si++) {
			for (var sj = 0; sj < kColumn; sj++) {
				if ($boxMC[si * kColumn + sj].textContent !== '@') {
					continue;
				}
				var isMoveLoss = [];
				for (i = 0; i < 4; i++) {
					isMoveLoss[i] = this.chronicles[chronolen - i] && this.chronicles[chronolen - i].marks.includes('trapMoveLoss');
				}
				var directionsShouldBeShown = !isMoveLoss[0] || (isMoveLoss[1] && (!isMoveLoss[2] || isMoveLoss[3]));
				if (directionsShouldBeShown) {
					if ($boxMC[(si - 1) * kColumn + sj].textContent !== '#' || isJumping && (si === 1 || si !== 1 && $boxMC[(si - 2) * kColumn + sj].textContent !== '#')) {
						$box[0].style.visibility = ''; // north
					}
					if ($boxMC[(si + 1) *kColumn + sj].textContent !== '#' || isJumping && (si === kRow - 2 || si !== kRow - 2 && $boxMC[(si + 2) *kColumn + sj].textContent !== '#')) {
						$box[1].style.visibility = ''; // south
					}
					if ($boxMC[si * kColumn + sj - 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj - 2].textContent !== '#') {
						$box[2].style.visibility = ''; // west
					}
					if ($boxMC[si * kColumn + sj + 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj + 2].textContent !== '#') {
						$box[3].style.visibility = ''; // east
					}
					if (this.chronicles[chronolen] && (this.chronicles[chronolen].marks.includes('staircaseHint') || this.chronicles[chronolen].marks.includes('staircase')) &&
						(ui_data.availableGameModes.includes('mining') || ui_data.availableGameModes.includes('souls'))) {
						if (!isCellar) {
							$box[4].style.display = ''; // downstairs
						} else {
							$box[5].style.display = ''; // upstairs
						}
					}
					if (isJumping && ui_storage.getFlag('Option:jumpingOverrideDirections')) {
						for (i = 0; i < 4; i++) {
							$box[i].style.visibility = '';
						}
					}
				}
			}
		}
	}
};
ui_improver._toFixedPoint = function(m0) {
	return (+m0).toFixed(1);
};
ui_improver.improveStats = function() {
	var i, brNode, handler;
	if ((brNode = document.querySelector('#hk_bricks_cnt .l_val'))) {
		brNode.textContent = brNode.textContent.replace(/[\d.]+/, this._toFixedPoint);
	}
	if (ui_improver.isFirstTime) {
		Array.prototype.forEach.call(
			document.querySelectorAll(ui_data.isMobile ? '.e_mt_stats ~ .line .p_bar' : '#stats .p_bar, #o_hl1 .p_bar, #m_info .p_bar, #b_info .p_bar'),
			GUIp.common.tooltips.watchSubtree
		);
	}
	if (ui_data.isDungeon) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Dungeon');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Exp', ui_stats.Exp());
			ui_storage.set('Logger:Map_Level', ui_stats.Level());
			ui_storage.set('Logger:Map_Gold', ui_storage.get('Logger:Gold'));
			ui_storage.set('Logger:Map_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Alls_HP', ui_stats.Map_Alls_HP());
			for (i = 1; i <= 5; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('dungeon'));
		return;
	}
	if (ui_data.isSail) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Sail');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Supplies',ui_stats.Map_Supplies());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_Name', '');
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('sail'));
		return;
	}
	if (ui_data.isMining) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Mining');
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Bits',ui_stats.Bits());
			ui_storage.set('Logger:Bytes',ui_stats.Bytes());
			ui_storage.set('Logger:Push_Readiness',ui_stats.Push_Readiness());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
		}
		return;
	}
	if (ui_data.isFight) {
		if (this.isFirstTime) {
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
			ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Hero_Charges', ui_stats.Charges());
			ui_storage.set('Logger:Enemy_HP', ui_stats.Enemy_HP());
			ui_storage.set('Logger:Enemy_Gold', ui_stats.Enemy_Gold());
			ui_storage.set('Logger:Enemy_Inv', ui_stats.Enemy_Inv());
			ui_storage.set('Logger:Hero_Alls_HP', ui_stats.Hero_Alls_HP());
			for (i = 1; i <= 11; i++) {
				ui_storage.set('Logger:Hero_Ally'+i+'_HP', ui_stats.Hero_Ally_HP(i));
			}
			for (i = 1; i <= 6; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
			ui_storage.set('Logger:Enemy_AliveCount', ui_stats.Enemy_AliveCount());
		}
		ui_informer.update('low health', ui_stats.lowHealth('fight'));
		return;
	}
	if (ui_data.hasShop) {
		if (ui_stats.inShop()) {
			if (ui_storage.get('Logger:Location') === 'Field') {
				ui_storage.set('Logger:Location', 'Store');
				ui_logger.suppressOldStats();
				ui_logger.update(ui_logger.fieldWatchers);
				ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
				ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			}
			ui_data.inShop = true;
			document.body.classList.add('in_own_shop');
		} else if (ui_data.inShop) {
			ui_logger.update(ui_logger.shopWatchers);
			ui_data.inShop = false;
			document.body.classList.remove('in_own_shop');
		}
	}
	if (!ui_data.inShop && ui_storage.get('Logger:Location') !== 'Field') {
		ui_storage.set('Logger:Location', 'Field');
	}
	if (!ui_utils.isAlreadyImproved(ui_data.isMobile ? document.getElementsByClassName('e_mt_stats')[0] : document.getElementById('stats'))) {
		// Add voicegens
		ui_utils.addVoicegen(document.querySelector('#hk_level .l_capt'), worker.GUIp_i18n.study, 'exp', worker.GUIp_i18n.fmt('ask_to_study', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_health .l_capt'), worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_gold_we .l_capt'), worker.GUIp_i18n.dig, 'dig', worker.GUIp_i18n.fmt('ask_to_dig', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.cancel_task, 'cancel_task', worker.GUIp_i18n.fmt('ask_to_cancel_task', ui_data.char_sex[0]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.do_task, 'do_task', worker.GUIp_i18n.fmt('ask_to_do_task', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_death_count .l_capt'), worker.GUIp_i18n.die, 'die', worker.GUIp_i18n.fmt('ask_to_die', ui_data.char_sex[0]));
		handler = ui_improver.calculateButtonsVisibility.bind(ui_improver, true);
		ui_storage.addListener('Option:disableDieButton', handler);
		ui_storage.addListener('Option:disableVoiceGenerators', handler);
	}
	if (!document.querySelector('#hk_distance .voice_generator')) {
		ui_utils.addVoicegen(document.querySelector('#hk_distance .l_capt'), (!ui_data.isMobile && document.querySelector('#main_wrapper.page_wrapper_5c')) ? '回' : worker.GUIp_i18n.return, 'town', worker.GUIp_i18n.fmt('ask_to_return', ui_data.char_sex[0]));
	}

	ui_informer.update('much gold', ui_stats.Gold() >= (ui_stats.hasTemple() ? 10000 : 3000));
	ui_informer.update('dead', ui_stats.HP() === 0);
	var questName = ui_stats.Task_Name();
	ui_informer.update('guild quest', questName.match(/членом гильдии|member of the guild/) && !questName.match(/\((отменено|cancelled)\)/));
	ui_informer.update('mini quest', questName.match(/\((мини|mini)\)/) && !questName.match(/\((отменено|cancelled)\)/));

	var townInformer = false;
	if (this.informTown === ui_stats.townName() || !isNaN(+this.informTown) && Math.abs(ui_stats.mileStones() - +this.informTown) >= 50) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	} else if (!ui_stats.isGoingBack() && ui_stats.townName() === '' && this.informTown === ui_stats.nearbyTown() || !isNaN(+this.informTown) && ui_stats.mileStones() >= +this.informTown) {
		townInformer = true;
	}
	ui_informer.update('selected town', townInformer);
	GUIp.common.tooltips.watchSubtree(document.querySelector('#hk_distance .l_val'));

	// shovel imaging
	if (!ui_data.isMobile) {
		var digVoice = document.querySelector('#hk_gold_we .voice_generator');
		if (this.isFirstTime) {
			if (worker.GUIp_browser !== 'Opera') {
				digVoice.style.backgroundImage = 'url(' + worker.GUIp_getResource('images/shovel.png') + ')';
			} else {
				worker.GUIp_getResource('images/shovel.png',digVoice);
			}
		}
		if (ui_stats.goldTextLength() > 16 - 2*document.getElementsByClassName('page_wrapper_5c').length) {
			digVoice.classList.add('shovel');
			digVoice.classList.toggle('compact',
				ui_stats.goldTextLength() > 20 - 3*document.getElementsByClassName('page_wrapper_5c').length
			);
		} else {
			digVoice.classList.remove('shovel');
		}
	}
	// improve title of side quest bar with exact values
	var sjName = ui_stats.Side_Job_Name();
	if (sjName) {
		var sjMlt, sjCur = ui_stats.Side_Job(),
			sjReq = ui_stats.Side_Job_Requirements(sjName),
			sjBar = document.querySelector('#hk_quests_completed + .line .p_bar');
		if (sjBar && sjReq > 0) {
			if (sjReq > 100 && sjCur !== 100) {
				sjMlt = Math.pow(10, Math.floor(Math.log10(Math.max(100, sjReq - 5000))) - 1);
				sjCur = '\u2248' + (Math.round(sjCur / 100 * sjReq / sjMlt) * sjMlt);  /* approximately equal */
			} else {
				sjCur = Math.round(sjCur / 100 * sjReq);
			}
			sjBar.title = sjBar.title.replace(/( \(.?\d+\/\d+\))/,'') + ' (' + sjCur + '/' + sjReq + ')';
		}
	}
};
ui_improver.addLastFightLink = function() {
	var container = document.querySelector('#hk_arena_won .l_val'),
		a, child;
	if (!container || container.classList.contains('div_link') || container.getElementsByTagName('a')[0]) {
		return;
	}
	a = document.createElement('a');
	a.href = '/hero/last_fight';
	a.target = '_blank';
	a.style.display = 'block';
	while ((child = container.firstChild)) {
		a.appendChild(child);
	}
	container.appendChild(a);
};
ui_improver.improveShop = function() {
	var trdNode = ui_data.isMobile ? document.getElementsByClassName('e_m_shop')[0] : document.getElementById('trader'), node;
	if (trdNode && !ui_utils.isAlreadyImproved(trdNode)) {
		if (ui_data.isMobile) {
			node = trdNode.previousElementSibling;
		} else {
			node = trdNode.getElementsByClassName('l_slot')[0];
		}
		if (node) {
			node.insertAdjacentHTML('afterbegin', '<div id="trademode_timer" class="fr_new_badge ' + (ui_data.isMobile ? 'e_badge_pos ' : '') + 'hidden"></div>');
			GUIp.common.tooltips.watchSubtree(node.firstChild);
		}
		GUIp.common.tooltips.watchSubtree(trdNode.getElementsByClassName('p_bar')[0]);
	}
};
ui_improver.improvePet = function(forcedUpdate) {
	var timeRemaining, node,
		petNode = ui_data.isMobile ? document.getElementsByClassName('e_m_pet')[0] : document.getElementById('pet'),
		petBadge = document.getElementById('pet_badge'),
		petLevelLabel = document.querySelector('#hk_pet_class + div .l_val'),
		level = ui_stats.Pet_Level(),
		petRes  = (level * 0.4 + 0.5).toFixed(1) + 'k',
		petRes2 = (level * 0.8 + 0.5).toFixed(1) + 'k';
	if (ui_stats.petIsKnockedOut()) {
		if (!ui_utils.isAlreadyImproved(petNode)) {
			if (ui_data.isMobile) {
				node = petNode.previousElementSibling;
			} else {
				node = petNode.getElementsByClassName('r_slot')[0];
			}
			if (node) {
				node.insertAdjacentHTML('afterbegin', '<div id="pet_badge" class="fr_new_badge e_badge_pos hidden">0</div>');
				petBadge = node.firstChild;
				GUIp.common.tooltips.watchSubtree(petBadge);
			}
		}
		if (ui_data.isMobile || document.querySelector('#pet .block_content').style.display !== 'none') {
			petBadge.title = worker.GUIp_i18n.badge_pet2;
			petBadge.textContent = petRes;
		} else {
			timeRemaining = ui_utils.findLabel(worker.$('#pet'), worker.GUIp_i18n.pet_status_label).siblings('.l_val').text().match(/(?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/) || '';
			petBadge.title = worker.GUIp_i18n.badge_pet1;
			petBadge.textContent = ('00' + (timeRemaining[1] || '')).substr(-2) + ':' + ('00' + (timeRemaining[2] || '')).substr(-2)
		}
		ui_utils.hideElem(petBadge, false);
	} else if (petBadge) {
		ui_utils.hideElem(petBadge, true);
	}
	if (petLevelLabel) {
		petLevelLabel.title = '↑ ' + petRes + (worker.GUIp_locale === 'ru' ? '\n⇈ ' + petRes2 : '');
		GUIp.common.tooltips.watchSubtree(petLevelLabel);
	}
	if (!ui_data.isMobile) {
		if (this.isFirstTime || forcedUpdate) {
			var node2, node3, buttonInPetBlock, relocatePetsButton = ui_storage.getFlag('Option:relocatePetsButton');
			buttonInPetBlock = document.querySelector('#pet #pcmd');
			if (relocatePetsButton && !buttonInPetBlock) {
				node = document.querySelector('#pet .block_content');
				node2 = document.getElementById('pcmd');
			} else if (!relocatePetsButton && buttonInPetBlock) {
				node = document.querySelector('#ark .block_content');
				node2 = buttonInPetBlock;
			}
			if (node && node2) {
				node3 = node2;
				while (node3 = node3.previousElementSibling) {
					if (/^(Pets|Питомцы)/.test(node3.textContent)) {
						node.insertAdjacentElement('beforeend', node3);
						break;
					}
				}
				node.insertAdjacentElement('beforeend', node2);
			}
		}
		if (this.isFirstTime) {
			ui_storage.addListener('Option:relocatePetsButton', ui_improver.improvePet.bind(ui_improver, true));
		}
	}
	// knock out informer
	ui_informer.update('pet knocked out', ui_stats.petIsKnockedOut());
};
ui_improver.describeEquipBoldness = function(mutations, observer) {
	var names = document.querySelectorAll(ui_data.isMobile ? '.e_m_equipment .eq_name' : '#equipment .eq_name'),
		name, eq;
	for (var i = 0, len = names.length; i < len; i++) {
		name = names[i];
		eq = name.parentNode;
		if (name.classList.contains('eq_b')) {
			eq.classList.add('e_eq_bold');
			name.title = GUIp_i18n.equip_boldness[i] ? eq.firstElementChild.textContent + ': ' + GUIp_i18n.equip_boldness[i] : '';
		} else {
			eq.classList.remove('e_eq_bold');
			name.title = '';
		}
	}
	observer.takeRecords();
};
ui_improver.improveEquip = function() {
	var equipNode = ui_data.isMobile ? document.getElementsByClassName('e_m_equipment')[0] : document.getElementById('equipment'), node, observer;
	if (!equipNode) return;
	if (!ui_utils.isAlreadyImproved(equipNode)) {
		if (ui_data.isMobile) {
			node = equipNode.previousElementSibling;
		} else {
			node = equipNode.getElementsByClassName('r_slot')[0];
		}
		if (node) {
			node.insertAdjacentHTML('afterbegin', '<div id="equip_badge" class="fr_new_badge e_badge_pos">0</div>');
			GUIp.common.tooltips.watchSubtree(node.firstChild);
		}
		observer = GUIp.common.newMutationObserver(ui_improver.describeEquipBoldness);
		observer.observe(equipNode, {subtree: true, attributes: true, attributeFilter: ['class']});
		ui_improver.describeEquipBoldness(null, observer);
		Array.prototype.forEach.call(equipNode.getElementsByClassName('eq_name'), GUIp.common.tooltips.watchSubtree);
	}
	var equipBadge = document.getElementById('equip_badge'),
		averageEquipLevel = 0;
	for (var i = 1; i <= 7; i++) {
		averageEquipLevel += ui_stats['Equip' + i]();
	}
	averageEquipLevel = averageEquipLevel / 7 - ui_stats.Level();
	averageEquipLevel = (averageEquipLevel >= 0 ? '+' : '') + averageEquipLevel.toFixed(1);
	if (equipBadge.textContent !== averageEquipLevel) {
		equipBadge.title = worker.GUIp_i18n.badge_equip;
		equipBadge.textContent = averageEquipLevel;
	}
};
ui_improver.improveSkills = function() {
	var skillsNode = ui_data.isMobile ? document.getElementById('s_b_id') : document.getElementById('skills'), node;
	if (!skillsNode) return;
	if (!ui_utils.isAlreadyImproved(skillsNode)) {
		if (ui_data.isMobile) {
			// weird but skills div has its own native id, but between it and its header there's an empty nameless div. why?
			node = document.getElementsByClassName('e_mt_skills')[0];
		} else {
			node = skillsNode.getElementsByClassName('r_slot')[0];
		}
		if (node) {
			node.insertAdjacentHTML('afterbegin', '<div id="skill_badge" class="fr_new_badge e_badge_pos"></div>');
			GUIp.common.tooltips.watchSubtree(node.firstChild);
		}
		Array.prototype.forEach.call(skillsNode.getElementsByClassName('skill_info'), GUIp.common.tooltips.watchSubtree);
	}
	var skillBadge = document.getElementById('skill_badge'),
		skillList = skillsNode.getElementsByClassName('skill_info'),
		minSkillLevel = Infinity,
		minSkillPrice = '',
		skill, m, level, price;
	for (var i = 0, len = skillList.length; i < len; i++) {
		skill = skillList[i];
		if (!(m = /\d+/.exec(skill.textContent))) {
			continue;
		}
		level = +m[0];
		price = ((level + 1) / 2).toFixed(1) + 'k';
		skill.title = '↑ ' + price;
		if (level < minSkillLevel) {
			minSkillLevel = level;
			minSkillPrice = price;
		}
	}
	if (minSkillPrice && skillBadge.textContent !== minSkillPrice) {
		skillBadge.title = worker.GUIp_i18n.badge_skill;
		skillBadge.textContent = minSkillPrice;
	}
	ui_utils.hideElem(skillBadge, !minSkillPrice);
};
ui_improver.improvePlayers = function() {
	var nodes = document.querySelectorAll(ui_data.isMobile ? '.e_m_alls .opp_n.opp_ng span, .e_m_opps .opp_n.opp_ng span' : '#alls .opp_n.opp_ng span, #bosses .opp_n.opp_ng span, #o_hk_godname .l_val a');
	GUIp.common.markBlacklistedPlayers(nodes, ui_words.allyBlacklist);
	Array.prototype.forEach.call(nodes, GUIp.common.tooltips.watchSubtree);
	Array.from(document.querySelectorAll('.opp_n .opp_g:not(.improved)')).forEach(function(a) {
		var godName;
		a.classList.add('improved');
		if (!a.firstChild || !(godName = a.firstChild.textContent.match(/\((.*?)\)$/)) || !(godName = godName[1])) {
			return;
		}
		// in sailing and datamine we can see our own nickname in ally list
		if (godName === ui_data.god_name) {
			return;
		}
		a.insertAdjacentHTML('beforeend','<a class="e_gc_link em_font div_link" title="' + worker.GUIp_i18n.call_in_gc + '">#</a>');
		ui_improver._improvePlayersFontResizer(a.parentNode, 12);
		GUIp.common.addListener(a.lastElementChild, 'click', function(ev) {
			var node;
			if (node = document.getElementsByClassName('e_guild_council')[0]) {
				// guild council can be already opened but not focused, just click on it in this case
				if (!document.querySelector('.e_guild_council.frbutton_pressed') && node.firstElementChild) {
					node.firstElementChild.click();
				}
				// immediately insert godname into the textarea
				if ((node = document.querySelector('.e_guild_council .frInputArea textarea'))) {
					ui_improver._insertGodNameToChat(node, godName);
				}
			} else if ((node = document.querySelector('.msgDockPopup .show_gc'))) {
				// open guild council chat if it still wasn't here
				node.click();
				var gcWaiter = GUIp.common.setInterval(function() {
					// wait till the textarea appears
					if ((node = document.querySelector('.e_guild_council .frInputArea textarea'))) {
						worker.clearInterval(gcWaiter);
						ui_improver._insertGodNameToChat(node, godName);
					}
				},150);
			}
			ev.preventDefault();
		});
	});
};

/**
 * this function idea was "borrowed" from original godville package to autoresize spans with names (jquery-based)
 * @param {jQuery} container
 * @param {number} maxFontPixels
 */
ui_improver._improvePlayersFontResizer = function(container, maxFontPixels) {
	container = worker.$(container);
	GUIp.common.setTimeout(function() {
		var sHeight, sWidth, s = worker.$('span:first', container),
			cHeight = container.height(),
			cWidth = container.width();
		do
		{
			s.css('font-size', maxFontPixels);
			sHeight = s.height();
			sWidth = s.width();
			maxFontPixels -= 1;
		} while ((sHeight > cHeight || sWidth > cWidth) && maxFontPixels > 3);
	}, 50);
};

ui_improver.improveHP = function() {
	if (ui_data.isFight) {
		var i, len, changed = false;
		// if we have just opened the page, try to find already processed data from before
		if (this.isFirstTime) {
			this.allsHP = GUIp.common.parseJSON(ui_storage.get('Log:' + ui_data.logId + ':allshp')) || {};
		}
		// from now on we must have allsHP.a, reinitialize the object if necessary
		if (!this.allsHP.a) {
			this.allsHP = {a: [], e: [], sum: 0};
		}
		// if length of allsHP.a is equal to the current count of allies, then we don't need to recalculate anything here
		len = ui_stats.Hero_Alls_Count();
		if (len !== this.allsHP.a.length) {
			changed = true;
			this.allsHP.sum = 0;
			for (i = 0; i < len; i++) {
				this.allsHP.a[i] = this.allsHP.a[i] || ui_stats.Hero_Ally_MaxHP(i+1) || 0;
				// sum only health for heroes, not for summonned bosses
				if (ui_stats.Hero_Ally_Name(i)[0] !== '+') {
					this.allsHP.sum += this.allsHP.a[i];
				}
			}
		}
		// do almost the same for enemies but not in sail, as collecting maxhp list there is useless
		if (!ui_data.isSail) {
			// reset current maxhp list on first load, as there are different enemies during dungeon session
			// and we're not sure if it's a new boss or we just reloaded the page on the same one
			if (this.isFirstTime && (ui_data.isDungeon || ui_data.isBoss)) {
				this.allsHP.e = [];
			}
			len = ui_stats.Enemy_Count();
			if (len !== this.allsHP.e.length) {
				changed = true;
				for (i = 0; i < len; i++) {
					this.allsHP.e[i] = this.allsHP.e[i] || ui_stats.EnemySingle_MaxHP(i+1) || 0;
				}
			}
		}
		// save changes if any
		if (changed) {
			ui_storage.set('Log:' + ui_data.logId + ':allshp', JSON.stringify(ui_improver.allsHP));
		}
	}

	var hp, diff, generator = function(isAlls, hps, diffs) {
		var hpNodes = document.querySelectorAll(hps),
			diffNodes = document.querySelectorAll(diffs);
		for (var i = 0, len = hpNodes.length; i < len; i++) {
			if ((hp = /^(\d+) ?\/ ?(\d+)$/.exec(hpNodes[i].textContent))) {
				hpNodes[i].title = ui_utils.hpPrc(+hp[1],+hp[2]);
			} else if ((hp = ui_improver.allsHP[isAlls ? 'a' : 'e'][i])) {
				hpNodes[i].title = '0 / ' + hp;
				hp = [, 0, hp];
			} else {
				hpNodes[i].title = '';
			}
			GUIp.common.tooltips.watchSubtree(hpNodes[i]);
			if (diffNodes[i]) {
				if (hp && +hp[2] && (diff = +diffNodes[i].textContent)) {
					diffNodes[i].title = ui_utils.hpPrc(+diff,+hp[2],true);
				} else {
					diffNodes[i].title = '';
				}
				GUIp.common.tooltips.watchSubtree(diffNodes[i]);
			}
		}
	}
	if (ui_data.isMobile) {
		generator(true, '.e_m_alls .opp_h, #hk_health .l_val', '.e_m_alls .hp_diff, #hk_health .hp_d');
		generator(false,
			'.e_m_opps .opp_h, #statusbar .e_sb_hero_hp, #statusbar .e_sb_opp_hp, #statusbar .e_sb_ark_hp, #statusbar .e_sb_boss_hp',
			'.e_m_opps .hp_diff, #statusbar .e_sb_hero_hp_diff, #statusbar .e_sb_opp_hp_diff, #statusbar .e_sb_ark_hp_diff, #statusbar .e_sb_boss_hp_diff'
		);
	} else {
		generator(true, '#alls .opp_h, #hk_health .l_val', '#alls .hp_diff, #hk_health .hp_d');
		generator(false,
			'#opps .opp_h, #bosses .opp_h, #o_hl1 .l_val',
			'#opps .hp_diff, #bosses .hp_diff, #o_hl1 .hp_d'
		);
	}
};
ui_improver.improveSendButtons = function(forcedUpdate) {
	var sendToButtons, pants = document.querySelector('#pantheons .block_content'),
		curGP = ui_stats.Godpower();
	if (this.isFirstTime) {
		sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
		for (var i = 0, len = sendToButtons.length; i < len; i++) {
			if (sendToButtons[i].textContent.match(/(Послать на тренировку|Spar a Friend)/i)) {
				sendToButtons[i].parentNode.classList.add("e_challenge_button");
			} else if (sendToButtons[i].textContent.match(/(Направить в подземелье|Drop to Dungeon)/i)) {
				sendToButtons[i].parentNode.classList.add("e_dungeon_button");
			} else if (sendToButtons[i].textContent.match(/(Снарядить в плавание|Set Sail)/i)) {
				sendToButtons[i].parentNode.classList.add("e_sail_button");
			} else if (sendToButtons[i].textContent.match(/(Посетить полигон|Explore Datamine)/i)) {
				sendToButtons[i].parentNode.classList.add("e_mining_button");
			}
		}
		if (ui_stats.Level() >= 14 && !ui_data.isMobile) {
			// personal statistics
			pants.insertAdjacentHTML('afterbegin', '<div class="guip p_group_sep"></div>');
			var panthLines = pants.getElementsByClassName('panth_line');
			ui_utils.observeUntil(pants, {childList: true, subtree: true}, function() {
				return panthLines[0];
			}).then(function() {
				pants.insertAdjacentHTML('beforeend', '<div class="guip_stats_lnk p_group_sep"></div>');
				pants.insertAdjacentHTML('beforeend',
					'<div class="guip_stats_lnk"><div class="line"><a class="no_link div_link"' +
					' href="https://stats.' + worker.location.host + '/me" target="_blank"' +
					' title="' + worker.GUIp_i18n.statistics_title + '">' +
						worker.GUIp_i18n.statistics +
					'</a></div></div>'
				);
			}).catch(GUIp.common.onUnhandledException);
		}
		ui_storage.addListener('Option:relocateDuelButtons', ui_improver.improveSendButtons.bind(ui_improver, true));
	}
	sendToButtons = document.querySelectorAll('a.to_arena');
	for (var lim, i = 0, len = sendToButtons.length; i < len; i++) {
		lim = 50;
		if (!i && ui_improver.dailyForecast.get().includes('arena')) {
			lim = 25;
		}
		sendToButtons[i].classList.toggle('e_low_gp', curGP < lim);
	}
	sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
	var sendToDelay, sendToStr, sendToDesc = document.querySelectorAll('#cntrl2 div.arena_msg, #cntrl2 span.to_arena');
	for (var i = 0, len = sendToDesc.length; i < len; i++) {
		GUIp.common.tooltips.watchSubtree(sendToDesc[i]);
		if (sendToDesc[i].style.display === 'none') {
			continue;
		}
		if ((!sendToDesc[i].title.length || (sendToDesc[i].dataset.expires < Date.now() + 5000)) && (sendToStr = sendToDesc[i].textContent.match(/(Подземелье откроется через|Отплыть можно через|Арена откроется через|Тренировка через|Полигон откроется через|Босс освободится через|Arena available in|Dungeon available in|Sail available in|Sparring available in|Datamine available in|Boss ready in) (?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/))) {
			sendToDelay = ((sendToStr[2] !== undefined ? +sendToStr[2] : 0) * 60 + +sendToStr[3]) * 60;
			sendToStr = sendToStr[1].replace(' через',' в').replace(' in',' at');
			sendToDesc[i].dataset.expires = Date.now() + sendToDelay * 1000;
			sendToDesc[i].title = sendToStr + ' ' + GUIp.common.formatTime(new Date(+sendToDesc[i].dataset.expires),'simpledatetime');
		}
	}
	if ((this.isFirstTime || forcedUpdate) && !ui_data.isMobile) {
		var relocated, buttonInPantheons, relocateDuelButtons = ui_storage.getList('Option:relocateDuelButtons');
		relocated = relocateDuelButtons.includes('arena');
		buttonInPantheons = document.querySelector('#pantheons .arena_link_wrap');
		if (relocated && !buttonInPantheons) {
			pants.insertAdjacentElement('afterbegin', document.getElementsByClassName('arena_link_wrap')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg'));
		}
		relocated = relocateDuelButtons.includes('chf');
		buttonInPantheons = document.querySelector('#pantheons .e_challenge_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_challenge_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg').nextSibling);
		}
		relocated = relocateDuelButtons.includes('dun');
		buttonInPantheons = document.querySelector('#pantheons .e_dungeon_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_dungeon_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[1]);
		}
		relocated = relocateDuelButtons.includes('sail');
		buttonInPantheons = document.querySelector('#pantheons .e_sail_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_sail_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[2]);
		}
		relocated = relocateDuelButtons.includes('min');
		buttonInPantheons = document.querySelector('#pantheons .e_mining_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_mining_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[3]);
		}
	}
};
/**
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_improver.readDiaryOnce = function() {
	var root = ui_data.isMobile ? document.getElementsByClassName('e_m_diary')[0] : document.getElementById('diary'),
		messages = root.querySelectorAll('.d_msg:not(.parsed)'),
		sortButton = root.getElementsByClassName('sort_ch')[0],
		result = [],
		html = '',
		newHTML = '',
		date, time, msg, link, value;
	for (var i = 0, len = messages.length; i < len; i++) {
		msg = messages[i];
		msg.classList.add('parsed');
		time = msg.parentNode.getElementsByClassName('d_time')[0];
		if (!time || !(date = ui_utils.parseDiaryDate(time.textContent))) {
			GUIp.common.warn('cannot parse diary: no time for "' + msg.textContent + '"');
			continue;
		}
		if (msg.getElementsByClassName('vote_links_b')[0]) {
			result.push({date: date, type: 'foreignVoice', msg: msg.textContent, logID: ''});
		} else {
			html = msg.innerHTML;
			if ((newHTML = ui_improver._improveMonsterNameHTML(html, true)) !== html) {
				msg.innerHTML = newHTML;
			}
			if (msg.classList.contains('m_infl')) {
				result.push({date: date, type: 'influence', msg: msg.textContent, logID: ''});
			} else {
				link = msg.getElementsByTagName('a')[0]; // weird, it has .vote_link class
				result.push({
					date: date,
					type: 'regular',
					msg: msg.textContent,
					logID: link ? link.href.slice(link.href.lastIndexOf('/') + 1) : ''
				});
				if (!link && (msg.textContent.includes('☥ ') || ui_improver.detectors.stateField.monster.startsWith('☥ ')) && (value = GUIp.common.parseGatheredSoul(msg.textContent))) {
					GUIp.common.updateGatheredSouls(ui_storage, date, 0, value);
				}
			}
		}
	}
	if (!sortButton || sortButton.textContent === '▼') {
		// we cannot simply sort the entries by their date, because some of their dates might be identical
		// and we must keep them in the correct order in that case
		result.reverse();
	}
	return result;
};
ui_improver.improveForeignVoices = function() {
	var blocks = document.getElementsByClassName('vote_links_b'),
		links;
	for (var i = 0, len = blocks.length; i < len; i++) {
		links = blocks[i].getElementsByClassName('vote_link');
		GUIp.common.tooltips.watchSubtree(links[2] || links[0]);
	}
};
ui_improver.improveDiary = function() {
	var diary = ui_improver.readDiaryOnce(),
		len = diary.length,
		infl = false,
		reply = false;
	if (len && !ui_improver.isFirstTime) {
		// run voice timeout
		for (var i = 0; i < len; i++) {
			if (diary[i].msg.charCodeAt(0) === 0x27A5) { // ➥
				reply = true;
			} else if (diary[i].type === 'influence') {
				infl = true;
			}
		}
		if (infl && reply) {
			ui_timeout.start();
		}
	}
	ui_timers.updateDiary(diary);
	ui_improver.improveForeignVoices();
};
ui_improver.detectors = {};
ui_improver.detectors.stateLS = {bp: -1, bt: 0, cp: -1, ct: 0, tpp: 0};
ui_improver.detectors.detectLS = function() {
	var tmTimer = document.getElementById('trademode_timer');
	ui_utils.hideElem(tmTimer, !ui_data.inShop);
	if (!ui_data.inShop || !tmTimer) {
		this.stateLS = {bp: -1, bt: 0, cp: -1, ct: 0, tpp: 0};
		return;
	}
	if (!this.stateLS.tpp && ui_data.inShop) {
		tmTimer.title = worker.GUIp_i18n.trademode_timer_wait;
		tmTimer.textContent = '––:––';
	}
	var updateTimer = function() {
		if (this.stateLS.tpp) {
			tmTimer.title = '[' + GUIp.common.formatTime(new Date(+this.stateLS.bt - (this.stateLS.bp - 0.5) * this.stateLS.tpp),'simpletime') + '~' + GUIp.common.formatTime(new Date(+this.stateLS.ct + (100.5 - cp) * this.stateLS.tpp),'simpletime') + ']';
			tmTimer.textContent = GUIp.common.formatTime((+this.stateLS.ct - Date.now() + (100.5 - cp) * this.stateLS.tpp) / 60000,'remaining');
		}
	};
	var cp = ui_stats.sProgress();
	if (this.stateLS.bp < 0) {
		// check cached data if any
		var cache = GUIp.common.parseJSON(ui_storage.get('Cache:stateLS'));
		if (cache && Math.abs(cache.ct + (100.5 - cache.cp) * cache.tpp - Date.now() - (100.5 - cp) * cache.tpp) < 1800e3) {
			this.stateLS = cache;
			updateTimer.call(this);
			return;
		}
		this.stateLS.bp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < 0) {
		this.stateLS.bp = this.stateLS.cp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < cp) {
		this.stateLS.cp = cp;
		this.stateLS.ct = Date.now();
		this.stateLS.tpp = (this.stateLS.ct - this.stateLS.bt)/(this.stateLS.cp - this.stateLS.bp);
		// cache obtained values
		ui_storage.set('Cache:stateLS',JSON.stringify(this.stateLS));
	}
	updateTimer.call(this);
};
ui_improver.detectors.stateGTF = {cnt: 0, res: false};
ui_improver.detectors.stateGTG = {init: -1, initTown: "", res: false};
ui_improver.detectors.stateFTC = {res: false};
ui_improver.detectors.stateField = {dst: 0, intown: false, nbtown: "", task: "", monster: "", ld: "", ln: "", hs: "", hp: -1, hpd: 0};
Object.defineProperty(ui_improver.detectors, 'stateGB', { get: function() { return this.stateGTG; } });
ui_improver.detectors.detectField = function() {
	var sp = ui_stats.sProgress(),
		intown = !!ui_stats.townName(),
		nbtown = ui_stats.nearbyTown(),
		monster = ui_stats.monsterName().replace(/^(Undead|Восставший) /,''),
		milestones = ui_stats.mileStones(),
		fullTask = ui_stats.Task_Name(),
		task = fullTask.replace(/ \((?:выполнено|отменено|эпик|completed|cancelled|epic)\)/g,''),
		taskActive = !/ \((?:выполнено|отменено|completed|cancelled)\)/.test(fullTask),
		hs = ui_stats.heroState(),
		ld = (ui_stats.lastDiaryRealEntry(0, true) || '').textContent,
		ln = ui_stats.lastNews(),
		hp = ui_stats.HP();
	if (hs === 'returning') {
		// return for full town cycle
		if (this.stateField.hs !== 'returning' && ld === this.stateField.ld || !taskActive) {
			this.stateFTC.res = true;
		}
		// going to capital
		if (this.stateGTG.init < 0) {
			if (!taskActive || milestones < (GUIp_locale === 'ru' ? 3 : 5) ||
				ui_improver.dailyForecast.get().includes('gvroads') || (this.stateField.nbtown && nbtown !== this.stateField.nbtown && sp <= 85) ||
				/\((Godville|Годвилль)\)/.test((document.querySelector('.e_hero_state[data-state="returning"]') || '').textContent)) {
				this.stateGTG.res = true;
			}
			this.stateGTG.init = this.stateField.dst ? this.stateField.dst : milestones;
			this.stateGTG.initTown = this.stateField.nbtown ? this.stateField.nbtown : nbtown;
		} else if (this.stateGTG.initTown && this.stateGTG.initTown !== nbtown) {
			this.stateGTG.res = true;
		}
	} else if (hs !== 'fishing') {
		if (this.stateGTG.init > -1) {
			this.stateGTG = {init: -1, initTown: "", res: false};
		}
		if (this.stateFTC.res && !intown) {
			this.stateFTC.res = false;
		}
	}
	// go to fields
	if (hs !== 'walking' && hs !== 'fishing' || this.stateGTF.res && sp < this.stateField.sp && !(this.stateField.hs === 'walking' && hs === 'fishing' || this.stateField.hs === 'fishing' && hs === 'walking')) {
		this.stateGTF = {cnt: 0, res: false};
	} else if (this.stateField.intown && !intown && !ui_stats.lastDiaryIsInfl()) {
		this.stateGTF.res = true;
	} else if (hs === 'walking' && milestones > this.stateField.dst && ui_stats.progressDiff() > 2 && !(/\((мини|mini|гильд|guild)\)/.test(task))) {
		this.stateGTF.cnt++;
		if (this.stateGTF.cnt >= 2) {
			this.stateGTF.res = true;
		}
	}
	// hp delta
	if (this.stateField.hp >= 0 && (this.stateField.hp !== hp || this.stateField.ln !== ln)) {
		this.stateField.hpd = hp - this.stateField.hp;
	}
	this.stateField.hp = hp;
	// common
	this.stateField.dst = milestones;
	this.stateField.intown = intown;
	this.stateField.nbtown = nbtown;
	this.stateField.task = task;
	this.stateField.monster = monster;
	this.stateField.ld = ld;
	this.stateField.ln = ln;
	this.stateField.hs = hs;
	if (!monster) {
		this.stateField.sp = sp;
	}
};
ui_improver.distanceInformerInit = function() {
	var dstSelected, dstContent, dstContentInner,
		dstLine = worker.$('#hk_distance .l_capt'),
		dstSaved = ui_storage.get('townInformer');
	if (dstLine) {
		dstLine.addClass('edst_header');
		dstLine.wup({
			title: worker.GUIp_i18n.town_informer,
			placement: 'bottom',
			mobile_root: ui_data.isMobile ? worker.$("#main_page") : undefined,
			width: 320,
			onShow: GUIp.common.try2.bind(null, function onTownTableShow(t) {
				var wup = t[0],
					pos = parseInt(wup.style.left);
				if (pos < 5) {
					wup.style.left = '10px';
					wup.firstElementChild.style.left = (parseInt(wup.firstElementChild.style.left) - (10 - pos)) + 'px';
				}
			}),
			content: GUIp.common.try2.bind(null, function createTownTableContent() {
				dstContent = $('<div></div>');
				dstSelected = worker.$('<div class="edst_headline"><div class="s">Aa</div><div class="c"></div></div>');
				GUIp.common.addListener(dstSelected[0].firstChild, 'click', function(ev) {
					ev.preventDefault();
					this.classList.toggle('alpha');
					var isAlpha = this.classList.contains('alpha');
					this.textContent = isAlpha ? '01' : 'Aa';
					ui_improver.distanceInformerListBuilder(dstContentInner, isAlpha);
				});
				GUIp.common.addListener(dstSelected[0].lastChild, 'click', function(ev) {
					ev.preventDefault();
					ui_improver.distanceInformerReset();
				});
				dstContent.append(dstSelected);
				dstContentInner = $('<div class="edst_towns"></div>');
				ui_improver.distanceInformerListBuilder(dstContentInner);
				GUIp.common.tooltips.watchSubtree(dstContentInner[0]);
				dstContent.append(dstContentInner);
				ui_improver.distanceInformerUpdate(dstLine,dstSelected);
				dstLine.wup("hide");
				return dstContent;
			})
		});
		if (dstSaved) {
			ui_improver.informTown = dstSaved;
			ui_improver.distanceInformerUpdate(dstLine,dstSelected);
		}
	}
};
ui_improver.distanceInformerUpdate = function(dstLine,dstSelected) {
	dstLine = dstLine || worker.$('#hk_distance .l_capt');
	dstSelected = (dstSelected || worker.$('.edst_headline')).children().last();
	var town = document.getElementsByClassName('e_selected_town')[0];
	if (town) {
		town.classList.remove('e_selected_town');
	}
	town = document.getElementsByClassName('e_chosen_town')[0];
	if (town) {
		town.classList.remove('e_chosen_town');
	}
	if (ui_improver.informTown) {
		dstSelected.html(worker.GUIp_i18n[isNaN(+this.informTown) ? 'town_informer_curtown' : 'town_informer_curms'] + ': <strong>' + ui_improver.informTown + '</strong> [x]');
		dstSelected.attr('title',worker.GUIp_i18n.town_informer_reset);
		dstLine.addClass('edst_header_active');
		dstSelected.addClass('edst_headline_active');
		town = Array.from(document.querySelectorAll('.edst_towns .l')).find(function(a) {
			return a.textContent === this.informTown;
		}.bind(this));
		if (town) {
			town.parentNode.classList.add('e_chosen_town');
		}
		ui_improver.nearbyTownsFix();
	} else {
		dstSelected.text(worker.GUIp_i18n.town_informer_choose);
		dstSelected.attr('title','');
		dstLine.removeClass('edst_header_active');
		dstSelected.removeClass('edst_headline_active');
	}
};
ui_improver.distanceInformerSet = function(town) {
	ui_storage.set('townInformer',town.name);
	ui_improver.informTown = town.name;
	ui_improver.distanceInformerUpdate();
};
ui_improver.distanceInformerReset = function() {
	if (ui_improver.informTown) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	}
};
ui_improver.distanceInformerListBuilder = function(container, isAlpha) {
	container.empty();
	var town, towns, towns_keys, dist, name, desc, townInfo, sh, title = [],
		towns_dists = {},
		gvRoads = ui_improver.dailyForecast.get().includes('gvroads'),
		currentTown = ui_stats.townName();
	try {
		towns = JSON.parse(localStorage.town_c);
		towns_keys = Object.keys(towns.t).sort(function(a, b) { return a - b; });
		for (var i = 0, len = towns_keys.length; i < len; i++) {
			towns_dists[towns_keys[i]] = towns_keys[i] + (towns_keys[i+1] ? '–' + (+towns_keys[i+1] - 1) : '+');
		}
		if (isAlpha) {
			towns_keys = Object.keys(towns.t).sort(function(a, b) { return towns.t[a].localeCompare(towns.t[b]); });
		}
		for (var i = 0, len = towns_keys.length; i < len; i++) {
			dist = +towns_keys[i];
			name = towns.t[dist];
			if (gvRoads && currentTown !== name) {
				continue;
			}
			title.length = 0;
			town = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="l">' + name + '</div><div class="r">(' + towns_dists[dist] + ')</div>');
			if (currentTown === name) {
				town.addClass('e_current_town');
				title.push(worker.GUIp_i18n.town_current);
			}
			if (ui_improver.informTown === name) {
				town.addClass('e_chosen_town');
			}
			if ((desc = towns.d[name])) {
				if (desc === 'capital') desc = 'being '+desc;
				title.push(worker.GUIp_i18n.town_banner + desc.replace(/\s*,\s*/, worker.GUIp_i18n.town_as_well_as));
			}
			if ((townInfo = ui_words.base.town_list.find(function(a) { return a.name === name; }))) {
				if (townInfo.wasteFraction &&
					/м(?:ног|ал)о пьют и сберегают|(?:lavish|cheap) parties and (?:good|bad) savings/.test(desc)
				) {
					title.push(GUIp_i18n.fmt('town_waste_fraction', townInfo.wasteFraction));
				}
				if ((townInfo.skillsDiscount || townInfo.skillsMarkup) &&
					/д(?:ешёвы|ороги)е умения|(?:cheap|expensive) skills/.test(desc)
				) {
					title.push(GUIp_i18n.fmt(
						townInfo.skillsDiscount ? 'town_skills_discount' : 'town_skills_markup',
						townInfo.skillsDiscount || townInfo.skillsMarkup
					));
				}
			}
			if ((sh = towns.sh.indexOf(dist)) > -1) {
				town.addClass('e_shifted_town');
				if (towns.t[towns.sh[sh^0x01]]) {
					title.push(worker.GUIp_i18n.fmt('town_shifted', towns.t[towns.sh[sh^0x01]]));
				}
			}
			if (title.length) {
				town.prop('title', title.join('. ') + '.');
			}
			if (!gvRoads) {
				GUIp.common.addListener(town[0], 'click', function(ev) {
					ev.preventDefault();
					ui_improver.distanceInformerSet(this);
				}.bind({name: name}));
			}
			town.appendTo(container);
		}
		if (gvRoads) {
			town = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="c">' + worker.GUIp_i18n.town_informer_gvroads + '</div>');
			town.appendTo(container);
		}
	} catch (e) {}
};
ui_improver.generateNewspaperSummary = function() {
	var s = GUIp_i18n.daily_forecast + (
		ui_improver.dailyForecast.get().length ? ': [' + ui_improver.dailyForecast.get().map(function(a) {
			if (a === 'specbosses' && ui_improver.dailyForecastSpecBoss.get().length) {
				a += ' (' + ui_improver.dailyForecastSpecBoss.get() + ')';
			}
			return a;
		}).join(', ') + ']\n' : ':\n'
	) + ui_improver.dailyForecastText.get();
	var couponPrize = ui_storage.get('Newspaper:couponPrize:raw') || '',
		activeAdvert = ui_storage.get('Newspaper:activeAdvert') || '',
		activeAdvertButton = ui_storage.get('Newspaper:activeAdvert:button');
	if (activeAdvert) {
		s += GUIp_i18n.fmt('ad_available', activeAdvert, activeAdvertButton);
	}
	if (ui_improver.bingoTries.get()) {
		s += GUIp_i18n.fmt('bingo_summary',
			GUIp_i18n.plfmt('bingo_slots_', ui_inventory.getBingoSlots().length),
			GUIp_i18n.plfmt('bingo_clicks_', ui_improver.bingoTries.get())
		);
	}
	if (couponPrize) {
		s += GUIp_i18n.fmt('coupon_available', couponPrize);
	}
	if (ui_storage.getFlag('Newspaper:godpowerCap')) {
		s += GUIp_i18n.godpower_cap_available;
	}
	return s;
};
ui_improver.showDailyForecast = function() {
	if (ui_data.isMobile) {
		var node = document.getElementById('e_forecast');
		if (node) {
			if (ui_improver.dailyForecast.get().length) {
				node.innerText = ui_improver.dailyForecast.get().map(function(a) {
					if (a === 'specbosses' && ui_improver.dailyForecastSpecBoss.get().length) {
						a += ' (' + ui_improver.dailyForecastSpecBoss.get() + ')';
					}
					return a;
				}).join(', ');
			} else {
				node.innerText = 'none';
			}
		}
		node = document.getElementsByClassName('e_m_available_coupon')[0];
		if (node) {
			var couponPrize = ui_storage.get('Newspaper:couponPrize:raw') || '';
			if (couponPrize) {
				node.firstChild.textContent = GUIp_i18n.fmt('coupon_available_mobile', couponPrize);
				ui_utils.hideElem(node,false);
			} else {
				ui_utils.hideElem(node,true);
			}
		}
		node = document.getElementsByClassName('e_m_available_ad')[0];
		if (node) {
			var adButton = ui_storage.get('Newspaper:activeAdvert:button') || '';
			if (adButton) {
				node.firstChild.textContent = GUIp_i18n.fmt('ad_available_mobile', adButton);
				ui_utils.hideElem(node,false);
			} else {
				ui_utils.hideElem(node,true);
			}
		}
		return;
	}
	var forecastLink = document.getElementById('e_forecast'),
		news = document.getElementById('m_hero').previousElementSibling,
		dfcTimer = 0,
		fr;
	if (forecastLink) {
		if (this.dailyForecastText.get()) {
			forecastLink.title = ui_improver.generateNewspaperSummary();
		} else {
			forecastLink.parentNode.removeChild(forecastLink);
		}
	} else if (news && this.dailyForecastText.get()) {
		forecastLink = document.createElement('a');
		forecastLink.id = 'e_forecast';
		forecastLink.textContent = '❅';
		forecastLink.href = '/news';
		forecastLink.title = ui_improver.generateNewspaperSummary();
		if (GUIp.common.isAndroid) {
			GUIp.common.addListener(forecastLink, 'click', function(ev) {
				ev.preventDefault();
				worker.alert(this.title);
			});
		} else {
			GUIp.common.addListener(forecastLink, 'click', function(ev) { ev.preventDefault(); });
		}
		fr = document.createDocumentFragment();
		fr.appendChild(document.createTextNode(' '))
		fr.appendChild(forecastLink);
		news.insertBefore(fr, news.lastElementChild.nextSibling);
	}
	if (!ui_utils.isAlreadyImproved(news)) {
		GUIp.common.addListener(news, 'mousedown', function(e) {
			if (e.button !== 0) { return; }
			dfcTimer = GUIp.common.setTimeout(function() {
				e.preventDefault();
				ui_data._getNewspaper(true);
				worker.alert(worker.GUIp_i18n.daily_forecast_update_notice);
			},1200);
		});
		GUIp.common.addListener(news, 'mouseup', function(e) {
			if (e.button !== 0) { return; }
			if (dfcTimer) {
				worker.clearTimeout(dfcTimer);
			}
		});
	}
};
ui_improver._onSubCheckboxClick = function() {
	// this: HTMLInputElement
	// see markup below
	var mask = this.dataset.eMask;
	if (!mask) return;
	var subscriptions = GUIp.common.parseJSON(ui_storage.get('ForumSubscriptions')) || {},
		tid = this.parentNode.dataset.eTid,
		sub = subscriptions[tid];
	if (!sub) return;

	if (this.checked) {
		sub.notifications |= mask;
	} else {
		sub.notifications &= ~mask;
	}
	ui_storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
};
ui_improver.showSubsLink = function() {
	if (ui_data.isMobile) return;
	var subsLink = document.getElementById('e_subs'),
		forumLink, fr;
	if (!subsLink) {
		forumLink = document.querySelector('#menu_bar a[href="/forums"]');
		if (forumLink) {
			var subs_target = '/forums/show/1/#guip_subscriptions';
			subsLink = document.createElement('a');
			subsLink.id = 'e_subs';
			subsLink.className = 'em_font';
			subsLink.textContent = '▶';
			subsLink.href = subs_target;
			subsLink.title = worker.GUIp_i18n.forum_subs_short;
			fr = document.createDocumentFragment();
			fr.appendChild(document.createTextNode(' '));
			fr.appendChild(subsLink);
			forumLink.parentElement.insertBefore(fr, forumLink.parentElement.lastElementChild.nextSibling);
			worker.$('#e_subs').wup({
				title: worker.GUIp_i18n.forum_subs_short,
				placement: 'bottom',
				onShow: GUIp.common.try2.bind(null, function onSubsShow(t) {
					var wup = t[0],
						pos = parseInt(wup.style.left);
					if (pos < 5) {
						wup.style.left = '10px';
						wup.firstElementChild.style.left = (parseInt(wup.firstElementChild.style.left) - (10 - pos)) + 'px';
					}
					wup.classList.add('e_subs_wup');
				}),
				content: GUIp.common.try2.bind(null, function createSubsContent() {
					var fsubsContent = $('<div class="esubs_content"></div>');
					var fsubsLine, subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
						informers = JSON.parse(ui_storage.get('ForumInformers')) || {},
						topics = worker.Object.keys(subscriptions),
						tid, sub, page, title;
					topics.sort(function(a,b) { return subscriptions[b].date - subscriptions[a].date; });
					for (var i = 0, len = topics.length; i < len; i++) {
						tid = topics[i];
						sub = subscriptions[tid];
						page = Math.ceil(sub.posts / 25);
						title = GUIp.common.escapeHTML(sub.name);
						fsubsLine = worker.$('<div class="esubs_line"></div>').html(
							'<div class="title">' +
								'<img alt="Comment" class="' + (tid in informers ? 'green' : 'grey') +
									'" src="/images/forum/clearbits/comment.gif" />' +
								' <a href="/forums/show_topic/' + tid + '" title="' + title + '" target="_blank">' +
									title +
								'</a>' +
							'</div>' +
							'<div class="info">' +
								GUIp.common.formatTime(new Date(sub.date),'simpledate') +
								', ' +
								GUIp.common.formatTime(new Date(sub.date),'simpletime') +
								'<span class="em_font"> ➠ </span>' +
								'<a href="/forums/show_topic/' + tid + '?page=' + page +
									'&epost=' + (sub.posts + 25 - page*25) +
									'" title="' + worker.GUIp_i18n.forum_subs_info + sub.by +
									'" target="_blank">' +
									sub.by +
								'</a>' +
							'</div>' +
							'<div class="options" data-e-tid="' + tid + '">' +
								(GUIp.common.notif.supported ?
									'<input type="checkbox" data-e-mask="1" ' +
									(sub.notifications & 0x1 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_desktop_notif + '" />'
								: '') +
								'<input type="checkbox" data-e-mask="2" ' +
									(sub.notifications & 0x2 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_sound_notif + '" />' +
							'</div>' +
							'<div style="clear: both;"></div>'
						);
						fsubsLine.appendTo(fsubsContent);
					}
					fsubsLine = worker.$('<div class="esubs_line"></div>').html('<div class="c"><a href="' + subs_target + '" target="_blank">' + worker.GUIp_i18n.forum_subs_full + '</a></div>');
					fsubsLine.appendTo(fsubsContent);
					fsubsContent.on('click', 'input', function() {
						GUIp.common.try2.call(this, ui_improver._onSubCheckboxClick);
					});
					worker.$('#e_subs').wup("hide");
					return fsubsContent;
				})
			});
		}
	}
};

ui_improver.parseDungeonSouls = function(finished) {
	if (!ui_data.availableGameModes.includes('souls')) {
		return;
	}
	var step, found = false,
		steps = Object.keys(this.chronicles).length;
	for (step = steps; step > 0; step--) {
		if (this.chronicles[step].marks.includes('boss') && this.parseDungeonFightResults(step)) {
			found = true;
			break;
		}
	}
	if (!found && finished) {
		ui_timers.updateDungeonSouls({
			type: 'souls',
			date: ui_utils.parseDiaryDate(this.chronicles[steps].time),
			result: 0,
			inID: ui_data.logId
		});
	}
};

ui_improver.parseDungeonFightResults = function(step) {
	if (!ui_data.availableGameModes.includes('souls')) {
		return;
	}
	var result, date;
	if (!(result = GUIp.common.parseDungeonResultFromStep(this.chronicles[step].text, ui_data.char_name, ui_stats.Hero_Ally_Names(), GUIp.common.parseGatheredSoul))) {
		return false;
	}
	date = this.chronicles[step+1] ? (ui_utils.parseDiaryDate(this.chronicles[step+1].time) - 30e3) : ui_utils.parseDiaryDate(this.chronicles[step].time); /* chronicle logs have times set at the start of a fight, so try getting the next step if available */
	ui_timers.updateDungeonSouls({
		type: 'souls',
		date: date,
		result: 1,
		inID: ui_data.logId
	});
	GUIp.common.updateGatheredSouls(ui_storage, date, 2, result);
	return true;
};

ui_improver.parseChronicles = function(xhr) {
	this.needLog = false;

	var page = document.createElement('div'),
		currentStep = ui_stats.currentStep();
	page.innerHTML = xhr.responseText;
	this.parseDungeonExtras(page.querySelectorAll('#last_items_arena .d_imp .text_content'), null, currentStep); // in case first step is already gone in on-page chronicles
	GUIp.common.parseChronicles.call(ui_improver, page, currentStep, {
		putDMLink: ui_storage.getList('Option:dungeonMapSettings').includes('dmln')
	});
	page = null;

	// data on the chronicle page is assumed always to be complete, so we can just rebuild allsHP object from scratch using it
	var hpMatch, hpRE = /<a href=["']\/gods\/.*?>(.*?)<\/a>[^]+?<span id=["']hp\d["']>\d+<\/span> \/ (\d+)/g,
		hpCountAllies = ui_stats.Hero_Alls_Count(),
		hpCountEnemies = ui_stats.Enemy_Count(),
		hpObj = {a: [], e: [], sum: 0};
	while (hpMatch = hpRE.exec(xhr.responseText)) {
		for (var i = 0; i < hpCountAllies; i++) {
			if (ui_stats.Hero_Ally_Name(i+1) === hpMatch[1]) {
				hpObj.a[i] = +hpMatch[2];
				if (hpMatch[1][0] !== '+') {
					hpObj.sum += hpObj.a[i];
				}
				break;
			}
		}
	}
	hpRE = /<a href=["']https?:\/\/wiki\.[^"']+\/.*?>(.*?)<\/a>[^]+?<span id=["']hp\d["']>\d+<\/span> \/ (\d+)/g
	while (hpMatch = hpRE.exec(xhr.responseText)) {
		for (var i = 0; i < hpCountEnemies; i++) {
			if (ui_stats.EnemySingle_Name(i+1) === hpMatch[1]) {
				hpObj.e[i] = +hpMatch[2];
				break;
			}
		}
	}
	if (hpObj.sum > 0 && hpObj.a.length === hpCountAllies && hpObj.e.length === hpCountEnemies) {
		ui_improver.allsHP = hpObj;
		ui_storage.set('Log:' + ui_data.logId + ':allshp', JSON.stringify(ui_improver.allsHP));
	}
	this.parseDungeonSouls(this.isDungeonFinished(currentStep));
	this.colorDungeonMap();
};
ui_improver.deleteInvalidChronicles = function() {
	var isHiddenChronicles = true,
		chronicles = document.querySelectorAll('#m_fight_log .line.d_line');
	for (var i = chronicles.length - 1; i >= 0; i--) {
		if (isHiddenChronicles) {
			if (chronicles[i].style.display !== 'none') {
				isHiddenChronicles = false;
			}
		} else {
			if (chronicles[i].style.display === 'none') {
				chronicles[i].parentNode.removeChild(chronicles[i]);
			}
		}
	}
};
ui_improver.improveChronicles = function() {
	if (!GUIp.common[GUIp.common.dungeonPhrases[GUIp.common.dungeonPhrases.length - 1] + 'RegExp']) {
		GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
	} else {
		//ui_improver.deleteInvalidChronicles();
		var i, len, lastNotParsed, cur_pos, cur_step, time = '', texts = [], infls = [],
			chronicles = document.querySelectorAll(ui_data.isMobile ? '.e_m_fight_log .d_msg:not(.parsed)' : '#m_fight_log .d_msg:not(.parsed)'),
			sort_ch = ui_data.isMobile && document.getElementsByClassName('sort_ch')[0],
			ch_down = !(sort_ch && sort_ch.textContent === '▲'),
			step = ui_stats.currentStep();
		for (cur_step = step, len = chronicles.length, i = ch_down ? 0 : len - 1; (ch_down ? i < len : i >= 0) && step; ch_down ? i++ : i--) {
			lastNotParsed = true;
			if (!chronicles[i].className.includes('m_infl')) {
				texts = [chronicles[i].textContent].concat(texts);
			} else {
				infls = [chronicles[i].textContent].concat(infls);
			}
			if (chronicles[i].previousElementSibling && chronicles[i].previousElementSibling.className.includes('d_time')) {
				time = chronicles[i].previousElementSibling.textContent.trim() || time;
			}
			if (chronicles[i].parentNode.className.includes(ch_down ? 'turn_separator' : 'turn_separator_inv')) {
				GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, time, step);
				lastNotParsed = false;
				time = '';
				texts = [];
				infls = [];
				step--;
			}
			if (!chronicles[i].className.includes('m_infl')) {
				if (GUIp.common.bossHintRegExp.test(chronicles[i].textContent)) {
					chronicles[i].parentNode.classList.add('bossHint');
					// workaround for an issue when there are two adjacent bosses and bossHint entry is added to the chronicle with a delay,
					// so it never gets inserted into chronicles object and never painted on the already processed map
					if (!this.needLog && step === cur_step && ui_improver.chronicles[cur_step] && !ui_improver.chronicles[cur_step].marks.includes('bossHint')) {
						ui_improver.chronicles[cur_step].marks.push('bossHint');
						if (cur_pos = document.querySelector((ui_data.isMobile ? '.e_m_dmap' : '#map') + ' .map_pos')) {
							cur_pos.classList.add('bossHint');
						}
					}
				}
				if (/voice from above announced that all bosses in|голос откуда-то сверху сообщил, что ни единого живого босса/.test(chronicles[i].textContent)) {
					chronicles[i].innerHTML = chronicles[i].innerHTML.replace(/A pleasant voice from above announced that all bosses in this dungeon have perished and wished the intruders to burn in hell\.|Приятный голос откуда-то сверху сообщил, что ни единого живого босса (?:в этом подземелье|здесь) не осталось, и пожелал виновникам гореть в аду\./, '<strong>$&</strong>');
				}
			}
			chronicles[i].classList.add('parsed');
		}
		if (lastNotParsed) {
			GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, time, step);
		}

		if (this.needLog) {
			if (worker.Object.keys(this.chronicles)[0] === '1' && chronicles.length < 20 && (cur_step === document.querySelectorAll((ui_data.isMobile ? '.e_m_fight_log' : '#m_fight_log') + ' .turn_separator' + (!ch_down ? '_inv' : '')).length)) {
				this.needLog = false;
				this.parseDungeonSouls(this.isDungeonFinished(cur_step));
				this.colorDungeonMap();
			} else if (this.dungeonXHRCount < 3) {
				this.dungeonXHRCount++;
				GUIp.common.requestLog(ui_data.logId, ui_improver.parseChronicles.bind(ui_improver));
			}
		} else {
			if (this.dungeonExtras.transformationPending || !this.dungeonExtras.stairsOffset) {
				// we may need to call this on transformationPending since title in map block can be updated with an arbitrary delay, and also until stairsOffset is populated
				this.parseDungeonExtras([], ui_data.isMobile ? document.getElementsByClassName('e_m_dmap')[0] : document.getElementById('map'), cur_step);
			}
			// check for gathered souls
			if (this.chronicles[cur_step]) {
				if (this.isDungeonFinished(cur_step)) {
					this.parseDungeonSouls(true);
				} else if (this.chronicles[cur_step].marks.includes('boss')) {
					this.parseDungeonFightResults(cur_step);
				}
			}
		}

		// informer
		if (worker.Object.keys(this.chronicles).length) {
			ui_informer.update('close to boss', false); // we want to explicitly _restart_ this informer over again on each step that has the bossHint mark
			ui_informer.update('close to boss', this.chronicles[worker.Object.keys(this.chronicles).reverse()[0]].marks.includes('bossHint'));
		}

		if (ui_storage.get('Log:current') !== ui_data.logId) {
			ui_storage.set('Log:current', ui_data.logId);
			ui_storage.set('Log:' + ui_data.logId + ':dlMoves', '{}');
			ui_storage.set('Log:' + ui_data.logId + ':whMoves', '{}');
			// detect dungeons of pledge and dungeons of mysterious pledge
			if (this.checkParsedDungeonType('pledge')) {
				ui_logger.appendLogs(-1);
				ui_logger.finishBatch();
			} else if (this.checkParsedDungeonType('mystery')) {
				GUIp.common.getDomainXHR('/gods/api/' + encodeURIComponent(ui_data.god_name), function(xhr) {
					var logs = +ui_storage.get('Logger:Logs');
					try {
						logs = JSON.parse(xhr.responseText).wood_cnt - logs;
						if (!logs) return;
					} catch (e) {
						GUIp.common.warn('got invalid JSON from /gods/api/:', xhr.responseText);
						return;
					}
					// we calculate a difference instead of just showing "wd-1" in case user has opened a dungeon
					// after being inactive for some time
					ui_logger.appendLogs(logs);
					ui_logger.finishBatch();
					// and update detected type
					ui_improver.parseDungeonExtras([], 'pledge', cur_step);
				}, function(xhr) {
					GUIp.common.warn('failed to load /gods/api/ (' + xhr.status + '):', xhr.responseText);
				});
			}
		}
		ui_storage.set('Log:' + ui_data.logId + ':steps', ui_stats.currentStep());
		ui_storage.set('Log:' + ui_data.logId + ':map', JSON.stringify(ui_improver.getDungeonMap()));

		// stream the dungeon
		if (this.streamingManager.active) {
			this.streamingManager.uploadStep();
		}
	}
};

ui_improver.isDungeonFinished = function(step) {
	var step = step || ui_stats.currentStep(),
		chronicle = this.chronicles[step] || this.chronicles[Object.keys(this.chronicles).length];
	if (
		chronicle.marks.includes('treasureChest') || chronicle.marks.includes('vault') ||
		(step >= (this.checkParsedDungeonType('hurry') ? 50 : 100)) ||
		(ui_stats.Hero_Alls_AliveCount() + (ui_stats.HP() > 1 ? 1 : 0)) < 2
	) {
		return true;
	}
	return false;
};

ui_improver.getDungeonMapNodes = function() {
	var result = [], rows = document.querySelectorAll((ui_data.isMobile ? '.e_m_dmap' : '#map') + ' .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		result.push(Array.from(rows[i].getElementsByClassName('dmc')));
	}
	return result;
};

ui_improver.getDungeonMap = function() {
	var result = [], cells, row, rows = document.querySelectorAll((ui_data.isMobile ? '.e_m_dmap' : '#map') + ' .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		row = [];
		cells = rows[i].querySelectorAll('.dmc');
		for (var j = 0, len2 = cells.length; j < len2; j++) {
			row.push(cells[j].textContent.trim());
		}
		result.push(row);
	}
	return result;
};

ui_improver.colorDungeonMap = function() {
	if (!ui_data.isDungeon || ui_improver.needLog || ui_data.isMobile && !document.getElementsByClassName('e_m_dmap')[0]) {
		return;
	}
	var step, mapCells, currentCell, trapMoveLossCount = 0,
		isJumping = this.checkParsedDungeonType('jumping'),
		isStepInCellar = false,
		isMapInCellar = GUIp.common.isDungeonCellar(),
		coords = GUIp.common.calculateExitXY(isMapInCellar),
		heroesCoords = GUIp.common.calculateXY(GUIp.common.getOwnCell()),
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length;
	if (steps_max !== ui_stats.currentStep()) {
		GUIp.common.warn('step count mismatch: parsed=' + steps_max + ', required=' + ui_stats.currentStep());
		return;
	}
	if ((this.dungeonExtras.nookOffset.x || this.dungeonExtras.nookOffset.y) && (this.dungeonExtras.nookOffset.x + coords.x) === heroesCoords.x && (this.dungeonExtras.nookOffset.y + coords.y) === heroesCoords.y) {
		GUIp.common.debug('party has entered the nook');
		if ((this.dungeonExtras.reward === 'transformation' || this.dungeonExtras.reward === 'unknown') && !this.dungeonExtras.transformationComplete) {
			this.dungeonExtras.transformationPending = true; // now we should wait for a transformation
		}
	}
	// load guidedSteps data if it's not present yet
	this.dungeonGuidedSteps = this.dungeonGuidedSteps || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':guidedSteps')) || {};
	// describe map
	mapCells = document.querySelectorAll((ui_data.isMobile ? '.e_m_dmap' : '#map') + ' .dml');
	GUIp.common.dmapTitlesFixup(mapCells);
	// add the description of the first step to the staircase cell if we're in a basement - to help remind dungeon start conditions
	if (isMapInCellar && mapCells[coords.y] && mapCells[coords.y].children[coords.x] && this.chronicles[1]) {
		GUIp.common.describeCell(mapCells[coords.y].children[coords.x],1,steps_max,this.chronicles[1],trapMoveLossCount);
	}
	for (step = 1; step <= steps_max; step++) {
		if (!this.chronicles[step]) {
			GUIp.common.error('data for step #' + step + ' is missing, colorizing map failed');
			return;
		}
		// check moving to or from the subfloor
		if (this.chronicles[step].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// if currently shown map is unrelated to this step - we just don't care and move on till we get again on the proper map
		if (isStepInCellar !== isMapInCellar) {
			GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,null); // this will do fine without coordinates or proper direction if it's not the dungeon of jumping
			continue;
		}
		// directionless step
		if (this.chronicles[step].directionless) {
			this.directionlessMoves = this.directionlessMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dlMoves')) || {};
			if (this.directionlessMoves[step]) {
				this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
				this.chronicles[step].directionless = false;
			} else {
				Object.assign(this.directionlessMoves,GUIp.common.calculateDirectionlessMove.call(ui_improver, ui_data.isMobile ? '.e_m_dmap' : '#map', coords, step));
				if (this.directionlessMoves[step]) {
					this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
					this.chronicles[step].directionless = false;
					ui_storage.set('Log:' + ui_data.logId + ':dlMoves',JSON.stringify(this.directionlessMoves));
				}
			}
		}
		// try to properly count the number of steps which had directional godvoices
		GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,coords);
		// normal step
		GUIp.common.moveCoords(coords, this.chronicles[step]);
		// wormhole jump
		if (this.chronicles[step].wormhole) {
			this.wormholeMoves = this.wormholeMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':whMoves')) || {};
			if (this.wormholeMoves[step]) {
				this.chronicles[step].wormholedst = this.wormholeMoves[step];
			} else if (step === steps_max) {
				GUIp.common.debug('getting wormhole target from actual coords mismatch');
				this.chronicles[step].wormholedst = [heroesCoords.y - coords.y, heroesCoords.x - coords.x];
				this.wormholeMoves[step] = this.chronicles[step].wormholedst;
				ui_storage.set('Log:' + ui_data.logId + ':whMoves',JSON.stringify(this.wormholeMoves));
			} else {
				var result = GUIp.common.calculateWormholeMove.call(ui_improver, ui_data.isMobile ? '.e_m_dmap' : '#map', coords, step);
				if (result.wm) {
					this.directionlessMoves = this.directionlessMoves || JSON.parse(ui_storage.get('Log:' + ui_data.logId + ':dlMoves')) || {};
					Object.assign(this.wormholeMoves,result.wm);
					Object.assign(this.directionlessMoves,result.dm);
					this.chronicles[step].wormholedst = this.wormholeMoves[step];
					ui_storage.set('Log:' + ui_data.logId + ':whMoves',JSON.stringify(this.wormholeMoves));
					ui_storage.set('Log:' + ui_data.logId + ':dlMoves',JSON.stringify(this.directionlessMoves));
				}
			}
			if (this.chronicles[step].wormholedst !== null) {
				if (mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
					currentCell = mapCells[coords.y].children[coords.x];
					GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount,true);
				}
				coords.y += this.chronicles[step].wormholedst[0];
				coords.x += this.chronicles[step].wormholedst[1];
			}
		}
		if (!mapCells[coords.y] || !mapCells[coords.y].children[coords.x]) {
			break;
		}
		currentCell = mapCells[coords.y].children[coords.x];
		if (currentCell.textContent.trim() === '#') {
			GUIp.common.error(
				'parsed chronicle does not match the map at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			break;
		}
		if (currentCell.textContent.trim() === '✖') {
			this.chronicles[step].chamber = true;
		} else if (currentCell.textContent.trim() === '◿') {
			this.chronicles[step].staircase = true;
		}
		if (this.chronicles[step].pointers.length > 0) {
			currentCell.dataset.pointers = this.chronicles[step].pointers.join(' ');
		}
		trapMoveLossCount = GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount);
	}
	if (heroesCoords.x !== coords.x || heroesCoords.y !== coords.y) {
		console.log(heroesCoords,coords);
		console.log(ui_improver.chronicles);
		console.log(JSON.stringify(ui_improver.chronicles));
		GUIp.common.error(
			'chronicle processing failed, coords diff: x: ' + (heroesCoords.x - coords.x) + ', y: ' + (heroesCoords.y - coords.y));
		if (ui_utils.hasShownInfoMessage !== true) {
			ui_utils.showMessage('info', {
				title: worker.GUIp_i18n.coords_error_title,
				content: '<div>' + worker.GUIp_i18n.coords_error_desc + ': [x:' + (heroesCoords.x - coords.x) + ', y:' + (heroesCoords.y - coords.y) + '].</div>'
			});
			ui_utils.hasShownInfoMessage = true;
		}
		// reset possibly wrong corrections to try again on the next step
		if (this.directionlessMoves) {
			Object.values(this.chronicles).forEach(function(ch) {
				if (ch.marks.includes('directionless')) {
					ch.direction = null;
					ch.directionless = true;
				}
			});
			this.directionlessMoves = null;
			ui_storage.set('Log:' + ui_data.logId + ':dlMoves','{}');
		}
		if (this.wormholeMoves) {
			this.wormholeMoves = null;
			ui_storage.set('Log:' + ui_data.logId + ':whMoves','{}');
		}
	}
	// check for evacuation
	coords = GUIp.common.calculateExitXY(isMapInCellar);
	if (steps_max > 20 && heroesCoords.x === coords.x && heroesCoords.y === coords.y) {
		this.parseDungeonSouls(true);
	}
	// save accumulated guided steps information
	ui_storage.set('Log:' + ui_data.logId + ':guidedSteps',JSON.stringify(this.dungeonGuidedSteps));
	// update broadcast link
	ui_utils.updateBroadcastLink(document.getElementById('e_broadcast'));
	ui_utils.updateBroadcastLink(document.querySelector('div.fightblink a'));
	// bind treasure pointer switchers
	GUIp.common.dmapDisabledPointersBind(function() {
		ui_storage.set('Log:' + ui_data.logId + ':dptr', JSON.stringify(GUIp.common.dmapDisabledPointersCache));
	}, GUIp.common.improveMap.bind(this, ui_data.isMobile ? '.e_m_dmap' : '#map'));
	// highlight treasury on the map
	GUIp.common.improveMap.call(this, ui_data.isMobile ? '.e_m_dmap' : '#map');
	// map settings
	var dmapSettings = ui_storage.getList('Option:dungeonMapSettings');
	// mark exclamations
	if (!this.isFirstTime && dmapSettings.includes('excl') && (['clarity','mystery'].some(function(type) { return ui_improver.checkParsedDungeonType(type); }) || Object.keys(GUIp.common.dmapExclCache).length)) {
		GUIp.common.dmapExcl();
		if (Object.keys(GUIp.common.dmapExclCache).length) {
			ui_storage.set('Log:' + ui_data.logId + ':excl', JSON.stringify(GUIp.common.dmapExclCache));
		}
	}
	// print dungeon coords
	GUIp.common.dmapCoords();
	// print dungeon dimensions
	var node = ui_data.isMobile ? document.getElementsByClassName('e_mt_dmap')[0] : document.querySelector('#map .block_title');
	if (node) {
		var mapDimensions;
		if (!(mapDimensions = node.getElementsByClassName('dmapDimensions')[0])) {
			mapDimensions = document.createElement('span');
			mapDimensions.classList.add('dmapDimensions');
			ui_utils.hideElem(mapDimensions, !dmapSettings.includes('dims'));
			node.insertBefore(mapDimensions,document.querySelector('#map .block_title #e_broadcast'));
		}
		mapDimensions.textContent = GUIp.common.dmapDimensions();
	}
};

ui_improver.parseDungeonExtras = function(impNodes, dungeonType, step) {
	var normalizedType, soffset, changed = false,
		xtras = this.dungeonExtras,
		types = xtras.types || [],
		challenges = {
			nook:      /специальной комнате|тайной комнаты|тайной ложи|запретное место| a nook |the nook's/i,
			silence:   /подрулившим гласами не более 8 раз|finding the treasure with 8 direction voices/i,
			agility:   /дошедшим до сокровища за 40 шагов|find the reward in 40 steps/i,
			ecology:   /не тронувшим ни одного босса|prize for avoiding all bosses/i,
			genocide:  /убившим всех боссов( на любом этаже)? здесь полагается награда|prize for hunting down all bosses/i,
			survivors: /до сокровища дойдут все. Либо только двое|whole team or exactly two of the members find/i
		},
		rewards = {
			resurrection: /павшие герои будут оживлены|reviving of knocked out heroes/i,
			key:          /найти ключ, открывающий сокровищницу|key to open the treasury/i,
			hints:        /указатели на сокровище можно включить только там|switch to turn on treasure clues/i,
			dice:         /все ловушки подземелья превратятся в тайники. Или наоборот|traps will turn into secret stashes... or vice versa/i,
			gold:         /найдется бонусная порция золота|extra gold in the treasury/i,
			artifacts:    /будет больше трофеев|more artifacts in the treasury/i,
			notraps:      /ловушки в подземелье отключатся|dungeon trap disabling switch/i,
			noboss:       /охраняющий сокровищницу босс уйдет со своего поста|ousting of the treasure boss/i,
			double:       /появится двойная порция бревен|find an extra log of gopher/i,
			clarity:      /отметятся все интересные места|location of all points of interest/i,
			transformation: /подземелье изменит свой тип|dungeon will change its type/i,
			unknown:      /не доставившего горному королю письмо|без каких-либо намеков на её смысл|nobody knows what the reward/i
		},
		dungeonTypes = { /* all possible dungeon types as of now */
			abundance:     /Abundance|Густоты/i,
			antiInfluence: /Anti-influence|Бессилия/i,
			clarity:       /Clarity|Заметности/i,
			demolition:    /Demolition|Сноса/i,
			emptiness:     /Emptiness|Пустоты/i,
			grace:         /Grace|Мольбы/i,
			halfTruth:     /Half Truth|Полуправды/i,
			highStakes:    /High Stakes|Риска/i,
			hoarding:      /Hoarding|Инкассации/i,
			hotness:       /Hotness|Термодинамики/i,
			hurry:         /Hurry|Спешки/i,
			jumping:       /Jumping|Прыгучести/i,
			migration:     /Migration|Миграции/i,
			mystery:       /Mystery|Загадки/i,
			normalcy:      /Normalcy|Обыденности/i,
			pledge:        /Pledge|Залога/i,
			rejuvenation:  /Rejuvenation|Бодрости/i,
			robbery:       /Robbery|Складчины/i,
			savings:       /Savings|Сбережения/i,
			solitude:      /Solitude|Заброшенности/i,
			surprise:      /Surprise|Бесшумности/i,
			toxicity:      /Toxicity|Токсичности/i,
			uncertainty:   /Uncertainty|Путаницы/i,
			wealth:        /Wealth|Достатка/i,
			woodness:      /Woodness|Изобилия/i
		},
		dungeonTypesTexts = {
			abundance:     /Almost every room here has something — either loot or a trap|Пустых комнат здесь почти нет, сплошь тайники да ловушки/i,
			antiInfluence: /Magic and influences don't work in this place because of a spell|Антибожественное покрытие туннелей экранирует влияния/i,
			clarity:       /Bright light illuminates interesting places on the map|Хорошее освещение позволяет издалека замечать интересные места/i,
			demolition:    /Miracles might destroy nearby walls|Здесь чудеса часто сносят прилегающие стены/i,
			emptiness:     /No traps, healing rooms or secret stashes here|Здесь нет ни мощных ловушек, ни серьезных лечилок, ни богатых тайников/i,
			grace:         /Almost every room in here gives a drop of godpower|Здесь почти каждая комната даёт по капле праны/i,
			halfTruth:     /Половина указателей на сокровищницу здесь врёт/i,
			highStakes:    /Winners get extra gold from the treasury, while those who are knocked out get nothing|Выжившие герои заберут все золото/i,
			hoarding:      /Most of the gold from the treasury is in the pockets of greedy bosses|Золото из сокровищницы боссы здесь носят с собой/i,
			hotness:       /All treasure hints in this dungeon play “Hot and Cold”|Указатели здесь работают по принципу «горячо-холодно»/i,
			hurry:         /In this dungeon heroes have 50 steps until evacuation instead of 100|Эвакуация здесь происходит через 50 ходов вместо 100/i,
			jumping:       /Anomalous gravity sometimes makes heroes jump over the squares|Постарайтесь не удариться головой при полетах через клетки/i,
			migration:     /Stronger bosses are not always near the treasure|Сила боссов здесь никак не связана с близостью сокровища/i,
			mystery:       /This dungeon is special, but nobody knows how|У подземелья есть особое свойство, но какое — непонятно/i,
			pledge:        /Everyone pledged one log of wood for a promise to leave the dungeon through the treasury|Здесь на входе у каждого берут в залог одно бревно/i,
			rejuvenation:  /The air here is alive and all healing|Здесь лечит сам воздух/i,
			robbery:       /Everyone contributed their gold to the treasury's fund|в призовой фонд сокровищницы, где и будет поделено выжившими/i,
			savings:       /After finding the treasure all heroes' gold will be automatically deposited|В сокровищнице вся наличность героев автоматически перейдёт в их сбережения/i,
			solitude:      /All bosses have left, leaving traps instead|Боссы ушли, расставив вместо себя ловушки/i,
			surprise:      /Quiet, but weak bosses tend to appear out of nowhere and attack all of a sudden|Боссы здесь тихие и ослабленные/i,
			toxicity:      /Toxic gas in the passages worsens the health of explorers|Ядовитый газ в коридорах подрывает здоровье исследователей/i,
			uncertainty:   /All direction arrows here are double-minded|Указатели на сокровищницу здесь никак не могут определиться/i,
			wealth:        /Gold seems to be scattered all over the place|Деньги тут валяются прямо под ногами/i,
			woodness:      /This dungeon has double the amount of wood in the treasury|В сокровищнице двойная порция бревен/i
		};
	if (!xtras.nookOffset) {
		xtras.nookOffset = GUIp.common.calculateSpecOffset('nook');
		changed = true;
	}
	if (!GUIp.common.isDungeonCellar() && !this.dungeonExtras.stairsOffset && (soffset = GUIp.common.calculateSpecOffset('stairs')).x !== null) {
		this.dungeonExtras.stairsOffset = soffset;
		changed = true;
	}
	if (impNodes.length) {
		Array.from(impNodes).map(function(a) { return a.textContent; }).forEach(function(text) {
			for (var key in challenges) {
				if (challenges[key].test(text) && key !== xtras.challenge) {
					xtras.challenge = key;
					for (var key in rewards) {
						if (rewards[key].test(text)) {
							xtras.reward = key;
							break;
						}
					}
					if (!xtras.reward) xtras.reward = 'unknown';
					changed = true;
					break;
				}
			}
			for (var key in dungeonTypesTexts) {
				if (dungeonTypesTexts[key].test(text) && !types.includes(key)) {
					types.push(key);
					xtras.types = types;
					changed = true;
				}
			}
		});
	}
	if (dungeonType) {
		dungeonType = dungeonType.textContent || dungeonType;
		normalizedType = Object.keys(dungeonTypes).find(function(key) { return dungeonTypes[key].test(dungeonType); }) || 'multi';
		if (normalizedType !== xtras.initialType) {
			xtras.type = xtras.initialType = normalizedType; // transformation challenge reward could change the dungeon type, so we need to store both declared dungeon type and possibly detected one
			xtras.typeStep = step;
			if (xtras.transformationPending) {
				xtras.transformationComplete = true;
				delete xtras.transformationPending;
			}
			changed = true;
		}
	}
	if (changed) {
		ui_storage.set('Log:' + ui_data.logId + ':extras',JSON.stringify(xtras));
	}
};

ui_improver.checkParsedDungeonType = function(type) {
	return this.dungeonExtras.type === type || this.dungeonExtras.type === 'multi' && this.dungeonExtras.types && this.dungeonExtras.types.includes(type);
};

ui_improver.getLocalizedDungeonType = function(type) {
	var dtypes = worker.GUIp_locale === 'ru' ? {
		abundance:     "Густоты",
		antiInfluence: "Бессилия",
		clarity:       "Заметности",
		demolition:    "Сноса",
		emptiness:     "Пустоты",
		grace:         "Мольбы",
		halfTruth:     "Полуправды",
		highStakes:    "Риска",
		hoarding:      "Инкассации",
		hotness:       "Термодинамики",
		hurry:         "Спешки",
		jumping:       "Прыгучести",
		migration:     "Миграции",
		mystery:       "Загадки",
		normalcy:      "Обыденности",
		pledge:        "Залога",
		rejuvenation:  "Бодрости",
		robbery:       "Складчины",
		savings:       "Сбережения",
		solitude:      "Заброшенности",
		surprise:      "Бесшумности",
		toxicity:      "Токсичности",
		uncertainty:   "Путаницы",
		wealth:        "Достатка",
		woodness:      "Изобилия"
	} : {
		abundance:     "Abundance",
		antiInfluence: "Anti-influence",
		clarity:       "Clarity",
		demolition:    "Demolition",
		emptiness:     "Emptiness",
		grace:         "Grace",
		halfTruth:     "Half Truth",
		highStakes:    "High Stakes",
		hoarding:      "Hoarding",
		hotness:       "Hotness",
		hurry:         "Hurry",
		jumping:       "Jumping",
		migration:     "Migration",
		mystery:       "Mystery",
		normalcy:      "Normalcy",
		pledge:        "Pledge",
		rejuvenation:  "Rejuvenation",
		robbery:       "Robbery",
		savings:       "Savings",
		solitude:      "Solitude",
		surprise:      "Surprise",
		toxicity:      "Toxicity",
		uncertainty:   "Uncertainty",
		wealth:        "Wealth",
		woodness:      "Woodness"
	};
	return dtypes[type] || 'unknown';
};

ui_improver.improveMutationChronicles = function(oldAbility, newAbility) {
	var node, chronicle, chronicles = document.querySelectorAll(ui_data.isMobile ? '.e_m_fight_log .d_msg:not(.parsed)' : '#m_fight_log .d_msg:not(.parsed)'),
		sort_ch = ui_data.isMobile && document.getElementsByClassName('sort_ch')[0],
		ch_down = !(sort_ch && sort_ch.textContent === '▲'),
		replaced = false;
	for (var i = 0, len = chronicles.length; i < len; i++) {
		var chronicle = chronicles[ch_down ? i : len - 1 - i];
		// this will add abilities swap details to latest mutation event chronicle entry
		// in assumtion that exatly this is the one related to the most recent mutation;
		// also mark everything else as 'parsed' to ensure that we won't touch them later
		if (!replaced && chronicle.textContent.startsWith('\uD83E\uDDEC ')) { /* 🧬 */
			chronicle.innerHTML = chronicle.innerHTML.replace('\uD83E\uDDEC', '<span class="e_mutated e_select_disabled">\uD83E\uDDEC</span>');
			replaced = true;
			if ((node = chronicle.getElementsByClassName('e_mutated')[0])) {
				node.title = ui_utils.capitalizeFirstLetter(oldAbility) + ' \u2192 ' /* → */ + ui_utils.capitalizeFirstLetter(newAbility);
				GUIp.common.tooltips.watchSubtree(node);
			}
		}
		chronicle.classList.add('parsed');
	}
};

/** @namespace */
ui_improver.reporter = {
	// module loading order is arbitrary, so we cannot simply assign a field
	/** @type {string} */
	get url() { return (worker.GUIp_locale === 'ru' ? GUIp.common.erinome_url : GUIp.common.erinome_url_en) + '/reporter'; },
	/** @type {string} */
	get urlMS() { return GUIp.common.erinome_url + '/mapStreamer'; },

	/**
	 * @param {string} logID
	 * @returns {string}
	 */
	getLogURL: function(logID) {
		return ui_data.isDungeon ? '/duels/log/' + logID + '?sort=desc&estreaming=1' : this.url + '/duels/log/' + logID;
	},

	_getHTML: function(id) {
		var node = document.getElementById(id);
		return node ? node.outerHTML : '';
	},

	/** @const {number} */
	protocolVersion: 3,

	/**
	 * @returns {!Object<string, (string|number)>}
	 */
	collect: function() {
		if (ui_data.isDungeon) {
			return {
				id: ui_data.logId,
				step: ui_stats.currentStep(),
				map: JSON.stringify(ui_improver.getDungeonMap()),
				lang: worker.GUIp_locale
			};
		}
		// https://github.com/Godvillers/ReporterServer/blob/master/docs/api.md
		var data = ['alls', 's_map', 'm_fight_log'].map(this._getHTML).join('<&>');
		return {
			protocolVersion: this.protocolVersion,
			agent: 'eGUI+/' + ui_data.currentVersion,
			link: GUIp.common.resolveURL(ui_utils.getStat('#fbclink', 'href') || worker.location.href),
			stepDuration: 18,
			timezone: -new Date().getTimezoneOffset(),
			step: ui_stats.currentStep(),
			playerNumber: ui_improver.islandsMap.ownArk + 1,
			cargo: ui_stats.cargo(),
			data: worker.base64js.fromByteArray(worker.pako.deflate(data)),
			clientData: '{"erinome":' + JSON.stringify({
				conditions: Object.keys(ui_improver.islandsMap.manager.conditions)
			}) + '}'
		};
	},

	/**
	 * @param {!Object} data
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	send: function(data, onsuccess, onfailure) {
		GUIp.common.postXHR((ui_data.isDungeon ? this.urlMS + '/port' : this.url + '/send'), data, 'form-data', onsuccess, onfailure);
	},

	/**
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	fetchAPIInfo: function(onsuccess, onfailure) {
		GUIp.common.getXHR(this.url + '/api.json', onsuccess, onfailure);
	},

	/**
	 * @param {string} text
	 * @returns {?Object<string, *>}
	 */
	parseAPIInfo: function(text) {
		try {
			var api = JSON.parse(text);
			return api.currentVersion >= api.minSupportedVersion ? api : null;
		} catch (e) {
			return null;
		}
	},

	/**
	 * @param {!Object} api
	 * @returns {boolean}
	 */
	checkVersion: function(api) {
		return this.protocolVersion >= api.minSupportedVersion && this.protocolVersion <= api.currentVersion;
	}
};

ui_improver.streamingManager = {
	onsuccess: [],
	onfailure: [],
	onincompatibility: [],
	lastUploadedStep: 0,

	get active() {
		return ui_storage.get('Log:streaming') === ui_data.logId;
	},

	set active(value) {
		ui_storage.set('Log:streaming', value ? ui_data.logId : '');
	},

	_runCallbacks: function(callbacks) {
		callbacks.forEach(function(f) { f(); });
	},

	uploadStep: function() {
		var self = this,
			reporter = ui_improver.reporter,
			step = ui_stats.currentStep();
		if (step <= this.lastUploadedStep) return;

		reporter.send(reporter.collect(), function() {
			// requests are asynchronous, so perform an extra check
			if (step > self.lastUploadedStep) {
				self.lastUploadedStep = step;
				self._runCallbacks(self.onsuccess);
			}
		}, function(xhr) {
			GUIp.common.error('cannot stream step ' + step + ' (' + xhr.status + '):', xhr.responseText);
			// investigate the problem
			if (xhr.status === 501 /*Not Implemented*/) {
				// stop trying to stream
				self.active = false;
				// apparently, we are using an outdated protocol - let's check that
				reporter.fetchAPIInfo(function(xhr) {
					var api = reporter.parseAPIInfo(xhr.responseText);
					if (api == null) {
						// got invalid JSON?
						GUIp.common.error("cannot parse Reporter API: '" + xhr.responseText + "'");
						self._runCallbacks(self.onfailure);
						return;
					}
					GUIp.common.info('version = ' + reporter.protocolVersion + ', api =', api);
					if (reporter.checkVersion(api)) {
						// our version seems to be OK, but we still got 501. Something mystical
						self._runCallbacks(self.onfailure);
					} else {
						// yes, we're indeed too ancient
						self._runCallbacks(self.onincompatibility);
					}
					// as said before, don't try again
				}, function(xhr) {
					GUIp.common.error('cannot fetch Reporter API:', xhr.status);
					self._runCallbacks(self.onfailure);
				});
				return;
			}
			// try again later
			GUIp.common.setTimeout(function() {
				if (step === ui_stats.currentStep()) {
					self.uploadStep();
				}
			}, 5e3);
		});
	}
};

ui_improver.initStreaming = function(container) {
	if (!container) {
		GUIp.common.error('streaming link injection failed');
		return;
	}

	container.insertAdjacentHTML('beforeend',
		'<div id="e_streaming">' +
			'<a href="#">' + GUIp_i18n.start_streaming + '</a>' +
			'<span class="hidden">' + GUIp_i18n.streaming_now +
				' <span class="e_emoji e_emoji_cross eguip_font">❌</span></span>' +
			'<a href="' + this.reporter.getLogURL(ui_data.logId) + '" target="_blank">' +
				GUIp_i18n.streaming_now +
			'</a>' +
			' <a class="e_cancel e_emoji e_emoji_cross eguip_font" href="#" title="' + GUIp_i18n.cancel_streaming +
			'">❌</a>' +
		'</div>'
	);
	container = document.getElementById('e_streaming');
	var startLink = container.firstChild,
		placeholder = startLink.nextSibling,
		streamingLink = placeholder.nextSibling,
		stopLink = container.lastChild, // streamingLink.nextSibling is a text node containing whitespace
		established = this.streamingManager.active;
	if (established) {
		startLink.classList.add('hidden');
		// streamingManager.uploadStep is called from elsewhere
	} else {
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
	}

	GUIp.common.addListener(startLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.add('hidden');
		if (!established) {
			placeholder.classList.remove('hidden');
		} else {
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
		ui_improver.streamingManager.active = true;
		ui_improver.streamingManager.uploadStep();
	});

	GUIp.common.addListener(stopLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.remove('hidden');
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
		ui_improver.streamingManager.active = false;
	});

	this.streamingManager.onsuccess.push(function() {
		if (!established) {
			established = true;
			placeholder.classList.add('hidden');
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
	});

	var handler = function(msg) {
		container.textContent = msg;
		container.classList.add('error');
	};
	this.streamingManager.onincompatibility.push(handler.bind(null, worker.GUIp_i18n.streaming_is_unsupported));
	this.streamingManager.onfailure.push(handler.bind(null, worker.GUIp_i18n.streaming_failed));
};

/** @namespace */
ui_improver.islandsMap = {
	/** @type {?GUIp.common.islandsMap.MigratingMVManager} */
	manager: null,

	_svg: null,

	/**
	 * @readonly
	 * @type {number}
	 */
	ownArk: -1,

	_saveData: function() {
		ui_storage.set('Log:current', ui_data.logId);
		ui_storage.set('Log:' + ui_data.logId + ':model',
			JSON.stringify(GUIp.common.islandsMap.conv.encode(this.manager.model))
		);
	},

	/**
	 * @private
	 * @returns {{model: ?GUIp.common.islandsMap.Model, relevant: boolean}}
	 */
	_tryLoadData: function() {
		var rawModel = GUIp.common.parseJSON(ui_storage.get('Log:' + ui_data.logId + ':model'));
		return rawModel ? {
			model: GUIp.common.islandsMap.conv.decode(rawModel),
			relevant: rawModel.step === ui_stats.currentStep()
		} : {model: null, relevant: false};
	},

	_getOwnArkIndex: function() {
		for (var i = 1; i <= 4; i++) {
			if (ui_stats.Hero_Ally_Name(i) === ui_data.char_name) {
				return i - 1;
			}
		}
		return -1;
	},

	_rollbackModifications: function(svg) {
		if (svg === this._svg) {
			GUIp.common.islandsMap.defaults.vTransUnbindAll(this.manager.model, this.manager.view);
		} else {
			this._svg = svg;
		}
	},

	_checkBeasties: function() {
		if (!GUIp.common.sailing.phrases.beastiesCount) {
			return;
		}
		var enemies = document.querySelectorAll('#opps .opp_n');
		for (var i = 0, len = enemies.length; i < len; i++) {
			var maxHP = ui_stats.EnemySingle_MaxHP(i + 1);
			if (maxHP >= 100) {
				continue; // an ark, apparently
			}
			var text = enemies[i].textContent.trim();
			if (text && !GUIp.common.sailing.isBeastie(text) && !enemies[i].classList.contains('improved')) {
				enemies[i].insertAdjacentHTML('beforeend',
					'<span class="e_beareport"> ' +
					'<a title="' + worker.GUIp_i18n.sea_monster_report +
					'" href="' + GUIp.common.erinome_url +
						'/beastiereport?name=' + encodeURIComponent(text) +
						'&hp=' + maxHP +
						'&locale=' + worker.GUIp_locale +
					'" target="_blank">[?]</a></span>'
				);
				enemies[i].classList.add('improved');
			}
		}
	},

	/**
	 * @private
	 * @param {!Element} mapContainer
	 * @param {number} ownArkPos
	 * @param {!Array<string>} settings
	 */
	_changeMapHeader: function(mapContainer, ownArkPos, settings) {
		var blockTitle, distInfo;
		if (!(blockTitle = (ui_data.isMobile ? document.querySelector('.e_mt_smap .l_header') : mapContainer.getElementsByClassName('block_title')[0]))) {
			return;
		}
		if (!(distInfo = blockTitle.getElementsByClassName('e_dist_info')[0])) {
			// opera 12.x can't insert multiple nodes in insertAdjacentHTML at once
			// when any ancestor node has DOMNodeInserted event attached
			blockTitle.appendChild(document.createTextNode(' '));
			blockTitle.insertAdjacentHTML('beforeend', '<span class="e_dist_info"></span>');
			distInfo = blockTitle.lastChild;
		}
		distInfo.innerHTML = ui_utils.formatSailingDistInfo(
			this.manager.model, ownArkPos, this.manager.conditions, settings
		);
	},

	_replaceModelAndView: function(step) {
		this.manager.replaceModelAndView(this._svg, step);
		this._saveData();
	},

	_applyModifications: function(mapContainer) {
		var model = this.manager.model,
			settings = ui_storage.getList('Option:islandsMapSettings'),
			ownArkPos = model.arks[this.ownArk],
			rivalsNearby = false;
		// (re)set monochrome icons if requested
		mapContainer.classList.toggle('e_monochrome', settings.includes('mch'));
		// apply changes to the SVG
		GUIp.common.islandsMap.defaults.vTransBindAll(
			model, this.manager.view, this.manager.conditions, settings, true
		);
		// detect missing beasties in our lists
		this._checkBeasties();
		// stream the sailing
		if (ui_improver.streamingManager.active) {
			// wait until beasties in the chronicle block are highlighted
			GUIp.common.setTimeout(function() { ui_improver.streamingManager.uploadStep(); }, 0);
		}

		if (ownArkPos !== 0x8080) {
			// remember distance to the port for custom informers
			ui_improver.sailPortDistance =
				GUIp.common.islandsMap.vec.dist(ownArkPos, model.port !== 0x8080 ? model.port : 0x0);
			// show distances to port & rim
			this._changeMapHeader(mapContainer, ownArkPos, settings);
			// check distances to other arks
			rivalsNearby = !this.manager.conditions.kindness && model.step >= 5 && model.arks.some(function(ark) {
				return ark !== 0x8080 && ark !== ownArkPos && GUIp.common.islandsMap.vec.dist(ark, ownArkPos) <= 3;
			});
		}
		ui_informer.update('close to rival', rivalsNearby);
	},

	_extractConditions: function(mapContainer) {
		var logID = ui_data.logId,
			key = 'Log:' + logID + ':conds',
			conditions = ui_storage.get(key);
		if (conditions != null) {
			this.manager.conditions = conditions ? GUIp.common.makeHashSet(conditions.split(',')) : Object.create(null);
			return;
		}

		conditions = GUIp.common.sailing.tryExtractConditions();
		if (conditions != null) {
			this.manager.conditions = GUIp.common.sailing.parseConditions(conditions);
			ui_storage.set(key, Object.keys(this.manager.conditions));
		} else {
			// TODO: retry on failure
			GUIp.common.requestLog(logID, function(xhr) {
				var conds;
				this.manager.conditions = GUIp.common.sailing.parseConditions(
					GUIp.common.sailing.extractConditionsFromHTML(xhr.responseText)
				);
				conds = Object.keys(this.manager.conditions);
				ui_storage.set(key, conds);
				if (conds.length) {
					// they could affect parsing the map
					this._rollbackModifications(mapContainer.getElementsByTagName('svg')[0]);
					this._replaceModelAndView(ui_stats.currentStep());
					this._applyModifications(mapContainer);
				}
			}.bind(this));
		}
	},

	_loadPOIColors: function() {
		if (!ui_storage.getList('Option:islandsMapSettings').includes('rndc')) {
			return;
		}

		var key = 'Log:' + ui_data.logId + ':poiColors',
			colors = GUIp.common.parseJSON(ui_storage.get(key)),
			colorizer = GUIp.common.islandsMap.vtrans.poiColorizer;
		if (GUIp.common.isIntegralArray(colors)) {
			colorizer.colors = colors;
		} else {
			GUIp.common.shuffleArray(colorizer.colors);
			ui_storage.set(key, '[' + colorizer.colors + ']');
		}
	},

	/**
	 * @param {!Element} mapContainer
	 */
	initMap: function(mapContainer) {
		var loaded = this._tryLoadData(), // instead of parsing each time the page is refreshed
			svgs = mapContainer.getElementsByTagName('svg'),
			config = {childList: true, subtree: true},
			observer;
		this.ownArk = this._getOwnArkIndex();
		this.manager = new GUIp.common.islandsMap.MigratingMVManager(loaded.model);
		this._extractConditions(mapContainer);
		this._loadPOIColors();

		// do initial processing
		this._svg = svgs[0]; // nothing to rollback; just assign
		if (loaded.relevant) {
			this.manager.replaceView(this._svg);
		} else {
			this._replaceModelAndView(ui_stats.currentStep());
		}
		this._applyModifications(mapContainer);

		// watch for changes
		observer = GUIp.common.islandsMap.observer.create(function() {
			var step = ui_stats.currentStep();
			this._rollbackModifications(svgs[0]);
			if (step !== this.manager.model.step) {
				this._replaceModelAndView(step);
			} else {
				// surprisingly, sometimes an SVG can be replaced with no changes to it.
				// no need to touch our model in that case.
				this.manager.replaceView(this._svg);
			}
			this._applyModifications(mapContainer);
		}, this);
		observer.observe(mapContainer, config);
		observer.observe(document.querySelector(ui_data.isMobile ? '.e_mt_fight_log .l_header' : '#m_fight_log h2'), config);
		GUIp.common.tooltips.watchSubtreeSVG(document.getElementById('map_wrap'));
	}
};

ui_improver._wrapCargoEmoji = function(emoji) {
	return (
		'<span class="e_emoji e_emoji_sailing' + (GUIp.common.renderTester.testChar(emoji) ? '' : ' eguip_font') +
		'">' + emoji + '</span>'
	);
};

ui_improver.improveCargo = function() {
	if (ui_data.isMobile) return; // possible selector: '#statusbar img[src="/images/sb/sb_ark_cargo.png"] + div'
	var node = document.querySelector('#hk_cargo .l_val'),
		html = '',
		newHTML = '';
	if (node.getElementsByClassName('e_emoji')[0]) {
		return;
	}
	html = node.innerHTML;
	newHTML = html.replace(/[♂♀]|\uD83D[\uDCB0\uDCE6]/g, ui_improver._wrapCargoEmoji); // ♂♀💰📦
	if (newHTML !== html) {
		node.innerHTML = newHTML;
	}
};

ui_improver.improveSailChronicles = function() {
	GUIp.common.sailing.describeBeastiesOnPage('.d_msg:not(.parsed)', 'parsed', ui_storage.getList('Option:islandsMapSettings').includes('bhp'));
};

ui_improver.initSailing = function() {
	var mapContainer = GUIp.common.sailing.tryFindMapBlock(ui_data.isMobile);
	if (!mapContainer) {
		GUIp.common.setTimeout(ui_improver.initSailing, 250);
		return;
	}
	var rulerContainer;
	if (ui_data.isMobile) {
		if (mapContainer.parentNode.parentNode.previousElementSibling) {
			// there's an unnamed empty header just before map wrapper suitable for the ruler
			rulerContainer = mapContainer.parentNode.parentNode.previousElementSibling;
			// and we'll use it for _changeMapHeader as well, so we need a way to search for it later
			rulerContainer.classList.add('e_mt_smap');
		} else {
			GUIp.common.error('failed to find sail map header container');
		}
	} else {
		rulerContainer = mapContainer.querySelector('.block_h .l_slot');
	}
	if (!rulerContainer) {
		GUIp.common.error('ruler injection failed');
	} else {
		rulerContainer.insertAdjacentHTML('beforeend',
			'<span id="e_ruler_button" class="e_emoji e_emoji_ruler eguip_font" title="' + GUIp_i18n.sail_ruler +
			'">📏</span>'
		);
		GUIp.common.islandsMap.vtrans.rulerManager.init(rulerContainer.lastChild);
	}
	if (!ui_data.isMobile) { // not supported for now
		ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content'));
	}
	ui_improver.islandsMap.initMap(mapContainer);
	ui_improver.improveCargo();
	ui_improver.improveSailChronicles();
};

ui_improver.whenWindowResize = function() {
	ui_improver.chatsFix();
	// sail mode column resizing
	if (ui_improver.sailPageResize && !ui_data.isMobile) {
		var sccwidth,
			sclwidth = worker.$('#a_left_block').outerWidth(true),
			scrwidth = worker.$('#a_right_block').outerWidth(true),
			scl2width = worker.$('#a_left_left_block'),
			scr2width = worker.$('#a_right_right_block'),
			maxwidth = worker.$(worker).width() * 0.85;
		scl2width = scl2width[0] && scl2width[0].offsetWidth ? scl2width.outerWidth(true) : 0;
		scr2width = scr2width[0] && scr2width[0].offsetWidth ? scr2width.outerWidth(true) : 0;
		sccwidth = Math.max(Math.min((maxwidth - sclwidth - scl2width - scrwidth - scr2width),932),448); // todo: get rid of hardcoded values?
		worker.$('#a_central_block').width(sccwidth);
		sccwidth = worker.$('#a_central_block').outerWidth(true);
		//worker.$('#main_wrapper').width(sccwidth + sclwidth + scl2width + scrwidth + scr2width + 20);
		worker.$('.c_col .block_title').each(function() { worker.$(this).width(sccwidth - 115); });
	}
	// body widening
	worker.$('body').width(worker.$(worker).width() < worker.$('#main_wrapper').width() ? worker.$('#main_wrapper').width() : '');
};
ui_improver._clockUpdate = function() {
	worker.$(ui_data.isMobile ? '.e_mt_remote .l_header' : '#control .block_title').text(ui_utils.formatClock(ui_utils.getPreciseTime(ui_improver.clock.offset)));
	if (Date.now() > ui_improver.clock.switchOffTime) {
		ui_improver._clockToggle();
	}
};
ui_improver._clockToggle = function(e) {
	if (e) {
		e.stopPropagation();
	}
	if (ui_improver.clockToggling) {
		return;
	}
	ui_improver.clockToggling = true;
	var $clock = worker.$(ui_data.isMobile ? '.e_mt_remote .l_header' : '#control .block_title');
	if (ui_improver.clock) {
		worker.clearInterval(ui_improver.clock.updateTimer);
		$clock.fadeOut(500, GUIp.common.try2.bind(ui_improver.clock.prevText, function() {
			$clock.removeClass('e_clock_inaccurate');
			$clock.text(this).fadeIn(500);
			$clock.prop('title', worker.GUIp_i18n.show_godville_clock);
			ui_improver.clockToggling = false;
		}));
		ui_improver.clock = null;
	} else {
		ui_improver.clock = {
			prevText: $clock.text(),
			switchOffTime: Date.now() + 300e3, // 5 min
			offset: ui_storage.getFlag('Option:localtimeGodvilleClock') ? (
				new Date().getTimezoneOffset() * -60e3
			) : (ui_storage.get('Option:offsetGodvilleClock') || 3) * 3600e3,
			updateTimer: 0
		};
		var ok = false;
		Promise.all([
			ui_utils.syncClock().then(function(success) {
				ok = success;
			}),
			new Promise(function(resolve) {
				$clock.fadeOut(500, GUIp.common.try2.bind(null, function() {
					$clock.fadeIn(500);
					ui_improver.clockToggling = false;
					if (!ok) {
						$clock.text('––:––:––').addClass('e_clock_inaccurate');
					}
					$clock.prop('title', worker.GUIp_i18n.hide_godville_clock);
					resolve();
				}));
			})
		]).then(function() {
			if (!ui_improver.clock || ui_improver.clock.updateTimer) {
				return; // user hid the clock before it got synced
			}
			if (ok) {
				ui_improver._clockUpdate();
				ui_improver.clock.updateTimer = GUIp.common.setTimeout(function() {
					ui_improver._clockUpdate();
					ui_improver.clock.updateTimer = GUIp.common.setInterval(ui_improver._clockUpdate, 1e3);
				}, 1e3 - ui_utils.getPreciseTime().getMilliseconds());
			} else {
				// ui_improver.clockToggling = false;
				ui_improver._clockToggle();
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};

ui_improver.improveInterface = function() {
	var node, handler;
	if (this.isFirstTime) {
		worker.$('a[href="#"]').removeAttr('href');
		ui_improver.whenWindowResize();
		GUIp.common.addListener(worker, 'resize', GUIp.common.debounce(250, ui_improver.whenWindowResize));
		ui_utils.loggerWidthChanger();
		if (ui_storage.getFlag('Option:themeOverride')) {
			ui_utils.switchTheme(ui_storage.get('ui_s'),true);
		} else {
			ui_utils.switchTheme();
		}
		GUIp.common.addListener(worker, 'focus', function() {
			ui_improver.checkGCMark('focus');
			ui_informer.updateCustomInformers();
			// forcefully fire keyup event on focus to prevent possible issues with alt key processing
			worker.$(document).trigger(worker.$.Event('keyup', {originalEvent: {}}));
		});
		// cache private messages
		ui_utils.pmNotificationInit();
		// experimental keep-screen-awake feature for Android
		if (GUIp.common.isAndroid && (worker.navigator.requestWakeLock || worker.navigator.wakeLock)) {
			ui_improver.createWakelock();
		}
		// generate nearby town information on usual place
		if (!ui_data.isFight) {
			// stupid hack to initialize the missing towns map on the page for mobile view
			// this works based on a (possibly) bug that the map stays on page after being closed
			if (ui_data.isMobile) {
				node = document.getElementById('tmap_icon');
				if (node) {
					node.click(); // map appears
					node.click(); // map disappears
					ui_improver.nearbyTownsFix();
				}
			} else {
				ui_improver.nearbyTownsFix();
			}
		} else if (ui_data.isMobile && (node = document.getElementById('dmap_icon'))) {
			// same thing as hmap and same bug goes for dungeons and their map
			node.click();
			// wup observers are too slow, we'll just run improver directly
			this.improveDmapMenu();
			// and immediately close the map like no one has seen it!
			node.click();
		}
		if (!ui_data.isFight && !ui_storage.getFlag('Option:disableGodvilleClock') && (node = document.querySelector(ui_data.isMobile ? '.e_mt_remote .l_header' : '#control .block_title'))) {
			node.title = worker.GUIp_i18n.show_godville_clock;
			node.style.cursor = 'pointer';
			GUIp.common.addListener(node, 'click', ui_improver._clockToggle);
		}
		var encButton = worker.$('#cntrl .enc_link'),
			punButton = worker.$('#cntrl .pun_link'),
			mirButton = worker.$('#cntrl .mir_link');
		if (ui_storage.getFlag('Option:improveDischargeButton')) {
			var dischargeHandlers, dischargeButton = worker.$('#acc_links_wrap .dch_link');
			encButton.click(function() { ui_improver.last_infl = 'enc'; });
			punButton.click(function() { ui_improver.last_infl = 'pun'; });
			mirButton.click(function() { ui_improver.last_infl = 'mir'; });
			if (dischargeButton.length) {
				dischargeHandlers = worker.$._data(dischargeButton[0],'events');
				if (dischargeHandlers && dischargeHandlers.click && dischargeHandlers.click.length === 1) {
					dischargeButton.click(function() {
						try {
							var limit = ui_stats.Max_Godpower() - 50;
							if (ui_improver.dailyForecast.get().includes('accu70')) {
								limit -= 20;
							}
							if (worker.$('#voice_submit').attr('disabled') === 'disabled') {
								limit += 5;
							}
							if (!worker.$('#cntrl .enc_link').hasClass('div_link')) {
								if (ui_improver.last_infl === 'mir') {
									limit += 50;
								} else {
									limit += 25;
								}
							}
							GUIp.common.debug('acc_discharge dynamic limit =', limit);
							localStorage.setItem('gp_thre',limit);
						} catch (e) {
							GUIp.common.error('setting discharge dynamic limit failed:', e);
						}
					});
					dischargeHandlers.click.reverse();
				}
			}
		}
		if (!ui_data.isFight) {
			ui_utils.addGPConfirmation(encButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_encourage_hero' : 'to_encourage_heroine']));
			ui_utils.addGPConfirmation(punButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_punish_hero' : 'to_punish_heroine']));
			ui_utils.addGPConfirmation(mirButton, 50,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n.to_make_a_miracle));
		}
		// hide_bless_button
		var bless_button = Array.from(document.querySelectorAll(ui_data.isMobile ? '#cntrl + .line > a.div_link' : '#stats .line > a.div_link')).find(function (a) { return /Благословить|Bless/.test(a.textContent) });
		if (bless_button) {
			bless_button.parentNode.classList.add('e_bless');
			bless_button.parentNode.style.display = ui_storage.getFlag('Option:hideBlessButton') ? 'none' : '';
		}
		// remove the mean 'new' label in a stupid blue box in the upgrader link (or any possible links in the future)
		var upgrader = Array.from(document.querySelectorAll(ui_data.isMobile ? '.e_mt_stats ~ .line > a.div_link' : '#stats .line > a.div_link')).find(function (a) { return a.firstChild.nodeType === 3 && a.firstChild.textContent.endsWith(' \uD83C\uDD95'); });
		if (upgrader) {
			upgrader.firstChild.textContent = upgrader.firstChild.textContent.replace(/ \uD83C\uDD95/g,''); /* 🆕 */
		}
		if (worker.GUIp_browser === 'Firefox') {
			try {
				worker.$(document).bind('click', function(e) {
					if (e.which !== 1) {
						e.stopImmediatePropagation();
					}
				});
				var rmClickFix, clickHandlers = worker.$._data(document,'events');
				if (clickHandlers.click && clickHandlers.click.length) {
					rmClickFix = clickHandlers.click.pop();
					clickHandlers.click.splice(clickHandlers.click.delegateCount || 0,0,rmClickFix);
				}
			} catch (e) {
				GUIp.common.error('failed to init rmclickfix workaround:', e);
			}
		}
		if (ui_data.isMobile) {
			// better tabs swiping (with shiny sparkles)
			var $hEvents, $hBlock = worker.$('#hero_block');
			// make sure we actually have a block and there's only one touchstart handler bound to it
			// in case there is more, then probably something has changed on page and we'd better won't get into it
			if ($hBlock.length && ($hEvents = $._data($hBlock[0],'events')) && $hEvents.touchstart && $hEvents.touchstart.length === 1) {
				// basically detect any touch on hero block
				$hBlock[0].addEventListener('touchstart', GUIp.common.try2.bind(null, function(e) {
					// respect original restrictions
					if (worker.$(e.target).parents('.tab-selector').length > 0 || worker.$(e.target).parents('#map_wrap').length || document.body.classList.contains('stop-scrolling')) {
						return;
					}
					// then begin building our own crutches
					var touchStart = e.touches[0] || e.changedTouches[0],
						diffX = 0, // shift in pixels
						diffY = 0,
						gap = 35, // shift less than this won't lead to tab swiping
						shiftStarted = false,
						scrollPos,
						tabButtons = document.querySelectorAll('#tabbar .tab-selector > .tab-btn'),
						tabContents = document.querySelectorAll('#tabbar .tab-content > div'),
						currentIdx = Array.from(tabButtons[0].parentNode.children).indexOf(tabButtons[0].parentNode.getElementsByClassName('active')[0]), // any better idea how to get current tab?
						prevIdx = (currentIdx + tabContents.length - 1) % tabContents.length,
						nextIdx = (currentIdx + 1) % tabContents.length,
						currentWidth = worker.$(tabContents[currentIdx]).width();
					// technically we almost repeat the vanilla event scheme only changing the details... a lot
					var touchMove = GUIp.common.try2.bind(null, function(e) {
						var touchMove = e.touches[0] || e.changedTouches[0];
						diffX = touchMove.clientX - touchStart.clientX;
						diffY = touchMove.clientY - touchStart.clientY;
						// note that diffX should be much larger than diffY to swipe, which should make vertical scrolling more solid
						if (Math.abs(diffX) > gap && Math.abs(diffY/diffX) < 0.25) {
							if (!shiftStarted) {
								shiftStarted = true;
								// default dragging class
								$hBlock.addClass('mpage_drag');
								// keeping this open while swiping would look weird, although vanilla kept it in place till touchend
								worker.$(document).trigger('close.wup');
								// add required styles for low-quality magic
								scrollPos = worker.pageYOffset;
								tabContents[currentIdx].style.top = (tabContents[currentIdx].offsetTop - scrollPos) + 'px';
								tabContents[currentIdx].style.width    = tabContents[prevIdx].style.width    = tabContents[nextIdx].style.width    = currentWidth + 'px';
								tabContents[currentIdx].style.position = tabContents[prevIdx].style.position = tabContents[nextIdx].style.position = 'fixed';
							}
							// this time we start shifting the tab considering the gap size too, so it won't jump around
							tabContents[currentIdx].style.left = diffX + (diffX > 0 ? -gap : gap) + 'px';
							// and partially show adjacent tabs
							if (diffX > 0) {
								tabContents[prevIdx].style.left = (-currentWidth + diffX - gap) + 'px';
								tabContents[prevIdx].style.display = '';
								tabContents[nextIdx].style.display = 'none';
							} else {
								tabContents[nextIdx].style.left = (currentWidth + diffX + gap) + 'px';
								tabContents[nextIdx].style.display = '';
								tabContents[prevIdx].style.display = 'none';
							}
						}
					});
					// when action has ended
					var touchEnd = GUIp.common.try2.bind(null, function(e) {
						// if we even started
						if (shiftStarted) {
							var targetIdx = diffX > 0 ? prevIdx : nextIdx,
								animationCnt = 0;
							// vanilla drag class whatever it does should be gone here
							$hBlock.removeClass('mpage_drag');
							// function to cleaning after ourselves, and pretending like nothing has happened
							var cleanStyles = function(visibleIdx) {
								// iterate through all participating tabs
								[currentIdx, prevIdx, nextIdx].forEach(function(a) {
									tabContents[a].style.position = '';
									tabContents[a].style.top = '';
									tabContents[a].style.left = '';
									tabContents[a].style.width = '';
									tabContents[a].style.display = visibleIdx === a ? '' : 'none';
								});
								// also we have a chance to close possibly opened tooltips we accidentally created during swiping
								Array.from(document.getElementsByClassName('e_tooltip')).forEach(function (a) {
									a.parentNode.removeChild(a);
								});
							};
							// function to do when animation finishes
							var afterAnimation = function(swiped) {
								// we do have two different animations and no nice way to do something after _both_ completes
								if (++animationCnt < 2) {
									return;
								}
								// whether we actually changed the tab
								if (swiped) {
									// remove unnecessary styles from content containers leaving target container visible
									cleanStyles(targetIdx);
									// and then officially move to another tab; won't emulate original code but rather just naively click on the tab-selector instead
									tabButtons[targetIdx].click();
								} else {
									// remove unnecessary styles from content containers leaving old container visible
									cleanStyles(currentIdx);
									// restore old scroll position
									worker.scrollTo(null,scrollPos);
								}
							}
							// actual actions: if we've shifted far enough, let's switch there
							if (Math.abs(diffX) > currentWidth / 3) {
								// do some fancy animations (probably would be better to use css animations one day)
								$(tabContents[currentIdx]).animate({left: (diffX > 0 ? currentWidth : -currentWidth)}, 225, GUIp.common.try2.bind(null, afterAnimation, true));
								$(tabContents[targetIdx]).animate({left: 0}, 225, GUIp.common.try2.bind(null, afterAnimation, true));
							} else {
								$(tabContents[currentIdx]).animate({left: 0}, 150, GUIp.common.try2.bind(null, afterAnimation, false));
								$(tabContents[targetIdx]).animate({left: (diffX > 0 ? -currentWidth : currentWidth)}, 150, GUIp.common.try2.bind(null, afterAnimation, false));
							}
							// also please don't propagate it anywhere
							e.stopPropagation();
						}
						// and we must remove unnecessary events when they aren't needed anymore
						document.removeEventListener('touchmove', touchMove);
						document.removeEventListener('touchend', touchEnd, true);
					});
					// we use low-level event listeners here because:
					// 1. jquery's do not allow us to useCapture, and because of that a number of issues happens all around
					//    when vanilla's item craft uses a touchend handler which boldly stops our code it the middle of action
					// 2. GUIp's addListener doesn't provide a way to remove it when we're finished
					document.addEventListener('touchmove', touchMove);
					document.addEventListener('touchend', touchEnd, true);
				}));
				// get rid of default touchstart handler
				worker.$($hBlock).off('touchstart');
			}
		}
		GUIp.common.addListener(document, 'visibilitychange', function() {
			if (document.visibilityState !== 'hidden') {
				return;
			}
			// save unsent personal messages
			if (ui_data.isMobile) {
				ui_utils.saveMobileUnsentMessage(document.querySelector('#hero_block .mpage:not(#tabbar)'));
			} else {
				var node = document.getElementsByClassName('chat_ph')[0], text;
				if (node) {
					var textareas = node.getElementsByTagName('textarea'),
						names = node.getElementsByClassName('dockfrname');
					for (var i = 0, len = Math.min(textareas.length, names.length); i < len; i++) {
						node = textareas[i];
						if (!node.disabled) {
							if ((text = node.value)) {
								ui_tmpstorage.set('PM:' + names[i].textContent, text);
							} else {
								ui_tmpstorage.remove('PM:' + names[i].textContent);
							}
						}
					}
				}
			}
		});
		if ((node = document.getElementById('logout')) ||
			ui_data.isMobile && (node = document.querySelector('.e_mt_menu + .line + .line + .line + .line') /* best selector ever! */ ) && /Выход|Logout/.test(node.textContent)) {
			GUIp.common.addListener(node, 'click', ui_tmpstorage.wipeOut.bind(ui_tmpstorage));
		}
		// workaround for title-related issues
		Object.defineProperty(document,'title', {
			set: function(val) {
				try {
					ui_data.docTitle = val;
					ui_informer.redrawTitle();
					return val;
				} catch (e) { return val; }
			},
			get: function() {
				try {
					return document.querySelector('title').childNodes[0].nodeValue;
				} catch (e) { return ''; }
			},
			configurable: true
		});
		ui_storage.addImmediateListener('Option:discardTitleChanges', ui_informer.redrawTitle.bind(ui_informer));
		ui_storage.addImmediateListener('UserCss', GUIp.common.addCSSFromString);
	}
	if (ui_data.isFight && !document.getElementById('e_broadcast')) {
		var qselector = document.querySelector(ui_data.isDungeon ? '#map .block_title, #control .block_title, #m_control .block_title, .e_mt_remote div' : '#control .block_title, #m_control .block_title, .e_mt_remote div');
		if (qselector) {
			if ((ui_data.isSail || ui_data.isMining) && worker.GUIp_locale === 'en') {
				qselector.textContent = 'Remote';
			}
			qselector.insertAdjacentHTML('beforeend', '<a id="e_broadcast" class="broadcast" href="/duels/log/' + ui_data.logId + ui_utils.generateBroadcastLinkXtra() + '" target="_blank">' + worker.GUIp_i18n.broadcast + '</a>');
		}
	}
	if (!document.getElementById('e_custom_informers_setup') && ui_informer.activeInformers.get().custom_informers) {
		ui_utils.generateLightboxSetup('custom_informers','#stats .block_title, #m_info .block_title, #b_info .block_title',function() { ui_words.init(); ui_informer.purgeFlags(); });
	}
	if (!ui_data.isFight && !document.getElementById('e_custom_craft_setup') && ui_storage.getFlag('Option:enableCustomCraft') && !ui_data.isMobile) {
		ui_utils.generateLightboxSetup('custom_craft','#inventory .block_title',function() { ui_words.init(); ui_inventory.rebuildCustomCraft(); ui_improver.calculateButtonsVisibility(true); });
		ui_storage.addListener('Option:fixedCraftButtons', ui_improver.calculateButtonsVisibility.bind(ui_improver, true));
	}
	if (ui_data.isFight && !document.getElementById('e_ally_blacklist_setup') && document.querySelector('#alls .block_title, #bosses .block_title, #o_info .block_title')) {
		ui_utils.generateLightboxSetup('ally_blacklist',(document.querySelector('#o_info .l_val a[href*="/gods/"]') ? '#o_info' : '#alls')+' .block_title, #bosses .block_title',function() { ui_words.init(); ui_improver.improvePlayers(); });
	}
	if (this.isFirstTime) {
		handler = ui_words.init.bind(ui_words);
		ui_storage.addListener('CustomWords:custom_informers', function() {
			ui_words.init();
			ui_informer.purgeFlags();
		});
		if (ui_data.isFight) {
			ui_storage.addListener('CustomWords:ally_blacklist', function() {
				ui_words.init();
				ui_improver.improvePlayers();
			});
		} else {
			ui_storage.addListener('CustomWords:chosen_monsters', handler);
			ui_storage.addListener('CustomWords:special_monsters', handler);
			ui_storage.addListener('CustomWords:custom_craft', function() {
				ui_words.init();
				ui_inventory.rebuildCustomCraft();
				ui_improver.calculateButtonsVisibility(true);
			});
		}
		ui_storage.addImmediateListener('Option:useBackground', GUIp.common.setPageBackground);
	}
};
/**
 * @param {!Element} root
 * @returns {!Array<!GUIp.common.activities.LastFightsEntry>}
 */
ui_improver.getLastFights = function(root) {
	var ftypes = root.getElementsByClassName('wl_ftype');
	if (!ftypes.length) return [];
	root = ftypes[0].parentNode.parentNode;
	var dates = root.getElementsByClassName('wl_date'),
		result = [],
		ftype, link, href;
	for (var i = 0, len = Math.min(ftypes.length, dates.length); i < len; i++) {
		ftype = ftypes[i];
		link = ftype.getElementsByTagName('a')[0];
		href = link.href;
		result[i] = {
			date: +GUIp.common.parseDateTime(dates[i].textContent),
			type: GUIp.common.activities.parseFightType(link.textContent),
			logID: href.slice(href.lastIndexOf('/') + 1),
			success: ftype.textContent.includes('✓')
		};
	}
	// replicate souls from dungeons (other ideas?)
	if (ui_data.availableGameModes.includes('souls'))
	for (var i = 0, len = result.length; i < len; i++) {
		if (result[i].type !== 'dungeon') {
			continue;
		}
		result.push({
			date: result[i].date,
			type: 'souls',
			logID: result[i].logID,
			success: result[i].success
		});
	}
	return result.sort(ui_utils.byDate);
};
ui_improver.improveLastFights = function() {
	var popover = document.getElementById('lf_popover_c');
	if (!popover || ui_utils.isAlreadyImproved(popover)) {
		return;
	}
	var wup = $(popover).closest('.wup')[0],
		wupStyle = wup.style;
	wupStyle.width = ((wupStyle.width ? parseInt(wupStyle.width) : wup.offsetWidth) + 30) + 'px';
	ui_utils.observeUntil(popover, {childList: true, subtree: true}, function() {
		var fights = ui_improver.getLastFights(popover);
		if (fights.length) return fights;
	}).then(function(fights) {
		var activities = ui_timers.updateLastFights(fights),
			byID = Object.create(null),
			soulsCombined = Object.create(null),
			links = popover.querySelectorAll('.wl_ftype.wl_stats a'),
			desc = null,
			i, len, act, link, id, content;
		for (i = 0, len = activities.length; i < len; i++) {
			act = activities[i];
			if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
				byID[act.logID] = act;
			} else if (act.type === 'souls') {
				id = act.logID || act.inID;
				if (!soulsCombined[id]) {
					soulsCombined[id] = act;
					soulsCombined[id].src = 1;
				} else {
					if (soulsCombined[id].result < 0) {
						soulsCombined[id].result = act.result; // actually this never should happen
					} else if (act.result >= 0) {
						soulsCombined[id].result += act.result;
					}
					soulsCombined[id].src++;
				}
			}
		}
		for (i = 0, len = links.length; i < len; i++) {
			link = links[i];
			id = link.href;
			id = id.slice(id.lastIndexOf('/') + 1);
			content = '';
			if ((act = soulsCombined[id]) && (act.result || act.src < 2)) {
				desc = GUIp.common.activities.describe(act, desc);
				content += '<span class="' + desc.class + '" title="' + desc.title + '">' + desc.content + '</span>';
			}
			if ((act = byID[id]) && act.result) {
				desc = GUIp.common.activities.describe(act, desc);
				content += '<span class="' + desc.class + '" title="' + desc.title + '">' + desc.content + '</span>';
			}
			if (content) {
				link.parentNode.insertAdjacentHTML('afterend',
					'<div class="e_fight_result" data-e-id="' + id + '">' + content + '</div>'
				);
			}
		}
	}).catch(GUIp.common.onUnhandledException);
};
/**
 * @param {string} activitiesStr
 */
ui_improver.redrawLastFights = function(activitiesStr) {
	var root = document.getElementById('lf_popover_c'),
		desc = null,
		nodes, activities, byID, soulsCombined, i, len, id, act, node, span;
	if (!root) return;
	nodes = Array.from(root.getElementsByClassName('e_fight_result_unknown'));
	if (!nodes.length) return;
	activities = JSON.parse(activitiesStr);
	byID = Object.create(null);
	soulsCombined = Object.create(null);
	for (i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
			byID[act.logID] = act;
		} else if (act.type === 'souls') {
			id = act.logID || act.inID;
			if (!soulsCombined[id]) {
				soulsCombined[id] = act;
				soulsCombined[id].src = 1;
			} else {
				if (soulsCombined[id].result < 0) {
					soulsCombined[id].result = act.result;
				} else if (act.result >= 0) {
					soulsCombined[id].result += act.result;
				}
				soulsCombined[id].src++;
			}
		}
	}
	for (i = 0, len = nodes.length; i < len; i++) {
		node = nodes[i].parentNode;
		if ((act = soulsCombined[node.dataset.eId]) && act.result >= 0 && act.src >= 2 || (act = byID[node.dataset.eId]) && act.result >= 0) {
			desc = GUIp.common.activities.describe(act, desc);
			if (span = node.getElementsByClassName('e_fight_result_unknown_' + act.type)[0]) {
				span.title = desc.title;
				span.textContent = desc.content;
				span.classList.remove('e_fight_result_unknown_' + act.type);
				if (desc.class) {
					span.classList.add(desc.class);
				}
			}
		}
	}
};
ui_improver.improveLastVoices = function() {
	if (!document.querySelector('#gv_popover_c .gv_list_empty')) {
		return;
	}
	var lvContent = document.querySelectorAll('.lv_text .div_link_nu');
	if (!lvContent.length) {
		return;
	}
	var handler = ui_utils.triggerChangeOnVoiceInput.bind(ui_utils);
	for (var i = 0, len = lvContent.length; i < len; i++) {
		GUIp.common.addListener(lvContent[i], 'click', handler);
	}
};
ui_improver.improveStoredPets = function() {
	var wtitle = document.querySelector('.wup-inner > .wup-title');
	if (!wtitle || !/Ковчег|Ark|Питомцы|Pets/.test(wtitle.textContent)) {
		return;
	}
	var splinks = wtitle.parentNode.querySelectorAll('.wup-content .wup_line > a:not(.no_link)');
	if (!splinks.length) {
		return;
	}
	var sp = [];
	for (var i = 0, len = splinks.length; i < len; i++) {
		sp.push(splinks[i].textContent.toLowerCase());
	}
	ui_storage.set('charStoredPets',JSON.stringify(sp));
	ui_data.storedPets = sp;
};
/**
 * @private
 * @param {!GUIp.inventory.Model} inv
 * @returns {!Object<string, !Array<{type: string, bossAndLevel: string}>>}
 */
ui_improver._getBossParts = function(inv) {
	var result = {},
		itemName = '',
		part, parts;
	for (var i = 0, len = inv.length; i < len; i++) {
		if (!inv.isBossPart(i)) {
			continue;
		}
		itemName = inv.getName(i);
		part = ui_inventory.splitBossPart(itemName);
		if ((parts = result[part.type])) {
			parts.push(part);
		} else {
			result[part.type] = [part];
		}
		part.type = itemName; // reusing existing field
	}
	return result;
};
ui_improver.improveLab = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeLab = function(mutations,observer) {
			if (!container.querySelector('.bps_mf.wup_line')) {
				return;
			}
			// improve boss parts list
			var itm, bSrc, parts,
				enemyName = ui_stats.Enemy_Bossname(),
				bpLines = container.querySelectorAll('.wup_line.bps_line'),
				// `ui_inventory` is not initialized in duel modes so we force scanning
				available = ui_improver._getBossParts(ui_data.isDungeon ? ui_inventory.getInventory().md : ui_inventory.model),
				toggleHighlighting = function() {
					var bossName = this.textContent,
						highlighted = this.classList.toggle('e_match_part'),
						part;
					for (var i = 0, len = bpLines.length; i < len; i++) {
						if ((part = bpLines[i].getElementsByClassName('bps_val')[0])) {
							part.classList.toggle('e_match_part', highlighted && part.textContent === bossName);
						}
					}
				};
			for (var i = 0, len = bpLines.length; i < len; i++) {
				itm = bpLines[i].querySelector('div.bps_val');
				bSrc = itm.textContent;
				GUIp.common.addListener(itm, 'click', toggleHighlighting);
				if (ui_data.isBoss) {
					if (bSrc.startsWith(enemyName) || (bSrc.startsWith(enemyName, 5) && ui_stats.Enemy_HasAbility(/зовущий|summoning/))) {
						itm.classList.add('e_current_boss');
					}
				} else if ((parts = available[GUIp_i18n.bp_types[i]])) {
					for (var j = 0, jlen = parts.length; j < jlen; j++) {
						if (parts[j].bossAndLevel === bSrc) {
							continue; // exactly the same part; ignore
						}
						if (!itm.title) {
							itm.title = worker.GUIp_i18n.bp_others;
							itm.classList.add('e_new_part');
							GUIp.common.tooltips.watchSubtree(itm);
						}
						itm.title += '\n · ' + parts[j].type; // actually, this field contains item's name
					}
				}
			}
			// update logger with lab creatures
			ui_logger.update(ui_logger.labWatchers);
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeLab);
		observer.observe(container, {subtree: true, childList: true});
		observeLab(null, observer);
	}
};
ui_improver.improveForge = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeForge = function(mutations,observer) {
			if (!container.getElementsByClassName('dm_map_header')[0]) {
				return;
			}
			var mnav = container.getElementsByClassName('mnav')[0];
			if (mnav) {
				mnav.parentNode.insertBefore(mnav,mnav.parentNode.getElementsByClassName('dm_map_title')[0]);
				mnav.classList.remove('mnav');
				mnav.classList.add('mnav_top');
			}
			var fmap = document.getElementById('wrd_map');
			if (fmap)
			Array.from(fmap.children).forEach(function(row) {
				Array.from(row.children).forEach(function(cell) {
					switch (cell.textContent) {
						case '\uD83D\uDEAA': /*🚪*/ cell.classList.add('map_exit_pos_' + worker.GUIp_locale); break;
						case '\uD83D\uDCB0': /*💰*/ cell.classList.add('treasureChestForge'); break;
						case '\u2620':       /*☠*/ cell.classList.add('lesserBossForge'); break;
						case '\uD83D\uDC7E': /*👾*/ cell.classList.add('masterBossForge'); break;
						case '\uD83D\uDD73': /*🕳*/ /*cell.classList.add('trapForge');*/
						default: cell.classList.remove('masterBossForge');
					};
				});
			});
			ui_logger.update(ui_logger.forgeWatchers);
			ui_informer.updateCustomInformers();
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeForge);
		observer.observe(container, {subtree: true, childList: true, attributes: true, attributeFilter: ['class']});
		observeForge(null, observer);
	}
};
ui_improver.improveSpiritRefinery = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeWr = function(mutations,observer) {
			if (!container.querySelector('.bps_mf')) {
				return;
			}
			var node, value, list, regex;
			if ((node = container.querySelector('#sl_list + .sl_l')) && (value = node.textContent.match(/(?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/))) {
				value = new Date(Date.now() + (value[1] !== undefined ? +value[1] : 0) * 3600e3 + +value[2] * 60e3);
				node.title = worker.GUIp_i18n.refinery_available_at + GUIp.common.formatTime(value,'simpletime');
				ui_storage.set('Logger:Souls_Processed_Exp', value.getTime());
			} else {
				ui_storage.set('Logger:Souls_Processed_Exp', 0);
			}
			if ((node = container.querySelector('.sl_u + .sl_l')) && (value = node.textContent.match(/(\d+) (?:minute|минут)|(\d+) (?:hour|час)|(\d+) (?:день|дня|дней|day)/))) {
				value  = new Date(Date.now() + (value[3] !== undefined ? +value[3] * 86400e3 : (value[2] !== undefined ? +value[2] * 3600e3 : (value[1] !== undefined ? +value[1] * 60e3 : 0))));
				node.title = worker.GUIp_i18n.immortality_up_to + (value.getTime() - Date.now() < 86400e3 ? GUIp.common.formatTime(value,'simpletime') : GUIp.common.formatTime(value,'simpledate'));
				ui_storage.set('Logger:Immortality_Exp', value.getTime());
			} else {
				ui_storage.set('Logger:Immortality_Exp', 0);
			}
			list = [];
			regex = new RegExp('(' + worker.GUIp_i18n.gathered_soul_types.join('|') + ')' + (worker.GUIp_locale === 'en' ? ' soul' : 'ая душ(?:онк)?а') + '(?: \\(([^\\)]+)\\))?');
			Array.from(container.querySelectorAll('#sl_list li')).forEach(function(a) {
				if (value = regex.exec(a.textContent)) {
					list.unshift({date: Date.now(), origin: -1, kind: worker.GUIp_i18n.gathered_soul_types.indexOf(value[1]) + 1, value: value[2] === '?' ? null : parseInt(value[2]) || 0});
				}
				if (/чистая душа|pure soul/.test(a.textContent)) {
					list.unshift({date: Date.now(), origin: -1, kind: 0});
				}
			});
			ui_storage.set('LastGatheredSouls', JSON.stringify(list));
			ui_logger.update(ui_logger.refineryWatchers);
			ui_informer.updateCustomInformers();
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeWr);
		observer.observe(container, {subtree: true, childList: true});
		observeWr(null, observer);
	}
};
ui_improver.improveUpgrader = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeWr = function(mutations,observer) {
			if (!container.querySelector('#upgrades .up_line')) {
				return;
			}
			ui_logger.update(ui_logger.upgraderWatchers);
			ui_informer.updateCustomInformers();
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeWr);
		observer.observe(container, {subtree: true, childList: true});
		observeWr(null, observer);
	}
};
ui_improver.improveTraderOrders = function(container) {
	if (!ui_utils.isAlreadyImproved(container)) {
		var observer, observeTo = function(mutations,observer) {
			var node = document.getElementById('t_orders'),
				fb = document.getElementById('e_fetch_orders_info');
			if (!node || fb) {
				return;
			}
			node.insertAdjacentHTML('afterend','<div id="e_fetch_orders_info" class="line"><div class="no_link div_link" title="' + worker.GUIp_i18n.trade_orders_fetch_title + '">' + worker.GUIp_i18n.trade_orders_fetch + '</div></div>');
			fb = document.getElementById('e_fetch_orders_info');
			GUIp.common.addListener(fb, 'click', function(e) {
				if (fb.classList.contains('e_wait')) {
					return;
				}
				var items = Array.from(document.getElementById('t_orders').children);
				GUIp.common.getXHR(GUIp.common.erinome_url + '/traders?lang=' + worker.GUIp_locale + '&items=' + encodeURIComponent(items.map(function(a) { return a.firstChild.textContent; }).join(',')), function(xhr) {
					ui_utils.hideElem(fb, true);
					var data = GUIp.common.parseJSON(xhr.responseText) || {};
					items.forEach(function(a) {
						var span, item = a.firstChild.textContent;
						if (data[item]) {
							if (data[item].records.length) {
								span = document.createElement('span');
								span.classList.add('e_info');
								span.textContent = a.firstChild.textContent;
								span.title = data[item].records.map(function(b) { return GUIp.common.formatTime(new Date(b.date),'simpledate') + ' – ' + b.count + ' (' + b.trader + ')';  }).join('\n');
								a.replaceChild(span, a.firstChild);
							}
							if (data[item]['ratio']) {
								span = document.createElement('span');
								span.classList.add('e_ratio');
								span.textContent = data[item]['ratio'];
								span.title = '~'+parseFloat(data[item]['avg']).toFixed(2);
								a.insertBefore(span, null);
							}
							if (data[item]['bold']) {
								a.classList.add('e_bold');
							}
							if (data[item]['active']) {
								a.classList.add('e_active');
							}
						}
					});
				}, function() {
					fb.classList.remove('e_wait')
				});
			});
			observer.takeRecords();
		};
		observer = GUIp.common.newMutationObserver(observeTo);
		observer.observe(container, {subtree: true, childList: true});
		observeTo(null, observer);
		GUIp.common.tooltips.watchSubtree(container);
	}
};
ui_improver.improveSparMenu = function() {
	var lines, names, content = document.getElementById('chf_popover_c');
	if (!content || ui_data.isMobile) {
		return;
	}
	lines = content.getElementsByClassName('chf_line');
	for (var i = 0, len = lines.length; i < len; i++) {
		names = lines[i].firstChild.textContent.match(/^(.+?) \((.+?)\)/);
		if (!names || names.length < 3) {
			continue;
		}
		lines[i].insertAdjacentHTML('beforeend','<div class="e_chfr"><a class="div_link em_font">[✉]</a><a class="div_link em_font">[➠]</a></div>');
		(function(name) {
			GUIp.common.addListener(lines[i].lastChild.firstChild, 'click', function(e) {
				ui_utils.openChatWith(name);
				e.stopPropagation();
			});
			lines[i].lastChild.firstChild.title = worker.GUIp_i18n.open_pchat;
			GUIp.common.addListener(lines[i].lastChild.lastChild, 'click', function(e) {
				worker.open("/gods/" + encodeURIComponent(name));
				e.stopPropagation();
			});
			lines[i].lastChild.lastChild.title = worker.GUIp_i18n.open_ppage;
		})(names[1]);
	}
};
ui_improver.improveCraftMenu = function() {
	var menus = document.getElementsByClassName('inv_craft_popover'),
		container = document.querySelector('.wup.in'),
		inv = ui_inventory.model,
		index = NaN,
		isBaseCraftable = true,
		node, items;
	// check if the base item is not allowed to be crafted
	// it appears that span element of the item has the an attribute containing id of the corresponding popup
	// so in fact we can just check whether it's parent node has a class of an uncraftable item
	if (container && document.querySelector('.ul_inv > li.e_craft_impossible > span[data-target="' + container.id + '"]')) {
		isBaseCraftable = false;
	}
	for (var i = 0, len = menus.length; i < len; i++) {
		node = menus[i];
		if (node.classList.contains('improved')) {
			continue;
		}
		node.classList.add('improved');
		// strike through suggested items that we can't craft with
		items = node.getElementsByClassName('div_link');
		for (var j = 0, jlen = items.length; j < jlen; j++) {
			node = items[j];
			// no need to check anything if the base item is already disallowed
			if (!isBaseCraftable) {
				node.classList.add('e_uncraftable_choice');
				continue;
			}
			index = +inv.getItemIndex(node.textContent);
			// we need to check that the item is present in the inventory because Godville's craft menu is buggy
			if (index === index && !inv.isCraftable(index)) {
				node.classList.add('e_uncraftable_choice');
			}
			node.classList.toggle('bingo_item', ui_improver.bingoTries.get() && (node.textContent === '❄' || (ui_improver.bingoItems.get() && ui_improver.bingoItems.get().test(node.textContent))));
		}
	}
};
ui_improver.improveDmapMenu = function() {
	var wtitle = document.querySelector('.wup-inner > .wup-title');
	if (!wtitle || !/Карта|Map/.test(wtitle.textContent)) {
		return;
	}
	if (wtitle && wtitle.nextElementSibling && wtitle.nextElementSibling.firstChild) {
		// map popup title block to put the dimensions into. it is destoyed with the popup
		wtitle.classList.add('e_mt_dmap');
		// map container itself. it is preserved after popup is closed
		var node = wtitle.nextElementSibling.firstChild;
		node.classList.add('e_m_dmap');
		GUIp.common.debug('mobile dmap (re)marked');
		GUIp.common.tooltips.watchSubtree(node);
		// initial extras parsing is happening before we mark this as the map,
		// thus we are losing the dungeon type. and repeating this here kinda fixes it
		this.parseDungeonExtras(document.querySelectorAll('.e_m_fight_log .d_imp .d_msg'), node, ui_stats.currentStep());
		ui_improver.colorDungeonMap();
	}
};
ui_improver.calculateButtonsVisibility = function(forcedUpdate) {
	var i, j, len, gv = document.getElementById('godvoice'),
		baseCond = gv && (!ui_utils.isHidden(gv) || ui_data.isMobile && gv.style.display !== 'none') && !ui_storage.getFlag('Option:disableVoiceGenerators') && ui_stats.HP() > 0,
		isMonster = ui_stats.monsterName();
	if (!ui_data.isFight) {
		// inspect buttons
		var inspBtns = document.getElementsByClassName('inspect_button'),
			inspBtnsBefore = [], inspBtnsAfter = [];
		for (i = 0, len = inspBtns.length; i < len; i++) {
			inspBtnsBefore[i] = !inspBtns[i].classList.contains('hidden');
			inspBtnsAfter[i] = baseCond && !isMonster;
		}
		ui_improver.setButtonsVisibility(inspBtns, inspBtnsBefore, inspBtnsAfter);
		// craft buttons
		if (this.isFirstTime || forcedUpdate) {
			this.crftBtns = [document.getElementsByClassName('craft_button b_b')[0],
							 document.getElementsByClassName('craft_button b_r')[0],
							 document.getElementsByClassName('craft_button r_r')[0],
							 document.getElementsByClassName('craft_button span')[0],
							 document.getElementsByClassName('craft_group b_b')[0],
							 document.getElementsByClassName('craft_group b_r')[0],
							 document.getElementsByClassName('craft_group r_r')[0]
							];
			this.crftCustom = [[],[],[]];
			for (i = 0; i < 3; i++) {
				var ccrbs = this.crftBtns[i+4].getElementsByClassName('craft_button');
				for (j = 0, len = ccrbs.length; j < len; j++) {
					this.crftCustom[i].push(this.crftBtns.length);
					this.crftBtns.push(ccrbs[j]);
				}
			}
		}
		var crftBtnsBefore = [], crftBtnsAfter = [],
			crftFixed = ui_storage.getFlag('Option:fixedCraftButtons');
		for (i = 0, len = this.crftBtns.length; i < len; i++) {
			crftBtnsBefore[i] = !this.crftBtns[i].classList.contains('hidden');
			crftBtnsAfter[i] = !(!crftFixed && (!baseCond || isMonster));
		}
		crftBtnsAfter[0] = crftBtnsAfter[0] && ui_inventory.b_b.length;
		crftBtnsAfter[1] = crftBtnsAfter[1] && ui_inventory.b_r.length;
		crftBtnsAfter[2] = crftBtnsAfter[2] && ui_inventory.r_r.length;
		crftBtnsAfter[3] = crftBtnsAfter[0] || crftBtnsAfter[1] || crftBtnsAfter[2];

		for (i = 7, len = this.crftBtns.length; i < len; i++) {
			crftBtnsAfter[i] = crftBtnsAfter[i] && ui_inventory[this.crftBtns[i].id] && ui_inventory[this.crftBtns[i].id].length;
		}
		for (i = 0; i < 3; i++) {
			var crftGroupActive = false;
			for (j = 0, len = this.crftCustom[i].length; j < len; j++) {
				if (crftBtnsAfter[this.crftCustom[i][j]]) {
					crftGroupActive = true;
					break;
				}
			}
			crftBtnsAfter[i+4] = crftBtnsAfter[i+4] && crftGroupActive && crftBtnsAfter[i];
		}
		ui_improver.setButtonsVisibility(this.crftBtns, crftBtnsBefore, crftBtnsAfter);

		if (crftFixed) {
			for (i = 0, len = this.crftBtns.length; i < len; i++) {
				if (baseCond && !isMonster && !ui_data.inShop) {
					this.crftBtns[i].classList.remove('crb_inactive');
				} else {
					this.crftBtns[i].classList.add('crb_inactive');
				}
			}
		}
		// if we're in trader mode then try to mark some buttons as unavailable
		if (ui_data.hasShop) {
			ui_improver.switchButtonsForStore(!!ui_data.inShop);
		}
	}
	// voice generators
	if (this.isFirstTime) {
		this.voicegens = document.getElementsByClassName('voice_generator');
		this.voicegenClasses = [];
		for (i = 0, len = this.voicegens.length; i < len; i++) {
			this.voicegenClasses[i] = this.voicegens[i].className;
		}
	}
	var voicegensBefore = [], voicegensAfter = [],
		specialConds, specialClasses;
	if (!ui_data.isFight) {
		var isGoingBack = ui_stats.isGoingBack(),
			isTown = ui_stats.townName(),
			isSearching = ui_improver.detectors.stateGTF.res || ui_stats.lastNews().includes('дорогу'),
			isResting = /^(Переводит дух|Сушит вещи|Заканчивает перекур|Catching h.. breath|Drying h.. clothes)\.\.\.$/.test(ui_stats.lastNews()),
			dieIsDisabled = ui_storage.getFlag('Option:disableDieButton'),
			isFullGP = ui_stats.Godpower() === ui_stats.Max_Godpower(),
			isFullHP = ui_stats.HP() === ui_stats.Max_HP(),
			canQuestBeAffected = !/\((?:выполнено|completed|отменено|cancelled)\)/.test(ui_stats.Task_Name());
		specialClasses = ['heal', 'do_task', 'cancel_task', 'die', 'exp', 'dig', 'town', 'pray', 'sacrifice'];
		specialConds = [isMonster || isGoingBack || isTown || isSearching || isFullHP,				// heal
						isMonster || isGoingBack || isTown || isSearching || !canQuestBeAffected,	// do_task
																			 !canQuestBeAffected,	// cancel_task
						isMonster ||				isTown ||				 dieIsDisabled,			// die
						isMonster,																	// exp
						isMonster ||				isTown,											// dig
						isMonster || isGoingBack || isTown || isSearching || isResting,				// town
						isMonster ||										 isFullGP,				// pray
													isTown 											// sacrifice
					   ];
	}
	baseCond = baseCond && !worker.$('.r_blocked:visible').length;
	for (i = 0, len = this.voicegens.length; i < len; i++) {
		voicegensBefore[i] = !this.voicegens[i].classList.contains('hidden');
		voicegensAfter[i] = baseCond;
		if (baseCond && !ui_data.isFight) {
			for (var j = 0, len2 = specialConds.length; j < len2; j++) {
				if (specialConds[j] && this.voicegenClasses[i] && this.voicegenClasses[i].match(specialClasses[j])) {
					voicegensAfter[i] = false;
				}
			}
		}
	}
	ui_improver.setButtonsVisibility(this.voicegens, voicegensBefore, voicegensAfter);
};
ui_improver.setButtonsVisibility = function(btns, before, after) {
	for (var i = 0, len = btns.length; i < len; i++) {
		if (before[i] && !after[i]) {
			ui_utils.hideElem(btns[i], true);
		}
		if (!before[i] && after[i]) {
			ui_utils.hideElem(btns[i], false);
		}
	}
};
ui_improver.switchButtonsForStore = function(disable) {
	var activityButtons = document.querySelectorAll('#cntrl1 a, #cntrl2 a, a.voice_generator, a.craft_button, a.inspect_button');
	for (var i = 0, len = activityButtons.length; i < len; i++) {
		activityButtons[i].classList.toggle('crb_inactive', disable);
	}
	if (document.getElementById('voice_submit')) {
		document.getElementById('voice_submit').style.color = disable ? 'silver' : '';
	}
};
ui_improver.improveTownAbbrs = function(towns) {
	try {
		var ntname, abbr;
		for (var i = 0, len = towns.length; i < len; i++) {
			if (ntname = ui_words.base.town_list.find(function(a) { return a.abbr && a.name === towns[i].name; })) {
				abbr = towns[i].g.lastChild;
				if (abbr.textContent !== ntname.abbr) {
					abbr.textContent = ntname.abbr;
				}
			}
		}
	} catch(e) {}
};
ui_improver.nearbyTownsFix = function(onlyDistance) {
	if (!document.getElementById('hmap_svg')) {
		return;
	}
	try {
		if (worker.GUIp_browser === 'Opera') {
			ui_improver.operaWMapFix();
		}
		var tmr, pos, town, towns, lval = document.querySelector('#hk_distance .l_val');
		pos = ui_stats.mileStones();
		towns = Array.from(document.querySelectorAll('#hmap_svg g.tl title, #hmap_svg g.sl title, #hmap_svg g.poi title'),
				function(a) {
					var b;
					if (b = a.textContent.match(/^(.*?) \((\d+)\)/)) {
						return {name:b[1], dst:+b[2], g: a.parentNode};
					} else if (b = a.textContent.match(/^(.*?), (\d+) ..\./)) {
						return {name:b[2], dst:+b[2], g: a.parentNode};
					}
				}).sort(function(a,b) { return a.dst - b.dst; });
		if (!onlyDistance) {
			var switchTI = function(town) {
				if (ui_improver.informTown === town.name) {
					ui_improver.distanceInformerReset();
				} else if (!ui_improver.dailyForecast.get().includes('gvroads') || !isNaN(+town.name)) {
					ui_improver.distanceInformerSet(town);
				}
			}, listenerD = function(town, e) {
				if (e.which === 2) {
					switchTI(town);
					e.preventDefault();
					return;
				}
				tmr = GUIp.common.setTimeout(function() {
					switchTI(town);
					if (towns[0].g.parentNode) towns[0].g.parentNode.classList.add('block_once');
					tmr = 0;
				},1e3);
			}, listenerU = function() {
				if (tmr) {
					worker.clearTimeout(tmr);
				}
			};
			for (var i = 0, len = towns.length; i < len; i++) {
				if (!ui_utils.isAlreadyImproved(towns[i].g)) {
					GUIp.common.addListener(towns[i].g, 'mousedown', listenerD.bind(null, towns[i]));
					GUIp.common.addListener(towns[i].g, 'touchstart', listenerD.bind(null, towns[i]));
					GUIp.common.addListener(towns[i].g, 'mouseup', listenerU);
					GUIp.common.addListener(towns[i].g, 'touchend', listenerU);
				}
				if (ui_improver.informTown === towns[i].name) {
					towns[i].g.classList.add('e_selected_town');
				}
			}
			if (towns.length && !towns[0].g.parentNode.classList.contains('improved')) {
				GUIp.common.addListener(towns[0].g.parentNode, 'click', function(e) {
					if (this.classList.contains('block_once')) {
						this.classList.remove('block_once');
						e.stopPropagation();
					}
				}, true);
				towns[0].g.parentNode.classList.add('improved');
			}
			if (ui_storage.getFlag('Option:improveTownAbbrs')) {
				ui_improver.improveTownAbbrs(towns);
			}
		}
		town = towns.filter(function(b) { return isNaN(+b.name) && b.dst <= pos; }).pop();
		if (!lval.title.includes(town.name)) {
			lval.title = worker.GUIp_i18n.fmt('nearby_town', town.name, town.dst);
		}
	} catch(e) {}
};
ui_improver.operaWMapFix = function() {
	if (ui_improver.operaWMapObserver) {
		ui_improver.operaWMapObserver.disconnect();
	} else {
		ui_improver.operaWMapObserver = GUIp.common.newMutationObserver(function(mutations) {
			var f = false;
			for (i = 0, len = mutations.length; i < len; i++) {
				if (mutations[i].attributeName === 'd' || mutations[i].attributeName === 'to' && mutations[i].target.nodeName === 'animateTransform' && mutations[i].target.getAttribute('type') === 'translate') {
					mutations[i].target.removeAttribute('oprfix');
					GUIp.common.debug('removing oprfix from "' + mutations[i].attributeName + '"')
					f = true;
				}
			}
			if (f) {
				ui_improver.operaWMapFix();
			}
		});
	}
	// info popups positioned at the bottom most of the time are glitched and not repainted properly, placing them at the top kinda "resolves" this issue
	var hinfos = document.querySelectorAll('.hmap_info');
	for (var i = 0, len = hinfos.length; i < len; i++) {
		hinfos[i].style.bottom = 'inherit';
		hinfos[i].style.top = '0';
	}
	// if the map is placed at central column then it's not required to apply rescaling to it
	if (document.querySelector('.c_col #hmap')) {
		return;
	}
	var svg = document.getElementById('hmap_svg');
	var hscale = function(a) { return (a*0.65).toFixed(2); }
	var paths = svg.querySelectorAll('path');
	for (var j = 0, i = 0; i < paths.length; i++) {
		if (paths[i].getAttribute('d') && !paths[i].getAttribute('oprfix')) {
			paths[i].setAttribute('d',paths[i].getAttribute('d').replace(/\d+(\.\d+)?/g,hscale));
			paths[i].setAttribute('oprfix','1');
			j++;
		}
		if (paths[i].classList.contains('tmap')) {
			paths[i].style.strokeWidth = hscale(2);
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' paths');
	var gs = svg.querySelectorAll('g[transform^="translate"]');
	for (var j = 0, i = 0, len = gs.length; i < len; i++) {
		if (!gs[i].getAttribute('oprfix')) {
			gs[i].setAttribute('transform',gs[i].getAttribute('transform').replace(/\d+(\.\d+)?/g,hscale));
			gs[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' gs');
	var circles = svg.querySelectorAll('circle');
	for (var j = 0, i = 0, len = circles.length; i < len; i++) {
		if (!circles[i].getAttribute('oprfix')) {
			circles[i].setAttribute('r',hscale(circles[i].getAttribute('r')));
			circles[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' circles');
	var texts = svg.querySelectorAll('text');
	for (var j = 0, i = 0, len = texts.length; i < len; i++) {
		if (!texts[i].getAttribute('oprfix')) {
			texts[i].setAttribute('dy',hscale(texts[i].getAttribute('dy')));
			texts[i].setAttribute('font-size',hscale(texts[i].getAttribute('font-size')));
			texts[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' texts');
	var anims = svg.querySelectorAll('animateTransform[type="translate"]');
	for (var j = 0, i = 0, len = anims.length; i < len; i++) {
		if (!anims[i].getAttribute('oprfix')) {
			anims[i].setAttribute('from',anims[i].getAttribute('from').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('to',anims[i].getAttribute('to').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' anims');
	svg.style.transform = 'scale(1)';
	ui_improver.operaWMapObserver.observe(svg,{ subtree: true, attributes: true, attributeFilter: ['d','to']});
};
ui_improver.thirdEyePositionFix = function() {
	var owtop, oatop, wtop, atop,
		content = document.querySelector('.wup.in .tep'),
		wup = document.querySelector('.wup.in');
	try {
		owtop = wup.style.top;
		oatop = wup.firstChild.style.top;
		wup.style.top = '-2000px';
		ui_utils.hideElem(wup,false);
		content.style.maxHeight = (worker.innerHeight - 150) + 'px';
		wtop = worker.innerHeight + worker.scrollY - wup.clientHeight - 75;
		atop = document.getElementById('imp_button').getBoundingClientRect().top + worker.scrollY - wtop + 11;
		if (atop < 50) {
			wtop -= 50 - atop;
			atop = 50;
		}
		if (atop > wup.clientHeight) {
			wtop += atop - wup.clientHeight + 10;
			atop -= atop - wup.clientHeight + 15;
		}
		if (wtop < 0) wtop = 0;
		wup.firstChild.style.top = atop + 'px';
		wup.style.top = wtop + 'px';
	} catch (e) {
		wup && wup.firstChild && (wup.firstChild.style.top = oatop);
		wup && (wup.style.top = owtop);
	}
};
ui_improver.chatsFix = function() {
	if (ui_data.isMobile) return;
	var chats = document.getElementsByClassName('frDockCell'),
		len = chats.length;
	// fix overlapping issues when opening a huge number of chats
	for (var i = 0; i < len; i++) {
		var chat = chats[i];
		chat.classList.remove('left');
		chat.style.zIndex = len - i;
		if (chat.getBoundingClientRect().right < 350) {
			chat.classList.add('left');
		}
	}
	// padding for page settings link
	var paddingBottom = len ? chats[0].getBoundingClientRect().bottom - chats[len - 1].getBoundingClientRect().top : worker.GUIp_browser === 'Opera' ? 27 : 0,
		isBottom = worker.scrollY >= document.documentElement.scrollHeight - document.documentElement.clientHeight - 10;
	paddingBottom = Math.floor(paddingBottom*10)/10 + 10;
	paddingBottom = paddingBottom < 0 ? 0 : paddingBottom + 'px';
	document.getElementsByClassName('reset_layout')[0].style.paddingBottom = paddingBottom;
	if (isBottom) {
		worker.scrollTo(0, document.documentElement.scrollHeight - document.documentElement.clientHeight);
	}
};
ui_improver.improveChat = function() {
	var chats = ui_data.isMobile ? ui_improver._openedChatContents : document.getElementsByClassName('chat_ph')[0];
	if (!chats || ui_utils.isAlreadyImproved(chats)) {
		return;
	}
	// by default, guildmate's name is appended to the end of the message when clicked.
	// we insert it into current cursor's position instead
	GUIp.common.addListener(chats, 'click', function(ev) {
		var node = ev.target;
		// ignore unless a godname is clicked
		if (!node.classList.contains('gc_fr_god')) {
			return;
		}
		var godName = node.textContent;
		// find the textarea
		while (!(node = node.parentNode).classList.contains('frMsgBlock')) { }
		node = node.querySelector('.frInputArea textarea');
		// insert godname into it
		ui_improver._insertGodNameToChat(node,godName);
		// avoid executing original code
		ev.stopPropagation();
	}, true);
};
ui_improver._insertGodNameToChat = function(node, godName) {
	var text = node.value,
		pos = node.selectionDirection === 'backward' ? node.selectionStart : node.selectionEnd;
	node.value = text.slice(0, pos) + '@' + godName + ', ' + text.slice(pos);
	node.focus();
	node.selectionStart = node.selectionEnd = pos + godName.length + 3;
};
ui_improver._getChatMsgText = function(msg) {
	var author = msg.getElementsByClassName('gc_fr_god')[0],
		result = author ? author.textContent : '',
		classes;
	// there might be hyperlinks in the message
	for (var child = msg.firstChild; child && !((classes = child.classList) && classes.contains('fr_msg_meta')); child = child.nextSibling) {
		result += child.textContent;
	}
	return result;
};
ui_improver._fixChatScrolling = function(msgArea) {
	var msgs = msgArea.getElementsByClassName('fr_msg_l'),
		latestMsgText = '',
		latestMsgOffset = 0,
		scroll = msgArea.scrollTop;
	if (msgs.length) {
		latestMsgText = this._getChatMsgText(msgs[msgs.length - 1]);
		latestMsgOffset = msgs[msgs.length - 1].offsetTop;
	}

	msgArea.addEventListener('scroll', function() {
		// executed without a try-catch wrapper for speed; extra careful here
		scroll = this.scrollTop;
	});

	GUIp.common.newMutationObserver(function() {
		var i = msgs.length - 1;
		if (i < 0) return;
		var j = i,
			newLatestMsg = msgs[i],
			newLatestMsgText = ui_improver._getChatMsgText(newLatestMsg),
			latestMsg = newLatestMsg;
		// look for the message we've seen the last time
		if (newLatestMsgText !== latestMsgText) {
			while (i-- && ui_improver._getChatMsgText((latestMsg = msgs[i])) !== latestMsgText) { }
			latestMsgText = newLatestMsgText;
		}

		var newLatestMsgOffset = newLatestMsg.offsetTop;
		if (i < 0 || (j - i <= 5 && scroll >= latestMsgOffset + latestMsg.offsetHeight - msgArea.offsetHeight - 3)) {
			// we've lost it or were already at the bottom; scroll down to the bottom
			// but guild council preserves its scrolling when more than 5 messages arrive at once, so do we
			// note:
			// since firefox 124 value 2147483647 (2^31 - 1) has stopped working here,
			// so now the maximum scrollTop allowed seems to be 2147483583 (2^31 - 65)
			msgArea.scrollTop = 2147483583;
		} else {
			// restore scrolling with respect to new messages' height
			msgArea.scrollTop = scroll + (latestMsg.offsetTop - latestMsgOffset);
		}
		latestMsgOffset = newLatestMsgOffset;
	}).observe(msgArea, {childList: true, subtree: true});
};
ui_improver.processNewChat = function(chat) {
	var node = chat.getElementsByClassName(ui_data.isMobile ? 'ui-title' : 'dockfrname')[0],
		name, key, saved, header;
	if (!node || !(name = node.textContent) || !(node = chat.querySelector('.frInputArea textarea'))) {
		return;
	}

	key = 'PM:' + name;
	if ((saved = ui_tmpstorage.get(key))) {
		node.value = saved;
		node.dispatchEvent(new Event('change', {bubbles: true})); // ask Godville to resize the textarea
		if (node.disabled) {
			// Guild Council window is initially disabled, and when it gets enabled, the text is cleared
			GUIp.common.newMutationObserver(function(mutations, observer) {
				if (!node.disabled) {
					node.value = saved;
					observer.disconnect();
				}
			}).observe(node, {attributes: true, attributeFilter: ['disabled']});
		}
	}

	var saveText = function() {
		var text = node.value;
		if (text) {
			ui_tmpstorage.set(key, text);
		} else {
			ui_tmpstorage.remove(key);
		}
	};
	GUIp.common.addListener(node, 'blur', saveText);

	// on mobile version we don't care for scrolling for now, and we have proper full name from the beginning
	if (ui_data.isMobile) {
		ui_utils.hideNotification('pm_' + name);
		GUIp.common.addListener(node, 'focus', ui_utils.hideNotification.bind(ui_utils, 'pm_' + name));
	}
	// wait until chat is loaded
	else if ((header = chat.getElementsByClassName('fr_chat_header')[0])) {
		ui_utils.observeUntil(header, {characterData: true, childList: true}, function() {
			return header.textContent.trim() || undefined;
		}).then(function(friendName) {
			var pos = friendName.search(/ и е(?:го|ё) | and h[ie][sr] hero/);
			if (pos >= 0) {
				friendName = friendName.slice(0, pos);
			}
			// hide desktop notification when opening the chat
			// unfortunately, .dockfrname contains just abbreviated name
			ui_utils.hideNotification('pm_' + friendName);
			GUIp.common.addListener(node, 'focus', ui_utils.hideNotification.bind(ui_utils, 'pm_' + friendName));
			// scroll position is reset every time a new message appears. try to workaround that
			var msgArea = chat.getElementsByClassName('frMsgArea')[0];
			if (msgArea) {
				ui_improver._fixChatScrolling(msgArea);
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};
ui_improver.checkGCMark = function(csource) {
	var gc_tab = document.querySelector('.msgDock.frDockCell.frbutton_pressed .dockfrname');
	if (gc_tab && gc_tab.textContent.match(/Гильдсовет|Guild Council/) && gc_tab.parentNode.getElementsByClassName('fr_new_msg').length) {
		worker.$('.frbutton_pressed textarea').triggerHandler('focus');
	}
};
ui_improver.createWakelock = function() {
	var wakelockSwitch;
	if (ui_data.isMobile) {
		document.querySelector('.e_mt_forum_informers, .e_mt_menu').insertAdjacentHTML('beforebegin',
			'<div class="line" id="wakelock_switch"><div class="l_text"><input type="checkbox"/> ' + worker.GUIp_i18n.wakelock_menu + '</div></div>'
		);
		wakelockSwitch = document.getElementById('wakelock_switch');
	} else {
		wakelockSwitch = document.createElement('div');
		wakelockSwitch.id = 'wakelock_switch';
		wakelockSwitch.textContent = '\uD83D\uDCA1'; // light bulb
		document.getElementById('main_wrapper').appendChild(wakelockSwitch);
	}
	GUIp.common.addListener(wakelockSwitch, 'click', function() { ui_improver.switchWakelock(); });
	// we need to re-enable a modern wakelock on each visibility state change to visible
	if (worker.navigator.wakeLock) {
		GUIp.common.addListener(document, 'visibilitychange', function() {
			if (document.visibilityState === "visible" && ui_improver.wakeLock === null && ui_storage.getFlag('wakelockEnabled')) {
				ui_improver.switchWakelock(true);
			}
		});
	}
	ui_improver.wakeLock = null;
	if (ui_storage.getFlag('wakelockEnabled')) {
		ui_improver.switchWakelock(true);
	}
};
ui_improver.switchWakelock = function(enable) {
	var wakelockSwitch = document.getElementById('wakelock_switch'),
		wakelockCB = wakelockSwitch.querySelector('input'), 
		wakelockRemoved = function() {
			ui_improver.wakeLock = null;
			wakelockSwitch.classList.remove('wakelock_enabled');
			if (wakelockCB) {
				wakelockCB.checked = false;
			}
		},
		wakelockEnabled = function() {
			ui_storage.set('wakelockEnabled',true);
			wakelockSwitch.classList.add('wakelock_enabled');
			if (wakelockCB) {
				wakelockCB.checked = true;
			}
		};
	if (!ui_improver.wakeLock || enable) {
		if (worker.navigator.wakeLock) {
			// modern request
			worker.navigator.wakeLock.request("screen").then(function (wakeLock) {
				GUIp.common.debug('modern wakelock active.');
				ui_improver.wakeLock = wakeLock;
				GUIp.common.addListener(ui_improver.wakeLock, 'release', function() {
					GUIp.common.debug('modern wakelock released.');
					wakelockRemoved();
				});
				wakelockEnabled();
			}).catch(function(e) { GUIp.common.error('error trying to enable modern wakelock:',e) });
		} else {
			// legacy firefox
			ui_improver.wakeLock = worker.navigator.requestWakeLock('screen');
			if (ui_improver.wakeLock) {
				wakelockEnabled();
			}
		}
	} else {
		if (ui_improver.wakeLock.release) {
			ui_improver.wakeLock.release();
		} else if (ui_improver.wakeLock.unlock) {
			ui_improver.wakeLock.unlock();
			wakelockRemoved();
		}
		ui_storage.set('wakelockEnabled',false);
	}
};
ui_improver.initTouchDragging = function() {
	Array.prototype.forEach.call(document.getElementsByClassName('b_handle'), ui_dragger.register);
};
ui_improver.activity = function() {
	if (!ui_logger.updating) {
		ui_logger.updating = true;
		GUIp.common.setTimeout(function() {
			ui_logger.updating = false;
		}, 500);
		ui_logger.update();
	}
};
