// ui_observers
var ui_observers = worker.GUIp.observers = {};

ui_observers.init = function() {
	var objs = Object.values(ui_observers),
		obj;
	for (var i = 0, len = objs.length; i < len; i++) {
		if ((obj = objs[i]).condition) {
			ui_observers.start(obj);
		}
	}
};
ui_observers.start = function(obj) {
	var observer = GUIp.common.newMutationObserver(obj.func),
		target;
	for (var i = 0, len = obj.target.length; i < len; i++) {
		if ((target = document.querySelector(obj.target[i]))) {
			observer.observe(target, obj.config);
		}
	}
};
ui_observers.mutationChecker = function(mutations, check, callback) {
	if (!mutations.some(check)) {
		return false;
	}
	callback();
	return true;
};
ui_observers.chats = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		for (var i = 0, len = mutations.length; i < len; i++) {
			var mutation = mutations[i];
			for (var j = 0, jlen = mutation.addedNodes.length; j < jlen; j++) {
				var newNode = mutation.addedNodes[j];
				if (newNode.classList.contains('e_moved')) {
					continue;
				}
				newNode.classList.add('e_moved');
				mutation.target.appendChild(newNode); // move chat window to the left
				if (newNode.querySelector('.gc_topic')) {
					newNode.classList.add('e_guild_council'); // this is a guild chat
				}
				var msgArea = newNode.querySelector('.frMsgArea');
				if (msgArea) {
					msgArea.scrollTop = msgArea.scrollTopMax || msgArea.scrollHeight;
				}
				ui_improver.processNewChat(newNode);
			}
		}
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.addedNodes.length || mutation.removedNodes.length;
		}, function() {
			ui_improver.chatsFix();
			ui_informer.clearTitle();
		});
	},
	target: ['.chat_ph']
};
ui_observers.chatsMobile = {
	get condition() {
		return ui_data.isMobile;
	},
	config: {childList: true},
	func: function(mutations) {
		for (var i = 0, len = mutations.length; i < len; i++) {
			var mutation = mutations[i];
			if (mutation.addedNodes.length && mutation.addedNodes[0].classList.contains('mpage') && mutation.addedNodes[0].id !== 'tabbar') {
				// work with newly opened chat
				ui_improver._openedChatContents = mutation.addedNodes[0].querySelector('.msgDock');
				if (mutation.addedNodes[0].querySelector('.gc_topic') && ui_improver._openedChatContents) {
					ui_improver._openedChatContents.classList.add('e_guild_council'); // this is a guild chat
				}
				ui_improver.processNewChat(mutation.addedNodes[0]);
				ui_improver.improveChat();
			}
			if (mutation.removedNodes.length && mutation.removedNodes[0].classList.contains('mpage') && mutation.removedNodes[0].id !== 'tabbar') {
				// save typed text in a chat when it closes
				ui_utils.saveMobileUnsentMessage(mutation.removedNodes[0]);
				ui_improver._openedChatContents = null;
				// vanilla code uses history.back() or something, which restores the previous document title (in tab header) to text it had when we just opened the chat;
				// after that we appear to have document.title in HTML and actual title in browser header having DIFFERENT texts.
				// what's even worse: changing document.title to same value it already has in HTML does just nothing (at least in vivaldi 5)
				// so, to have browser redraw the visible title we need to change it to *something different* and then back to proper value that we want to have
				GUIp.common.setTimeout(function() { 
					var tmp = ui_data.docTitle;
					document.title = '_'; // this automagically changes ui_data.docTitle to new value too, so we need to restore it manually
					ui_data.docTitle = tmp;
					ui_informer.clearTitle(); // set proper prefixes
				}, 50);
			}
		}
	},
	target: ['#hero_block']
};
ui_observers.clearTitle = {
	condition: true,
	get config() {
		return ui_data.isMobile ? {
			childList: true,
			subtree: true
		} : {
			childList: true,
			attributes: true,
			subtree: true,
			attributeFilter: ['style']
		};
	},
	func: function(mutations) {
		if (ui_data.isMobile) {
			ui_observers.mutationChecker(mutations, function(mutation) {
				return mutation.target.classList && mutation.target.classList.contains('frmsg_i') || mutation.addedNodes.length && mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('fr_new_msg');
			}, function() { GUIp.common.setTimeout(ui_utils.pmNotification.bind(ui_utils), 100); });
			ui_observers.mutationChecker(mutations, function(mutation) {
				return mutation.target.classList && mutation.target.classList.contains('show_gc') ||
					  (mutation.target.className.includes('frline') && (mutation.removedNodes.length && mutation.removedNodes[0].className.includes('fr_new_msg') || mutation.addedNodes.length && mutation.addedNodes[0].className.includes('fr_new_msg')));
			}, ui_informer.clearTitle.bind(ui_informer));
			return;
		}
		var isFocused = document.hasFocus && document.hasFocus();
		ui_observers.mutationChecker(mutations, function(mutation) {
			return isFocused && mutation.addedNodes.length && mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('fr_new_msg')
		}, function() { GUIp.common.setTimeout(ui_improver.checkGCMark.bind(ui_improver,'observe'), 50); });
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.target.classList && (mutation.target.classList.contains('fr_new_badge_pos') || mutation.target.classList.contains('frmsg_i'));
		}, function() { GUIp.common.setTimeout(ui_utils.pmNotification.bind(ui_utils), 100); });
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.target.className.match(/fr_new_(?:msg|badge)/) ||
				  (mutation.target.className.includes('dockfrname_w') && (mutation.removedNodes.length && mutation.removedNodes[0].className.includes('fr_new_msg') || mutation.addedNodes.length && mutation.addedNodes[0].className.includes('fr_new_msg')));
		}, ui_informer.clearTitle.bind(ui_informer));
	},
	target: ['.msgDockWrapper, .e_m_friends']
};
ui_observers.voiceform = {
	condition: true,
	config: {
		attributes: true,
		attributeFilter: ['style']
	},
	func: function(mutations) {
		for (var i = 0, len = mutations.length; i < len; i++) {
			if (mutations[i].target.style.display) {
				ui_improver.improvementDebounce();
				break;
			}
		}
	},
	target: ['#cntrl .voice_line']
};
ui_observers.refresher = {
	condition: (worker.GUIp_browser !== 'Opera'),
	config: {
		attributes: true,
		characterData: true,
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		var toReset = false;
		for (var i = 0, len = mutations.length; i < len; i++) {
			var tgt = mutations[i].target,
				id = tgt.id,
				cl = tgt.className;
			if (!(id && id.match && id.match(/logger|pet_badge|equip_badge/)) &&
				!(cl && cl.match && cl.match(/voice_generator|inspect_button|m_hover|craft_button/))) {
				toReset = true;
				break;
			}
		}
		if (toReset) {
			worker.clearInterval(ui_improver.softRefreshInt);
			worker.clearInterval(ui_improver.hardRefreshInt);
			if (!ui_storage.getFlag('Option:disablePageRefresh')) {
				ui_improver.softRefreshInt = GUIp.common.setInterval(ui_improver.softRefresh, (ui_data.isFight || ui_data.isDungeon) ? 9e3 : 18e4);
				ui_improver.hardRefreshInt = GUIp.common.setInterval(ui_improver.hardRefresh, (ui_data.isFight || ui_data.isDungeon) ? 25e3 : 50e4);
			}
		}
	},
	target: ['#main_wrapper']
};
ui_observers.diary = {
	get condition() {
		return !ui_data.isFight;
	},
	config: {childList: true},
	func: function(mutations, observer) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() {
			ui_improver.improveDiary();
			ui_improver.improvementDebounce();
			observer.takeRecords();
		});
	},
	target: ['#diary .d_content, .e_m_diary .d_content']
};
ui_observers.news = {
	get condition() {
		return !ui_data.isFight;
	},
	config: {childList: true, characterData: true, subtree: true},
	func: function(mutations, observer) {
		ui_improver.improveMonsterName();
		ui_improver.improvementDebounce();
		observer.takeRecords();
	},
	target: ['.f_news']
};
ui_observers.hero_health = {
	get condition() {
		return !ui_data.isFight;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.addedNodes.length;
		}, ui_improver.improvementDebounce);
	},
	target: ['#hk_health']
};
ui_observers.wup_detector = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.addedNodes.length && mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('wup');
		}, function() {
			if (document.querySelector('.wup.in .tep') && ui_storage.getFlag('Option:tePosFix')) {
				ui_utils.hideElem(document.querySelector('.wup.in'),true);
				GUIp.common.setTimeout(function() { ui_improver.thirdEyePositionFix(); },0);
			}
			if (ui_improver.improveWupInt) {
				worker.clearInterval(ui_improver.improveWupInt);
			}
			if (!ui_data.isFight) {
				ui_timers.updateThirdEyeFromHTML();
				ui_improver.improveLastFights();
				ui_improver.improveLastVoices();
				ui_improver.improveStoredPets();
				ui_improver.improveSparMenu();
				ui_improver.improveCraftMenu();
			} else if (ui_data.isMobile) {
				ui_improver.improveDmapMenu();
				ui_improver.improveLastFights();
			}
			var wTitle = document.querySelector('.wup.in .wup-title') || '',
				wContent = document.querySelector('.wup.in .wup-content') || '';
			if (wContent.textContent) {
				return; // if content is already present, then we've already improved it above. below are improvers only for content that needs to be explicitly loaded
			}
			ui_improver.improveWupInt = GUIp.common.setInterval(function() {
				if (wContent.textContent) {
					worker.clearInterval(ui_improver.improveWupInt);
					if (['Лаборатория','Lab'].includes(wTitle.textContent)) {
						ui_improver.improveLab(wContent);
					}
					if (['Творительная','Dungeon Forge'].includes(wTitle.textContent)) {
						ui_improver.improveForge(wContent);
					}
					if (['Душевая','Spirit Refinery'].includes(wTitle.textContent)) {
						ui_improver.improveSpiritRefinery(wContent);
					}
					if (['Приделы','Constructs'].includes(wTitle.textContent)) {
						ui_improver.improveUpgrader(wContent);
					}
					if (['Заказать на завтра','Tomorrow\'s orders:'].includes(wTitle.textContent)) {
						ui_improver.improveTraderOrders(wContent);
					}
				}
			},200);
		});
	},
	target: ['body']
};
ui_observers.chronicles = {
	get condition() {
		return ui_data.isDungeon;
	},
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() { ui_improver.improveChronicles(); ui_improver.improvementDebounce(); });
	},
	target: ['#m_fight_log .d_content, .e_m_fight_log .d_content']
};
ui_observers.map_colorization = {
	get condition() {
		return ui_data.isDungeon;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length && !(mutation.target.classList && mutation.target.classList.contains('restoredExcl')); }, ui_improver.colorDungeonMap.bind(ui_improver));
	},
	target: ['#map .block_content, .e_m_dmap']
};
ui_observers.s_chronicles = {
	get condition() {
		return ui_data.isSail;
	},
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() { ui_improver.improveSailChronicles(); ui_improver.improvementDebounce(); });
	},
	target: ['#m_fight_log h2, .e_mt_fight_log .l_header']
};
ui_observers.cargo = {
	get condition() {
		return ui_data.isSail;
	},
	config: {childList: true, characterData: true},
	func: function(mutations, observer) {
		ui_improver.improveCargo();
		observer.takeRecords();
	},
	target: ['#hk_cargo .l_val']
};
ui_observers.h_map = {
	get condition() {
		return !ui_data.isFight;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes[0] && mutation.addedNodes[0].tagName === 'g'; }, ui_improver.nearbyTownsFix.bind(ui_improver)) ||
			ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes[0] && mutation.target.tagName === 'title'; }, ui_improver.nearbyTownsFix.bind(ui_improver,true));
	},
	target: ['#hmap_svg']
};
ui_observers.mining_map = {
	get condition() {
		return ui_data.isMining;
	},
	config: {
		childList: true,
		subtree: true
	},
	chooseAction: function(mutations) {
		var action = 0,
			mutation, node;
		for (var i = 0, len = mutations.length; i < len; i++) {
			mutation = mutations[i];
			if (!mutation.addedNodes.length) continue;
			for (node = mutation.target; node; node = node.parentElement) {
				if (node.classList.contains('wrmap')) {
					return 2;
				}
			}
			action = 1;
		}
		return action;
	},
	func: function(mutations, observer) {
		var action = ui_observers.mining_map.chooseAction(mutations);
		if (!action) return;
		ui_improver.improvementDebounce();
		ui_mining[action === 2 ? 'processMap' : 'processStepChange']();
		observer.takeRecords();
	},
	target: ['#r_map .block_title, .e_mt_fight_log', '.wrmap']
};
ui_observers.theme_switcher = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		var href = '', nodes, theme;
		for (var i = 0, len = mutations.length; i < len; i++) {
			nodes = mutations[i].addedNodes;
			if (nodes.length && (href = nodes[0].href) && (theme = /\/stylesheets\/+(th_.*?)\.css/.exec(href))) {
				ui_utils.switchTheme(theme[1]);
				break;
			}
		}
	},
	target: ['head']
};
ui_observers.logger_width_changer = {
	condition: (worker.GUIp_browser !== 'Opera'),
	config: {attributes: true, attributeFilter: ['style']},
	func: ui_utils.loggerWidthChanger,
	target: ['#hero_columns:not([style*="display:none;"]) #central_block, #arena_columns:not([style*="display:none;"]) #a_central_block']
};
ui_observers.node_insertion = {
	condition: true,
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			// to prevent improving WHEN ENTERING FUCKING TEXT IN FUCKING TEXTAREA
			return mutation.addedNodes.length && mutation.addedNodes[0].nodeType !== 3;
		}, ui_improver.improvementDebounce);
	},
	target: ['body']
};
