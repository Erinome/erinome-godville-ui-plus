// ui_data
var ui_data = worker.GUIp.data = {};

// base variables initialization
ui_data.init = function() {
	var onAlertToggle;
	ui_data._initVariables();
	// init mobile cookies
	GUIp.common.forceDesktopPage();
	// desktop notifications permissions
	if (ui_storage.getFlag('Option:enableInformerAlerts') || ui_storage.getFlag('Option:enablePmAlerts')) {
		GUIp.common.notif.initialize();
	}
	onAlertToggle = function(newValue) {
		if (newValue === 'true') GUIp.common.notif.initialize();
	};
	ui_storage.addListener('Option:enableInformerAlerts', onAlertToggle);
	ui_storage.addListener('Option:enablePmAlerts', onAlertToggle);
	ui_data._createNewspaperVars();
	ui_data._getNewspaper();
	GUIp.common.setInterval(ui_data._getNewspaper, 5*60*1000);
};
ui_data._initVariables = function() {
	this.currentVersion = '@VERSION@';
	this.docTitle = document.title;
	this.isMobile = document.body.classList.contains('mbg');
	if (this.isMobile) {
		// adding meaningful classes for titlebar contents
		var statusBarScheme = [
			['hero_hp','godpower'],
			['hero_gold', 'hero_inventory'],
			['opp_hp'],
			['boss_hp','godpower'],
			['boss_bits','boss_push'],
			['ark_hp','godpower'],
			['ark_cargo','ark_supplies']
		];
		var node, statusBarContent = document.querySelectorAll('#statusbar > div');
		for (var i = 0; i < statusBarContent.length; i++) {
			if (!statusBarScheme[i]) break;
			for (var j = 0; j < statusBarScheme[i].length; j++) {
				node = statusBarContent[i].children[j];
				if (!node || !node.lastChild || !node.lastChild.lastChild) continue;
				node.lastChild.lastChild.classList.add('e_sb_' + statusBarScheme[i][j]);
				if (statusBarScheme[i][j].endsWith('_hp') && node.lastChild.lastChild.previousElementSibling) {
					node.lastChild.lastChild.previousElementSibling.classList.add('e_sb_' + statusBarScheme[i][j] + '_diff');
				}
			};
		}
		// creating a way to refer to former blocks
		var bContents = {
			alls: ['Союзники','Участники','Allies','Sailors'],
			opps: ['Противники','Rivals'],
			diary: ['Дневник','Diary'],
			equipment: ['Снаряжение','Equipment'],
			pet: ['Питомец','Pet'],
			friends: ['Друзья','Friends'],
			shop: ['Лавка','Shop'],
			fight_log: /Хроника (боя|подземелья|экспедиции)|(Fight|Dungeon|Sail) Chronicle/
		}
		var bTitles = {
			menu: ['Меню','Menu'],
			remote: ['Пульт', 'Remote'],
			diary: ['Дневник','Diary'],
			stats: ['Данные героя','Данные героини','Stats'],
			equipment: ['Снаряжение','Equipment'],
			skills: ['Умения','Skills'],
			pet: ['Питомец','Pet'],
			shop: ['Лавка','Shop'],
			fight_log: /Хроника (боя|подземелья|экспедиции)|Тренировочный бой|Вести с арены|(Fight|Dungeon|Sail) Chronicle|Sparring Chronicle|Arena Journal/
		}
		Array.from(document.querySelectorAll('.line.hline .l_header')).forEach(function(a) {
			Object.keys(bContents).forEach(function(b) {
				if (a.parentNode.nextElementSibling && Array.isArray(bContents[b]) && bContents[b].includes(a.textContent) ||
					bContents[b] instanceof RegExp && bContents[b].test(a.textContent)) {
					a.parentNode.nextElementSibling.classList.add('e_m_' + b);
				}
			});
			Object.keys(bTitles).forEach(function(b) {
				if (Array.isArray(bTitles[b]) && bTitles[b].includes(a.textContent) ||
					bTitles[b] instanceof RegExp && bTitles[b].test(a.textContent)) {
					a.parentNode.classList.add('e_mt_' + b);
				}
			});
		});
		// special hacks for mining mode (but this.isMining isn't set yet)
		if (document.getElementsByClassName('wrmap')[0]) {
			node = document.getElementsByClassName('e_m_alls')[0];
			// desktop rival #bosses are marked as allies in mobile version, so just re-mark them as opps
			// to be consistent with what we already had before
			if (node) {
				node.className = node.className.replace('e_m_alls','e_m_opps');
			}
			// also there's a header having no id nor special class name
			// nor standard structure for headers used above, so mark it manually
			if (node.nextElementSibling) {
				node.nextElementSibling.classList.add('e_mt_fight_log');
			}
		}
	}
	this.isFight = ui_stats.isFight();
	this.isDungeon = ui_stats.isDungeon();
	this.isSail = ui_stats.isSail();
	this.isMining = ui_stats.isMining();
	this.isBoss = ui_stats.fightType() === 'monster';
	this.logId = ui_stats.logId();
	this.god_name = ui_stats.godName();
	// from now on, ui_storage is fully functional
	this.char_name = ui_stats.charName();
	this.char_sex = ui_stats.isMale() ? worker.GUIp_i18n.hero : worker.GUIp_i18n.heroine;
	GUIp.common.setCurrentGodname(this.god_name);
	ui_storage.set('charName', this.char_name);
	ui_storage.set('charIsMale', ui_stats.isMale());
	if (ui_stats.checkShop()) {
		ui_storage.set('charHasShop',true);
		this.hasShop = true;
	} else {
		this.hasShop = ui_storage.getFlag('charHasShop') || false;
	}
	this.inShop = false;
	this.storedPets = JSON.parse(ui_storage.get('charStoredPets')) || [];
	if (!ui_storage.get('Option:activeInformers')) {
		// default preset of informers
		var informersPreset = {
			full_godpower:48, much_gold:48, dead:48, low_health:48, fight:48, selected_town:48, wanted_monster:48, special_monster:16, tamable_monster:112, chosen_monster:48,pet_knocked_out:48, close_to_boss:48, close_to_rival:48, guild_quest:16, custom_informers:48,
			treasure_box:48, charge_box:48, gift_box:48, good_box:48
		};
		if (!ui_stats.hasTemple()) {
			informersPreset['smelter'] = informersPreset['smelt!'] = informersPreset['transformer'] = informersPreset['transform!'] = 48;
		}
		ui_storage.set('Option:activeInformers',JSON.stringify(informersPreset));
		ui_storage.set('Option:disableDieButton',true);
		ui_storage.set('Option:useShortPhrases',true);
		ui_storage.set('Option:enableInformerAlerts',true);
		ui_storage.set('Option:enablePmAlerts',true);
	}
	if (!ui_storage.get('ForumSubscriptions')) {
		ui_storage.set('ForumSubscriptions', '{}');
		ui_storage.set('ForumInformers', '{}');
	}
	this.availableGameModes = this._getAvailableModes();
	document.body.classList.add(
		'superhero',
		this.isDungeon ? 'dungeon' : this.isSail ? 'sail' : this.isMining ? 'mining' : this.isFight ? 'fight' : 'field'
	);
	if (ui_stats.hasTemple()) {
		document.body.classList.add('has_temple');
	}
	if (this.isFight) {
		var abilities = ui_stats.Enemy_AbilitiesText().toLowerCase(),
			atarget = document.querySelector(ui_data.isMobile ? '.e_m_opps' : '#o_hk_gold_we + .line:not(#o_hk_death_count) .l_val');
		this._parseBossAbs(abilities, false);
		if (/mutating|мутирующий|escalating|крепчающий/i.test(abilities) && atarget) {
			GUIp.common.newMutationObserver(this._parseBossAbs.bind(this, null, true)).observe(atarget,{childList: true});
		}
	} else {
		this.lastFieldInit = ui_storage.set_with_diff('lastFieldInit',Date.now());
	}
	ui_utils.voiceInput = document.getElementById('god_phrase') || document.getElementById('godvoice');
};
ui_data._parseBossAbs = function(abilities, mutated) {
	abilities = abilities || ui_stats.Enemy_AbilitiesText().toLowerCase();
	var abilitiesList = GUIp.common.bossAbilitiesList,
		abilitiesVals = Object.values(abilitiesList);
	Array.from(document.body.classList).forEach(function(a) {
		if (a.startsWith('boss_')) {
			document.body.classList.remove(a);
		}
	});
	abilities.split(',').forEach(function(a) {
		a = a.trim();
		if (abilitiesList[a] || abilitiesVals.includes(a)) {
			document.body.classList.add('boss_' + (abilitiesList[a] || a));
		}
	});
	if (/mutating|мутирующий/i.test(abilities)) {
		var newAbilities = abilities.split(',').map(function(a) { return a.trim(); });
		if (mutated && ui_data._oldAbilities !== undefined) {
			var oldAbility = ui_data._oldAbilities.find(function(a) { return !newAbilities.includes(a); }),
				newAbility = newAbilities.find(function(a) { return !ui_data._oldAbilities.includes(a); });
			if (oldAbility && newAbility) {
				ui_improver.improveMutationChronicles(oldAbility, newAbility);
			}
		}
		ui_data._oldAbilities = newAbilities;
	}
};
ui_data._createNewspaperVars = function() {
	var updateNewspaperSummary = function() {
		ui_improver.showDailyForecast();
		ui_informer.updateCustomInformers();
	};
	ui_improver.wantedItems = ui_storage.createVar('Newspaper:wantedItems',
		function(text) { return text ? new RegExp('^(?:' + text + ')$', 'i') : null; },
		function(regex) { return regex ? regex.source.slice(4, -2) : ''; },
		ui_inventory.tryUpdate
	);
	ui_improver.wantedMonsterRewards = ui_storage.createVar('Newspaper:wantedMonsterRewards',
		function(text) { return JSON.parse(text || '{}'); },
		JSON.stringify
	);
	ui_improver.dailyForecast = ui_storage.createVar('Newspaper:dailyForecast', ui_utils.splitList);
	ui_improver.dailyForecastText = ui_storage.createVar('Newspaper:dailyForecastText',
		function(text) { return text || ''; },
		null,
		updateNewspaperSummary
	);
	ui_improver.dailyForecastSpecBoss = ui_storage.createVar('Newspaper:dailyForecast:specBoss', function(text) { return text || ''; });
	ui_improver.bingoTries = ui_storage.createVar('Newspaper:bingoTries',
		function(text) { return +text || 0; },
		null,
		updateNewspaperSummary
	);
	ui_improver.bingoItems = ui_storage.createVar('Newspaper:bingoItems',
		function(text) { return text ? new RegExp(text, 'i') : null; },
		function(regex) { return regex ? regex.source : ''; },
		ui_inventory.tryUpdate
	);
	ui_storage.addListener('Newspaper:couponPrize', updateNewspaperSummary);
	ui_storage.addListener('Newspaper:godpowerCap', updateNewspaperSummary);
	ui_storage.addListener('Newspaper:activeAdvert', updateNewspaperSummary);
};
ui_data._getNewspaper = function(forced) {
	var date = +ui_storage.get('Newspaper:date');
	if (forced || date !== date || ui_utils.dateToMoscowTimeZone(date) < ui_utils.dateToMoscowTimeZone(Date.now())) {
		if (!forced) {
			ui_improver.wantedItems.setTemp(null);
			ui_improver.wantedMonsterRewards.setTemp({});
			ui_improver.dailyForecast.setTemp([]);
			ui_improver.dailyForecastText.setTemp('');
			ui_improver.dailyForecastSpecBoss.setTemp('');
			ui_improver.bingoTries.setTemp(0);
			ui_improver.bingoItems.setTemp(null);
		}
		GUIp.common.getDomainXHR('/news', ui_data._parseNewspaper);
	} else {
		ui_improver.showDailyForecast();
	}
};
ui_data._parseNewspaper = function(xhr) {
	var newsDate = 0,
		mRewards = {},
		bingoItems = [],
		bingoRE = /<td><span>(.*?)<\/span><\/td>/g,
		text = '',
		temp;
	if (temp = xhr.responseText.match(/<div id="date"[^>]*>[^<]*<span>(\d+)<\/span>/)) {
		newsDate = temp[1] * 86400000 + ((worker.GUIp_locale === 'ru') ? 1195560000000 : 1273492800000);
	} else {
		newsDate = Date.now();
	}
	ui_storage.set('Newspaper:date', newsDate);
	temp = xhr.responseText.match(/(?:Куплю-продам|Buy and Sell)[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>/);
	ui_improver.wantedItems.set(temp && new RegExp('^(?:' + temp[1] + '|' + temp[2] + ')$', 'i'));
	temp = xhr.responseText.match(/(?:Разыскиваются|Wanted)<\/h2>[^]+?<p>([^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?)<\/p>[^]+?<p>([^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?)<\/p>/);
	if (temp) {
		mRewards[temp[2].toLowerCase()] = temp[1].replace(/<a[^]+?<\/a>/g,temp[2]);
		mRewards[temp[4].toLowerCase()] = temp[3].replace(/<a[^]+?<\/a>/g,temp[4]);
	}
	ui_improver.wantedMonsterRewards.set(mRewards);
	temp = xhr.responseText.match(/(?:Астропрогноз|Daily Forecast)[^]+?<p>([^<]+?)<\/p>(?:[^<]+?<p>([^<]+?)<\/p>)?/);
	ui_improver.dailyForecastSpecBoss.set('');
	if (temp) {
		ui_improver.dailyForecastText.set(temp[1] ? (temp[1] + (temp[2] ? '\n' + temp[2] : '')).replace(/&#0149;/g, '•') : '');
		ui_improver.dailyForecast.set(GUIp.common.parseForecasts(ui_improver.dailyForecastText.get()));
		if (ui_improver.dailyForecast.get().includes('specbosses')) {
			var key, input = ui_improver.dailyForecastText.get().toLowerCase();
			for (var key in GUIp.common.bossAbilitiesList) {
				if (input.includes(key) || input.includes(GUIp.common.bossAbilitiesList[key])) {
					ui_improver.dailyForecastSpecBoss.set(ui_utils.capitalizeFirstLetter(worker.GUIp_locale === 'ru' ? key : GUIp.common.bossAbilitiesList[key]));
				}
			}
		}
	} else {
		ui_improver.dailyForecastText.set('');
		ui_improver.dailyForecast.set([]);
	}
	while ((temp = bingoRE.exec(xhr.responseText))) {
		bingoItems.push(temp[1]);
	}
	temp = /<span\s[^<>]*?\bid\s*=\s*["']?b_cnt\b[^<>]*>(.*?)<\//.exec(xhr.responseText); // span#b_cnt
	ui_improver.bingoTries.set((temp && parseInt(temp[1])) || 0);
	ui_improver.bingoItems.set(bingoItems.length ? new RegExp(bingoItems.join('|'), 'i') : null);
	if ((temp = /<br\s*\/?>([^<>]*)<br\s*\/?>\s*<input(\s[^<>]*?\bid\s*=\s*["']?coupon_b\b[^<>]*)/.exec(xhr.responseText))) { // input#coupon_b
		text = /\sdisabled\b/.test(temp[2]) ? '' : temp[1].trim().replace(/&#39;/g, "'");
		ui_storage.set('Newspaper:couponPrize:raw', text);
		ui_storage.set('Newspaper:couponPrize', text && text.replace(/^(?:an?|the|some) /, ''));
	}
	ui_storage.set('Newspaper:godpowerCap', /\bgp_cap_avail\s*=\s*true\b/.test(xhr.responseText));
	if ((temp = /<div\s[^<>]*?\bclass\s*=\s*["']?add_block\b[^<>]*>([^<>]*)\s*<div id="ad_b"><input(\s[^<>]*?\bid\s*=\s*["']?ad_b\b[^<>]*\bvalue\s*=\s*["']?([^"']*)[^<>]*)/.exec(xhr.responseText))) { // input#ad_b
		text = /\sdisabled\b/.test(temp[2]) ? '' : temp[1].trim().replace(/&#39;/g, "'");
		ui_storage.set('Newspaper:activeAdvert', text);
		ui_storage.set('Newspaper:activeAdvert:button', text && temp[3]);
	} else {
		ui_storage.set('Newspaper:activeAdvert', '');
		ui_storage.set('Newspaper:activeAdvert:button', '');
	}
	ui_improver.showDailyForecast();
	if (document.querySelector('#inventory li.improved')) {
		ui_inventory._update();
	}
};

ui_data._getAvailableModes = function() {
	var result = [];
	// dungeon and sailing aren't needed currently
	if ((ui_data.isMining || +ui_storage.get('Logger:Book_Bytes') || +ui_storage.get('Logger:Forge_Rank') || ui_stats.Book_Bytes() || ui_stats.checkDungeonForge())) {
		result.push('mining');
	}
	if ((+ui_storage.get('Logger:Souls') || +ui_storage.get('Logger:Forge_Rank') || ui_stats.Souls() || ui_stats.checkDungeonForge())) {
		result.push('souls');
	}
	return result;
};
