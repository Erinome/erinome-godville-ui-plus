// ui_help
var ui_help = worker.GUIp.help = {};

ui_help.init = function() {
	ui_help._createHelpDialog();
	ui_help._createButtons();
};
// creates ui dialog
ui_help._createHelpDialog = function() {
	document.getElementById('menu_bar').insertAdjacentHTML('afterend',
		'<div id="ui_help" class="hint_bar" style="padding-bottom: 0.7em; display: none;">' +
		'<div class="hint_bar_capt"><b>Erinome Godville UI+ (v' + ui_data.currentVersion + ')</b>, ' + worker.GUIp_i18n.help_dialog_capt + '</div>' +
		'<div class="hint_bar_content" style="padding: 0.5em 0.8em;">'+
			'<div style="text-align: left;">' +
				'<div>' + worker.GUIp_i18n.how_to_update + '</div>' +
				'<ol>' +
					worker.GUIp_i18n['help_update_all'] +
				'</ol>' +
				'<div>' + worker.GUIp_i18n.help_useful_links + '</div>' +
				'<div>' + worker.GUIp_i18n.help_ci_links + '</div>' +
				'<div>' + worker.GUIp_i18n.help_reset_links + '</div>' +
			'</div>' +
		'</div>' +
		'<div class="hint_bar_close"></div></div>'
	);
	GUIp.common.addListener(document.getElementById('checkUIUpdate'), 'click', function(ev) {
		ev.preventDefault();
		ui_utils.checkVersion(ui_utils.informNoNewVersionAvailable, ui_utils.informNewVersionAvailable, ui_utils.informVersionCheckFailed);
	});
	GUIp.common.addListener(document.getElementById('checkCIExpression'), 'click', function(ev) {
		ev.preventDefault();
		ui_informer.checkCustomExpression();
	});
	GUIp.common.addListener(document.getElementById('resetGVSettings'), 'click', function(ev) {
		ev.preventDefault();
		if (worker.confirm(worker.GUIp_i18n.reset_gv_settings)) {
			ui_storage.clear('Godville');
		}
	});
	GUIp.common.addListener(document.getElementById('resetUISettings'), 'click', function(ev) {
		ev.preventDefault();
		if (worker.confirm(worker.GUIp_i18n.reset_ui_settings)) {
			ui_storage.clear('eGUI');
		}
	});
	GUIp.common.addListener(document.getElementById('resetPhrasesDB'), 'click', function(ev) {
		ev.preventDefault();
		localStorage['LogDB:lastSerial'] = "";
		localStorage['LogDB:lastUpdate'] = 0;
		localStorage['LogDB:lastSailSerial'] = "";
		localStorage['LogDB:lastSailUpdate'] = 0;
		worker.alert(worker.GUIp_i18n.reset_phrases_db);
	});
	if (ui_storage.getFlag('helpDialogVisible')) {
		worker.$('#ui_help').show();
	}
};
ui_help._createButtons = function() {
	if (ui_data.isMobile) {
		document.querySelector('.e_mt_menu').insertAdjacentHTML('beforebegin',
			'<div class="line hline"><div class="l_header">UI+</div></div>' +
			'<div class="line e_m_forecast"><div class="l_text">' + worker.GUIp_i18n.daily_forecast + ': [<span id="e_forecast"></span>]</div></div>' +
			'<div class="line e_m_available_coupon hidden"><div class="l_text"></div></div>' +
			'<div class="line e_m_available_ad hidden"><div class="l_text"></div></div>' +
			'<div class="line e_m_ui_settings"><div class="l_text">' + worker.GUIp_i18n.ui_settings + '</div></div>' +
			'<div class="line e_m_ui_help"><div class="l_text">' + ui_utils.capitalizeFirstLetter(worker.GUIp_i18n.help_dialog_capt) + '</div></div>' +
			'<div class="line hline e_mt_forum_informers hidden"><div class="l_header">' + worker.GUIp_i18n.forum_subs + '</div></div>'
		);
		GUIp.common.addListener(document.getElementsByClassName('e_m_ui_settings')[0], 'click', function(e) {
			location.href = '/user/profile#ui_settings';
		});
		GUIp.common.addListener(document.getElementsByClassName('e_m_ui_help')[0], 'click', function(e) {
			ui_help.toggleDialog();
		});
		GUIp.common.addListener(document.getElementsByClassName('e_m_forecast')[0], 'click', function(e) {
			ui_data._getNewspaper(true);
			worker.alert(worker.GUIp_i18n.daily_forecast_update_notice);
		});
		GUIp.common.addListener(document.getElementsByClassName('e_m_available_coupon')[0], 'click', function(e) {
			location.href = '/news';
		});
		GUIp.common.addListener(document.getElementsByClassName('e_m_available_ad')[0], 'click', function(e) {
			location.href = '/news';
		});
		ui_improver.showDailyForecast();
	} else {
		var lastItem = document.querySelector('#menu_bar ul > li:last-child'),
			lastSeparator = lastItem.firstChild;
		if (lastSeparator.nodeType === 3 && lastSeparator.nodeValue.trim() === '|') {
			// Godville puts the last separator in the beginning of the last menu item. move it to the end
			// of the previous one.
			lastItem.previousElementSibling.appendChild(lastSeparator);
		}
		lastItem.insertAdjacentHTML('beforebegin',
			'<li><a href="/user/profile#ui_settings" target="_blank">' + worker.GUIp_i18n.ui_settings_top_menu +
			'</a> | </li><li> | </li>'
		);
		ui_help._addToggleButton(lastItem.previousSibling, '<strong>' + worker.GUIp_i18n.ui_help + '</strong>');
	}
	if (ui_storage.getFlag('Option:enableDebugMode')) {
		ui_help._addDumpButton('<span>dump: </span>', 'all');
		ui_help._addDumpButton('<span>, </span>', 'options', 'Option');
		ui_help._addDumpButton('<span>, </span>', 'logger', 'Logger');
		ui_help._addDumpButton('<span>, </span>', 'forum', 'Forum');
		ui_help._addDumpButton('<span>, </span>', 'log', 'Log:');
	}
	ui_help._addToggleButton(document.getElementsByClassName('hint_bar_close')[0], worker.GUIp_i18n.close);
};
// gets toggle button
ui_help._addToggleButton = function(elem, text) {
	elem.insertAdjacentHTML('afterbegin', '<a class="close_button">' + text + '</a>');
	GUIp.common.addListener(elem.getElementsByClassName('close_button')[0], 'click', function(ev) {
		ev.preventDefault();
		ui_help.toggleDialog();
	});
};
// gets dump button with a given label and selector
ui_help._addDumpButton = function(text, label, selector) {
	var hint_bar_content = document.getElementsByClassName('hint_bar_content')[0];
	hint_bar_content.insertAdjacentHTML('beforeend', text + '<a class="devel_link" id="dump_' + label + '">' + label + '</a>');
	GUIp.common.addListener(document.getElementById('dump_' + label), 'click', function() {
		ui_storage.dump(selector);
	});
};
ui_help.toggleDialog = function(visible) {
	ui_storage.set('helpDialogVisible', !ui_storage.getFlag('helpDialogVisible'));
	worker.$('#ui_help').slideToggle('slow');
};
