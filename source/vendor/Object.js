// polyfills of several ES2015's object methods for Opera 12
(function() {
'use strict';

var descriptor = {configurable: true, writable: true, value: null};

if (!Object.assign) {
	descriptor.value = function assign(target, source) {
		for (var i = 1, len = arguments.length; i < len; i++) {
			source = arguments[i];
			if (source == null) continue;
			var keys = Object.keys(source);
			for (var j = 0, jlen = keys.length; j < jlen; j++) {
				var key = keys[j];
				target[key] = source[key];
			}
		}
		return target;
	};
	Object.defineProperty(Object, 'assign', descriptor);
}

if (!Object.values) {
	descriptor.value = function values(obj) {
		var result = [], keys = Object.keys(obj);
		for (var i = 0, len = keys.length; i < len; i++) {
			result[i] = obj[keys[i]];
		}
		return result;
	};
	Object.defineProperty(Object, 'values', descriptor);
}

})();
