// polyfills of several ES2015's number methods for Opera 12
(function() {
'use strict';

var descriptor = {configurable: true, writable: true, value: null};

if (!Number.isInteger) {
	descriptor.value = function isInteger(x) {
		return Math.floor(x) === x && isFinite(x);
	};
	Object.defineProperty(Number, 'isInteger', descriptor);
}

if (!Number.isSafeInteger) {
	descriptor.value = function isSafeInteger(x) {
		return Math.floor(x) === x && x >= -0x1FFFFFFFFFFFFF && x <= 0x1FFFFFFFFFFFFF;
	};
	Object.defineProperty(Number, 'isSafeInteger', descriptor);
}

if (!Math.log10) {
	descriptor.value = function log10(x) {
		return Math.log(x) / Math.LN10;
	};
	Object.defineProperty(Math, 'log10', descriptor);
}

})();
