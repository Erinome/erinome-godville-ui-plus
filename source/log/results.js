/**
 * @param {string} msg
 * @param {string} heroName
 * @returns {boolean}
 */
ui_log.parseSparResult = function(msg, heroName) {
	var pos = msg.search(/ получает порцию опыта| gets experience points for today/i);
	return pos >= 0 && msg.endsWith(heroName, pos);
};

/**
 * @param {!GUIp.common.activities.Activity} act
 */
ui_log.addActivity = function(act) {
	if (act.date < Date.now() - GUIp.common.activities.storageTime) {
		return;
	}
	var activities = GUIp.common.activities.load(this.storage),
		existing = activities.find(function(existing) {
			return existing.type === act.type && (act.inID ? (existing.inID === act.inID) : (existing.logID === act.logID)); /* souls from the tb have special inID field */
		});
	if (!existing) {
		activities.push(act);
	} else if (existing.result < 0 && !act.inID) {
		existing.result = act.result;
		// should not update date here, since we might have inaccurate one
	} else if (act.inID) {
		// for souls from the tb always update result and update date but only if it's newer
		existing.result = act.result;
		if (existing.date < act.date) {
			existing.date = act.date;
		}
	} else {
		if (existing.result !== act.result) {
			GUIp.common.warn('activity result mismatch:', existing.result, '!=', act.result);
		}
		return;
	}
	GUIp.common.activities.save(this.storage, activities.sort(function(a, b) { return a.date - b.date; }));
};

ui_log.saveSparResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroName = this.getArenaHeroName('hero1_info') || this.getArenaHeroName('hero2_info');
	if (!entry || !heroName) return;
	this.addActivity({
		type: 'spar',
		date: this.getApproximateEndDate(),
		result: +this.parseSparResult(entry.textContent, heroName),
		logID: this.logID
	});
};

ui_log.saveDungeonResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroNames = this.getDungeonHeroNames(),
		date, value;
	if (!entry || !heroNames[this.godname]) return;
	date = this.getApproximateEndDate();
	this.addActivity({
		type: 'dungeon',
		date: date,
		result: GUIp.common.parseDungeonResultFromStep(entry.textContent, heroNames[this.godname], Object.values(heroNames), function(text) { return (text.match(/бревно для ковчега|ещё одно бревно|log for the ark/gi) || '').length; }),
		logID: this.logID
	});
	value = GUIp.common.parseDungeonResultFromStep(entry.textContent, heroNames[this.godname], Object.values(heroNames), GUIp.common.parseGatheredSoul);
	this.addActivity({
		type: 'souls',
		date: date,
		result: value ? 1 : 0,
		logID: this.logID
	});
	if (value > 0) {
		GUIp.common.updateGatheredSouls(ui_log.storage, date, 1, value);
	}
};

ui_log.saveDungeonFightResults = function(steps) {
	var date, value, step, found = 0,
		heroNames = this.getDungeonHeroNames();
	if (!heroNames[this.godname]) return;
	for (step = steps; step > 0; step--) {
		if (this.chronicles[step].marks.includes('boss')) {
			if (value = GUIp.common.parseDungeonResultFromStep(this.chronicles[step].text, heroNames[this.godname], Object.values(heroNames), GUIp.common.parseGatheredSoul)) {
				found = true;
				date = this.getApproximateEndDate(step+1) - 30e3; /* in chronicles boss step contains fight start time, but we need its end */
				this.addActivity({
					type: 'souls',
					date: date,
					result: 1,
					inID: this.logID
				});
				GUIp.common.updateGatheredSouls(ui_log.storage, date, 2, value);
				break; // there's only one soul-containing boss in a dungeon, so no reason to search further
			}
		}
	}
	if (!found) {
		this.addActivity({
			type: 'souls',
			date: this.getApproximateEndDate(),
			result: 0,
			inID: this.logID
		});
	}
};
