var analysis = ui_lmining.analysis = {};
(function() {

/**
 * @typedef {Object} GUIp.log.mining.analysis.BossStats
 * @property {!Array<number>} collected - Indexed by frame number.
 * @property {!Array<number>} pushes - Indexed by frame number.
 * @property {!Array<number>} encouragements - Indexed by frame number.
 * @property {!Array<number>} punishments - Indexed by frame number.
 * @property {!Array<number>} miracles - Indexed by frame number.
 * @property {!Array<number>} createdBits - Indexed by frame number.
 * @property {!Array<number>} successSteps
 */

/**
 * @typedef {Object} GUIp.log.mining.analysis.Model
 * @property {number} bitsPerByte
 * @property {!Array<!GUIp.log.mining.analysis.BossStats>} bossStats
 * @property {!Array<number>} initialBits - Indexed by frame number.
 * @property {!Array<number>} freeUnlockedBits - Indexed by frame number.
 * @property {!Array<number>} freeLockedBits - Indexed by frame number.
 * @property {!Array<number>} synthesizedBits - Indexed by frame number.
 * @property {!Array<number>} destroyedBits - Indexed by frame number.
 * @property {!Array<number>} destroyedUndiscoveredBits - Indexed by frame number.
 * @property {!Array<number>} firstFrame - Indexed by step number.
 * @property {!Array<!GUIp.log.mining.grdigest.MapGroupDigest>} mapGroupDigests - Indexed by step number.
 */

/**
 * @private
 * @typedef {Object} GUIp.log.mining.analysis._Context
 * @property {boolean} noFoggedBits
 * @property {boolean} canSnatchBits
 * @property {number} mapWidth
 * @property {!GUIp.common.mining.Map} oldMap
 * @property {!Array<number>} oldPos
 * @property {!Array<number>} oldHP
 * @property {number} createdBits
 * @property {number} expectedActor
 * @property {!Array<number>} nextAliveBoss
 * @property {{lower: number, upper: number}} boundaries - If `lower <= cur < upper`, can do `cur += step`.
 * @property {{overflownFoggedCells: number, overflownBits: number}} flight
 * @property {!Int8Array} hadSkullAt
 * @property {!Array<number>} skulls
 */

// map legend can be found in common/mining.js
var _charCodes = {
	'?': 0x1,
	'#': 0x2,
	'$': 0x3,
	'*': 0x4,
	'⁂': 0x5,
	'+': 0x6,
	'x': 0x7,
	'A': 0x8,
	'B': 0x9,
	'C': 0xA,
	'D': 0xB,
	'💀': 0x40
};

var _markerCharCodes = {
	'e': 0x10,
	'p': 0x20,
	'm': 0x30
};

var _trailCharCodes = {
	'💀': 0x40,
	'⇡': 0x080,
	'⇢': 0x180,
	'⇣': 0x280,
	'⇠': 0x380
};

/**
 * @param {!Array<!Array<(string|!Array<?string>)>>} rows
 * @returns {!GUIp.common.mining.Map}
 */
analysis.parseStoredMap = function(rows) {
	var map = [],
		k = 0,
		charCodes = _charCodes,
		row, cell;
	for (var i = 0, len = rows.length; i < len; i++) {
		row = rows[i];
		for (var j = 0, jlen = row.length; j < jlen; j++) {
			cell = row[j];
			map[k++] = typeof cell === 'string' ? (
				charCodes[cell] | 0
			) : cell ? (
				charCodes[cell[0]] | _trailCharCodes[cell[1]] | _markerCharCodes[cell[2]]
			) : 0x0; // ' '
		}
	}
	return map;
};

var _parseStoredHP = function(hp) {
	return [hp.A, hp.B, hp.C, hp.D];
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.BossStats} bossStats
 * @param {number} fi - Frame index.
 * @param {{A: number, B: number, C: number, D: number}} collected
 */
var _addCollected = function(bossStats, fi, collected) {
	bossStats[0].collected[fi] = collected.A;
	bossStats[1].collected[fi] = collected.B;
	bossStats[2].collected[fi] = collected.C;
	bossStats[3].collected[fi] = collected.D;
};

/**
 * @private
 * @param {number} bitsPerByte
 * @returns {!GUIp.log.mining.analysis.Model}
 */
var _createModel = function(bitsPerByte) {
	var bossStats = [];
	for (var i = 0; i < 4; i++) {
		bossStats[i] = {
			collected: [0],
			pushes: [0],
			encouragements: [0],
			punishments: [0],
			miracles: [0],
			createdBits: [0],
			successSteps: []
		};
	}
	return {
		bitsPerByte: bitsPerByte,
		bossStats: bossStats,
		initialBits: [0],
		freeUnlockedBits: [0],
		freeLockedBits: [0],
		synthesizedBits: [0],
		destroyedBits: [0],
		destroyedUndiscoveredBits: [0],
		firstFrame: [0],
		mapGroupDigests: [[[], []]]
	};
};

/**
 * @private
 * @param {!Array} initialFrame
 * @param {!Object<string, boolean>} conditions
 * @returns {!GUIp.log.mining.analysis._Context}
 */
var _createContext = function(initialFrame, conditions) {
	var mapRows = initialFrame[1],
		map = analysis.parseStoredMap(mapRows);
	return {
		noFoggedBits: false,
		canSnatchBits: !!conditions.snatching,
		mapWidth: (mapRows[0] && mapRows[0].length) || 1,
		oldMap: map,
		oldPos: [-1, -1, -1, -1],
		oldHP: _parseStoredHP(initialFrame[2]),
		createdBits: 0,
		expectedActor: 0,
		nextAliveBoss: [1, 2, 3, 0],
		boundaries: {lower: 0, upper: 0},
		flight: {overflownFoggedCells: 0, overflownBits: 0},
		hadSkullAt: new Int8Array(map.length),
		skulls: []
	};
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 */
var _processInitialState = function(md, ctx) {
	var map = ctx.oldMap,
		unlocked = 0,
		locked = 0,
		cell = 0x0;
	for (var p = 0, plen = map.length; p < plen; p++) {
		cell = map[p];
		if ((cell & 0xF) === 0x3) { // wall with bits
			locked += 2;
		} else if ((cell & 0xE) === 0x4) { // bits
			unlocked += (cell & 0xF) - 0x3;
		} else if ((cell & 0xC) === 0x8) { // a boss
			ctx.oldPos[cell & 0x3] = p;
		}
	}
	md.initialBits[0] = unlocked + locked;
	md.freeUnlockedBits[0] = unlocked;
	md.freeLockedBits[0] = locked;
	// if we see lots of locked bits on the 1st step, it must be the special condition
	ctx.noFoggedBits = locked >= 32;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {!Array<number>} newHP
 * @returns {number} Bit mask: bosses who could have acted in this frame.
 */
var _determinePossibleActors = function(ctx, newMap, newHP) {
	var p = 0,
		cur = 0x0,
		mask =
			(newHP[0] !== ctx.oldHP[0]) |
			(newHP[1] !== ctx.oldHP[1]) << 1 |
			(newHP[2] !== ctx.oldHP[2]) << 2 |
			(newHP[3] !== ctx.oldHP[3]) << 3;
	for (var i = 0; i < 4; i++) {
		p = ctx.oldPos[i];
		if (p >= 0) {
			cur = newMap[p];
			// set bit if we either moved from here or got a different divine marker than before
			mask |= ((cur & 0xC) !== 0x8 || (cur & 0x30 && (cur & 0x30) !== (ctx.oldMap[p] & 0x30))) << i;
		}
	}
	return mask;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {number} possibleActors
 * @returns {number}
 */
var _guessActor = function(ctx, possibleActors) {
	var actor = 0;
	switch (possibleActors) {
		case 0x1: return 0;
		case 0x2: return 1;
		case 0x4: return 2;
		case 0x8: return 3;
		case 0x0:
			// nobody did anything. either it was the turn of a dead boss, or some stupid boss just stayed there, idle.
			return (
				// here we exploit the fact that dead boss's `nextAliveBoss` property stays constant
				(ctx.oldHP[0] <= 0 && ctx.nextAliveBoss[0] === ctx.expectedActor) ||
				(ctx.oldHP[1] <= 0 && ctx.nextAliveBoss[1] === ctx.expectedActor) ||
				(ctx.oldHP[2] <= 0 && ctx.nextAliveBoss[2] === ctx.expectedActor) ||
				(ctx.oldHP[3] <= 0 && ctx.nextAliveBoss[3] === ctx.expectedActor)
			) ? -1 : ctx.expectedActor;
	}
	// 2 (or more?) possible actors. select one that is closer to the expected.
	actor = ctx.expectedActor;
	while (!(possibleActors & 1 << actor)) {
		actor = ctx.nextAliveBoss[actor];
	}
	return actor;
};

/**
 * @private
 * @param {number} possibleActors
 * @param {number} actor
 * @returns {number}
 */
var _getAssistant = function(possibleActors, actor) {
	if (actor >= 0) {
		switch (possibleActors & ~(1 << actor)) {
			case 0x1: return 0;
			case 0x2: return 1;
			case 0x4: return 2;
			case 0x8: return 3;
		}
	}
	return -1;
};

/**
 * @private
 * @param {!Array<number>} oldPos
 * @param {!GUIp.common.mining.Map} newMap
 * @param {number} actor
 * @param {number} assistant
 * @returns {boolean}
 */
var _wasPushingUsed = function(oldPos, newMap, actor, assistant) {
	var trail = newMap[oldPos[actor]] & 0x380;
	// if two bosses collided, check that they flew in the same direction
	return !!trail && (assistant < 0 || (newMap[oldPos[assistant]] & 0x380) === trail);
};

/**
 * @private
 * @param {number} curCell
 * @param {number} prevCell
 * @param {boolean} pushed
 * @returns {boolean}
 */
var _wasInfluenceUsed = function(curCell, prevCell, pushed) {
	// precondition: curCell & 0x30
	// there are a few cases that this code misses. some of them are checked later.
	return !!((curCell ^ prevCell) & 0x30) || ( // there was a different divine marker or no marker at all; or
		(curCell & 0xC) === 0x4 && ( // there is a bit, medkit, or thruster, and
			!(prevCell & 0xF) || // it was either an empty cell before (perhaps, with a skull), or
			(pushed && (prevCell & 0xC) === 0x8) // there was a boss that got pushed away
		)
	);
};

/**
 * @private
 * @param {number} oldCell
 * @param {!GUIp.common.mining.Map} oldMap
 * @param {number} newPos
 * @param {!Array<number>} collected
 * @param {number} fi - Frame index.
 * @returns {boolean}
 */
var _wasBitSynthesized = function(oldCell, oldMap, newPos, collected, fi) {
	var expected = collected[fi - 1];
	if (newPos >= 0) {
		switch (oldMap[newPos] & 0xF) {
			case 0x3: case 0x5: expected += 2; break; // bits
			case 0x4: expected++; break; // bit
		}
	}
	return (oldCell & 0xF) === 0x4 && collected[fi] >= expected;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {number} pos
 * @param {number} dir
 * @param {{lower: number, upper: number}} boundaries - To be assigned.
 * @returns {number} Step.
 */
var _setupNavigator = function(ctx, pos, dir, boundaries) {
	var mapWidth = ctx.mapWidth,
		totalLen = ctx.oldMap.length;
	switch (dir) {
		case 0x080: // north
			boundaries.lower = mapWidth;
			boundaries.upper = totalLen;
			return -mapWidth;
		case 0x180: // east
			boundaries.lower = 0;
			boundaries.upper = pos - pos % mapWidth + mapWidth - 1;
			return 1;
		case 0x280: // south
			boundaries.lower = 0;
			boundaries.upper = totalLen - mapWidth;
			return mapWidth;
		case 0x380: // west
			boundaries.lower = pos - pos % mapWidth + 1;
			boundaries.upper = totalLen;
			return -1;
	}
	throw new Error('invalid direction: ' + dir);
};

/**
 * @private
 * @param {!GUIp.common.mining.Map} oldMap
 * @param {!GUIp.common.mining.Map} newMap
 * @param {number} pos
 * @param {number} step
 * @param {{lower: number, upper: number}} boundaries
 * @param {{overflownFoggedCells: number, overflownBits: number}} flight - To be assigned.
 * @returns {number} Destination position.
 */
var _trackAliveBossFlight = function(oldMap, newMap, pos, step, boundaries, flight) {
	var lower = boundaries.lower,
		upper = boundaries.upper,
		cur = newMap[pos],
		prev = 0x0,
		ofc = 0,
		ob = 0;
	while ((cur & 0xC) !== 0x8 && pos >= lower && pos < upper) { // until we find the boss
		pos += step;
		cur = newMap[pos];
		prev = oldMap[pos] & 0xF;
		if (prev === 0x1) { // fog of war
			ofc++;
		} else if ((prev & 0xE) === 0x4 && (cur & 0xE) !== 0x4) { // unlocked bits
			ob += prev - 0x3;
		}
	}
	flight.overflownFoggedCells = ofc;
	flight.overflownBits = ob;
	return pos;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {number} pos
 * @param {number} dir
 * @param {number} step
 * @param {{lower: number, upper: number}} boundaries
 * @param {number} forbiddenPos
 * @param {{overflownFoggedCells: number, overflownBits: number}} flight - To be assigned.
 * @returns {number} Destination position.
 */
var _trackDeadBossFlight = function(ctx, newMap, pos, dir, step, boundaries, forbiddenPos, flight) {
	var oldMap = ctx.oldMap,
		lower = boundaries.lower,
		upper = boundaries.upper,
		cur = 0x0,
		prev = 0x0,
		ofc = 0,
		ob = 0,
		skullPos = -1,
		destPos = pos,
		prevOFC = 0,
		prevOB = 0;
	while (
		pos >= lower && pos < upper &&
		(pos += step) !== forbiddenPos && // must not fly over forbidden cell
		((cur = newMap[pos]) & 0x380) === dir && // while the arrow stays the same
		(cur & 0xC) !== 0x8 // until we find another boss
	) {
		if (cur & 0x40) { // a skull
			flight.overflownFoggedCells = ofc;
			flight.overflownBits = ob;
			if (!ctx.hadSkullAt[pos]) return pos;
			skullPos = pos;
		}
		destPos = pos; // this variable is necessary because we can leave the loop with or without incrementing `pos`
		prevOFC = ofc;
		prevOB = ob;
		prev = oldMap[pos] & 0xF;
		if (prev === 0x1) { // fog of war
			ofc++;
		} else if ((prev & 0xE) === 0x4 && (cur & 0xE) !== 0x4) { // unlocked bits
			ob += prev - 0x3;
		}
	}
	if (skullPos >= 0) return skullPos;
	// we exclude the destination cell here
	flight.overflownFoggedCells = prevOFC;
	flight.overflownBits = prevOB;
	return destPos;
};

var _updateExpectedActor = function(ctx, curActor) {
	var next = ctx.nextAliveBoss[curActor];
	if (ctx.oldHP[next] <= 0) { // oops, it's not alive any more
		do {
			next = ctx.nextAliveBoss[next];
		} while (ctx.oldHP[next] <= 0 && next !== curActor);
		ctx.nextAliveBoss[curActor] = next;
	}
	ctx.expectedActor = next;
};

var _processFrame = (function() {

var _discoveredInitialBits = 0,
	_pickedUpInitialBits = 0,
	_unlockedBits = 0,
	_lockedBits = 0,
	_synthesizedBits = 0,
	_destroyedBits = 0,
	_destroyedUndiscoveredBits = 0,
	_usedInfluence = 0x0;

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {boolean} pushed
 * @returns {!Array<number>} Positions of bosses on the new map.
 */
var _scanMap = function(md, ctx, newMap, pushed) {
	var newPos = [-1, -1, -1, -1],
		fi = md.freeUnlockedBits.length, // frame index
		oldMap = ctx.oldMap,
		cur = 0x0,
		prev = 0x0,
		tmp = 0x0,
		collected;
	for (var p = 0, plen = newMap.length; p < plen; p++) {
		cur = newMap[p];
		prev = oldMap[p];
		tmp = cur & 0xF;
		if (tmp >= 0x3 && tmp <= 0x5) { // bits
			if (tmp === 0x3) { // wall with bits
				_lockedBits += 2;
			} else {
				_unlockedBits += tmp - 0x3;
			}
			if ((prev & 0xF) === 0x1 && !(cur & 0x30)) { // bits without divine marker appeared in the fog of war
				_discoveredInitialBits += tmp === 0x4 ? 1 : 2;
			}
		} else if ((cur & 0xC) === 0x8) { // a boss
			newPos[cur & 0x3] = p;
			if ((prev & 0xF) === 0x1 && // fog of war
				(tmp = (collected = md.bossStats[cur & 0x3].collected)[fi] - collected[fi - 1]) > 0
			) {
				_pickedUpInitialBits += tmp;
			}
		} else if ((tmp = prev & 0xF) >= 0x3 && tmp <= 0x5) { // bits
			_destroyedBits += tmp === 0x4 ? 1 : 2;
		} else if (cur & 0x40 && tmp === 0x1 && !ctx.noFoggedBits) { // a skull appeared in the fog of war
			_destroyedUndiscoveredBits += 2;
		}
		if (cur & 0x40) { // a skull
			ctx.skulls.push(p);
		}
		if (cur & 0x30 && _wasInfluenceUsed(cur, prev, pushed)) { // divine marker
			_usedInfluence = cur & 0x30;
		}
	}
	return newPos;
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {!Array<number>} newPos
 * @param {!Array<number>} newHP
 * @param {number} actor
 * @param {number} assistant
 */
var _handleRegularStep = function(md, ctx, newMap, newPos, newHP, actor, assistant) {
	var fi = md.freeUnlockedBits.length, // frame index
		actorPos = 0,
		marker = 0x0;
	if (assistant >= 0) {
		if (_wasBitSynthesized(
			newMap[ctx.oldPos[actor]], ctx.oldMap, newPos[actor], md.bossStats[actor].collected, fi
		)) {
			_synthesizedBits++;
		}
		if (_wasBitSynthesized(
			newMap[ctx.oldPos[assistant]], ctx.oldMap, newPos[assistant], md.bossStats[assistant].collected, fi
		)) {
			_synthesizedBits++;
		}
	} else if (!_usedInfluence && // we have not detected influence yet
		actor >= 0 && (actorPos = newPos[actor]) >= 0 && // actor is alive
		(marker = newMap[actorPos] & 0x30) && // it currently has a divine marker
		!(ctx.oldMap[actorPos] & 0x4F) && // it came to a cell that was empty (without even a skull)
		newHP[actor] !== ctx.oldHP[actor] // but its health changed for some reason
	) {
		_usedInfluence = marker;
		// known bugs:
		// * we can miss influence if an object is placed on a skull with an existing marker and the actor collects
		//   it in that very frame;
		// * we can miss encouragement if a repair kit is placed on an existing marker, the actor collects it
		//   in that very frame but already has maximal health;
		// * we can miss punishment if a thruster is placed on an existing marker, the actor collects it in that
		//   very frame and dies on it;
		// * we can miss influence if an object is placed on an existing marker, then two bosses collide, and
		//   the actor lands on this newly created object - all of that in the same frame.
	}
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {!Array<number>} newPos
 * @param {number} boss
 * @param {boolean} pushed
 * @param {number} forbiddenPos
 */
var _handleSnatchflightFor = function(md, ctx, newMap, newPos, boss, pushed, forbiddenPos) {
	var srcPos = ctx.oldPos[boss],
		srcCell = newMap[srcPos],
		destPos = newPos[boss],
		died = destPos < 0,
		step = _setupNavigator(ctx, srcPos, srcCell & 0x380, ctx.boundaries),
		collected = md.bossStats[boss].collected,
		destWasFogged = true,
		bitsAtDest = 0,
		foggedBits = 0,
		covertBits = 0;
	if (died) {
		destPos = _trackDeadBossFlight(
			ctx, newMap, srcPos, srcCell & 0x380, step, ctx.boundaries, forbiddenPos, ctx.flight
		);
		bitsAtDest = ctx.oldMap[destPos];
		destWasFogged = (bitsAtDest & 0xF) === 0x1; // fog of war
		bitsAtDest = destWasFogged ? 2 : (bitsAtDest & 0xE) === 0x4 ? (bitsAtDest & 0xF) - 0x3 : 0 // unlocked bits
	} else {
		_trackAliveBossFlight(ctx.oldMap, newMap, srcPos, step, ctx.boundaries, ctx.flight);
	}

	foggedBits = ctx.flight.overflownFoggedCells * 2;
	covertBits = collected[collected.length - 1] - collected[collected.length - 2] - ctx.flight.overflownBits;
	// caution: dark magic ahead
	if (covertBits >= 0) {
		if (!pushed && (srcCell & 0xF) === 0x4) { // a bit was dropped
			if (!destWasFogged && !foggedBits && covertBits === bitsAtDest - 1) {
				return; // the boss dropped a bit, then picked up the bit(s) at destination, then died
			}
			_synthesizedBits++;
		}
		if (died) {
			if (destWasFogged && !foggedBits && covertBits === 1) {
				// the boss picked up one fogged bit at destination, then died
				_pickedUpInitialBits++;
				return;
			} else if (covertBits >= foggedBits + bitsAtDest) {
				// the boss picked up all bits on its path, including destination cell, then died
				_pickedUpInitialBits += foggedBits + destWasFogged * 2;
				return;
			}
		}
		if (covertBits > bitsAtDest) {
			_pickedUpInitialBits += Math.min(covertBits - bitsAtDest, foggedBits);
		}
	}
	if (died && (newMap[destPos] & 0xE) !== 0x4) { // the boss died, and there were no bits left on its corpse
		if (!destWasFogged && covertBits < bitsAtDest) {
			// the bits at destination were certainly destroyed
			_destroyedBits += bitsAtDest;
		} else {
			// the bits at destination might be destroyed if there were bits on the boss's path
			// in the fog of war; or there were no bits at destination at all
			_destroyedUndiscoveredBits += bitsAtDest;
		}
	}
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!GUIp.common.mining.Map} newMap
 * @param {!Array<number>} newPos
 * @param {number} actor
 * @param {number} assistant
 * @param {boolean} pushed
 */
var _handleSnatchflight = function(md, ctx, newMap, newPos, actor, assistant, pushed) {
	// we got these variables wrong, need to recalculate them
	_pickedUpInitialBits = _synthesizedBits = _destroyedBits = _destroyedUndiscoveredBits = 0;
	_handleSnatchflightFor(md, ctx, newMap, newPos, actor, pushed, assistant >= 0 ? ctx.oldPos[assistant] : -1);
	if (assistant >= 0) {
		_handleSnatchflightFor(md, ctx, newMap, newPos, assistant, pushed, -1);
	}
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {number} actor
 * @param {boolean} pushed
 */
var _updateModel = function(md, ctx, actor, pushed) {
	var fi = md.freeUnlockedBits.length, // frame index
		created = 0,
		stat;
	md.initialBits[fi] = md.initialBits[fi - 1] + _discoveredInitialBits + _pickedUpInitialBits;
	md.freeUnlockedBits[fi] = _unlockedBits;
	md.freeLockedBits[fi] = _lockedBits;
	md.synthesizedBits[fi] = md.synthesizedBits[fi - 1] + _synthesizedBits;
	md.destroyedBits[fi] = md.destroyedBits[fi - 1] + _destroyedBits;
	md.destroyedUndiscoveredBits[fi] = md.destroyedUndiscoveredBits[fi - 1] + _destroyedUndiscoveredBits;
	for (var boss = 0; boss < 4; boss++) {
		stat = md.bossStats[boss];
		stat.pushes[fi] = stat.pushes[fi - 1];
		stat.encouragements[fi] = stat.encouragements[fi - 1];
		stat.punishments[fi] = stat.punishments[fi - 1];
		stat.miracles[fi] = stat.miracles[fi - 1];
		stat.createdBits[fi] = stat.createdBits[fi - 1];
		if (boss === actor) {
			if (pushed) stat.pushes[fi]++;
			switch (_usedInfluence) {
				case 0x10: stat.encouragements[fi]++; break;
				case 0x20: stat.punishments[fi]++; break;
				case 0x30: stat.miracles[fi]++; break;
			}
			created =
				_unlockedBits + _lockedBits +
				md.bossStats[0].collected[fi] +
				md.bossStats[1].collected[fi] +
				md.bossStats[2].collected[fi] +
				md.bossStats[3].collected[fi] +
				md.destroyedBits[fi] -
				(md.initialBits[fi] + md.synthesizedBits[fi]);
			// known bugs:
			// * we can mistake `initial` for `synthesized` if a boss flew to the fog of war (and survived);
			// * we can miss `created` (or attribute it to a wrong god) if a bit was created on a corpse
			//   and hidden under it (this has been fixed in Godville but can still occur in old logs);
			// * we can miss `synthesized` if a bit was dropped onto a corpse and hidden under it (ditto);
			// * we can mistake `destroyed` for `created` (yes, we're able to deduce that the number of created bits
			//   is negative) if a boss with 2+[7] bits collected a pair: only 1 bit is picked up in that case, while
			//   another is destroyed;
			// * we can wrongly decrease `created` as long as a boss with 3 bosscoins is staying on a bit (that bit
			//   is not destroyed but simply hidden under the boss);
			// * if some other crazy case occurs, it will affect `created` since it's a calculated parameter.
			stat.createdBits[fi] += created - ctx.createdBits;
			ctx.createdBits = created;
		}
	}
};

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!Array} frame
 * @returns {!GUIp.common.mining.Map}
 */
return function _processFrame(md, ctx, frame) {
	var newMap = analysis.parseStoredMap(frame[1]),
		newHP = _parseStoredHP(frame[2]),
		possibleActors = _determinePossibleActors(ctx, newMap, newHP),
		actor = _guessActor(ctx, possibleActors),
		assistant = _getAssistant(possibleActors, actor),
		pushed = actor >= 0 && _wasPushingUsed(ctx.oldPos, newMap, actor, assistant),
		newPos;
	_discoveredInitialBits = _pickedUpInitialBits = _unlockedBits = _lockedBits = _synthesizedBits =
	_destroyedBits = _destroyedUndiscoveredBits = 0;
	_usedInfluence = 0x0;
	ctx.skulls.length = 0;
	_addCollected(md.bossStats, md.freeUnlockedBits.length, frame[4]);
	// process the map
	newPos = _scanMap(md, ctx, newMap, pushed);
	if ((pushed || assistant >= 0) && ctx.canSnatchBits) {
		// trivial processing performed by `_scanMap` is insufficient for handling bits that were snatched on the fly
		_handleSnatchflight(md, ctx, newMap, newPos, actor, assistant, pushed);
	} else if (!pushed) {
		_handleRegularStep(md, ctx, newMap, newPos, newHP, actor, assistant);
	}
	// update model
	_updateModel(md, ctx, actor, pushed);
	// update context
	ctx.oldHP = newHP;
	if (actor >= 0) {
		_updateExpectedActor(ctx, actor);
		// when dead boss A makes its no-turn, all markers disappear from the map for 1 step for some reason. this makes
		// our influence detection code give false positives. thus we only update the context with the new map for those
		// steps where somebody acted.
		ctx.oldMap = newMap;
	}
	ctx.oldPos = newPos;
	for (var i = 0, len = ctx.skulls.length; i < len; i++) {
		ctx.hadSkullAt[ctx.skulls[i]] = 1;
	}
	return newMap;
};

})(); // _processFrame

/**
 * @private
 * @param {!GUIp.log.mining.analysis.Model} md
 * @param {!GUIp.log.mining.analysis._Context} ctx
 * @param {!Array<!Array>} frameGroup
 */
var _processFrameGroup = function(md, ctx, frameGroup) {
	var si = md.firstFrame.length, // step index
		fi = md.freeUnlockedBits.length, // frame index
		prevStepLastFI = fi - 1,
		maps = [],
		i = 0,
		groupLen = frameGroup.length,
		firstActor = -1,
		lastActor = -1,
		stat;
	md.firstFrame[si] = fi;
	for (i = 0; i < groupLen; i++) {
		maps[i] = _processFrame(md, ctx, frameGroup[i]);
		if (si === 2) {
			// in ancient datamines, the order in which bosses acted had been chosen randomly at the start. we need
			// to detect it. at those times, not all bosses might act on the 1st step so we check the 2nd step instead
			// (assuming that incorrect ordering will not get in our way during the processing of first two steps).
			lastActor = lastActor !== -1 ? (
				(ctx.nextAliveBoss[lastActor] = (ctx.expectedActor + 3) & 0x3)
			) : (firstActor = (ctx.expectedActor + 3) & 0x3);
		}
	}
	if (si === 2) ctx.nextAliveBoss[lastActor] = firstActor;
	md.mapGroupDigests[si] = grdigest.create(maps);

	fi = md.freeUnlockedBits.length - 1;
	for (i = 0; i < 4; i++) {
		stat = md.bossStats[i];
		if (Math.floor(stat.collected[fi] / md.bitsPerByte) * md.bitsPerByte > stat.collected[prevStepLastFI]) {
			stat.successSteps.push(si);
		}
	}
};

/**
 * @param {!Array<!Array<!Array>>} frameGroups
 * @param {number} bitsPerByte
 * @param {!Object<string, boolean>} conditions
 * @returns {!GUIp.log.mining.analysis.Model}
 */
analysis.processFrameGroups = function(frameGroups, bitsPerByte, conditions) {
	var md = _createModel(bitsPerByte),
		ctx = _createContext(frameGroups[0][0], conditions);
	_processInitialState(md, ctx);
	for (var i = 1, steps = frameGroups.length; i < steps; i++) {
		_processFrameGroup(md, ctx, frameGroups[i]);
	}
	return md;
};

})(); // ui_log.mining.analysis
