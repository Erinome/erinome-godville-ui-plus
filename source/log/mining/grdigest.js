var grdigest = ui_lmining.grdigest = {};
(function() {

/**
 * @typedef {Array<!Array<number>>} GUIp.log.mining.grdigest.MapGroupDigest
 */

/**
 * Find positions where maps differ. However, if there are only 2 maps, find only their first difference.
 *
 * @private
 * @param {!Array<!GUIp.common.mining.Map>} maps
 * @returns {!Array<number>}
 */
var _findDiffIndices = function(maps) {
	var diffIndices = [],
		firstMap = maps[0],
		ref = 0x0,
		jlen = maps.length;
	if (jlen >= 2) {
		for (var i = 0, len = firstMap.length; i < len; i++) {
			ref = firstMap[i];
			for (var j = 1; j < jlen; j++) {
				if (maps[j][i] !== ref) {
					diffIndices.push(i);
					if (jlen === 2) return diffIndices;
					break;
				}
			}
		}
	}
	return diffIndices;
};

/**
 * @param {!Array<!GUIp.common.mining.Map>} maps
 * @returns {!GUIp.log.mining.grdigest.MapGroupDigest}
 */
grdigest.create = function(maps) {
	var result = [],
		diffIndices = _findDiffIndices(maps),
		len = maps.length,
		jlen = diffIndices.length,
		map, reduced;
	for (var i = 0; i < len; i++) {
		map = maps[i];
		reduced = result[i] = [];
		// keep only those elements that are listed in diffIndices
		for (var j = 0; j < jlen; j++) {
			reduced[j] = map[diffIndices[j]];
		}
	}
	result[len] = diffIndices;
	return result;
};

/**
 * @param {!GUIp.log.mining.grdigest.MapGroupDigest} mapGroupDigest
 * @param {!HTMLCollection} cells
 * @returns {number} Map's index in the group the digest was created for.
 */
grdigest.recognize = function(mapGroupDigest, cells) {
	var candidatesLen = mapGroupDigest.length - 1,
		candidates = new Float64Array(candidatesLen),
		diffIndices = mapGroupDigest[candidatesLen],
		i = 0,
		j = 0,
		len = 0,
		cell = 0x0;
	for (j = 1; j < candidatesLen; j++) {
		candidates[j] = j;
	}
	for (i = 0, len = diffIndices.length; candidatesLen > 1 && i < len; i++) {
		cell = GUIp.common.mining.parseMapCell(cells[diffIndices[i]]);
		for (j = 0; j < candidatesLen; j++) {
			if (mapGroupDigest[candidates[j]][i] !== cell) {
				candidates[j--] = candidates[--candidatesLen];
			}
		}
	}
	return candidates[0];
};

})(); // ui_log.mining.grdigest
