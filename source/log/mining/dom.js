var dom = ui_lmining.dom = {};

/**
 * @returns {!Array<!Array<!Array>>} [step, map, hp, hpDiff, bits]
 */
dom.extractFrameGroups = function() {
	var regex = /\bd\s*=\s*(\[\[\[.*?\]\]\])\s*;/;
	return ui_log.extractFromScripts(regex);
};

/**
 * @returns {number}
 */
dom.getBitsPerByte = function() {
	var rx = /\[.*?\]/;
	// there will be no bracketed part if a boss has collected 3 bosscoins
	return (
		rx.exec(document.getElementById('pl_a_l').textContent) ||
		rx.exec(document.getElementById('pl_b_l').textContent) ||
		rx.exec(document.getElementById('pl_c_l').textContent) ||
		rx.exec(document.getElementById('pl_d_l').textContent)
	)[0].length - 2;
};

/**
 * @returns {string}
 */
dom.getSpecialCondition = function() {
	var node = document.getElementById('rah');
	return node ? node.textContent : '';
};

/**
 * @param {!GUIp.log.mining.analysis.Model} md
 * @returns {number}
 */
dom.getCurrentFrameIndex = function(md) {
	var si = +document.getElementById('turn_num').textContent - 1;
	return md.firstFrame[si] + grdigest.recognize(md.mapGroupDigests[si], document.getElementsByClassName('rmc'));
};
