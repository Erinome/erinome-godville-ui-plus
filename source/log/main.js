(function(worker) {
'use strict';

worker.GUIp = worker.GUIp || {};
var ui_log = GUIp.log = {};

//! include './misc.js';
//! include './results.js';
//! include './lem_uploader.js';
//! include './dungeon.js';
//! include './sailing/package.js';
//! include './mining/package.js';
//! include './saver.js';
//! include './starter.js';
//! include 'mixins/module.js';

registerModule('log', ui_log.starter, ui_log);

})(this);
