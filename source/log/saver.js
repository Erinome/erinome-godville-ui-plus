ui_log.saverSendLog = function() {
	var i, div = document.createElement('div'), inputs = '<input type="hidden" name="bosses_count" value="' + ui_log.saverBossesCnt + '"><input type="hidden" name="log_id" value="' + (ui_log.saverURL.includes('gdvl.tk/') && ui_log.logID.length >= 8 ? ui_log.logID.slice(0,5) : ui_log.logID) + '">';
	for (i = 0; i < ui_log.saverPages.length; i++) {
		inputs += '<input type="hidden" name="' + i + '">';
	}
	if (ui_log.islandsMap.manager && ui_log.islandsMap.manager.rawModel && ui_log.islandsMap.manager.rawModel.pois.length) {
		inputs += '<input type="hidden" name="poi" value="' + ui_log.islandsMap.manager.rawModel.pois + '">';
	}
	inputs += '<input type="hidden" name="tzo" value="' + (new Date()).getTimezoneOffset() + '">';
	if (ui_log.saverPreview) {
		if (ui_log.isDungeonLog) {
			var ndm, ndoc = new DOMParser().parseFromString(ui_log.saverPages[0], "text/html"),
				nsteps, nheader = ndoc.querySelector('#fight_log_capt, #last_items_arena .block_h, #fight_chronicle .block_h, #m_fight_log .block_h, .step_capt');
			if (nheader && /Dungeon Journal|Хроника подземелья/.test(nheader.textContent) && (nsteps = +GUIp.common.matchRegex(/(?:шаг|step)\s*(?:\w*\s*\/\s*)?(\d+)/, nheader.textContent, 0)) === ui_log.steps) {
				ui_log.saverPreview.dmap = Array.from(document.querySelectorAll('#dmap .dml'))
					.map(function(l) { return Array.from(l.querySelectorAll('.dmc'))
						.map(function(c) { return [c.textContent.trim(), c.className, c.title]; }); // we need the already parsed map cause right now we're lazy enough to avoid parsing it on the server-side
					});
				if (ui_log.chronicles[1] && (ndm = GUIp.common.customRegExp.exec(ui_log.chronicles[1].text))) {
					ui_log.saverPreview.dm = GUIp.common.findNonEmptyCapture(ndm) || '-';
				} else {
					ui_log.saverPreview.dm = null;
				}
				ui_log.saverPreview.bc = ui_log.isBroadcast;
				ui_log.saverPreview.date = ui_log.isBroadcast ? Date.now() : +ui_log.getStartDate();
				ui_log.saverPreview.steps = nsteps;
			} else {
				ui_log.saverRemoveLoader();
				if (worker.confirm(worker.GUIp_i18n.map_preview_stale)) {
					worker.location.reload();
				}
				return;
			}
		}
		inputs += '<input type="hidden" name="preview">';
	}
	div.insertAdjacentHTML('beforeend', '<form method="post" action="' + ui_log.saverURL + '" enctype="multipart/form-data" accept-charset="utf-8">' + inputs + '</form>');
	for (i = 0; i < ui_log.saverPages.length; i++) {
		div.querySelector('input[name="' + i + '"]').setAttribute('value', ui_log.saverPages[i]);
	}
	if (ui_log.saverPreview) {
		div.querySelector('input[name="preview"]').setAttribute('value', JSON.stringify(ui_log.saverPreview));
	}
	document.body.appendChild(div);
	div.firstChild.submit();
	document.body.removeChild(div);
};

ui_log.saverFetchPage = function(boss_no) {
	GUIp.common.getDomainXHR(location.pathname + (boss_no ? '?boss=' + boss_no : ''), ui_log.saverProcessPage.bind(this, boss_no), ui_log.saverFetchFailed);
};

ui_log.saverProcessPage = function(boss_no, xhr) {
	if (!xhr.responseText.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c)) && xhr.responseText.includes('class="lastduelpl"')) {
		var page = xhr.responseText.replace(/<img[^>]+>/g, '')
								 .replace(/\.js\?\d+/g, '.js')
								 .replace(/\.css\?\d+/g, '.css')
								 .replace(/не менее 30 дней/, 'по мере возможности')
								 .replace(/for at least 30 days after the fight is over/, 'as far as possible')
		page = page.replace(/(<script[^>]*?>[^]*?<\/script>)/g, function(script) { return script.match(/Tracker|analytics|googletagmanager/) ? '' : script.replace(/\.js\?\d+/g, '.js'); });
		// workaround inability of gdvl.tk to save 9-char logids
		if (ui_log.saverURL.includes('gdvl.tk/') && ui_log.logID.length >= 8) {
			page = page.replace(new worker.RegExp('/+duels/+log/+' + ui_log.logID,'g'),'/duels/log/' + ui_log.logID.slice(0,5));
		}
		if (!page.includes('text/html; charset=')) {
			page = page.replace('<head>', '<head>\n<meta http-equiv="content-type" content="text/html; charset=UTF-8">')
		}
		ui_log.saverPages.push(page);
		if (boss_no < ui_log.saverBossesCnt) {
			GUIp.common.setTimeout(function() { ui_log.saverFetchPage(boss_no + 1); }, 2e3);
		} else {
			ui_log.saverSendLog();
		}
	} else {
		ui_log.saverRemoveLoader();
		worker.alert(worker.GUIp_i18n.saver_error + ' (#1)');
	}
};

ui_log.saverFetchFailed = function() {
	ui_log.saverRemoveLoader();
	worker.alert(worker.GUIp_i18n.saver_error + ' (#2)');
};

ui_log.saverAddLoader = function() {
	document.body.insertAdjacentHTML('beforeend', '<div id="erinome_chronicle_loader" style="position: fixed; left: 50%; top: 50%; margin: -24px; padding: 8px; background: rgba(255,255,255,0.9);"><img src="' + (worker.GUIp_browser !== 'Opera' ? GUIp_getResource('images/loader.gif') : '//gv.erinome.net/images/loader.gif') + '"></div>');
};

ui_log.saverRemoveLoader = function() {
	if (document.getElementById('erinome_chronicle_loader')) {
		document.body.removeChild(document.getElementById('erinome_chronicle_loader'));
	}
};

ui_log.saverPrepareLog = function(type) {
	ui_log.saverPreview = null;
	switch (type) {
		case "preview":
			ui_log.saverPreview = {};
			if (worker.GUIp_locale === 'ru') {
				ui_log.saverURL = '//gv.erinome.net/processpreview';
			} else {
				ui_log.saverURL = '//gvg.erinome.net/processpreview';
			}
			break;
		default:
			if (worker.GUIp_locale === 'ru') {
				ui_log.saverURL = '//gv.erinome.net/processlog';
			} else {
				ui_log.saverURL = '//gvg.erinome.net/processlog';
			}
	}
	try {
		ui_log.saverPages = [];
		if (!ui_log.logID) {
			throw worker.GUIp_i18n.saver_invalid_log;
		}
		if (document.getElementById('search_status') && document.getElementById('search_status').textContent.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c))) {
			throw worker.GUIp_i18n.saver_missing_log;
		}
		if (ui_log.isBroadcast && !ui_log.saverPreview) {
			throw worker.GUIp_i18n.saver_broadcast_log;
		}
		if (document.getElementById('erinome_chronicle_loader')) {
			worker.alert(worker.GUIp_i18n.saver_already_working);
			return;
		} else {
			ui_log.saverAddLoader();
		}
		ui_log.saverBossesCnt = ui_log.saverPreview ? 0 : document.querySelectorAll('a[href*="boss"]').length;
		if (ui_log.saverPreview && document.getElementById('slider') && $('#slider').val() !== $('#slider').attr('max')) { // if #slider exists then jquery also should be available
			$('#slider').val($('#slider').attr('max')).change();
			GUIp.common.setTimeout(function() { ui_log.saverFetchPage(null); }, 5e2);
			return;
		}
		ui_log.saverFetchPage(null);
	} catch (e) {
		ui_log.saverRemoveLoader();
		worker.alert(worker.GUIp_i18n.error_message_subtitle + ' ' + e);
	}
};
