ui_log.starter = function() {
	ui_log.queryString = GUIp.common.parseQueryString(location.search);

	// init mobile cookies
	GUIp.common.forceDesktopPage();

	// detect godname
	this.godname = GUIp.common.getCurrentGodname(this.customDomain);

	// check whether log is finished
	this.isBroadcast = document.getElementsByClassName('lastduelpl')[1] && (new worker.RegExp(worker.GUIp_i18n.saver_broadcast_log_c)).test(document.getElementsByClassName('lastduelpl')[1].textContent);

	// try to load streamed dungeon map
	if (this.logID.length === 9 && this.queryString.get1('estreaming') === '1' && this.isBroadcast && !this.streamRequested) {
		this.streamRequested = true; // to use ONLY once per page load
		var steps = (((document.querySelector('#last_items_arena .block_h') || {}).textContent || '').match(/(?:шаг|step) (?:\d+ \/ )?(\d+)\)/) || [])[1] || 0;
		if (steps > 0 && !document.getElementById('dmap') && !document.getElementById('opps')) {
			var retry = 0,
				fn = function() {
					GUIp.common.getXHR(GUIp.common.erinome_url + '/mapStreamer/port?id=' + ui_log.logID + '&step=' + steps + '&lang=' + worker.GUIp_locale, function(xhr) {
						try {
							var data = JSON.parse(xhr.responseText);
							if (data && data.map) {
								ui_log.restoreDungeonMap(data.map,true);
							}
						} catch (e) {}
						if (!document.getElementById('dmap') && retry++ < 3) {
							GUIp.common.setTimeout(fn.bind(null), retry*250);
						} else {
							GUIp.common.setTimeout(ui_log.starter.bind(ui_log), 50);
						}
					}, function(xhr) {
						if (retry++ < 3) {
							GUIp.common.setTimeout(fn.bind(null), retry*200);
						} else {
							GUIp.common.setTimeout(ui_log.starter.bind(ui_log), 50);
						}
					});
				};
			fn();
			return;
		}
	}

	// check for missing chronicle
	if (document.getElementById('search_status') && document.getElementById('search_status').textContent.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c))) {
		if (!ui_log.customDomain) {
			if (worker.GUIp_locale === 'ru') {
				document.getElementById('search_status').insertAdjacentHTML('beforeend','<br/><br/>Попробуйте поискать хронику на <a href="//gv.erinome.net/duels/log/' + ui_log.logID + '?from=search">gv.erinome.net</a>');
			} else {
				document.getElementById('search_status').insertAdjacentHTML('beforeend','<br/><br/>Try searching your chronicle at <a href="//gvg.erinome.net/duels/log/' + ui_log.logID + '?from=search">gvg.erinome.net</a>');
			}
		}
		return;
	}

	var fight_log_capt = document.querySelector('#fight_log_capt, #last_items_arena .block_h, #fight_chronicle .block_h, #m_fight_log .block_h, .step_capt');
	if (!fight_log_capt) {
		return;
	}

	// clean old entries
	GUIp.common.cleanupLogStorage();

	// mark page as chronicle for use in custom CSS
	document.body.classList.add('chronicle');
	// support other color themes
	GUIp.common.exposeThemeName(localStorage.ui_s || 'th_classic');
	if (worker.GUIp_browser === 'Opera') {
		document.body.classList.add('e_opera');
	}

	// add some styles
	GUIp.common.addCSSFromString(this.storage.get('UserCss'));
	var background = this.storage.get('Option:useBackground');
	if (background) {
		document.querySelector('.lastduelpl').style.cssText = 'margin-top: 0; padding-top: 10px;';
		GUIp.common.setPageBackground(background);
	}

	// live chronicles autoreload timeout
	var tm = document.head.innerHTML.match(/var tm = (\d+)(.\d+)?;/);
	if (tm && +tm[1] > 5) {
		var bar = document.createElement('div');
		bar.id = 'timeout_bar';
		document.body.insertBefore(bar, document.body.firstChild);
		bar.style.transitionDuration = +tm[1] + '000ms';
		GUIp.common.setTimeout(function() { bar.classList.add('running'); }, 100);
	}

	// add save links (but NOT on mobile devices since now godville generates DIFFERENT code for them)
	if (!ui_log.customDomain && !this.isBroadcast) {
		var savelnk, savediv = document.createElement('div');
		if (!document.querySelector('.rpl.mb')) {
			savediv.appendChild(document.createTextNode(worker.GUIp_i18n.save_log_to + ' '));
			savelnk = document.createElement('a');
			if (worker.GUIp_locale === 'ru') {
				GUIp.common.addListener(savelnk, 'click', ui_log.saverPrepareLog.bind(ui_log));
				savelnk.textContent = 'gv.erinome.net';
			} else {
				GUIp.common.addListener(savelnk, 'click', ui_log.saverPrepareLog.bind(ui_log));
				savelnk.textContent = 'gvg.erinome.net';
			}
			savediv.appendChild(savelnk);
		} else {
			savediv.textContent = worker.GUIp_i18n.save_log_fullpage;
		}
		var ldplf = document.getElementsByClassName('lastduelpl_f');
		ldplf[ldplf.length - 1].appendChild(savediv);
	}

	// add usercss for custom domains
	if (ui_log.customDomain) {
		var uclink = document.querySelector('.lastduelpl_f a[href="#"]');
		if (uclink) {
			uclink.insertAdjacentHTML('afterend','<span>, <a id="user_css_edit" href="#">CSS ►</a><div id="user_css_form"><textarea></textarea></div></span>');
			GUIp.common.addListener(document.getElementById('user_css_edit'), 'click', function(ev) {
				ev.preventDefault();
				var ucform = document.getElementById('user_css_form');
				if (ucform.style.display === 'block') {
					this.textContent = 'CSS ►';
					ui_log.storage.set('UserCss',document.querySelector('#user_css_form textarea').value);
					GUIp.common.addCSSFromString(ui_log.storage.get('UserCss'));
					ucform.style.display = 'none';
				} else {
					this.textContent = 'CSS ▼';
					document.querySelector('#user_css_form textarea').value = ui_log.storage.get('UserCss');
					ucform.style.display = 'block';
				}
			});
		}
	} else {
		ui_log.initBlacklisting();
	}

	if (document.getElementsByClassName('rpl')[0]) {
		// make replay's keydown hook less aggressive
		GUIp.common.addListener(worker, 'keydown', function(ev) {
			var target = ev.target, key = ev.keyCode;
			if ((target.type === 'text' || target.tagName === 'TEXTAREA') &&
				(key === 13 || key === 37 || key === 39) && !ev.ctrlKey && !ev.altKey && !ev.shiftKey) {
				ev.stopPropagation();
			}
		}, true);
	}

	if (location.search.length > 1 && location.search !== '?sort=desc') {
		var a = document.querySelector('.block_h > a');
		if (a) {
			var q = ui_log.queryString.clone();
			if (q.get1('sort') === 'desc') {
				q.remove('sort');
			} else {
				q.set('sort', ['desc']);
			}
			a.href = location.pathname + GUIp.common.stringifyQueryString(q);
		}
	}

	// add step numbers to chronicle log
	this.enumerateSteps();

	// gizmo for mutating bosses
	this.processMutatingBoss();

	// save results for last fights list
	if (!this.customDomain && !this.isBroadcast) {
		try {
			if (/Тренировочный бой|Sparring Fight/.test(fight_log_capt.textContent)) {
				this.saveSparResults();
			} else if (/Хроника подземелья|Dungeon Journal/.test(fight_log_capt.textContent)) {
				this.saveDungeonResults();
			}
		} catch (e) {
			GUIp.common.error(e);
		}
	}

	ui_lmining.init();

	if (location.href.includes('boss=') || (!/Хроника (подземелья|заплыва)|(Dungeon|Sail) Journal/.test(fight_log_capt.textContent) && !document.querySelector('#s_map'))) {
		return;
	}

	try {
		this.steps = +GUIp.common.matchRegex(/(?:шаг|step)\s*(?:\w*\s*\/\s*)?(\d+)/, fight_log_capt.textContent, 0);
		// specific code for sailing
		if (this.isSailingLog()) {
			this.improveSailChronicles();
			this.lemUploader.init();
			this.islandsMap.prepare(function(mapContainer) {
				var controls = document.createElement('div');
				controls.className = 'e_sailing_controls';
				controls.appendChild(this.islandsMapCreateRuler());
				controls.appendChild(this.islandsMapCreateOptionBox());
				controls.appendChild(this.createLEMUploaderUI());
				mapContainer.insertAdjacentElement('afterend', controls);
			}, this);
			GUIp.common.renderTester.deinit();
			return;
		}
		// update LEM restrictions
		this.getLEMRestrictions();
		// add a pre-saved map for a translation-type chronicle if available
		if (this.isBroadcast && !document.getElementById('dmap') && this.steps === +this.storage.get('steps', true)) {
			this.restoreDungeonMap(this.storage.getJSON('map', true));
		}

		if (document.querySelector('#hero2 .box') && typeof window.$ !== 'function') {
			worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
			GUIp.common.setTimeout(worker.onscroll, 500);
		}
		// add some colors and other info to the map
		GUIp.common.setExtraDiscardData(Object.values(ui_log.getDungeonHeroNames()));
		if (ui_log.storage.get('dptr',true)) {
			GUIp.common.dmapDisabledPointersCache = ui_log.storage.getJSON('dptr',true);
		}
		if (ui_log.storage.getList('Option:dungeonMapSettings').includes('excl') && ui_log.storage.get('excl',true)) {
			GUIp.common.dmapExclCache = ui_log.storage.getJSON('excl',true);
		}
		GUIp.common.getDungeonPhrases(ui_log.initColorMap.bind(ui_log),ui_log.observeDungeonMap.bind(ui_log));
	} catch (e) {
		GUIp.common.error(e);
	}
};
