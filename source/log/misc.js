ui_log.customDomain = !/^https?:\/\/(b\.)?godville(?:\.net|game\.com)\/+duels\/+log\//.test(location.href);
ui_log.logID = (/\/duels\/+log\/+([^?#]+?)\/*(?:[?#]|$)/.exec(location.pathname) || [])[1];
ui_log.queryString = null;
// .startsWith relies on a polyfill in Opera 12 which may not be loaded yet
ui_log.isStream = location.pathname.indexOf('/reporter/') === 0;
ui_log.chronicles = {};
ui_log.directionlessMoves = null;
ui_log.wormholeMoves = null;
ui_log.dungeonGuidedSteps = null;
ui_log.corrections = {n: 'north', e: 'east', s: 'south', w: 'west'};

/** @namespace */
ui_log.storage = {
	_localPrefix: 'Log:' + ui_log.logID + ':',

	_keyOf: function(id, local) {
		return 'eGUI_' + ui_log.godname + (local ? ':' + this._localPrefix : ':') + id;
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {?string}
	 */
	get: function(id, local) {
		return localStorage.getItem(this._keyOf(id, local));
	},

	/**
	 * @param {string} id
	 * @param {(string|number|boolean)} value
	 * @param {boolean=} local
	 */
	set: function(id, value, local) {
		localStorage[this._keyOf(id, local)] = value;
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 */
	remove: function(id, local) {
		delete localStorage[this._keyOf(id, local)];
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {boolean}
	 */
	getFlag: function(id, local) {
		return this.get(id, local) === 'true';
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {!Array<string>}
	 */
	getList: function(id, local) {
		var s = this.get(id, local);
		return s ? s.split(',') : [];
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {*}
	 */
	getJSON: function(id, local) {
		return GUIp.common.parseJSON(this.get(id, local));
	},

	/**
	 * @param {string} id
	 * @param {*} value
	 * @param {boolean=} local
	 */
	setJSON: function(id, value, local) {
		this.set(id, JSON.stringify(value), local);
	}
};

// TODO: implement more robust checks
ui_log.isOldDungeonLog = function() {
	return this.logID.length === 5;
};

ui_log.isDungeonLog = function() {
	return this.logID.length === 9 && document.getElementById('dmap');
};

ui_log.isSailingLog = function() {
	return this.logID.length === 7;
};

ui_log.clearDungeonPhrases = function() {
	for (var key in localStorage) {
		if (key.startsWith('LogDB:')) {
			localStorage.removeItem(key);
		}
	}
};

/**
 * @param {string} selector
 * @returns {?Element}
 */
ui_log.getChronicleLastLine = function(selector) {
	var items = document.querySelectorAll(selector);
	return items[ui_log.queryString.get1('sort') === 'desc' ? 0 : items.length - 1] || null;
};

/**
 * @param {string} blockID
 * @returns {string}
 */
ui_log.getArenaHeroName = function(blockID) {
	var heroBlock = document.getElementById(blockID), fields;
	if (!heroBlock) return '';
	fields = heroBlock.getElementsByClassName('field_content');
	return fields.length >= 2 && fields[1].textContent.trim() === this.godname ? fields[0].textContent.trim() : '';
};

/**
 * @returns {!Object<string, string>} godName: heroName
 */
ui_log.getDungeonHeroNames = function() {
	var links = document.querySelectorAll('#hero1_info .l_capt a'),
		result = Object.create(null),
		link;
	for (var i = 0, len = links.length; i < len; i++) {
		link = links[i];
		result[decodeURIComponent(/\/([^/]+)$/.exec(link.href)[1])] = link.textContent;
	}
	return result;
};

/**
 * @returns {!Date}
 */
ui_log.getStartDate = function() {
	return GUIp.common.parseDateTime(ui_log.customDomain ? Array.from(document.getElementsByClassName('lastduelpl_f')).map(function(a) { return a.textContent; }).join() : (
		document.getElementsByClassName('ft')[0].textContent
	));
};

/**
 * @returns {number}
 */
ui_log.getApproximateEndDate = function(toStep) {
	var date = ui_log.getStartDate(),
		startDate = +date,
		revCaptions = Array.from(document.querySelectorAll('#last_items_arena .d_capt')),
		caption = '',
		endDate = 0,
		m;
	if (ui_log.queryString.get1('sort') !== 'desc') {
		revCaptions.reverse();
	}
	for (var i = 0, len = revCaptions.length; i < len; i++) {
		if ((caption = revCaptions[i].textContent.trim())) {
			if (toStep && (m = /(?:step|шаг) (\d+)/.exec(caption)) && +m[1] > toStep) {
				continue;
			}
			m = /(\d+):(\d+)/.exec(caption);
			endDate = date.setHours(+m[1], +m[2], 0, 0);
			return endDate >= startDate ? endDate : endDate + 86400e3; // 24h
		}
	}
	throw new Error('cannot determine ' + (toStep ? 'step date in' : 'end date of') + ' the chronicle');
};

ui_log.initBlacklisting = function() {
	var node, players;
	if ((node = document.querySelector('#hero1_info .block_h, #fight_chronicle .block_h'))) {
		node.insertAdjacentHTML('beforeend',
			'<span id="e_ally_blacklist_setup" class="em_font e_t_icon e_icon" title="' + GUIp_i18n.lb_ally_blacklist_title + '">⚙</span>'
		);
		GUIp.common.addListener(node.lastChild, 'click', function() {
			GUIp.common.createLightbox('ally_blacklist', ui_log.storage, GUIp_words(), null);
			document.getElementById('optlightbox').classList.add('e_box');
		});
	}
	players = document.querySelectorAll('#hero1_info .l_capt > span, #h_tbl .t_line a');
	if (!players.length && (node = document.querySelector('#hero1_info a'))) {
		players = [node];
		if ((node = document.querySelector('#hero2_info a'))) {
			players[1] = node;
		}
	}
	GUIp.common.markBlacklistedPlayers(
		players,
		GUIp.common.preprocessPlayerBlacklist(ui_log.storage.getJSON('CustomWords:ally_blacklist') || [])
	);
	Array.prototype.forEach.call(players, GUIp.common.tooltips.watchSubtree);
};

ui_log.processMutatingBoss = function() {
	var abilities = (document.getElementsByClassName('opp_meta')[0] || '').textContent || '';
	if (!/мутирующий|mutating/i.test(abilities)) {
		return;
	}
	var chronicles = Array.from(document.querySelectorAll('#last_items_arena .text_content:not(.infl)')).filter(function(a) {
		if (a.textContent.trim().startsWith('\uD83E\uDDEC ')) { /* 🧬 */
			return true;
		}
		return false;
	});
	var node, currentAbilities = [], oldAbilities = [],
		lostAbility = '', gainedAbility = '',
		allAbilities = worker.GUIp_locale === 'ru' ? Object.keys(GUIp.common.bossAbilitiesList) : Object.values(GUIp.common.bossAbilitiesList),
		capitalize = function(str) { return str.charAt(0).toUpperCase() + str.slice(1); };
	chronicles.forEach(function(a) {
		currentAbilities = [];
		allAbilities.forEach(function(b) {
			if (a.textContent.includes(capitalize(b))) {
				currentAbilities.push(b);
			}
		});
		currentAbilities.forEach(function(b) {
			if (!oldAbilities.includes(b)) {
				gainedAbility = b;
			}
		});
		oldAbilities.forEach(function(b) {
			if (!currentAbilities.includes(b)) {
				lostAbility = b;
			}
		});
		a.innerHTML = a.innerHTML.replace('\uD83E\uDDEC', '<span class="e_mutated e_select_disabled">\uD83E\uDDEC</span>');
		if ((node = a.getElementsByClassName('e_mutated')[0])) {
			if (oldAbilities.length) {
				node.title = capitalize(lostAbility) + ' \u2192 ' /* → */ + capitalize(gainedAbility);
			} else {
				node.title = '(?)' + ' \u2192 ' /* → */ + currentAbilities.filter(function(b) { return !/мутирующий|mutating/.test(b); }).map(capitalize).join(', ');
			}
			GUIp.common.tooltips.watchSubtree(node);
		}
		oldAbilities = currentAbilities;
	});
};

ui_log.getNodeIndex = function(node) {
	var i = 0;
	while ((node = node.previousElementSibling)) {
		i++;
	}
	return i;
};

ui_log.extractFromScripts = function(regex, obj) {
	var scripts = document.getElementsByTagName('script'),
		m;
	for (var i = 0, len = scripts.length; i < len; i++) {
		if ((m = regex.exec(scripts[i].textContent))) {
			try {
				return JSON.parse(m[1]);
			} catch (e) { }
		}
	}
	return obj ? {} : [];
};

ui_log.onscroll = function() {
	var $box = document.querySelector('#hero2 .box'),
		isFixed = $box.style.position === 'fixed',
		resSM = (ui_log.lfExp || !ui_log.customDomain || !ui_log.isOldDungeonLog()) ? 0 : 100;
	if (worker.scrollY > $box.offsetTop && ($box.offsetHeight + resSM) < worker.innerHeight && !isFixed) {
		$box.style.position = 'fixed';
		$box.style.top = 0;
	} else if ((worker.scrollY <= $box.offsetTop || ($box.offsetHeight + resSM) >= worker.innerHeight) && isFixed) {
		$box.style.position = 'static';
	}
};

ui_log.scrollTo = function(target) {
	if (!target) {
		return;
	}
	if (typeof target.scrollIntoView === 'function') {
		// older versions of firefox throw errors with {block: 'center'}, newer versions implement it but use 'start' as default value
		// although in MDN there's clearly stated that center should be the default
		try {
			target.scrollIntoView({behavior: 'smooth', block: 'center'});
		} catch (e) {
			target.scrollIntoView({behavior: 'smooth'});
		}
	}
};
