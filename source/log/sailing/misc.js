/**
 * Find an element suitable for inserting custom controls before it, or null.
 *
 * @param {boolean} [aboveDelimiter=false]
 * @returns {HTMLElement}
 */
ui_log.islandsMapGetAnchor = function(aboveDelimiter) {
	var mapAnchor = document.getElementsByClassName('c_center')[0] || document.getElementById('h_tbl');
	return mapAnchor && ((aboveDelimiter && mapAnchor.previousSibling) || mapAnchor);
};

/**
 * Insert the element before the chronicle anchor.
 *
 * @param {!HTMLElement} elem
 * @param {boolean} [aboveDelimiter=false]
 * @returns {boolean} true iff succeeds.
 */
ui_log.islandsMapInsertElement = function(elem, aboveDelimiter) {
	var mapAnchor = this.islandsMapGetAnchor(aboveDelimiter);
	return !!(mapAnchor && mapAnchor.insertAdjacentElement('beforebegin', elem));
};

/**
 * Insert the HTML before the chronicle anchor.
 *
 * @param {string} html
 * @param {boolean} [aboveDelimiter=false]
 * @returns {boolean} true iff succeeds.
 */
ui_log.islandsMapInsertHTML = function(html, aboveDelimiter) {
	var mapAnchor = this.islandsMapGetAnchor(aboveDelimiter);
	if (!mapAnchor) {
		return false;
	}
	mapAnchor.insertAdjacentHTML('beforebegin', html);
	return true;
};

ui_log.improveSailChronicles = function() {
	GUIp.common.sailing.describeBeastiesOnPage('.new_line, .d_msg:not(.parsed)', null,
		ui_log.storage.getList('Option:islandsMapSettings').includes('bhp')
	);
};

/** @namespace */
ui_log.islandsMap = new function() {
	var imap = this;

	/**
	 * @private
	 * @type {!Object<string, boolean>}
	 */
	imap._conditions = {};

	/** @type {?GUIp.common.islandsMap.IMVManager} */
	imap.manager = null;

	imap._svg = null;

	/**
	 * @private
	 * @type {?MutationObserver}
	 */
	imap._observer = null;

	/**
	 * @private
	 * @type {?function(!GUIp.common.islandsMap.conv.RawModel)}
	 */
	imap._redrawMap = null;

	imap._curExpansion = 0; // 0 (none), 1 (borders), or 2 (fill)

	/** @type {boolean} */
	Object.defineProperty(imap, 'canRedraw', {configurable: true, get: function() { return !!imap._redrawMap; }});

	var _getCurrentStep = function() {
		var node, m;
		return (
			(node = document.getElementById('turn_num')) && +node.textContent
		) || (
			(node = document.getElementById('fight_log_capt')) && (m = /\d+/.exec(node.textContent)) && +m[0]
		) || 0;
	};

	/**
	 * @private
	 * @param {!GUIp.log.islandsMapExt.Data} extracted
	 * @returns {?GUIp.common.islandsMap.conv.RawModel}
	 */
	var _tryCreateRawModel = function(extracted) {
		var map = extracted.map,
			pois = extracted.pois;
		if (!map) {
			// load the model from the local storage (for broadcast page) unless it is disabled in the settings
			return ui_log.storage.getList('Option:islandsMapSettings').includes('conv') ? (
				ui_log.storage.getJSON('model', true)
			) : null;
		}
		if (!pois) {
			// check if the Chronicle Archive tells us points of interest
			pois = GUIp.common.isIntegralArray(worker.e_islandsMapPoints) && e_islandsMapPoints.length ? (
				e_islandsMapPoints
			) : GUIp.common.islandsMap.conv.guessPOIsFromMap(map);
		}
		return GUIp.common.islandsMap.conv.createRawModel(
			_getCurrentStep(), map, GUIp.common.islandsMap.conv.ejectArksFromMap(map), pois
		);
	};

	/**
	 * @private
	 * @param {!Object<number, !Array>} tracks
	 * @returns {!Object<string, string>}
	 */
	var _mergeTracks = function(tracks) {
		var result = {},
			keys = Object.keys(tracks).sort(),
			key = '',
			positions;
		for (var i = 0, len = keys.length; i < len; i++) {
			key = keys[i];
			positions = tracks[key];
			for (var j = 0, jlen = positions.length; j < jlen; j++) {
				result[positions[j]] = key;
			}
		}
		return result;
	};

	/**
	 * @private
	 * @param {!Object<number, !Array>} tracks
	 * @returns {?function(!GUIp.common.islandsMap.conv.RawModel)}
	 */
	var _tryCreateMapRedrawer = function(tracks) {
		var hexer, mergedTracks;
		if (!ui_log.customDomain) {
			return null; // do not ever try to redraw a map on the original domain
		} else if (typeof worker.exported_map_reload === 'function') {
			return function(rawModel) {
				exported_map_reload(rawModel.map);
			};
		} else if (typeof worker.Hexer === 'function') {
			hexer = new Hexer;
			mergedTracks = _mergeTracks(tracks);
			return function(rawModel) {
				hexer.draw_smap($('#sail_map'), rawModel.map, {}, 0, '', mergedTracks, rawModel.pois);
			};
		} else if (typeof worker.make_map_log === 'function') {
			mergedTracks = _mergeTracks(tracks);
			return function(rawModel) {
				make_map_log($('#sail_map'), rawModel.map, {}, 0, '', mergedTracks);
			};
		}
		return null;
	};

	/**
	 * @private
	 * @returns {!Object<string, boolean>}
	 */
	var _extractConditions = function() {
		// load from the local storage
		var conditions = ui_log.storage.get('conds', true),
			data;
		if (conditions != null) {
			return conditions ? GUIp.common.makeHashSet(conditions.split(',')) : Object.create(null);
		}

		// check if we're watching a stream
		data = worker.gReporterClientData; // see superhero/improver.js#reporter.collect
		if (data && (data = data.erinome) && Array.isArray(data.conditions)) {
			return GUIp.common.makeHashSet(data.conditions);
		}

		// parse ourselves
		conditions = GUIp.common.sailing.tryExtractConditions();
		if (conditions != null) {
			return GUIp.common.sailing.parseConditions(conditions);
		}

		GUIp.common.warn('cannot detect sailing conditions, assuming there are none');
		return Object.create(null);
	};

	/**
	 * @private
	 * @param {!GUIp.log.islandsMapExt.Data} extracted
	 * @returns {?GUIp.common.islandsMap.Model}
	 */
	var _tryCreateModel = function(extracted) {
		var rawModel = _tryCreateRawModel(extracted),
			model;
		if (!rawModel) return null;
		model = GUIp.common.islandsMap.conv.decode(rawModel);
		if (extracted.tracks) {
			ui_imapext.setArksFromTracks(model, extracted.tracks);
		}
		return model;
	};

	/**
	 * @private
	 * @param {!GUIp.log.islandsMapExt.Data} extracted
	 * @param {!Object<string, boolean>} conditions
	 * @returns {!GUIp.common.islandsMap.IMVManager}
	 */
	var _createManager = function(extracted, conditions) {
		var mger;
		if (extracted.layeredMap && extracted.tracks) {
			return ui_imapwcm.create(extracted, _getCurrentStep(), conditions);
		}
		mger = new GUIp.common.islandsMap.MigratingMVManager(_tryCreateModel(extracted));
		mger.conditions = conditions;
		return mger;
	};

	/**
	 * @param {string} type
	 */
	imap.changeHintDrawer = function(type) {
		var cimap = GUIp.common.islandsMap;
		cimap.vtrans.hintManager.drawer = cimap.vtrans.createHintDrawer(
			imap.manager.model,
			imap.manager.view,
			type,
			{whirlpoolZoneRadius: cimap.defaults.getWhirlpoolZoneRadius(imap._conditions)}
		);
	};

	var _rollbackModifications = function(svg) {
		if (svg === imap._svg) {
			GUIp.common.islandsMap.defaults.vTransUnbindAll(imap.manager.model, imap.manager.view);
		} else {
			imap._svg = svg;
		}
	};

	/**
	 * @param {!Array<string>} mapSettings
	 * @returns {number}
	 */
	imap.getExpansionLevel = function(mapSettings) {
		return mapSettings.includes('mbc') ? mapSettings.includes('mfc') ? 2 : 1 : 0;
	};

	var _changeExpansion = function(expansionLevel) {
		var caller;
		if (expansionLevel < imap._curExpansion) {
			caller = imap.manager.unexpandMap();
			imap._curExpansion = 0;
		}
		if (expansionLevel > imap._curExpansion) {
			caller = imap.manager.expandMap(expansionLevel === 2);
			imap._curExpansion = expansionLevel;
		}
		if (caller) {
			caller(imap._redrawMap);
			imap.manager.view = null;
		}
	};

	var _replaceView = function(svg) {
		imap._svg = svg;
		imap.manager.replaceView(svg);
	};

	/**
	 * @private
	 * @param {!Element} mapContainer
	 */
	var _applyModifications = function(mapContainer) {
		var settings = ui_log.storage.getList('Option:islandsMapSettings');
		mapContainer.classList.toggle('e_monochrome', settings.includes('mch'));
		GUIp.common.islandsMap.defaults.vTransBindAll(
			imap.manager.model,
			imap.manager.view,
			imap._conditions,
			settings,
			false
		);
		GUIp.common.tooltips.watchSubtreeSVG(document.getElementById('map_wrap') || mapContainer);
	};

	/**
	 * @param {number} expansionLevel
	 */
	imap.changeExpansion = function(expansionLevel) {
		var mapContainer = GUIp.common.sailing.tryFindMapBlock();
		// at the moment, `_redrawMap` always creates an SVG from scratch so rolling back is not necessary.
		// uncomment when this becomes not the case.
		// _rollbackModifications(imap._svg);
		_changeExpansion(expansionLevel);
		_replaceView(mapContainer.getElementsByTagName('svg')[0]);
		_applyModifications(mapContainer);
		imap._observer.takeRecords();
	};

	var _onMapMutation = function() {
		var mapContainer = GUIp.common.sailing.tryFindMapBlock(),
			svg = mapContainer.getElementsByTagName('svg')[0];
		_rollbackModifications(svg);
		imap.manager.replaceModelAndView(svg, _getCurrentStep());
		_applyModifications(mapContainer);
	};

	/**
	 * @private
	 * @param {!Array<string>} settings
	 */
	var _loadPOIColors = function(settings) {
		var colors;
		if (!settings.includes('rndc')) {
			return;
		}
		colors = ui_log.storage.getJSON('poiColors', true);
		if (GUIp.common.isIntegralArray(colors)) {
			GUIp.common.islandsMap.vtrans.poiColorizer.colors = colors;
		} else {
			GUIp.common.shuffleArray(GUIp.common.islandsMap.vtrans.poiColorizer.colors);
		}
	};

	/**
	 * @param {!Element} mapContainer
	 * @param {!Object<string, boolean>} settings
	 */
	var _finishInitialization = function(mapContainer, settings) {
		_loadPOIColors(settings);
		_applyModifications(mapContainer);
		imap._observer = GUIp.common.islandsMap.observer.create(_onMapMutation);
		// in streams, #s_map itself is replaced on each step, not just its contents
		imap._observer.observe(
			ui_log.isStream ? mapContainer.parentNode : mapContainer,
			{childList: true, subtree: true}
		);
	};

	/**
	 * @private
	 * @returns {!Object<number, number>}
	 */
	var _getHP150 = function() {
		var result = {},
			nodes = document.getElementsByClassName('ple');
		for (var i = 0, len = nodes.length; i < len; i++) {
			if (nodes[i].textContent.includes('150')) {
				result[i + 1] = 1;
			}
		}
		return result;
	};

	/**
	 * @param {function(!Element)} onMapPresent
	 * @param {*} [thisArg]
	 */
	imap.prepare = function(onMapPresent, thisArg) {
		var extracted = ui_imapext.extractData(),
			mapContainer, settings, svgs;
		imap._redrawMap = _tryCreateMapRedrawer(extracted.tracks || {});
		imap._conditions = _extractConditions();
		imap.manager = _createManager(extracted, imap._conditions);

		if ((mapContainer = GUIp.common.sailing.tryFindMapBlock())) {
			// the map is already here. it's either a finished chronicle, or a live stream.
			settings = ui_log.storage.getList('Option:islandsMapSettings');
			svgs = mapContainer.getElementsByTagName('svg');
			imap._svg = svgs[0];
			if (!imap.manager.model) {
				imap.manager.replaceModelAndView(imap._svg, _getCurrentStep());
			}
			if (imap.canRedraw) {
				_changeExpansion(imap.getExpansionLevel(settings));
			}
			if (!imap.manager.view) {
				_replaceView(svgs[0]);
			}
			GUIp.common.try2.call(thisArg, onMapPresent, mapContainer);
			_finishInitialization(mapContainer, settings);
		} else if (imap.manager.model) {
			// we're on a broadcast page - draw a map ourselves
			GUIp.common.loadDomainScript('sail_' + worker.GUIp_locale + '_packaged.js',
				function() {
					return typeof worker.HS2 === 'function';
				},
				function() {
					var rawModel, mapContainer;
					if (!ui_log.islandsMapInsertHTML('<div id="sail_map"></div>')) {
						throw new Error('cannot insert a map');
					}

					rawModel = GUIp.common.islandsMap.conv.encode(imap.manager.model);
					GUIp.common.islandsMap.conv.putArksOntoMap(rawModel);
					try {
						new HS2().s(
							// {!Array<number>} map
							rawModel.map,
							// {!Object<number, !Array<number>>} layeredMap
							{},
							// {!Object<number, !Array<string>>} tracks
							{},
							// {!Array<number>} pois
							rawModel.pois,
							// {!Object<number, !Array<number>>} layeredPois
							{},
							// {!Object<number, number>} hp150
							_getHP150(),
							// {number} step
							rawModel.step,
							// {!Object<number, !Object<number, number>>} hp
							{},
							// {!Object<number, !Object<number, string>>} cargo
							{}
						);
					} catch (e) {
						GUIp.common.error('map builder failed:', e);
						return;
					}

					if (!(mapContainer = GUIp.common.sailing.tryFindMapBlock())) {
						throw new Error('#sail_map got lost after creating a map'); // evil HS2...
					}
					_replaceView(mapContainer.getElementsByTagName('svg')[0]);
					GUIp.common.try2.call(thisArg, onMapPresent, mapContainer);
					_finishInitialization(mapContainer, ui_log.storage.getList('Option:islandsMapSettings'));
				},
				function() {
					GUIp.common.error('cannot load the map builder');
				}
			);
		} else {
			// we're on a broadcast page but don't have a map
			GUIp.common.info('no data to build a map with');
		}
	};
};

ui_log.islandsMapCreateRuler = function() {
	var container = document.createElement('div');
	container.className = 'e_ruler_button_wrap';
	container.innerHTML = '<span id="e_ruler_button" class="e_emoji e_emoji_ruler eguip_font" title="' +
		GUIp_i18n.sail_ruler +
	'">📏</span>';
	GUIp.common.islandsMap.vtrans.rulerManager.init(container.lastChild);
	return container;
};

/**
 * @class
 */
ui_log.IslandsMapOptionBox = function() {
	/** @type {!Element} */
	this.container = document.createElement('div');
	this.container.className = 'e_option_box';
	/**
	 * @private
	 * @type {!Object<string, !HTMLInputElement>}
	 */
	this._group = {};
	var mapSettings = ui_log.storage.getList('Option:islandsMapSettings');
	if (ui_log.islandsMap.canRedraw) {
		this.container.appendChild(this._createOption('mbc', mapSettings));
		this.container.appendChild(document.createTextNode(' '));
		this.container.appendChild(this._createOption('mfc', mapSettings));
		this.container.appendChild(document.createTextNode(' '));
	}
	this.container.appendChild(this._createOption('shh', mapSettings));
};

ui_log.IslandsMapOptionBox.prototype = {
	constructor: ui_log.IslandsMapOptionBox,

	/**
	 * @private
	 * @param {string} optionName
	 */
	_onClick: function(optionName) {
		var settings = ui_log.storage.getList('Option:islandsMapSettings'),
			checked = this._group[optionName].checked,
			needsRedrawing = false,
			index = -1;
		if (checked) {
			if (!settings.includes(optionName)) {
				settings.push(optionName);
			}
		} else {
			GUIp.common.linearRemove(settings, optionName);
		}

		switch (optionName) {
			case 'mbc':
				if (!checked && (index = settings.indexOf('mfc')) >= 0) {
					settings.splice(index, 1);
					this._group.mfc.checked = false;
				}
				needsRedrawing = true;
				break;

			case 'mfc':
				if (checked && !settings.includes('mbc')) {
					settings.push('mbc');
					this._group.mbc.checked = true;
				}
				needsRedrawing = true;
				break;

			case 'shh':
				ui_log.islandsMap.changeHintDrawer(
					checked ? 'polylinear' : settings.includes('newhints') ? 'tileMarking' : 'tileMarkingOld'
				);
				break;
		}

		ui_log.storage.set('Option:islandsMapSettings', settings);
		if (needsRedrawing) {
			ui_log.islandsMap.changeExpansion(ui_log.islandsMap.getExpansionLevel(settings));
		}
	},

	/**
	 * @private
	 * @param {string} optionName
	 * @param {!Array<string>} mapSettings
	 * @returns {!Element}
	 */
	_createOption: function(optionName, mapSettings) {
		var checkbox = this._group[optionName] = document.createElement('input');
		checkbox.type = 'checkbox';
		if (mapSettings.includes(optionName)) {
			checkbox.checked = true;
		}
		GUIp.common.addListener(checkbox, 'click', this._onClick.bind(this, optionName));

		var label = document.createElement('label');
		label.textContent = worker.GUIp_i18n['islands_map_' + optionName];
		label.insertAdjacentElement('afterbegin', checkbox);
		return label;
	}
};

/**
 * @returns {!Element}
 */
ui_log.islandsMapCreateOptionBox = function() {
	return new this.IslandsMapOptionBox().container;
};
