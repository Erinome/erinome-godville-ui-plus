ui_log.dmapDynamic = document.body.innerHTML.indexOf('var d_maps = [[[') !== -1; // this is how we check whether this log has dynamic dmap. any better idea?
ui_log.dmapMaxStep = function() {
	var steps = Object.keys(this.chronicles);
	if (this.dmapDynamic) {
		return Math.min(+(document.getElementById('turn_num') || '').textContent || Infinity,steps.length);
	}
	return steps.length;
}

ui_log.extractDungeonExcls = function() {
	var maps = this.extractFromScripts(/\bd_maps\s*=\s*(\[\[\[.*?\]\]\])\s*;/),
		titles = this.extractFromScripts(/\bd_aura\s*=\s*({.*?})\s*;/);
	GUIp.common.dmapExclCache = {};
	for (var k = 0, len = this.dmapMaxStep(); k < len; k++) {
		if (!maps[k] || !titles[k]) continue;
		var cellarShift = /, (2Э|B)$/.test(titles[k]) ? 100 : 0,
			exitMark = cellarShift ? '◿' : '🚪',
			groupPos, exitPos;
		for (var i = 0, len2 = maps[k].length; i < len2; i++) {
			for (var j = 0, len3 = maps[k][i].length; j < len3; j++) {
				if (maps[k][i][j] === '@') {
					groupPos = [i,j];
				}
				if (maps[k][i][j] === exitMark) {
					exitPos = [i,j];
				}
			}
		}
		if (exitPos === undefined) {
			if (groupPos !== undefined) {
				exitPos = groupPos;
			} else {
				continue;
			}
		}
		for (var ki, i = 0, len2 = maps[k].length; i < len2; i++) {
			ki = i - exitPos[0] + cellarShift;
			for (var kj, j = 0, len3 = maps[k][i].length; j < len3; j++) {
				kj = j - exitPos[1] + cellarShift;
				if (maps[k][i][j] === '!') {
					if (!GUIp.common.dmapExclCache[ki]) GUIp.common.dmapExclCache[ki] = {};
					GUIp.common.dmapExclCache[ki][kj] = true;
				} else if (maps[k][i][j] !== '?' && GUIp.common.dmapExclCache[ki] && GUIp.common.dmapExclCache[ki][kj]) {
					delete GUIp.common.dmapExclCache[ki][kj];
				}
			}
		}
	}
	Object.keys(GUIp.common.dmapExclCache).forEach(function(a) {
		if (!Object.keys(GUIp.common.dmapExclCache[a]).length) {
			delete GUIp.common.dmapExclCache[a];
		}
	});
}

ui_log.processDungeonMap = function(parsed) {
	this.prepareMap();
	if (parsed) {
		this.describeMap();
	}
	GUIp.common.dmapDisabledPointersBind(function() {
		ui_log.storage.setJSON('dptr', GUIp.common.dmapDisabledPointersCache, true);
	}, this.highlightTreasuryZone);
	this.highlightTreasuryZone();
	GUIp.common.tooltips.watchSubtree(document.getElementById('dmap'));
	GUIp.common.dmapCoords();
	if (this.customDomain || !this.isBroadcast && this.storage.getList('Option:dungeonMapSettings').includes('excl')) {
		this.extractDungeonExcls();
	}
	if (Object.keys(GUIp.common.dmapExclCache).length) {
		GUIp.common.dmapExcl();
	}
	if ((ui_log.customDomain || ui_log.storage.getList('Option:dungeonMapSettings').includes('dims')) && document.querySelector('#hero2 .block_h')) {
		var mapDimensions = document.querySelector('.dmapDimensions');
		if (!mapDimensions) {
			mapDimensions = document.createElement('span');
			mapDimensions.classList.add('dmapDimensions');
			document.querySelector('#hero2 .block_h').appendChild(mapDimensions);
		}
		mapDimensions.textContent = GUIp.common.dmapDimensions();
	}
};

ui_log.initColorMap = function() {
	if (localStorage.getItem('debugGVP') || ui_log.storage.getFlag('Option:enableDebugMode')) {
		GUIp.common['treasureChestRegExp'] = new worker.RegExp(localStorage.getItem('LogDB:treasureChestPhrases').replace(/(plunder the treasure trove and divide the loot|потрошат сокровищницу|делят награб)\|?/g,'').replace(/\|$/,''));
		GUIp.common['bossRegExp'] = new worker.RegExp(localStorage.getItem('LogDB:bossPhrases').replace(/(The heroes defeated|defeated the nasty creature|arrogant monster|the heroes are deciding what to do next|successfully defeating the boss|defeated the nasty boss|тяжело рухнул|победу над наглым|Собранные судьбой|предсмертными хрипами|золото и трофеи|порван на куски|над исчадием зла|побеждённого в столичный зоопарк|плохо себя вёл и остаётся без обеда. А также золота)\|?/g,'').replace(/\|$/,''));
	}
	// finally process everything
	if (this.steps > 1) {
		GUIp.common.parseChronicles.call(ui_log, document.body, this.steps, {
			compat: !!document.getElementById('fight_log_capt'),
			putDMLink: this.customDomain || ui_log.storage.getList('Option:dungeonMapSettings').includes('dmln'),
			gvURL: this.customDomain ? (
				/^(?:https?:\/\/)?[^/]*/.exec(document.querySelector('#hero1_info a').href)[0]
			) : ''
		});
		// as we now have chronicles parsed, check for possibly gathered souls after the fights
		if (!this.customDomain && !this.isBroadcast) {
			this.saveDungeonFightResults(this.steps);
		}
	}
	if ((this.isCGM = !document.getElementById('dmap'))) {
		this.buildMap();
	}
	this.observeDungeonMap(true);
};

ui_log.observeDungeonMap = function(parsed) {
	var dmap = document.getElementById('dmap');
	if (dmap) {
		this.processDungeonMap(parsed);
		GUIp.common.newMutationObserver(function(mutations, observer) {
			if (!dmap.getElementsByClassName('.map_pos')[0]) {
				dmap.classList.remove('e_prepared', 'e_described');
				this.processDungeonMap(parsed);
				observer.takeRecords();
			}
		}.bind(this)).observe(dmap, { childList: true, subtree: true });
		this.createDungeonButtons();
	}
};

ui_log.prepareMap = function() {
	// make dmap feel a bit like normal map
	var dmap = document.getElementById('dmap');
	if (dmap.classList.contains('e_prepared')) {
		return;
	}
	dmap.innerHTML = dmap.innerHTML.replace(/>\s{2,}</g, "><");
	var cells = document.querySelectorAll('.dml .dmc');
	for (var i = 0, len = cells.length; i < len; i++) {
		if (cells[i].textContent.includes('@')) {
			cells[i].classList.add('map_pos');
			break;
		}
	}
	// expand minimap
	try {
		var mw, styles, overhead = 0, boxStyle;
		styles = worker.getComputedStyle(dmap);
		overhead += parseInt(styles.paddingLeft) + parseInt(styles.paddingRight);
		styles = worker.getComputedStyle(dmap.parentNode);
		overhead += parseInt(styles.paddingLeft) + parseInt(styles.paddingRight) + 2;
		overhead = Math.max((overhead || 0), 20);
		if (worker.GUIp_browser !== 'Opera') {
			dmap.style.width = dmap.scrollWidth > (400 - overhead) ? (400 - overhead) + 'px' : 'initial';
		} else {
			dmap.style.width = dmap.scrollWidth > (400 - overhead) ? (400 - overhead) + 'px' : 'inherit';
			dmap.style.overflowY = 'auto';
			dmap.style.paddingBottom = '25px';
		}
		mw = Math.min((dmap.scrollWidth + overhead),400) + 'px';
		boxStyle = dmap.parentNode.parentNode.style;
		GUIp.common.addListener(worker, 'scroll', function() {
			boxStyle.width = mw;
		});
		boxStyle.width = mw;
		// prevent hiding the horizontal scrollbar on narrow screens when the page is scrolled down
		if (overhead = document.getElementById('hscrollfix')) {
			overhead.style.width = mw;
		} else {
			dmap.parentNode.parentNode.insertAdjacentHTML('afterend', '<div id="hscrollfix" style="width: ' + mw + ';">\xA0</div>');
		}
	} catch (e) {
		GUIp.common.error('expanding minimap has failed:', e);
	}
	dmap.classList.add('e_prepared');
};

ui_log.buildMap = function() {
	var step, lastSentence, blocked, bounds, txmi, txma, pos = {x: 0, y: 0},
		mapData = {}, mapArray = [], text, noffset, soffset,
		trapMoveLossCount = 0,
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length,
		dtitle, isStepInCellar = false, isMapInCellar = false;
	this.directionlessMoves = ui_log.storage.getJSON('dlMoves', true) || {};
	this.wormholeMoves = ui_log.storage.getJSON('whMoves', true) || {};
	if (location.search) {
		if ((text = ui_log.queryString.get1('edm'))) {
			text.split(',').forEach(function(a) { a = a.split(':'); ui_log.directionlessMoves[+a[0]] = a[1]; });
		}
		if ((text = ui_log.queryString.get1('ewh'))) {
			text.split(',').forEach(function(a) { a = a.split(':'); ui_log.wormholeMoves[+a[0]] = [-a[2],+a[1]]; });
		}
		if ((text = ui_log.queryString.get1('eno'))) {
			text = text.split(':');
			noffset = [-text[1],text[0]];
		}
		if ((text = ui_log.queryString.get1('eso'))) {
			text = text.split(':');
			soffset = [-text[1],text[0]];
		}
	}
	var setCell = function(position, content) {
		if (!mapData[position.y]) {
			mapData[position.y] = {}
		}
		if ((content === '#' || content === ' ') && mapData[position.y][position.x] !== undefined) {
			return;
		}
		mapData[position.y][position.x] = content;
	}
	var matchPointers = function(cellPointers, matchPointers) {
		return cellPointers.includes(matchPointers[0]) && cellPointers.includes(matchPointers[1]);
	}
	// detecting on which floor we are currently on
	for (step = 1; step <= steps_max; step++) {
		if (this.chronicles[step].marks.includes('staircase')) {
			isMapInCellar = !isMapInCellar;
		}
	}
	// iterate normally
	for (step = 1; step <= steps_max; step++) {
		if (!this.chronicles[step]) {
			GUIp.common.warn('data from step #' + step + ' missing: map generation not possible!');
			this.buildMapError = worker.GUIp_i18n.dungeon_map_failed;
			break;
		}
		// check moving to or from the subfloor
		if (this.chronicles[step].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// we are NOT interested in building the map of the wrong floor
		if (isStepInCellar !== isMapInCellar) {
			continue;
		}
		// trying to build the usual way
		if (this.chronicles[step].directionless) {
			if (this.directionlessMoves[step]) {
				GUIp.common.moveCoords(pos, { direction: this.corrections[this.directionlessMoves[step]] });
			} else {
				var newDirection = null;
				if (this.chronicles[step-1]) {
					var prevDirection = this.chronicles[step-1].direction;
					if ((blocked = GUIp.common.calcBlocked(this.chronicles, step-1)) && blocked.toString(2).match(/1/g).length === 3) { // this is effectively the dead end - there's only one available exit (as 3 of 4 are blocked)
						switch (~blocked & 0x0f) {
							case 0x01: newDirection = 'north'; break;
							case 0x02: newDirection = 'south'; break;
							case 0x04: newDirection = 'west'; break;
							case 0x08: newDirection = 'east'; break;
						}
					} else if (this.chronicles[step].infls.length && (newDirection = GUIp.common.effectiveGodvoiceDirection(this.chronicles,step))) { // this _can_ make false assumptions but it's better than nothing
						GUIp.common.debug('tried to guess directionless move from step #' + step + ' to "' + newDirection + '" based on godvoice commands');
					} else {
						switch (prevDirection) {
							case 'север':
							case 'north':
								if ((blocked & 0x08) && !(blocked & 0x01)) {
									newDirection = 'north';
								}
								break;
							case 'юг':
							case 'south':
								if ((blocked & 0x04) && !(blocked & 0x02)) {
									newDirection = 'south';
								}
								break;
							case 'запад':
							case 'west':
								if ((blocked & 0x01) && !(blocked & 0x04)) {
									newDirection = 'west';
								}
								break;
							case 'east':
							case 'восток':
								if ((blocked & 0x02) && !(blocked & 0x08)) {
									newDirection = 'east';
								}
								break;
						}
						if (newDirection) {
							GUIp.common.info('tried to guess directionless move at step #' + step + ' to "' + newDirection + '" based on the motion vector');
						}
					}
				}
				if (newDirection) {
					GUIp.common.moveCoords(pos, { direction: newDirection });
				} else {
					GUIp.common.warn('detected directionless move at step #' + step + ': map generation not possible!');
					setCell(pos, '@');
					this.buildMapError = worker.GUIp_i18n.fmt('cgm_no_dlm',step);
					break;
				}
			}
		} else {
			GUIp.common.moveCoords(pos, this.chronicles[step]);
		}
		if (this.chronicles[step].wormhole) {
			setCell(pos, '~');
			if (this.wormholeMoves[step]) {
				pos.y += this.wormholeMoves[step][0];
				pos.x += this.wormholeMoves[step][1];
			} else {
				this.buildMapError = worker.GUIp_i18n.fmt('cgm_no_wm',step);
				break;
			}
		}
		if (this.chronicles[step].pointers.length) {
			var pchar = ' ';
			if (this.chronicles[step].pointers.length === 2) {
				if (matchPointers(this.chronicles[step].pointers, ['north', 'east'])) {
					pchar = '⌊';
				} else if (matchPointers(this.chronicles[step].pointers, ['north', 'west'])) {
					pchar = '⌋';
				} else if (matchPointers(this.chronicles[step].pointers, ['south', 'east'])) {
					pchar = '⌈';
				} else if (matchPointers(this.chronicles[step].pointers, ['south', 'west'])) {
					pchar = '⌉';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_east', 'north_west'])) {
					pchar = '∨';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_east', 'south_east'])) {
					pchar = '<';
				} else if (matchPointers(this.chronicles[step].pointers, ['south_east', 'south_west'])) {
					pchar = '∧';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_west', 'south_west'])) {
					pchar = '>';
				}
			} else {
				switch (this.chronicles[step].pointers[0]) {
					case 'north_east': pchar = '↗'; break;
					case 'north_west': pchar = '↖'; break;
					case 'south_east': pchar = '↘'; break;
					case 'south_west': pchar = '↙'; break;
					case 'north_side': pchar = '╩'; break;
					case 'east_side':  pchar = '╠'; break;
					case 'south_side': pchar = '╦'; break;
					case 'west_side':  pchar = '╣'; break;
					case 'north':      pchar = '↑'; break;
					case 'east':       pchar = '→'; break;
					case 'south':      pchar = '↓'; break;
					case 'west':       pchar = '←'; break;
					case 'freezing': pchar = '✵'; break;
					case 'cold':     pchar = '❄'; break;
					case 'mild':     pchar = '☁'; break;
					case 'warm':     pchar = '♨'; break;
					case 'hot':      pchar = '☀'; break;
					case 'burning':  pchar = '✺'; break;
				}
			}
			setCell(pos, pchar);
		} else if (this.chronicles[step].marks.includes('boss')) {
			setCell(pos, '💀');
		} else if (this.chronicles[step].marks.join().includes('trap')) {
			setCell(pos, '🕳');
		} else if (this.chronicles[step].marks.includes('staircase') || this.chronicles[step].marks.includes('staircaseHint')) {
			setCell(pos, '◿');
		} else {
			setCell(pos, ' ');
		}
		if (blocked = GUIp.common.calcBlocked(this.chronicles, step)) {
			if (blocked & 0x01) {
				setCell({y: pos.y - 1, x: pos.x}, '#');
			}
			if (blocked & 0x02) {
				setCell({y: pos.y + 1, x: pos.x}, '#');
			}
			if (blocked & 0x04) {
				setCell({y: pos.y, x: pos.x - 1}, '#');
			}
			if (blocked & 0x08) {
				setCell({y: pos.y, x: pos.x + 1}, '#');
			}
		}
	}
	if (noffset && !isMapInCellar) {
		setCell({x: noffset[1], y: noffset[0]}, '✖');
	}
	if (soffset && !isMapInCellar) {
		setCell({x: soffset[1], y: soffset[0]}, '◿');
	}
	setCell({x: 0, y: 0}, !isMapInCellar ? '🚪' : '◿');
	if (step > steps_max) {
		setCell(pos, '@');
	}
	var bounds = {xmi: 0, xma: 0, ymi: 0, yma: 0};
	for (var y in mapData) {
		txmi = Math.min.apply(null,Object.keys(mapData[y]));
		txma = Math.max.apply(null,Object.keys(mapData[y]));
		if (txmi < bounds.xmi) {
			bounds.xmi = txmi;
		}
		if (txma > bounds.xma) {
			bounds.xma = txma;
		}
	}
	bounds.ymi = Math.min.apply(null,Object.keys(mapData));
	bounds.yma = Math.max.apply(null,Object.keys(mapData));
	txmi = false;
	txma = false;
	for (var y in mapData) {
		if (!txmi && mapData[y][bounds.xmi] && mapData[y][bounds.xmi] !== '#') {
			bounds.xmi--;
			txmi = true;
		}
		if (!txma && mapData[y][bounds.xma] && mapData[y][bounds.xma] !== '#') {
			txma = true;
			bounds.xma++;
		}
	}
	for (var x in mapData[bounds.ymi]) {
		if (mapData[bounds.ymi][x] !== '#') {
			bounds.ymi--;
			break;
		}
	}
	for (var x in mapData[bounds.yma]) {
		if (mapData[bounds.yma][x] !== '#') {
			bounds.yma++;
			break;
		}
	}
	for (var y = bounds.ymi, i = 0; y <= bounds.yma; y++, i++) {
		mapArray.push([]);
		for (var x = bounds.xmi; x <= bounds.xma; x++) {
			if (!mapData[y] || !mapData[y][x]) {
				mapArray[i].push('?');
			} else {
				mapArray[i].push(mapData[y][x]);
			}
		}
	}
	dtitle = document.querySelector('.lastduelpl').textContent.trim();
	if (isMapInCellar && !dtitle.includes(worker.GUIp_i18n.map_cellar_substr)) {
		dtitle += worker.GUIp_i18n.map_cellar_substr;
	}
	var map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.map + ' <span title="' + worker.GUIp_i18n.map_cgm + '">(CGM)</span></div><div style="text-align:center;clear:both;padding-top: 0.4em;">' + dtitle + '</div><div id="dmap" class="new_line em_font">';
	for (var i = 0, ilen = mapArray.length; i < ilen; i++) {
		map_elem += '<div class="dml" style="width:' + (mapArray[0].length * 21) + 'px;">';
		for (var j = 0, jlen = mapArray[0].length; j < jlen; j++) {
			map_elem += '<div class="dmc' + (mapArray[i][j] === '#' ? ' dmw' : '') + '" style="left:' + (j * 21) + 'px">' + mapArray[i][j] + '</div>';
		}
		map_elem += '</div>';
	}
	map_elem += '</div></div></div></div>';
	document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
	worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
	GUIp.common.setTimeout(worker.onscroll, 500);
	this.chronicleGeneratedMap = true;
};

ui_log.traceMapProcess = function(direction) {
	var chronicle, coords2, exitXY = GUIp.common.calculateExitXY(),
		currentCell, mapCells = document.querySelectorAll('#dmap .dml'),
		progressbar = document.getElementById('trace_progress');
	if (!mapCells.length || (this.traceStep + direction) > progressbar.max) {
		ui_log.traceMapStop();
		return;
	}
	var highlightThis = function(traceCoords) {
		currentCell = mapCells[traceCoords.y + exitXY.y] && mapCells[traceCoords.y + exitXY.y].children[traceCoords.x + exitXY.x];
		if (!currentCell) {
			ui_log.traceMapStop();
			return false;
		}
		currentCell.classList.add('dtrace');
		return true;
	}
	currentCell = document.querySelectorAll('.dmc.dtrace');
	for (var i = 0, len = currentCell.length; i < len; i++) {
		currentCell[i].classList.remove('dtrace');
	}
	if (direction !== -1) {
		if (this.chronicles[this.traceStep] && this.chronicles[this.traceStep].wormhole) {
			if (this.chronicles[this.traceStep].wormholedst) {
				this.traceCoords.y += this.chronicles[this.traceStep].wormholedst[0];
				this.traceCoords.x += this.chronicles[this.traceStep].wormholedst[1];
			} else {
				ui_log.traceMapStop();
				return false;
			}
		}
	} else {
		if (this.chronicles[this.traceStep - 1] && this.chronicles[this.traceStep - 1].wormhole) {
			coords2 = {x: this.traceCoords.x, y: this.traceCoords.y};
			GUIp.common.moveCoords(coords2, this.chronicles[this.traceStep], direction);
			if (!highlightThis(coords2)) {
				return;
			}
			if (this.chronicles[this.traceStep - 1].wormholedst) {
				this.traceCoords.y -= this.chronicles[this.traceStep - 1].wormholedst[0];
				this.traceCoords.x -= this.chronicles[this.traceStep - 1].wormholedst[1];
			} else {
				ui_log.traceMapStop();
				return false;
			}
		}
	}
	this.traceStep += direction;
	if (this.traceStep === 0) {
		ui_log.traceMapCalcCoords(progressbar.max);
		ui_log.traceStep++;
	} else {
		chronicle = this.chronicles[this.traceStep + (direction === -1 ? 1 : 0)];
		if (chronicle) {
			if (chronicle.marks.includes('staircase') && !ui_log.traceBegin) {
				var found = false;
				while (this.traceStep >= 0 && this.traceStep < progressbar.max) {
					this.traceStep += direction;
					chronicle = this.chronicles[this.traceStep + (direction === -1 ? 1 : 0)];
					if (chronicle && chronicle.marks.includes('staircase')) {
						found = true;
						break;
					}
				}
				if (!found) return;
			}
			ui_log.traceBegin = false;
			GUIp.common.moveCoords(this.traceCoords, chronicle, direction);
			if (chronicle.wormholedst && direction !== -1) {
				if (!highlightThis({y: this.traceCoords.y + chronicle.wormholedst[0], x: this.traceCoords.x + chronicle.wormholedst[1]})) {
					return;
				}
			};
		}
	}
	progressbar.value = this.traceStep;
	progressbar.title = worker.GUIp_i18n.trace_map_progress_step + ' #' + this.traceStep;
	if (!highlightThis(this.traceCoords)) {
		return;
	}
	currentCell = document.querySelector('.new_line.dtrace');
	if (currentCell) {
		currentCell.classList.remove('dtrace');
	}
	mapCells = document.querySelectorAll('.new_line .d_turn');
	for (var i = 0, len = mapCells.length; i < len; i++) {
		if ((mapCells[i].textContent.match(/[0-9]+/) || [])[0] === this.traceStep.toString()) {
			mapCells[i].parentNode.parentNode.classList.add('dtrace');
			break;
		}
	}
	ui_log.traceDir = direction;
};
ui_log.traceMapCalcCoords = function(maxStep) {
	var isStepInCellar = false, isMapInCellar = GUIp.common.isDungeonCellar();
	ui_log.traceCoords = {x: 0, y: 0};
	// this will adjust traceStep in cases an unavailable from this floor step was requested
	for (var i = 0; i < maxStep; i++) {
		// check moving to or from the subfloor (and looking ahead in one step due to the fact it is used with one step lookahead in traceMapProcess())
		if (this.chronicles[i+1] && this.chronicles[i+1].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// skip steps on another floor
		if (isStepInCellar !== isMapInCellar) {
			continue;
		}
		if (i >= ui_log.traceStep) {
			ui_log.traceStep = i;
			break;
		}
	}
	// and this one will actually move coordinates for the relevant cell. todo: make the same in one loop
	isStepInCellar = false;
	for (var i = 1; i <= ui_log.traceStep; i++) {
		// check moving to or from the subfloor
		if (this.chronicles[i] && this.chronicles[i].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// skip steps on another floor
		if (isStepInCellar !== isMapInCellar) {
			continue;
		}
		if (ui_log.chronicles[i - 1] && ui_log.chronicles[i - 1].wormhole) {
			if (ui_log.chronicles[i - 1].wormholedst) {
				ui_log.traceCoords.y += ui_log.chronicles[i - 1].wormholedst[0];
				ui_log.traceCoords.x += ui_log.chronicles[i - 1].wormholedst[1];
			} else {
				return;
			}
		}
		GUIp.common.moveCoords(ui_log.traceCoords, ui_log.chronicles[i]);
	}
	// actually all of this is quiet sad, it reminds about the obvious necessity to rewrite this feature from scratch to get rid of the awful hackery
};
ui_log.traceMapProgressClick = function(targetStep,max) {
	if (ui_log.traceInt) {
		ui_log.traceMapPause();
	}
	ui_log.traceDir = 1;
	ui_log.traceStep = Math.min(targetStep,max - 1);
	ui_log.traceMapCalcCoords(max);
	ui_log.traceBegin = true; // this hack allows us to start tracing directly on a step with a staircase, otherwise it'll try to skip everything like it was on a different floor
	ui_log.traceMapProcess(1);
};
ui_log.traceMapStart = function() {
	if (this.traceInt) {
		return;
	}
	this.traceInt = GUIp.common.setInterval(function() {
		if (ui_log.traceStep >= document.getElementById('trace_progress').max) {
			ui_log.traceStep = -1;
		}
		ui_log.traceMapProcess(1);
	},500);
	document.querySelectorAll('#trace_button_play img')[0].style.display = 'none';
	document.querySelectorAll('#trace_button_play img')[1].style.display = '';
	document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_pause;
};

ui_log.traceMapPause = function() {
	if (this.traceInt) {
		worker.clearInterval(this.traceInt);
	}
	delete this.traceInt;
	document.querySelectorAll('#trace_button_play img')[1].style.display = 'none';
	document.querySelectorAll('#trace_button_play img')[0].style.display = '';
	document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_start;
};

ui_log.traceMapStop = function() {
	if (this.traceInt) {
		worker.clearInterval(this.traceInt);
		document.querySelectorAll('#trace_button_play img')[1].style.display = 'none';
		document.querySelectorAll('#trace_button_play img')[0].style.display = '';
		document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_start;
	}
	delete this.traceInt;
	delete this.traceStep;
	delete this.traceCoords;
	var cell = document.querySelectorAll('.dmc.dtrace')
	for (var i = 0, len = cell.length; i < len; i++) {
		cell[i].classList.remove('dtrace');
	}
	cell = document.querySelector('.new_line.dtrace')
	if (cell) {
		cell.classList.remove('dtrace');
	}
	document.getElementById('trace_progress').value = 0;
	document.getElementById('trace_progress').title = worker.GUIp_i18n.trace_map_progress_stopped;
};

ui_log.enumerateSteps = function() {
	if (!this.customDomain || document.querySelector('.d_capt .d_turn') || this.logID.length === 6) return;
	var i, len, matches, step, stepholder, steplines = [], dcapt = false,
		chronobox = document.querySelector('#last_items_arena, #fight_chronicle, #m_fight_log'),
		reversed = location.href.includes('sort=desc'),
		flc = document.getElementById('fight_log_capt') || chronobox.querySelector('.block_h'),
		duel = !flc.textContent.match(/Хроника подземелья|Dungeon Journal/) || location.href.includes('boss=');
	if (!chronobox || !(matches = chronobox.getElementsByClassName('new_line'))) {
		return;
	}
	for (i = 0, len = matches.length; i < len; i++) {
		steplines.push(matches[i]);
	}
	if (reversed) {
		steplines.reverse();
	}
	for (i = 0, step = duel ? 0 : 1, len = steplines.length; i < len; i++) {
		stepholder = steplines[i].getElementsByClassName('d_capt')[0];
		stepholder.title = worker.GUIp_i18n.step_n+step;
		dcapt |= stepholder.textContent.length > 0;
		if ((!reversed && steplines[i].style.length > 0 || reversed && (!steplines[i+1] || steplines[i+1].style.length > 0)) && (!duel || dcapt)) {
			step++;
			dcapt = false;
		}
	}
};

ui_log.stoneEaterCompat1 = function(combo) {
	var result = '';
	if (combo) Object.values(combo).forEach(function (i) {
		result += i;
	});
	return result;
};

ui_log.stoneEaterCompat2 = function(combo) {
	var result = [];
	if (combo) Object.values(combo).forEach(function (i) {
		result.push(i[1]);
		result.push(-i[0]);
	});
	return result.join(',');
};

ui_log.describeMap = function() {
	var step, mapCells, currentCell, trapMoveLossCount = 0,
		isJumping = /Прыгучести|Jumping/.test(document.querySelector('.lastduelpl').textContent),
		isStepInCellar = false,
		isMapInCellar = GUIp.common.isDungeonCellar(),
		dmap = document.getElementById('dmap'),
		coords = GUIp.common.calculateExitXY(isMapInCellar),
		steps_max = this.dmapMaxStep(),
		descFailed = false;
	mapCells = document.querySelectorAll('#dmap .dml');
	GUIp.common.dmapTitlesFixup(mapCells);
	if (dmap.classList.contains('e_described')) {
		return;
	}
	if (location.search && this.queryString.get1('efc')) { // corrections were forced from URI
		this.directionlessMoves = {};
		this.wormholeMoves = {};
		var text;
		if ((text = ui_log.queryString.get1('edm'))) {
			text.split(',').forEach(function(a) { a = a.split(':'); ui_log.directionlessMoves[+a[0]] = a[1]; });
		}
		if ((text = ui_log.queryString.get1('ewh'))) {
			text.split(',').forEach(function(a) { a = a.split(':'); ui_log.wormholeMoves[+a[0]] = [-a[2],+a[1]]; });
		}
	}
	this.dungeonGuidedSteps = this.dungeonGuidedSteps || ui_log.storage.getJSON('guidedSteps', true) || {};
	// add the description of the first step to the staircase cell if we're in a basement - to help remind dungeon start conditions
	if (isMapInCellar && mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
		GUIp.common.describeCell(mapCells[coords.y].children[coords.x],1,steps_max,this.chronicles[1],trapMoveLossCount);
	}
	for (step = 1; step <= steps_max; step++) {
		// check moving to or from the subfloor
		if (this.chronicles[step].marks.includes('staircase')) {
			isStepInCellar = !isStepInCellar;
		}
		// if currently shown map is unrelated to this step - we just don't care and move on till we get again on the proper map
		if (isStepInCellar !== isMapInCellar) {
			GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,null);
			continue;
		}
		// we move the coordinates only on the currently shown map, since we have only one map at every moment of time
		if (this.chronicles[step].directionless) {
			this.directionlessMoves = this.directionlessMoves || ui_log.storage.getJSON('dlMoves', true) || {};
			if (this.directionlessMoves[step]) {
				this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
			} else {
				Object.assign(this.directionlessMoves,GUIp.common.calculateDirectionlessMove.call(ui_log, '#dmap', coords, step));
				if (this.directionlessMoves[step]) {
					this.chronicles[step].direction = this.corrections[this.directionlessMoves[step]];
					if (!this.customDomain) {
						this.storage.setJSON('dlMoves', this.directionlessMoves, true);
					}
				} else {
					descFailed = true;
				}
			}
			this.chronicles[step].directionless = false;
		}
		// parse guided steps data for reference
		GUIp.common.markGuidedSteps.call(this,step,isJumping,mapCells,coords);
		// move coordinates finally
		GUIp.common.moveCoords(coords, this.chronicles[step]);
		// and check for wormholes
		if (this.chronicles[step].wormhole) {
			this.wormholeMoves = this.wormholeMoves || ui_log.storage.getJSON('whMoves', true) || {};
			if (this.wormholeMoves[step]) {
				this.chronicles[step].wormholedst = this.wormholeMoves[step];
			} else {
				var result = GUIp.common.calculateWormholeMove.call(ui_log, '#dmap', coords, step);
				if (result.wm) {
					this.directionlessMoves = this.directionlessMoves || ui_log.storage.getJSON('dlMoves', true) || {};
					Object.assign(this.wormholeMoves,result.wm);
					Object.assign(this.directionlessMoves,result.dm);
					this.chronicles[step].wormholedst = this.wormholeMoves[step];
					if (!this.customDomain) {
						this.storage.setJSON('whMoves', this.wormholeMoves, true);
						this.storage.setJSON('dlMoves', this.directionlessMoves, true);
					}
				}
			}
			if (this.chronicles[step].wormholedst !== null) {
				if (mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
					currentCell = mapCells[coords.y].children[coords.x];
					GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount,true);
					if (!currentCell.classList.contains('e_clickable')) {
						currentCell.classList.add('e_clickable');
						GUIp.common.addListener(currentCell, 'click', function() { ui_log.cellClick(this); });
					}
				}
				GUIp.common.debug(
					'moving via wormhole to [' + this.chronicles[step].wormholedst.toString() + '] on step ' + step);
				coords.y += this.chronicles[step].wormholedst[0];
				coords.x += this.chronicles[step].wormholedst[1];
			} else {
				descFailed = true;
			}
		}
		if (!mapCells[coords.y] || !mapCells[coords.y].children[coords.x]) {
			GUIp.common.error(
				'the map does not match parsed chronicle at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			descFailed = true;
			break;
		}
		currentCell = mapCells[coords.y].children[coords.x];
		if (/[#?]/.test(currentCell.textContent)) {
			GUIp.common.error(
				'parsed chronicle does not match the map at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			descFailed = true;
			break;
		}
		if (currentCell.textContent.trim() === '✖') {
			this.chronicles[step].chamber = true;
		} else if (currentCell.textContent.trim() === '◿') {
			this.chronicles[step].staircase = true;
		}
		if (!currentCell.classList.contains('e_clickable')) {
			currentCell.classList.add('e_clickable');
			GUIp.common.addListener(currentCell, 'click', function() { ui_log.cellClick(this); });
		}
		if (this.chronicles[step].pointers.length > 0) {
			currentCell.dataset.pointers = this.chronicles[step].pointers.join(' ');
		}
		currentCell.classList.remove('dmv', 'dmh');
		trapMoveLossCount = GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount);
	}
	GUIp.common.debug('directional godvoices count: ' + (Object.keys(this.chronicles).length !== steps_max ? Object.keys(this.dungeonGuidedSteps).filter(function(a) { return +a <= steps_max; }).length + ' (till step ' + steps_max + ')' : Object.keys(this.dungeonGuidedSteps).length));
	if (!this.customDomain) {
		this.storage.setJSON('guidedSteps', this.dungeonGuidedSteps, true);
	}
	var heroesCoords = GUIp.common.calculateXY(document.getElementsByClassName('map_pos')[0]);
	if (heroesCoords.x !== coords.x || heroesCoords.y !== coords.y) {
		GUIp.common.error(
			'chronicle processing failed, coords diff: x: ' + (heroesCoords.x - coords.x) + ', y: ' + (heroesCoords.y - coords.y));
		descFailed = true;
	} else {
		dmap.classList.add('e_described');
	}
	// show that describing the map has failed and allow to input some corrections manually
	var efc = null;
	if (!document.querySelector('.dungeon_corrections') && (descFailed || this.buildMapError || (efc = ui_log.queryString.get1('efc')))) {
		var generateButtons = Object.values(this.chronicles).some(function(a) { return a.marks.includes('longJump') || a.marks.includes('directionless') }),
			correctionsBar = '<div class="dungeon_corrections"><div class="centered">' + (this.buildMapError ? this.buildMapError : (efc && !descFailed ? worker.GUIp_i18n.dungeon_map_failed_manual : worker.GUIp_i18n.dungeon_map_failed)) + '</div>';
		if (generateButtons) {
			correctionsBar += '<div><table><tr>' +
				'<td>' + worker.GUIp_i18n.dungeon_map_failed_dl_label + ':</td>' +
				'<td><input id="dungeon_corrections_dl" type="text" placeholder="' + worker.GUIp_i18n.dungeon_map_failed_dl_title + '" title="' + worker.GUIp_i18n.dungeon_map_failed_list + ' ' + worker.GUIp_i18n.dungeon_map_failed_dl_title + '"></td>' +
				'</tr><tr>' +
				'<td>' + worker.GUIp_i18n.dungeon_map_failed_wh_label + ':</td>' + 
				'<td><input id="dungeon_corrections_wh" type="text" placeholder="' + worker.GUIp_i18n.dungeon_map_failed_wh_title + '" title="' + worker.GUIp_i18n.dungeon_map_failed_list + ' ' + worker.GUIp_i18n.dungeon_map_failed_wh_title + '"></td>' + 
				'</tr><table></div>' +
				'<div class="centered">' + 
				'<button id="dungeon_corrections_reset">' + worker.GUIp_i18n.dungeon_map_failed_reset + '</button> ' + 
				'<button id="dungeon_corrections_apply" disabled>' + worker.GUIp_i18n.dungeon_map_failed_apply + '</button>' + 
				'</div>';
		}
		correctionsBar += '</div>';
		dmap.insertAdjacentHTML('afterend',correctionsBar);
		if (generateButtons) {
			var generateXhref = function() {
				var xhref = [];
				['u','s','sort','preview','estreaming'].forEach(function (a) {
					if (ui_log.queryString.get1(a)) {
						xhref.push(a+'='+ui_log.queryString.get1(a));
					}
				});
				return xhref;
			}
			document.getElementById('dungeon_corrections_dl').value = this.directionlessMoves && Object.keys(this.directionlessMoves).reduce(function(prev,current) {
				prev.push([current,ui_log.directionlessMoves[current]].join(':'));
				return prev;
			}, []).join(',') || '';
			document.getElementById('dungeon_corrections_wh').value = this.wormholeMoves && Object.keys(this.wormholeMoves).reduce(function(prev,current) {
				prev.push([current,ui_log.wormholeMoves[current][1],-ui_log.wormholeMoves[current][0]].join(':'));
				return prev;
			}, []).join(',') || '';
			GUIp.common.addListener(document.getElementById('dungeon_corrections_reset'), 'click', function() {
				document.getElementById('dungeon_corrections_dl').value = document.getElementById('dungeon_corrections_wh').value = '';
				ui_log.storage.remove('dlMoves', true);
				ui_log.storage.remove('whMoves', true);
				location.href = location.pathname + '?' + generateXhref().join('&');
			});
			GUIp.common.addListener(document.getElementById('dungeon_corrections_apply'), 'click', function() {
				var dlValues = [], whValues = [];
				try {
					document.getElementById('dungeon_corrections_dl').value.split(',').forEach(function (a) {
						if (!a) return;
						var c, b = a.split(':').map(function(a) { return a.trim(); });
						if (isNaN(+b[0])) throw 'invalid step: ' + b[0];
						switch (b[1]) {
							case 'n':
							case 'с':
							case 'north':
							case 'север':
								c = 'n';
								break;
							case 'e':
							case 'в':
							case 'east':
							case 'восток':
								c = 'e';
								break;
							case 's':
							case 'ю':
							case 'south':
							case 'юг':
								c = 's';
								break;
							case 'w':
							case 'з':
							case 'west':
							case 'запад':
								c = 'w';
								break;
							default:
								throw 'invalid direction: ' + b[1];
						}
						dlValues.push(+b[0] + ':' + c);
					});
					document.getElementById('dungeon_corrections_wh').value.split(',').forEach(function (a) {
						if (!a) return;
						var b = a.split(':').map(function(a) { return a.trim(); });
						if (isNaN(+b[0])) throw 'invalid step: ' + b[0];
						if (isNaN(+b[1])) throw 'invalid X-shift: ' + b[1];
						if (isNaN(+b[2])) throw 'invalid Y-shift: ' + b[2];
						whValues.push(+b[0] + ':' + +b[1] + ':' + +b[2]);
					});
				} catch (e) {
					GUIp.common.warn('wrong input:',e);
					worker.alert(worker.GUIp_i18n.dungeon_map_failed_parse);
					return;
				}
				var xhref = generateXhref();
				if (dlValues.length) {
					xhref.push('edm='+dlValues.join(','));
				}
				if (whValues.length) {
					xhref.push('ewh='+whValues.join(','));
				}
				xhref.push('efc=1');
				location.href = location.pathname + '?' + xhref.join('&');
			});
			var applyEnabled = function() {
				document.getElementById('dungeon_corrections_apply').disabled = false;
			}
			GUIp.common.addListener(document.getElementById('dungeon_corrections_dl'), 'input', applyEnabled);
			GUIp.common.addListener(document.getElementById('dungeon_corrections_wh'), 'input', applyEnabled);
		}
	}
};

ui_log.cellClick = function(cell) {
	var targetStep, maxSteps = this.dmapMaxStep(),
		cellSteps = cell.title.match(/#(\d+)\b/g);
	if (!cellSteps) {
		return;
	}
	targetStep = parseInt(cellSteps[0].slice(1));
	for (var i = 0, len = cellSteps.length; i < len - 1; i++) {
		if (parseInt(cellSteps[i].slice(1)) === ui_log.traceStep) {
			targetStep = parseInt(cellSteps[i + 1].slice(1));
			break;
		}
	}
	document.getElementById('trace_progress').max = maxSteps;
	ui_log.traceMapProgressClick(targetStep - 1,maxSteps);
	ui_log.scrollTo(document.querySelector('div.dtrace'));
};

ui_log.highlightTreasuryZone = function() {
	GUIp.common.improveMap.call(
		ui_log, '#dmap', ui_log.isCGM, ui_log.isBroadcast ? 0 : GUIp.common.getDungeonVersion(+ui_log.getStartDate()), ui_log.dmapMaxStep()
	);
};

ui_log.restoreDungeonMap = function(map,streaming) {
	var dtitle = document.querySelector('.lastduelpl').textContent.trim();
	var map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.map + (streaming ? ' <span title="' + worker.GUIp_i18n.map_sm + '">(SM)</span>' : '') + '</div><div style="text-align:center;clear:both;padding-top: 0.4em;">' + dtitle + '</div><div id="dmap" class="new_line em_font">';
	for (var i = 0, ilen = map.length; i < ilen; i++) {
		map_elem += '<div class="dml" style="width:' + (map[0].length * 21) + 'px;">';
		for (var j = 0, jlen = map[0].length; j < jlen; j++) {
			map_elem += '<div class="dmc' + (map[i][j] === '#' ? ' dmw' : '') + '" style="left:' + (j * 21) + 'px">' + map[i][j] + '</div>';
		}
		map_elem += '</div>';
	}
	map_elem += '</div></div></div></div>';
	document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
	worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
	GUIp.common.setTimeout(worker.onscroll, 500);
};

ui_log.createDungeonButtons = function() {
	var $box;
	if ($box = document.getElementById('dmap')) {
		$box.insertAdjacentHTML('afterend', '<div class="trace_div"><progress id="trace_progress" max="0" value="0" title="' + worker.GUIp_i18n.trace_map_progress_stopped + '"></progress><span class="trace_buttons"><button class="trace_button" id="trace_button_prev" title="' + worker.GUIp_i18n.trace_map_prev + '"><img src="' + worker.GUIp_getResource('images/trace_prev.png') + '"/></button><button class="trace_button" id="trace_button_stop" title="' + worker.GUIp_i18n.trace_map_stop + '"><img src="' + worker.GUIp_getResource('images/trace_stop.png') + '"/></button><button class="trace_button" id="trace_button_play" title="' + worker.GUIp_i18n.trace_map_start + '"><img src="' + worker.GUIp_getResource('images/trace_play.png') + '"/><img src="' + worker.GUIp_getResource('images/trace_pause.png') + '" style="display:none;"/></button><button class="trace_button" id="trace_button_next" title="' + worker.GUIp_i18n.trace_map_next + '"><img src="' + worker.GUIp_getResource('images/trace_next.png') + '"/></button></span></div>');
		if (worker.GUIp_browser === 'Opera') {
			worker.GUIp_getResource('images/trace_prev.png',document.querySelector('#trace_button_prev img'),true);
			worker.GUIp_getResource('images/trace_pause.png',document.querySelector('#trace_button_pause img'),true);
			worker.GUIp_getResource('images/trace_stop.png',document.querySelector('#trace_button_stop img'),true);
			worker.GUIp_getResource('images/trace_play.png',document.querySelectorAll('#trace_button_play img')[0],true);
			worker.GUIp_getResource('images/trace_pause.png',document.querySelectorAll('#trace_button_play img')[1],true);
			worker.GUIp_getResource('images/trace_next.png',document.querySelector('#trace_button_next img'),true);
		}
		GUIp.common.addListener(document.getElementById('trace_button_stop'), 'click', ui_log.traceMapStop.bind(ui_log));
		GUIp.common.addListener(document.getElementById('trace_button_play'), 'click', function() {
			if (!ui_log.traceCoords) {
				ui_log.traceStep = -1;
				ui_log.traceDir = 1;
				ui_log.traceCoords = {x: 0, y: 0};
			}
			document.getElementById('trace_progress').max = ui_log.dmapMaxStep();
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			} else {
				ui_log.traceMapStart();
			}
		});
		GUIp.common.addListener(document.getElementById('trace_button_prev'), 'click', function() {
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			}
			if (!ui_log.traceCoords || ui_log.traceStep <= 1) {
				return;
			}
			document.getElementById('trace_progress').max = ui_log.dmapMaxStep();
			ui_log.traceMapProcess(-1);
		});
		GUIp.common.addListener(document.getElementById('trace_button_next'), 'click', function() {
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			}
			if (!ui_log.traceCoords) {
				ui_log.traceStep = -1; // this will make initial coords recalculated
				ui_log.traceDir = 1;
			}
			document.getElementById('trace_progress').max = ui_log.dmapMaxStep();
			if (ui_log.traceStep === worker.Object.keys(ui_log.chronicles).length) {
				return;
			}
			ui_log.traceMapProcess(1);
		});
		var traceProgTimer = null, traceProgCancel = false;
		GUIp.common.addListener(document.getElementById('trace_progress'), 'click', function(e) {
			if (traceProgCancel) {
				traceProgCancel = false;
				return;
			}
			if (this.max === 0) {
				return;
			}
			ui_log.traceMapProgressClick(parseInt(e.offsetX * this.max / this.offsetWidth),this.max);
			ui_log.scrollTo(document.querySelector('div.dtrace'));
		});
		GUIp.common.addListener(document.getElementById('trace_progress'), 'mouseup', function(e) {
			if (traceProgTimer) {
				worker.clearTimeout(traceProgTimer);
			}
		});
		GUIp.common.addListener(document.getElementById('trace_progress'), 'mousedown', function(e) {
			var max = this.max = ui_log.dmapMaxStep();
			if (this.max === 0) {
				return;
			}
			traceProgCancel = false;
			traceProgTimer = GUIp.common.setTimeout(function() {
				var step = parseInt(worker.prompt(worker.GUIp_i18n.trace_map_progress_prompt));
				if (step > 0 && step <= max) {
					ui_log.traceMapProgressClick(step - 1,max);
					ui_log.scrollTo(document.querySelector('div.dtrace'));
				}
				traceProgCancel = true;
			},7e2);
		});
		GUIp.common.addListener(document.getElementById('last_items_arena'), 'click', function(ev) {
			var target = ev.target;
			while (!target.classList.contains('d_capt')) {
				if (target === this) return;
				target = target.parentNode;
			}
			var turn = target.getElementsByClassName('d_turn')[0],
				m = (turn && /\d+/.exec(turn.textContent)) || /\d+/.exec(target.title);
			if (!m) return;
			ev.stopPropagation();
			var maxSteps = Object.keys(ui_log.chronicles).length;
			document.getElementById('trace_progress').max = maxSteps;
			ui_log.traceMapProgressClick(+m[0] - 1, maxSteps);
		}, true);
	}
	// erinome preview uploader for modern chronicles with any map present
	$box = document.querySelector('#hero2 fieldset, #hero2 .block') || document.getElementById('right_block')
	if (ui_log.isDungeonLog()) {
		if (!ui_log.queryString.get1('preview')) {
			var previewdiv = document.createElement('div'),
				previewlnk = document.createElement('button');
			previewlnk.textContent = worker.GUIp_i18n.map_preview_share;
			if (ui_log.chronicles[1] && GUIp.common.customRegExp.exec(ui_log.chronicles[1].text)) {
				previewlnk.innerHTML += '</br>(' + worker.GUIp_i18n.map_preview_search + ')';
			}
			previewdiv.className = 'e_map_preview'
			previewdiv.appendChild(previewlnk);
			$box.appendChild(previewdiv);
			GUIp.common.addListener(previewlnk, 'click', ui_log.saverPrepareLog.bind(ui_log, "preview"));
		}
	}
	// LEM's uploader for historical logs, or in case map wasn't build by some reason, or if it was simply enabled in options
	if (!ui_log.isDungeonLog() || ui_log.storage.getList('Option:dungeonMapSettings').includes('dlem')) {
		if (location.href.includes('sort')) {
			$box.insertAdjacentHTML('beforeend', '<span id="send_to_LEM_form">' + worker.GUIp_i18n.wrong_entries_order + '</span>');
			return;
		}
		if (this.steps < this.firstRequest) {
			$box.insertAdjacentHTML('beforeend', '<span id="send_to_LEM_form">' + worker.GUIp_i18n.fmt('the_button_will_appear_after', this.firstRequest) + '</span>');
			return;
		}
		$box.insertAdjacentHTML('beforeend',
			'<form target="_blank" method="post" enctype="multipart/form-data" action="//www.godalert.info/Dungeons/index' + (worker.GUIp_locale === 'en' ? '-eng' : '') + '.cgi" id="send_to_LEM_form" style="padding-top: calc(0.5em + 3px);">' +
				'<input type="hidden" id="fight_text" name="fight_text">' +
				'<input type="hidden" name="map_type" value="map_graphic">' +
				'<input type="hidden" name="min" value="X">' +
				'<input type="hidden" name="partial" value="X">' +
				'<input type="hidden" name="room_x" value="">' +
				'<input type="hidden" name="room_y" value="">' +
				'<input type="hidden" name="Submit" value="' + worker.GUIp_i18n.get_your_map + '">' +
				'<input type="hidden" name="guip" value="1">' +
				'<div' + (ui_log.customDomain && ui_log.isOldDungeonLog() ? '' : ' style="display: none;"') + '><input type="checkbox" id="match" name="match" value="1"><label for="match">' + worker.GUIp_i18n.search_database + '</label></div>' +
				'<div id="search_mode" style="display: none;">' +
					'<input type="checkbox" id="match_partial" name="match_partial" value="1"><label for="match_partial">' + worker.GUIp_i18n.relaxed_search + '</label>' +
					'<div><input type="radio" id="exact" name="search_mode" value="exact"><label for="exact">' + worker.GUIp_i18n.exact + '</label></div>' +
					'<div><input type="radio" id="high" name="search_mode" value="high"><label for="high">' + worker.GUIp_i18n.high_precision + '</label></div>' +
					'<div><input type="radio" id="medium" name="search_mode" value="medium" checked=""><label for="medium">' + worker.GUIp_i18n.normal + '</label></div>' +
					'<div><input type="radio" id="low" name="search_mode" value="low"><label for="low">' + worker.GUIp_i18n.primary + '</label></div>' +
				'</div>' +
				'<table style="box-shadow: none; width: 100%;"><tr>' +
					'<td style="border: none; padding: 0;"><label for="stoneeater">' + worker.GUIp_i18n.corrections + '</label></td>' +
					'<td style="border: none; padding: 0 1.5px 0 0; width: 100%;"><input type="text" id="stoneeater" name="stoneeater" value="' + this.stoneEaterCompat1(ui_log.directionlessMoves || ui_log.storage.getJSON('dlMoves', true)) + '" style=" width: 100%; padding: 0;"></td>' +
				'</tr><tr>' +
					'<td style="border: none; padding: 0;"><label for="teleports">' + worker.GUIp_i18n.wormholes + '</label></td>' +
					'<td style="border: none; padding: 0 1.5px 0 0; width: 100%;"><input type="text" id="teleports" name="teleports" value="' + this.stoneEaterCompat2(ui_log.wormholeMoves || ui_log.storage.getJSON('whMoves', true)) + '" style=" width: 100%; padding: 0;"></td>' +
				'</tr></table>' +
				'<input type="checkbox" id="high_contrast" name="high_contrast" value="1"><label for="high_contrast">' + worker.GUIp_i18n.high_contrast + '</label>' +
				'<button id="send_to_LEM" style="font-size: 15px; height: 100px; width: 100%;">' +
			'</form>');

		var match = document.getElementById('match'),
			search_mode = document.getElementById('search_mode'),
			high_contrast = document.getElementById('high_contrast');
		this.button = document.getElementById('send_to_LEM');
		GUIp.common.addListener(this.button, 'click', function(e) {
			e.preventDefault();
			ui_log.updateLogLimits();
			ui_log.updateButton();
			this.form.fight_text.value = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
															document.getElementsByTagName('html')[0].innerHTML.replace(/<(?:script|style)[^]+?<\/(?:script|style)>/g, '')
																											  .replace(/background-image: url\(&quot;data:image[^)]+\);/g, '')
																											  .replace(/onclick="[^"]+?"/g, '')
																											  .replace(/"javascript[^"]+"/g, '""')
																											  .replace(/<form[^]+?<\/form>/g, '')
																											  .replace(/<iframe[^]+?<\/iframe>/g, '')
																											  .replace(/\t/g, '')
																											  .replace(/<div[^>]+class="dmc[^>]+>/g,'<div class="dmc">')
																											  .replace(/ {2,}/g, ' ')
																											  .replace(/\n{2,}/g, '\n') +
													  '</html>';
			if (this.chronicleGeneratedMap) {
				this.form.fight_text.value = this.form.fight_text.value.replace(/<div id="hero2"[^]+?<\/div><\/div><\/div><\/div>/g, '');
			}
			this.form.submit();
			document.getElementById('match').checked = false;
			document.getElementById('match_partial').checked = false;
			document.getElementById('medium').click();
			document.getElementById('search_mode').style.display = "none";
		});
		ui_log.updateButton();
		GUIp.common.setInterval(function() {
			ui_log.updateButton();
		}, 10e3);
		GUIp.common.addListener(match, 'change', function() {
			search_mode.style.display = search_mode.style.display === 'none' ? 'block' : 'none';
			ui_log.lfExp = search_mode.style.display === 'block';
			if (worker.onscroll) { worker.onscroll(); }
		});
		high_contrast.checked = localStorage.getItem('eGUI_highContrast') === 'true';
		GUIp.common.addListener(high_contrast, 'change', function() {
			localStorage.setItem('eGUI_highContrast', document.getElementById('high_contrast').checked);
		});
	}
}
