/** @namespace */
ui_log.lemUploader = {
	_url: 'https://www.godalert.info' + (ui_log.isSailingLog() ? '/Sail' : '/Dungeons'),

	_form: document.createElement('form'),

	/**
	 * @readonly
	 * @type {?Object}
	 */
	restrictions: null,

	/** @type {!Array<function(!Object)>} */
	onupdate: [],

	_keyOf: function(id) {
		return 'LEMRestrictions:' + (ui_log.isSailingLog() ? id + 'S' : id);
	},

	/**
	 * @private
	 * @returns {!Object<string, !Array<number>>}
	 */
	_loadTimestamps: function() {
		var result = ui_log.storage.getJSON(this._keyOf('Sent'));
		if (!result || typeof result !== 'object' || Array.isArray(result)) {
			return {};
		}
		var now = Date.now(), threshold = now - this.restrictions.timeFrame * 1e3;
		for (var logID in result) {
			var moments = result[logID];
			if (!Array.isArray(moments) || !GUIp.common.filterInPlace(moments, function(x) {
				return typeof x === 'number' && x > threshold && x <= now;
			}).length) {
				delete result[logID];
			}
		}
		return result;
	},

	_parseRestrictions: function(text) {
		try {
			var response = JSON.parse(text);
			if (!Number.isSafeInteger(response.first_request) || response.first_request < 0 ||
				typeof response.time_frame !== 'number' || response.time_frame < 0 ||
				!Number.isSafeInteger(response.request_limit) || response.request_limit < 0) {
				return null;
			}
			return {
				firstRequest: response.first_request,
				timeFrame: response.time_frame * 60,
				requestLimit: response.request_limit
			};
		} catch (e) {
			return null;
		}
	},

	/**
	 * @returns {{tries: number, disabledTill: number}} Exactly one of these equals to 0.
	 */
	getStatus: function() {
		if (!this.restrictions) {
			throw new Error("calling getStatus on an uninitialized LEM's uploader");
		}
		var timestamps = this._loadTimestamps();
		ui_log.storage.setJSON(this._keyOf('Sent'), timestamps);
		var moments = timestamps[ui_log.logID] || [];
		if (moments.length < this.restrictions.requestLimit) {
			return {
				tries: this.restrictions.requestLimit - moments.length,
				disabledTill: 0
			};
		} else {
			return {
				tries: 0,
				disabledTill: Math.min.apply(null, moments) + this.restrictions.timeFrame * 1e3
			};
		}
	},

	/**
	 * @returns {?Object}
	 */
	collect: function() {
		var html = '',
			stepToSend = 0,
			mger, model, arks, rx;
		if (!ui_log.isSailingLog()) {
			throw new Error('not implemented');
		}
		html = document.getElementsByTagName('html')[0].innerHTML
			.replace(/<(style|form|svg|i?frame)\b[^]*?<\/\1>|\bonclick="[^"]*"|\bbackground-image\s*:\s*url\s*\(\s*&quot;data:image[^)]*\)\s*;?|\t+/g, '')
			.replace(/<script\b[^>]*>/g, '<notascript>')
			.replace(/ {2,}/g, ' ')
			.replace(/\n{2,}/g, '\n');
		mger = ui_log.islandsMap.manager;
		if (mger instanceof ui_imapwcm.WholeChronicleMVManager) {
			stepToSend = +prompt(GUIp_i18n.fmt('select_step_for_LEM', mger.models.length), mger.model.step);
			if (stepToSend < 1 || !Number.isInteger(stepToSend)) {
				return null;
			}
			if (stepToSend < mger.models.length) {
				// override map
				html += '<!-- var m = [' + mger.encodeMapWithDuplicatesUntil(stepToSend) + ']; -->';
				model = mger.models[stepToSend - 1];
			} else {
				model = mger.models[mger.models.length - 1];
			}
		} else {
			model = mger.model;
			arks = GUIp.common.islandsMap.conv.encodeArks(model);
			if (!(rx = /\bm\s*=\s*\[[^\]]+\]/g).test(html)) {
				// broadcast page or live streaming
				html += '<!-- var m = [' + GUIp.common.islandsMap.conv.encodeMap(model);
				if (arks.length) {
					html += ',' + arks;
				}
				html += ']; -->';
			} else if (arks.length) {
				// append arks to the array
				html = html.slice(0, rx.lastIndex - 1) + ',' + arks + html.slice(rx.lastIndex - 1);
			}
		}
		return {
			guip: 1,
			fight_text:
				'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n' +
				'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
				html +
				'<!-- var points = [' + GUIp.common.islandsMap.conv.encodePOIs(model) + ']; -->' +
				'<!-- <span id="turn_num">' + model.step + '</span> -->' +
				'</html>'
		};
	},

	/**
	 * @param {!Object} data
	 */
	send: function(data) {
		for (var key in data) {
			var input = document.createElement('input');
			input.type = 'hidden';
			input.name = key;
			input.value = data[key];
			this._form.appendChild(input);
		}
		document.body.appendChild(this._form);
		this._form.submit();
		document.body.removeChild(this._form);
		this._form.textContent = '';

		var timestamps = this._loadTimestamps(),
			moments = timestamps[ui_log.logID];
		if (moments) {
			moments.push(Date.now());
		} else {
			timestamps[ui_log.logID] = [Date.now()];
		}
		ui_log.storage.setJSON(this._keyOf('Sent'), timestamps);
	},

	init: function() {
		this._form.action = this._url + (worker.GUIp_locale === 'en' ? '/index-eng.cgi' : '/index.cgi');
		this._form.method = 'POST';
		this._form.enctype = 'multipart/form-data';
		this._form.target = '_blank';
		this._form.className = 'hidden';

		this.restrictions = {
			firstRequest: ui_log.isSailingLog() ? 5 : 12,
			timeFrame: 20 * 60,
			requestLimit: ui_log.isSailingLog() ? 8 : 5
		};
	}
};

/**
 * @class
 */
ui_log.LemUploaderUI = function() {
	if (!ui_log.isSailingLog()) {
		throw new Error('not implemented');
	}

	this._status = ui_log.lemUploader.getStatus();
	this._timer = 0;
	this._frequentTimer = false;
	this._startTimer();
	ui_log.lemUploader.onupdate.push(this.update.bind(this));

	this._button = document.createElement('button');
	GUIp.common.addListener(this._button, 'click', this._onSubmit.bind(this));
	this._updateButton();

	this.container = this._button;
};

ui_log.LemUploaderUI.prototype = {
	constructor: ui_log.LemUploaderUI,

	_getEnabledAnnotation: function(tries) {
		return (ui_log.isSailingLog() ? worker.GUIp_i18n.tries_left_s : worker.GUIp_i18n.tries_left) + tries;
	},

	_getDisabledAnnotation: function(cooldown) {
		var msg = ui_log.isSailingLog() ? worker.GUIp_i18n.till_next_try_s : worker.GUIp_i18n.till_next_try;
		return msg + Math.floor(cooldown / 60) + ':' + ('0' + cooldown % 60).slice(-2);
	},

	_formatButtonText: function() {
		var annotation =
			this._status.tries
			? this._getEnabledAnnotation(this._status.tries)
			: this._getDisabledAnnotation(Math.ceil((this._status.disabledTill - Date.now()) * 1e-3));
		annotation = ui_log.isSailingLog() ? ' (' + annotation + ')' : '<br />' + annotation;
		return worker.GUIp_i18n.send_log_to_LEMs_script + annotation;
	},

	_updateButton: function() {
		this._button.disabled = !this._status.tries;
		this._button.innerHTML = this._formatButtonText();
	},

	_requestStatus: function() {
		this._status = ui_log.lemUploader.getStatus();
	},

	_startTimer: function() {
		this._frequentTimer = !this._status.tries;
		this._timer = GUIp.common.setInterval(function(self) {
			if (self._status.tries || Date.now() >= self._status.disabledTill) {
				self._requestStatus();
				self._updateTimer();
			}
			self._updateButton();
		}, this._frequentTimer ? 1e3 : 10e3, this);
	},

	_updateTimer: function() {
		if (this._frequentTimer !== !this._status.tries) {
			worker.clearInterval(this._timer);
			this._startTimer();
		}
	},

	_onSubmit: function() {
		var data;
		this._requestStatus();
		if (this._status.tries) {
			if (!(data = ui_log.lemUploader.collect())) {
				return;
			}
			ui_log.lemUploader.send(data);
			this._requestStatus();
		}
		this._updateTimer();
		this._updateButton();
	},

	update: function() {
		this._requestStatus();
		this._updateTimer();
		this._updateButton();
	}
};

/**
 * @returns {!Node}
 */
ui_log.createLEMUploaderUI = function() {
	// TODO: check for firstRequest only when searching for a dungeon map in the database
	if (ui_log.steps < ui_log.lemUploader.restrictions.firstRequest && !ui_log.isStream) {
		return document.createDocumentFragment();
	}
	return new this.LemUploaderUI().container;
};

ui_log.getLEMRestrictions = function() {
	var postfix = this.isSailingLog() ? 'S' : '';
	this.firstRequest = postfix ? 5 : 12;
	this.timeFrameSeconds = (postfix ? 20 : 20)*60;
	this.requestLimit = postfix ? 8 : 5;
};

ui_log.updateLogLimits = function() {
	for (var i = ui_log.requestLimit; i > 1; i--) {
		ui_log.storage.set('sentToLEM' + i, ui_log.storage.get('sentToLEM' + (i - 1), true), true);
	}
	ui_log.storage.set('sentToLEM1', Date.now(), true);
};

ui_log.checkLogLimits = function() {
	var i, stored = ui_log.storage.get('sentToLEM' + ui_log.requestLimit, true);
	if (!isNaN(stored) && Date.now() - stored < ui_log.timeFrameSeconds*1000) {
		var time = ui_log.timeFrameSeconds - (Date.now() - stored)/1000,
			minutes = Math.floor(time/60),
			seconds = Math.floor(time%60);
		seconds = seconds < 10 ? '0' + seconds : seconds;
		return {allowed: false, minutes: minutes, seconds: seconds};
	} else {
		var tries = 0;
		for (i = 0; i < ui_log.requestLimit; i++) {
			stored = ui_log.storage.get('sentToLEM' + i, true);
			if (isNaN(stored) || Date.now() - stored > ui_log.timeFrameSeconds*1000) {
				tries++;
			}
		}
		return {allowed: true, tries: tries};
	}
};

ui_log.updateButton = function() {
	var isSail = ui_log.isSailingLog(),
		result = ui_log.checkLogLimits();
	if (result.allowed === false) {
		ui_log.button.innerHTML = worker.GUIp_i18n.send_log_to_LEMs_script + (isSail ? ' (' + worker.GUIp_i18n.till_next_try_s : '<br>' + worker.GUIp_i18n.till_next_try) + result.minutes + ':' + result.seconds + (isSail ? ')' : '');
		ui_log.button.setAttribute('disabled', 'disabled');
	} else {
		ui_log.button.innerHTML = worker.GUIp_i18n.send_log_to_LEMs_script + (isSail ? ' (' + worker.GUIp_i18n.tries_left_s : '<br>' + worker.GUIp_i18n.tries_left) + result.tries + (isSail ? ')' : '');
		ui_log.button.removeAttribute('disabled');
	}
};
