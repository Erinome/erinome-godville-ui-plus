GUIp.common.renderTester = (function() {
var renderTester = {};

renderTester._ctx = null;
renderTester._hashes = {};

var _calcHash = function(text) {
	var ctx = renderTester._ctx,
		hash = 1,
		data;
	if (!ctx) {
		ctx = renderTester._ctx = document.createElement('canvas').getContext('2d');
		ctx.textBaseline = 'top';
		ctx.font = '32px sans-serif';
		if (!('\uFFFF' in renderTester._hashes))
			_calcHash('\uFFFF'); // we've just created _ctx so this should not lead to infinite recursion
	}
	ctx.clearRect(0, 0, 32, 32);
	ctx.fillText(text, 0, 0);
	data = ctx.getImageData(0, 0, 32, 32).data;
	for (var i = 0, len = data.length; i < len; i++) {
		hash = (hash * 257 + data[i]) % 0x1FE01FE01FDF; // (0x1FE01FE01FDF - 1) * 257 + 255 < 1 << 53
	}
	return (renderTester._hashes[text] = hash);
};

/**
 * @param {string} c
 * @returns {boolean}
 */
renderTester.testChar = function(c) {
	// _calcHash must be called prior to accessing _hashes['\uFFFF']
	return (renderTester._hashes[c] || _calcHash(c)) !== renderTester._hashes['\uFFFF'];
};

renderTester.deinit = function() {
	renderTester._ctx = null;
};

return renderTester;
})(); // GUIp.common.renderTester
