/**
 * @alias GUIp.common.activities
 * @namespace
 */
var ui_cact = GUIp.common.activities = {};

/**
 * @typedef {Object} GUIp.common.activities.DiaryEntry
 * @property {number} date
 * @property {string} type - 'influence', 'foreignVoice', or 'regular'
 * @property {string} msg
 * @property {string} logID
 */

/**
 * @typedef {Object} GUIp.common.activities.LastFightsEntry
 * @property {number} date
 * @property {string} type
 * @property {string} logID
 * @property {boolean} success
 */

/**
 * @typedef {Object} GUIp.common.activities.Activity
 * @property {string} type
 * @property {number} date
 * @property {number} result - (-1) if unknown
 * @property {string} logID
 */

/**
 * @private
 * @typedef {Object} GUIp.common.activities.ActivityStatus
 * @property {boolean} reliable
 * @property {number} lastRevised
 */

/** @const */
ui_cact.storageTime = 2678400e3; // 31 days

/** @const */
ui_cact.guaranteeArr = [
	{
		type: 'spar',
		number: 1,
		timeout: 86400e3 // 24h
	}, {
		type: 'dungeon',
		number: 2,
		timeout: 86400e3 // 24h
	}, {
		type: 'mining',
		number: 3,
		timeout: 86400e3 // 24h
	}, {
		type: 'conversion',
		number: 1,
		timeout: 129600e3 // 36h
	}, {
		type: 'souls',
		number: 3,
		timeout: 86400e3 // 24h
	}
];

/** @const */
ui_cact.maxTimeout = 129600e3; // 36h

/** @const */
ui_cact.guarantee = {
	spar:       ui_cact.guaranteeArr[0],
	dungeon:    ui_cact.guaranteeArr[1],
	mining:     ui_cact.guaranteeArr[2],
	conversion: ui_cact.guaranteeArr[3],
	souls:      ui_cact.guaranteeArr[4]
};

ui_cact._dateGE = function(act) { return act.date >= this; };

/**
 * @param {{get: function(string): ?string}} storage
 * @returns {!Array<!GUIp.common.activities.Activity>}
 */
ui_cact.load = function(storage) {
	return GUIp.common.filterInPlace(
		GUIp.common.parseJSON(storage.get('ThirdEye:Activities')) || [],
		ui_cact._dateGE,
		Date.now() - ui_cact.storageTime
	);
};

/**
 * @param {{set: function(string, string)}} storage
 * @param {!Array<!GUIp.common.activities.Activity>} activities
 */
ui_cact.save = function(storage, activities) {
	storage.set('ThirdEye:Activities', JSON.stringify(activities));
};

/**
 * @param {!Array<!GUIp.common.activities.Activity>} activities
 * @returns {!Object<string, !GUIp.common.activities.Activity>}
 */
ui_cact.createActivitiesDict = function(activities) {
	var result = Object.create(null), act;
	for (var i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		// the only truly unique and reliable key is log's ID. dates are neither unique (spar followed by conversion can
		// happen within a minute) nor reliable (wrong date might be shown on log pages). unfortunately, conversion
		// events do not have an ID
		result[act.type+(act.logID || act.date)] = act;
	}
	return result;
};

/**
 * @param {{get: function(string): ?string}} storage
 * @returns {!Object<string, !GUIp.common.activities.ActivityStatus>}
 */
ui_cact.loadStatuses = function(storage) {
	var statuses = GUIp.common.parseJSON(storage.get('ThirdEye:ActivityStatuses')),
		template = {
		// presumption of innocence
		spar:       {reliable: true, lastRevised: 0},
		dungeon:    {reliable: true, lastRevised: 0},
		mining:     {reliable: true, lastRevised: 0},
		souls:      {reliable: true, lastRevised: 0},
		conversion: {reliable: true, lastRevised: 0}
	};
	return statuses ? Object.assign(template, statuses) : template;
};

/**
 * @param {{set: function(string, string)}} storage
 * @param {!Object<string, !GUIp.common.activities.ActivityStatus>} statuses
 */
ui_cact.saveStatuses = function(storage, statuses) {
	storage.set('ThirdEye:ActivityStatuses', JSON.stringify(statuses));
};

/**
 * @param {!GUIp.common.activities.ActivityStatus} status
 * @param {boolean} reliable
 * @param {number} date
 */
ui_cact.updateStatus = function(status, reliable, date) {
	if (date >= status.lastRevised) { // overwrite if dates are equal
		status.reliable = reliable;
		status.lastRevised = date;
	}
};

ui_cact._byDate = function(a, b) { return a.date - b.date; };

/**
 * @param {string} godName
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_cact.readThirdEyeFromLS = function(godName) {
	var obj = GUIp.common.parseJSON(localStorage['d_i_' + godName]),
		result = [],
		entries, entry;
	if (!obj) return result;
	entries = Object.values(obj);
	for (var i = 0, len = entries.length; i < len; i++) {
		entry = entries[i];
		if (!entry || entry.s === 'del' || !entry.time || !entry.msg) {
			continue;
		}
		result.push({
			date: Date.parse(entry.time),
			type: entry.infl ? 'influence' : 'regular',
			msg: entry.msg,
			logID: entry.f_id || ''
		});
	}
	return result.sort(ui_cact._byDate);
};

/**
 * @param {!Array<number>} gaps
 * @param {number} moment
 * @returns {boolean}
 */
ui_cact.gapsInclude = function(gaps, moment) {
	for (var i = 1, len = gaps.length; i < len; i += 2) {
		if (moment < gaps[i] && moment > gaps[i - 1]) {
			return true;
		}
	}
	return false;
};

/**
 * @param {{set: function(string, string)}} storage
 * @param {!Array<!GUIp.common.activities.DiaryEntry>} te
 * @param {!Array<number>} gaps
 * @param {!Array<!GUIp.common.activities.Activity>} activities
 * @param {!Array<!GUIp.common.activities.ActivityStatus>} statuses
 * @param {!Array<!GUIp.common.activities.LastFightsEntry>} fights
 * @returns {boolean} true iff we've learned something from the fight list.
 */
ui_cact.updateLastFights = function(storage, te, gaps, activities, statuses, fights) {
	var dict = ui_cact.createActivitiesDict(activities),
		len = te.length,
		earliestTEDate = len ? te[0].date : Infinity,
		seenInTE = Object.create(null),
		now = Date.now(),
		threshold = now - ui_cact.storageTime,
		changed = false,
		changedStatuses = false,
		i, f, act;
	for (i = 0; i < len; i++) {
		seenInTE[te[i].logID] = true;
	}
	for (i = 0, len = fights.length; i < len; i++) {
		f = fights[i];
		if (!(f.type in ui_cact.guarantee) || f.date < threshold) {
			continue; // not interested
		} else if ((act = dict[f.type+f.logID])) {
			if (act.date !== f.date) {
				// if we discovered that activity from its log page, it might have wrong date (off by a minute)
				act.date = f.date;
				changed = true;
			}
			if (f.date > earliestTEDate && !(f.logID in seenInTE)) {
				// we discovered that activity from its log page, but can't see it in the Third Eye.
				// seems that this type of activities is disabled in the Third Eye's settings
				ui_cact.updateStatus(statuses[f.type], false, f.date);
				changedStatuses = true;
			}
			continue;
		}
		activities.push({type: f.type, date: f.date, result: -f.success, logID: f.logID});
		changed = true;
		if (!ui_cact.gapsInclude(gaps, f.date)) {
			// we should have seen this in the Third Eye, but we haven't
			ui_cact.updateStatus(statuses[f.type], false, f.date);
			changedStatuses = true;
		}
	}
	if ((len = gaps.length) && now - (i = gaps[len - 1]) < ui_cact.guarantee.conversion.timeout) {
		// we had a gap in the Third Eye during which hero could convert gold into experience,
		// and we will never know whether he/she actually did it
		activities.push({type: 'conversion', date: i, result: -1, logID: ''});
		changed = true;
	}
	if (changed) {
		ui_cact.save(storage, activities.sort(ui_cact._byDate));
	}
	if (changedStatuses) {
		ui_cact.saveStatuses(storage, statuses);
	}
	if (len) {
		storage.set('ThirdEye:Gaps', '[]');
	}
	return changed || changedStatuses || !!len;
};

/**
 * @param {string} name
 * @returns {string}
 */
ui_cact.parseFightType = function(name) {
	if (/Подземелье|Dungeon/.test(name)) {
		return 'dungeon';
	} else if (/Заплыв|Sail/.test(name)) {
		return 'sail';
	} else if (/Полигон|Datamine/.test(name)) {
		return 'mining';
	} else if (/Тренировка|Challenge/.test(name)) {
		return 'spar';
	} else if (/Арена|Arena/.test(name)) {
		return 'arena';
	} else if (/Отряд|Group/.test(name)) { // there are no monster groups in English Godville
		return 'multi_monster';
	} else {
		return 'monster';
	}
};

/**
 * @param {number} len
 * @param {number} seed
 * @returns {string}
 */
ui_cact.generateBookWord = function(len, seed) {
	var result = '';
	seed = seed >>> 0 || 1;
	while (len-- > 0) {
		seed = seed * 48271 % 0x7FFFFFFF; // minstd_rand
		result += '┌┐└┘├┤┬┴┼═║╒╓╔╕╖╗╘╙╚╛╜╝╞╟╠╡╢╣╤╥╦╧╨╩╪╫╬'[seed % 38];
	}
	return result;
};

/**
 * @param {!GUIp.common.activities.Activity} act
 * @param {?Object} [desc]
 * @returns {{class: string, content: string, title: string}}
 */
ui_cact.describe = function(act, desc) {
	if (!desc) {
		desc = {class: '', content: '', title: ''};
	}
	if (act.result < 0 || act.type === 'souls' && act.src < 2) {
		switch (act.type) {
			case 'dungeon': desc.title = GUIp_i18n.open_dungeon_chronicle; break;
			case 'mining': desc.title = GUIp_i18n.mining_chronicle_unknown_result; break;
			case 'spar': desc.title = GUIp_i18n.open_spar_chronicle; break;
			case 'souls': desc.title = GUIp_i18n.open_dungeon_chronicle_souls; break;
			default: throw new Error('unexpected activity type: ' + act.type);
		}
		desc.class = 'e_fight_result_unknown e_fight_result_unknown_' + act.type;
		desc.content = '?';
	} else if (!act.result) {
		desc.class = desc.content = desc.title = '';
	} else {
		switch (act.type) {
			case 'dungeon':
				desc.class = 'e_fight_result_log';
				desc.content = '●'.repeat(act.result); // small, but it has the best support across browsers and OSes
				desc.title = act.result === 1 ? GUIp_i18n.got_log : GUIp_i18n.got_2_logs;
				break;
			case 'mining':
				desc.class = 'e_fight_result_byte';
				desc.content = ui_cact.generateBookWord(act.result, act.date * 1e-3);
				desc.title = act.result === 1 ? GUIp_i18n.got_byte : act.result === 2 ? GUIp_i18n.got_2_bytes : GUIp_i18n.got_3_bytes;
				break;
			case 'spar':
				desc.class = 'e_fight_result_exp';
				desc.content = '★';
				desc.title = GUIp_i18n.got_exp;
				break;
			case 'souls':
				desc.class = 'e_fight_result_soul';
				desc.content = act.result;
				desc.title = act.result === 1 ? GUIp_i18n.got_soul : GUIp_i18n.got_2_souls;
				break;
			default:
				throw new Error('unexpected activity type: ' + act.type);
		}
	}
	return desc;
};
