/**
 * @alias GUIp.common.sailing
 * @namespace
 */
var ui_sailing = GUIp.common.sailing = {};

/**
 * @returns {?Element}
 */
ui_sailing.tryFindMapBlock = function(isMobile) {
	// .querySelector is 40x slower
	return isMobile ? document.getElementById('map_wrap') : (document.getElementById('s_map') || document.getElementById('sail_map'));
};

/**
 * @returns {?Element}
 */
ui_sailing.tryFindChronicleBlock = function() {
	return document.getElementById('m_fight_log') || document.getElementById('fight_chronicle') || document.getElementsByClassName('e_m_fight_log')[0];
};

/**
 * @param {string} text
 * @returns {!Object<string, boolean>}
 */
ui_sailing.parseConditions = function(text) {
	var phrases = {
		pois:      /все подсказки в этом море окажутся метками|all treasure hints look like .[!]. here/i,
		migration: /в этом походе не будет зависимости силы тварей от расстояния|beasties are shuffled and can be anywhere in this sea/i,
		double:    /щедрое море сделает все клады двойными|all treasures are doubled in this generous sea/i,
		beasties:  /мешков не будет, во всех кладах твари|no gold bags here, only manimals and fenimals/i,
		winds:     /ветер разбросает ковчеги подальше от порта|wind disperses the arks all over the map/i,
		small:     /море тесное, а поход ограничен 50 ходами|this sea is small and the expedition must end in 50 turns/i,
		fires:     /в этом море огней маяков целая уйма|lots of lighthouses in this area/i,
		locked:    /граница на замке, покинуть заплыв можно только через порт|the border is closed, exit only through the port/i,
		roaming:   /в этом море все твари бродячие и пугливые|all beasties are roaming, but very shy/i,
		faststart: /на старте уже есть несколько подсказок|some treasure hints are already known/i,
		multipass: /многие острова можно будет посещать по несколько раз|many islands can be visited more than once/i,
		noempty:   /пустых островов здесь нет, лишь загадочные|the islands here are (?:not empty and mysterious|mysterious and never empty)/i,
		reefs:     /здесь столько рифов, что кусачих тварей почти не осталось|lots of reefs make this sea (almost )?uninhabitable/i,
		farsight:  /здесь видно всё, кроме указателей на клады|everything is perfectly visible here, but nothing hints the location of booty/i,
		kindness:  /тёплая атмосфера не даст ковчегам атаковать друг друга|peaceful atmosphere of this sea prevents arks from attacking each other/i,
		extrabooty:/вместо ящиков здесь есть лишний клад|this sea has an extra booty and no crates/i,
		whirlpools:/ласковые водовороты здесь встречаются на каждом шагу|vortexes here are ubiquitous and mild/i,
		spotlights:/все маяки в этом море работают прожекторами|lighthouses emit long and narrow beams/i
	};
	var conds = Object.create(null);
	for (var key in phrases) {
		if (phrases[key].test(text)) {
			conds[key] = true; // assign only those properties which are true
		}
	}
	return conds;
};

/**
 * @returns {?string}
 */
ui_sailing.tryExtractConditions = function() {
	var block = ui_sailing.tryFindChronicleBlock(),
		len, entries, node;
	if (!block) return null; // not sailing?
	if ((len = block.getElementsByClassName('line').length)) {
		// live sailing
		if (len >= 15) return null; // there are actually 20 entries in the block, but we test against 15 to be sure
		entries = block.getElementsByClassName('d_imp');
	} else {
		entries = block.getElementsByClassName('t1');
		if ((len = entries.length)) {
			// modern log
			for (var i = 0; i < len; i++) {
				node = entries[i];
				if (node.classList.contains('d_imp')) {
					return node.textContent;
				}
			}
			return '';
		}
		// we are viewing an old log in an archive
		entries = block.getElementsByClassName('d_imp');
		if (!entries.length) {
			// fallback for ancient logs, which do not use the d_imp class
			entries = block.getElementsByClassName('new_line');
		}
	}
	return Array.prototype.filter.call(entries, function(node) {
		return !/\bsaild_\d+\b/.test(node.className);
	}).map(function(node) {
		return node.textContent;
	}).join('\n');
};

/**
 * @param {string} html
 * @returns {string}
 */
ui_sailing.extractConditionsFromHTML = function(html) {
	var result = '', m,
		regex = /<div\s[^<>]*?\bclass\s*=\s*(["']?)([^"'<>]*?\bd_imp\b[^"'<>]*?)\1[^]*?<div\s[^<>]*?\bclass\s*=\s*["']?[^"'<>]*?\btext_content\b[^<>]*>([^]*?)<\/div>/ig;
	// roughly equivalent to .querySelectorAll('div.d_imp div.text_content')
	while ((m = regex.exec(html))) {
		if (!/\bsaild_\d+\b/.test(m[2])) {
			result += m[3] + '\n';
		}
	}
	return result;
};

/** @namespace */
ui_sailing.phrases = {
	/**
	 * @readonly
	 * @type {boolean}
	 */
	initialized: false,

	/**
	 * @readonly
	 * @type {number}
	 */
	beastiesCount: 0,

	/**
	 * @readonly
	 * @type {!Object<string, {name: string, hp: string, tre: ?(boolean|number|undefined)}>}
	 */
	beasties: Object.create(null),

	_beastiesRE: /[]/g,

	get _url() {
		var customChronicler = localStorage['LogDB:sailPhrasesURL'] || '';
		if (customChronicler.length >= 3) {
			return customChronicler;
		}
		return 'https://eximido.github.io/gvdb/seadb2_' + worker.GUIp_locale + '.json';
	},

	/**
	 * @private
	 * @param {*} beasties
	 */
	_assign: function(beasties) {
		this.initialized = true;
		if (!Array.isArray(beasties)) return;

		var regex = '(',
			dict = Object.create(null),
			count = 0;
		for (var i = 0, len = beasties.length; i < len; i++) {
			var beastie = beasties[i];
			if (!beastie) continue;
			var name = beastie.name;
			if (typeof name !== 'string' || typeof beastie.hp !== 'string') {
				continue;
			}

			var treType = typeof beastie.tre;
			if (treType !== 'boolean' && treType !== 'number' && beastie.tre != null) {
				beastie.tre = false;
			}
			if (count++) regex += '|';
			regex += GUIp.common.escapeRegex(name);
			dict[name] = beastie;
		}

		if (count) {
			this.beastiesCount = count;
			this.beasties = dict;
			this._beastiesRE = new RegExp(
				regex + (GUIp_locale === 'ru' ? ')(?!<)()' : ')([^\\s<]+|(?!<))') +
					'|[^>]([♂♀]|\uD83D[\uDCB0\uDCE6\uDC3E])', // ♂♀💰📦🐾
				'g'
			);
		}
	},

	_loadCached: function() {
		this._assign(GUIp.common.parseJSON(localStorage['LogDB:seaBeastiesList']));
	},

	/**
	 * @param {function()} callback
	 * @param {*} [thisArg]
	 */
	load: function(callback, thisArg) {
		if (this.initialized) {
			callback.call(thisArg);
			return;
		} else if (+localStorage['LogDB:lastSailUpdate'] >= Date.now() - 6*60*60*1000) { // take NaN into account
			this._loadCached(); // recent enough
			callback.call(thisArg);
			return;
		}

		// TODO: prevent multiple downloads
		var timeout = GUIp.common.setTimeout(function(self) {
			self._loadCached();
			callback.call(thisArg);
		}, 2e3, this);

		GUIp.common.getXHR(this._url, function(xhr) {
			worker.clearTimeout(timeout);

			if (xhr.lastModified && xhr.lastModified === localStorage['LogDB:lastSailSerial']) {
				GUIp.common.info('sail phrases DB is up to date');
				localStorage['LogDB:lastSailUpdate'] = Date.now();
				this._loadCached();
			} else {
				var response = GUIp.common.parseJSON(xhr.responseText);
				if (!response || response.status !== 'success') {
					GUIp.common.error('unexpected response to sea beasties update request:', xhr.responseText);
					this._loadCached();
				} else {
					// cache them
					localStorage['LogDB:seaBeastiesList'] = JSON.stringify(response.beasties);
					localStorage['LogDB:lastSailSerial'] = xhr.lastModified;
					localStorage['LogDB:lastSailUpdate'] = Date.now();
					this._assign(response.beasties);
				}
			}

			callback.call(thisArg);
		}.bind(this), function(xhr) {
			worker.clearTimeout(timeout);
			GUIp.common.error('cannot fetch sea beasties list (' + xhr.status + '):', xhr.responseText);
			this._loadCached();
			callback.call(thisArg);
		}.bind(this));
	}
};

/**
 * @param {string} name
 * @returns {boolean}
 */
ui_sailing.isBeastie = function(name) {
	if (!this.phrases.initialized) {
		throw new Error('using an uninitialized sailing phrases DB');
	}
	return name in this.phrases.beasties;
};

ui_sailing._bhp = false;

/**
 * @private
 * @param {string} m0
 * @param {(string|undefined)} name
 * @param {(string|undefined)} punctuation
 * @param {(string|undefined)} emoji
 * @returns {string}
 */
ui_sailing._replaceBeastie = function(m0, name, punctuation, emoji) {
	// we've lost `this` here
	var beastie = '',
		hp = '';
	if (emoji) return (
		m0[0] +
		'<span class="e_emoji e_emoji_sailing' + (GUIp.common.renderTester.testChar(emoji) ? '' : ' eguip_font') +
		'">' + emoji + '</span>'
	);
	beastie = ui_sailing.phrases.beasties[name];
	hp = GUIp.common.escapeHTML(beastie.hp);
	return (
		'<span class="e_smonster' + (beastie.tre ? ' e_smonster_tre' : '') +
		'" title="' + worker.GUIp_i18n.sea_monster + ' [' + hp + ']">' +
			name +
		'</span>' +
		punctuation +
		'<sup' + (ui_sailing._bhp ? '><wbr />[' : ' class="hidden"><wbr />[') + hp + ']</sup>'
	);
};

/**
 * @param {string} html
 * @returns {string}
 */
ui_sailing.describeBeasties = function(html) {
	if (!this.phrases.initialized) {
		throw new Error('using an uninitialized sailing phrases DB');
	}
	return html.replace(this.phrases._beastiesRE, this._replaceBeastie);
};

/**
 * @param {string} selector
 * @param {?string} [cls]
 * @param {?boolean} [showBeastiesHP]
 */
ui_sailing.describeBeastiesOnPage = function(selector, cls, showBeastiesHP) {
	this.phrases.load(function() {
		if (!this.phrases.beastiesCount) {
			return;
		}
		var block = this.tryFindChronicleBlock();
		if (!block) return;

		this._bhp = showBeastiesHP || GUIp.common.isAndroid;
		var nodes = block.querySelectorAll(selector);
		for (var i = 0, len = nodes.length; i < len; i++) {
			var node = nodes[i],
				original = node.innerHTML,
				processed = this.describeBeasties(original);
			if (original !== processed) {
				node.innerHTML = processed;
			}
			if (cls) {
				node.classList.add(cls);
			}
		}
	}, this);
};
