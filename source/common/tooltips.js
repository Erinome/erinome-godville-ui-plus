/**
 * @alias GUIp.common.tooltips
 * @namespace
 */
var ui_cttips = GUIp.common.tooltips = {};

ui_cttips._timer = 0;
ui_cttips._node = document.createElement('div');
ui_cttips._node.className = 'e_tooltip';
GUIp.common.addListener(ui_cttips._node, 'click', function onClick(ev) {
	ev.stopPropagation();
	ui_cttips.hide();
});

/**
 * @private
 * @param {!Element} target
 * @param {?Element} [titleNode]
 */
ui_cttips._onTimer = function(event, target, titleNode) {
	var title = titleNode ? titleNode.textContent : target.title,
		pos, left;
	ui_cttips._timer = 0;
	if (!title || !(pos = target.getBoundingClientRect()).width) {
		return; // the element lost its title or has been detached from the DOM
	}
	ui_cttips._node.textContent = title;
	// most targets has low height, so bounding tooltip vertical position directly to the target is ok
	ui_cttips._node.style.top = (pos.top + worker.pageYOffset + 10) + 'px';
	// however tooltip horizontal position for wide targets should depend on where exactly user touched them
	if (pos.width < 40 || event.targetTouches.length !== 1) {
		// if target is relatively small (or when there are several touches), use old base positioning for now
		left = pos.left + worker.pageXOffset + 10;
	} else {
		// otherwise try to use clientX of touch event
		left = event.targetTouches[0].clientX + worker.pageXOffset + 5;
	}
	// finally check that tooltip isn't too close to the right edge of the screen. is 100px enough?
	left = Math.min(left, Math.max(0, (document.documentElement.clientWidth + worker.pageXOffset) - 100));
	ui_cttips._node.style.left = left + 'px';
	document.body.appendChild(ui_cttips._node);
	worker.getSelection().removeAllRanges();
};

ui_cttips._onTouchStart = GUIp.common.try2.bind(null, function _onTouchStart(ev) {
	var guard, target;
	if (ui_cttips._timer) return;
	guard = ev.currentTarget;
	for (target = ev.target; !target.title; target = target.parentNode) {
		if (target === guard) return;
	}
	ui_cttips._timer = GUIp.common.setTimeout(ui_cttips._onTimer, 900, ev, target);
});

ui_cttips._onTouchStartSVG = GUIp.common.try2.bind(null, function _onTouchStartSVG(ev) {
	var guard, target;
	if (ui_cttips._timer) return;
	guard = ev.currentTarget;
	for (target = ev.target; target.tagName !== 'g'; target = target.parentNode) {
		if (target === guard) return;
	}
	if ((guard = target.getElementsByTagName('title')[0]) && guard.hasChildNodes()) {
		ui_cttips._timer = GUIp.common.setTimeout(ui_cttips._onTimer, 900, ev, target, guard);
	}
});

ui_cttips._onTouchEnd = GUIp.common.try2.bind(null, function _onTouchEnd() {
	if (ui_cttips._timer) {
		clearTimeout(ui_cttips._timer);
		ui_cttips._timer = 0;
	}
});

/**
 * @param {?Element} root
 */
ui_cttips.watchSubtree = function(root) {
	if (root) {
		// safe against multiple assignments
		root.addEventListener('touchstart', ui_cttips._onTouchStart);
		root.addEventListener('touchend', ui_cttips._onTouchEnd);
	}
};

/**
 * @param {?Element} root
 */
ui_cttips.watchSubtreeSVG = function(root) {
	if (root) {
		// safe against multiple assignments
		root.addEventListener('touchstart', ui_cttips._onTouchStartSVG);
		root.addEventListener('touchend', ui_cttips._onTouchEnd);
	}
};

ui_cttips.hide = function() {
	if (ui_cttips._node.parentNode) {
		ui_cttips._node.parentNode.removeChild(ui_cttips._node);
	}
};
