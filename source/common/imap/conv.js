/**
 * Functions for converting data between different formats (internal and raw).
 *
 * @alias GUIp.common.islandsMap.conv
 * @namespace
 */
ui_imap.conv = {};

/**
 * @typedef {Object} GUIp.common.islandsMap.conv.RawModel
 * @property {number} step - 1-based step number.
 * @property {!Array<number>} map
 * @property {!Array<number>} arks
 * @property {!Array<GUIp.common.islandsMap.Vec>} arkDirections
 * @property {!Array<GUIp.common.islandsMap.Vec>} visited
 * @property {!Array<number>} pois - A flattened array of encoded tiles.
 */

/** @const {!Array<number>} */
ui_imap.conv.poiCodes = [0x21, 0x60, 0x7E, 0x5E, 0x26, 0x28, 0x29]; // !`~^&()

/** @const {!Object<number, number>} */
ui_imap.conv.poiCodesDict = {0x21: 0, 0x60: 1, 0x7E: 2, 0x5E: 3, 0x26: 4, 0x28: 5, 0x29: 6};

/**
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {number} code
 * @returns {number}
 */
ui_imap.conv.encodeTilePosCode = function(pos, code) {
	// [q, -(q + r), r, code]
	// yes, the second byte is redundant
	return (pos & 0xFF) | ((~(pos + (pos << 8)) + 0x0100) & 0xFF00) | (pos & 0xFF00) << 8 | code << 24;
};

/**
 * @param {number} mask
 * @returns {GUIp.common.islandsMap.Vec}
 */
ui_imap.conv.decodeTilePos = function(mask) {
	return (mask & 0xFF) | (mask >> 8 & 0xFF00);
};

/**
 * @param {number} mask
 * @returns {number}
 */
ui_imap.conv.decodeTileCode = function(mask) {
	return mask >>> 24;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @returns {!Array<number>}
 */
ui_imap.conv.encodeMap = function(model) {
	var result = [],
		i = 0,
		tiles = model.tiles;
	for (var key in tiles) { // walk through the prototype chain, taking tiles from all layers of the map
		result[i++] = this.encodeTilePosCode(+key, tiles[key]);
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @returns {!Array<number>}
 */
ui_imap.conv.encodeArks = function(model) {
	var result = [];
	for (var i = 0, len = model.arks.length; i < len; i++) {
		if (model.arks[i] !== 0x8080) {
			result.push(this.encodeTilePosCode(model.arks[i], i + 0x31 /*1234*/));
		}
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @returns {!Array<number>}
 */
ui_imap.conv.encodePOIs = function(model) {
	var result = [];
	for (var i = 0, len = model.poiGroups.length; i < len; i++) {
		var group = model.poiGroups[i],
			// already revealed points of interest have their code replaced
			code = this.poiCodes[i % this.poiCodes.length];
		for (var j = 0, jlen = group.length; j < jlen; j++) {
			result.push(this.encodeTilePosCode(group[j], code));
		}
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @returns {!GUIp.common.islandsMap.conv.RawModel}
 */
ui_imap.conv.encode = function(model) {
	return {
		step: model.step,
		map: this.encodeMap(model),
		arks: this.encodeArks(model),
		arkDirections: model.arkDirections.slice(),
		visited: model.visited.slice(),
		pois: this.encodePOIs(model)
	};
};

/**
 * @param {!Array<number>} rawMap
 * @returns {!Array<number>}
 */
ui_imap.conv.ejectArksFromMap = function(rawMap) {
	var len = rawMap.length,
		tile = 0x0,
		arks;
	for (var i = 0; i < len; i++) {
		tile = rawMap[i];
		if (((tile - 0x01000000) & 0xFC000000) === 0x30000000) { // an ark
			rawMap[i--] = rawMap[--len];
			rawMap[len] = tile;
		}
	}
	arks = rawMap.slice(len);
	rawMap.length = len;
	return arks;
};

/**
 * @param {!Array<number>} rawMap
 * @returns {!Array<number>}
 */
ui_imap.conv.guessPOIsFromMap = function(rawMap) {
	var result = [], i, j, len;
outer:
	for (i = 0, len = rawMap.length; i < len; i++) {
		var offset = this.poiCodesDict[this.decodeTileCode(rawMap[i])] * 3;
		if (offset !== offset) continue;
		if (this.decodeTilePos(rawMap[i]) === 0x8080) {
			GUIp.common.warn('weird POI:', rawMap[i]);
			continue;
		}

		for (j = result.length; j < offset; j++) {
			result[j] = this.encodeTilePosCode(0x8080, this.poiCodes[Math.floor(j / 3)]);
		}
		// pick the first "empty" element
		for (j = 0; j < 3; j++) {
			if (result[offset + j] === undefined || this.decodeTilePos(result[offset + j]) === 0x8080) {
				result[offset + j] = rawMap[i];
				continue outer;
			}
		}
		GUIp.common.warn('too many same-colored POIs:', rawMap[i]);
	}
	// the last group may have less than 3 points of interest
	for (i = 2, len = result.length; i < len; i += 3) {
		if (this.decodeTilePos(result[i]) === 0x8080) {
			GUIp.common.warn('too few POIs of color #' + Math.floor(i / 3));
		}
	}
	return result;
};

/**
 * @param {number} step
 * @param {!Array<number>} map
 * @param {!Array<number>} arks
 * @param {!Array<number>} pois
 * @returns {!GUIp.common.islandsMap.conv.RawModel}
 */
ui_imap.conv.createRawModel = function(step, map, arks, pois) {
	return {step: step, map: map, arks: arks, arkDirections: [], visited: [], pois: pois};
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!Array<number>} rawMap
 */
ui_imap.conv.decodeAndAddTiles = function(model, rawMap) {
	var pos = 0x0,
		code = 0x0;
	for (var i = 0, len = rawMap.length; i < len; i++) {
		code = rawMap[i];
		pos = this.decodeTilePos(code);
		code = this.decodeTileCode(code);
		// we check only own properties because a map may have different tiles at the same position on different layers
		if (model.tiles.hasOwnProperty(pos)) {
			GUIp.common.warn('duplicate tile at ' + pos + ':', model.tiles[pos], 'vs', code);
			continue;
		}
		if (code >= 0x31 && code <= 0x34 /*1234*/) {
			if (model.arks[code - 0x31] === 0x8080) {
				model.arks[code - 0x31] = pos;
			} else {
				GUIp.common.warn('duplicate ark #' + (code - 0x30) + ':', model.arks[code - 0x31], 'vs', pos);
			}
			code = 0x20; /* */
		}
		ui_imap.mtrans.addTile(model, pos, code);
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!Array<number>} rawPois - A flattened array of 3-element groups.
 */
ui_imap.conv.decodeAndAddPois = function(model, rawPois) {
	var groupIndex = model.poiGroups.length,
		pos = 0x0,
		group;
	for (var i = 0, len = Math.floor((rawPois.length + 2) / 3); i < len; i++) {
		group = [];
		for (var j = i * 3, to = Math.min(i * 3 + 3, rawPois.length); j < to; j++) {
			pos = this.decodeTilePos(rawPois[j]); // ignore tile's code
			if (!(pos in model.tiles)) {
				GUIp.common.warn('dangling POI:', rawPois[j]);
				continue;
			}
			model.poiGroupIndexAt[pos] = groupIndex;
			group.push(pos);
		}
		model.poiGroups[groupIndex++] = group;
	}
};

/**
 * @param {!GUIp.common.islandsMap.conv.RawModel} rawModel
 * @returns {!GUIp.common.islandsMap.Model}
 */
ui_imap.conv.decode = function(rawModel) {
	var model = new ui_imap.Model(rawModel.step),
		i = 0,
		len = 0,
		pos = 0x0,
		index = 0;

	this.decodeAndAddTiles(model, rawModel.map);
	if (model.port === 0x8080) {
		ui_imap.mtrans.guessPort(model);
	}

	for (i = 0, len = rawModel.arks.length; i < len; i++) {
		index = rawModel.arks[i];
		pos = this.decodeTilePos(index);
		if (!(pos in model.tiles)) {
			GUIp.common.warn('dangling ark:', index);
			continue;
		}
		if (model.arks.includes(pos)) {
			GUIp.common.warn('multiple arks at the same position:', model.tiles[pos], 'vs', index);
			continue;
		}
		index = this.decodeTileCode(index) - 0x31; // 1234
		if (index < 0 || index >= 4) {
			GUIp.common.warn('pretending', rawModel.arks[i], 'to be an ark #' + (index + 1));
			continue;
		}
		if (model.arks[index] !== 0x8080) {
			GUIp.common.warn('duplicate ark:', model.arks[index], 'vs', rawModel.arks[i]);
			continue;
		}
		model.arks[index] = pos;
	}

	for (i = 0, len = rawModel.arkDirections.length; i < len; i++) {
		pos = rawModel.arkDirections[i];
		if (ui_imap.vec.len(pos) > 1) {
			GUIp.common.warn("invalid ark's direction:", pos);
			pos = 0x0;
		}
		model.arkDirections[i] = pos;
	}

	for (i = 0, len = rawModel.visited.length; i < len; i++) {
		pos = rawModel.visited[i];
		if (!(pos in model.tiles)) {
			GUIp.common.warn('dangling visited island:', rawModel.visited[i]);
			continue;
		}
		model.visited.push(pos);
	}

	this.decodeAndAddPois(model, rawModel.pois);

	return model;
};

/**
 * @param {!GUIp.common.islandsMap.conv.RawModel} rawModel
 */
ui_imap.conv.putArksOntoMap = function(rawModel) {
	var arkByPos = {},
		i = 0,
		len = 0,
		tile = 0x0;
	for (i = 0, len = rawModel.arks.length; i < len; i++) {
		tile = rawModel.arks[i];
		arkByPos[tile & 0xFFFFFF] = tile;
	}
	for (i = 0, len = rawModel.map.length; i < len; i++) {
		if ((tile = arkByPos[rawModel.map[i] & 0xFFFFFF] | 0)) {
			rawModel.map[i] = tile;
		}
	}
};
