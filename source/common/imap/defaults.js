/** @namespace */
ui_imap.defaults = {};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {{small: (boolean|undefined)}} conditions
 * @returns {number}
 */
ui_imap.defaults.predictBorderRadius = function(model, conditions) {
	return Math.max(model.borderRadius || (conditions.small ? 15 : 22), model.nonBorderRadius + 1);
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {{small: (boolean|undefined)}} conditions
 * @returns {number}
 */
ui_imap.defaults.predictMaxBorderRadius = function(model, conditions) {
	if (conditions.small) {
		return model.borderRadius ? Math.min(model.borderRadius + 1, 16) : 16;
	} else {
		return model.borderRadius ? Math.min(model.borderRadius + 2, 24) : 24;
	}
};

/**
 * @param {{small: (boolean|undefined), whirlpools: (boolean|undefined)}} conditions
 * @returns {number}
 */
ui_imap.defaults.getWhirlpoolZoneRadius = function(conditions) {
	return conditions.whirlpools ? 8 : conditions.small ? 12 : 18;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 * @param {!Object<string, boolean>} conditions
 * @param {!Array<string>} settings
 * @param {boolean} onSuperhero
 */
ui_imap.defaults.vTransBindAll = function(model, view, conditions, settings, onSuperhero) {
	var disableHighlighting = settings.includes('dhh'),
		disableTreasureHighlighter = settings.includes('dth'),
		drawerType =
			settings.includes('shh') ? 'polylinear' : settings.includes('newhints') ? 'tileMarking' : 'tileMarkingOld',
		radius = this.predictBorderRadius(model, conditions);

	ui_imap.vtrans.doIrreversibleChanges(model, view);
	ui_imap.vtrans.rulerManager.bind(model, view);
	ui_imap.vtrans.hintManager.requiresClick = disableHighlighting;
	ui_imap.vtrans.hintManager.bind(model, view, ui_imap.vtrans.createHintDrawer(model, view, drawerType, {
		whirlpoolZoneRadius: this.getWhirlpoolZoneRadius(conditions)
	}));
	ui_imap.vtrans.mapExpander.bind(model, view);
	ui_imap.vtrans.poiColorizer.bind(model, view);
	if (onSuperhero) {
		ui_imap.vtrans.poiPortDistanceInserter.bind(model, view);
	}
	if (!disableTreasureHighlighter) {
		ui_imap.vtrans.treasureHighlighter.bind(model, view);
	}
	if (!disableHighlighting) {
		ui_imap.vtrans.poiHighlighter.bind(model, view);
	}
	ui_imap.vtrans.borderDrawer.bind(model, view, radius, conditions);
	if (settings.includes('arknum')) {
		ui_imap.vtrans.arkTextRewriter.bind(model, view);
	}
	if (!onSuperhero || (settings.includes('arkdir') && (model.step >= 3 || conditions.winds))) {
		ui_imap.vtrans.arkDirectionDrawer.bind(model, view);
	}
	if (conditions.multipass) {
		ui_imap.vtrans.visitedFader.bind(model, view);
	}
	ui_imap.vtrans.beastieHighlighter.bind(model, view);
	ui_imap.vtrans.roamingBeastieHighlighter.bind(model, view);
	ui_imap.vtrans.aboveReefsHighlighter.bind(model, view);
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 */
ui_imap.defaults.vTransUnbindAll = function(model, view) {
	view.unregisterAll();
	// no checks required: just unbind everything
	ui_imap.vtrans.rulerManager.unbind(model, view);
	ui_imap.vtrans.hintManager.unbind(model, view);
	ui_imap.vtrans.mapExpander.unbind(model, view);
	ui_imap.vtrans.poiColorizer.unbind(model, view);
	ui_imap.vtrans.poiPortDistanceInserter.unbind(model, view);
	ui_imap.vtrans.treasureHighlighter.unbind(model, view);
	ui_imap.vtrans.poiHighlighter.unbind(model, view);
	ui_imap.vtrans.borderDrawer.unbind(model, view);
	ui_imap.vtrans.arkTextRewriter.unbind(model, view);
	ui_imap.vtrans.arkDirectionDrawer.unbind(model, view);
	ui_imap.vtrans.visitedFader.unbind(model, view);
	ui_imap.vtrans.beastieHighlighter.unbind(model, view);
	ui_imap.vtrans.roamingBeastieHighlighter.unbind(model, view);
	ui_imap.vtrans.aboveReefsHighlighter.unbind(model, view);
};
