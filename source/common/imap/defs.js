/**
 * @typedef {Object} GUIp.common.islandsMap.Arrow
 * @property {GUIp.common.islandsMap.Vec} pos
 * @property {GUIp.common.islandsMap.Vec} dir - A vector of minimal length to accurately represent that direction.
 */

/**
 * @class
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {number}  no   - There are no treasures up to (and including) this distance.
 * @param {number} [yes] - There is at least one treasure up to (and including) this distance.
 */
ui_imap.Thermo = function(pos, no, yes) {
	/** @type {GUIp.common.islandsMap.Vec} */
	this.pos = pos;
	/** @type {number} */
	this.no = no;
	/** @type {?number} */
	this.yes = yes || null;
};

ui_imap.Thermo.prototype = {
	constructor: ui_imap.Thermo,

	/**
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @returns {boolean} Whether a treasure can be in the given point, according to this hint.
	 */
	allows: function(pos) {
		return ui_imap.vec.dist(this.pos, pos) > this.no;
	},

	/**
	 * @param {!GUIp.common.islandsMap.Thermo} another
	 * @returns {boolean} Whether the hints cross (but not when one is contained inside another).
	 */
	crosses: function(another) {
		var d = ui_imap.vec.dist(this.pos, another.pos),
			r0 = this.yes || this.no,
			r1 = another.yes || another.no;
		if (d >= r0 && d >= r1) {
			return d <= r0 + r1;
		} else {
			// one circle lays inside another one
			return d >= Math.abs(r0 - r1) - !!(this.yes && another.yes);
		}
	}
};

/**
 * @class
 * @classdesc Contains all essential information about the map and objects on it.
 * @param {number} step - 1-based step number.
 */
ui_imap.Model = function(step) {
	/** @type {number} */
	this.step = step;
	/**
	 * Don't iterate through this dictionary. If you'd like to, then you probably should define an extra property here.
	 *
	 * @type {!Object<GUIp.common.islandsMap.Vec, number>}
	 */
	this.tiles = {};
	/** @type {number} */
	this.radius = 0;
	/** @type {number} */
	this.borderRadius = 0;
	/** @type {number} */
	this.nonBorderRadius = 0;
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.arks = [0x8080, 0x8080, 0x8080, 0x8080];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.arkDirections = [0x0, 0x0, 0x0, 0x0];
	/** @type {GUIp.common.islandsMap.Vec} */
	this.port = 0x8080;
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.visited = [];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.whirlpools = [];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.treasures = [];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.beasties = [];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.roamingBeasties = [];
	/** @type {!Array<GUIp.common.islandsMap.Vec>} */
	this.reefs = [];
	/** @type {!Array<!GUIp.common.islandsMap.Arrow>} */
	this.arrows = [];
	/** @type {!Array<!GUIp.common.islandsMap.Thermo>} */
	this.thermos = [];
	/** @type {!Array<!Array<GUIp.common.islandsMap.Vec>>} */
	this.poiGroups = []; // points of interest
	/** @type {!Object<GUIp.common.islandsMap.Vec, number>} */
	this.poiGroupIndexAt = {};
};

ui_imap.Model.prototype._foggedCodes = {
	0x3F: true, 0x21: true, 0x60: true, 0x7E: true, 0x5E: true, 0x26: true, 0x28: true, 0x29: true
};

ui_imap.Model.prototype._thermoCodes = {0x74: true, 0x79: true, 0x75: true, 0x6F: true, 0x5B: true, 0x5D: true};

/**
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {boolean}
 */
ui_imap.Model.prototype.isFoggedAt = function(pos) {
	return this.tiles[pos] in this._foggedCodes;
};

/**
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {boolean}
 */
ui_imap.Model.prototype.isNonBorderAt = function(pos) {
	var code = this.tiles[pos];
	return !!code && code !== 0x23 && code !== 0x24; // #$
};

/**
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {boolean}
 */
ui_imap.Model.prototype.isThermoAt = function(pos) {
	return this.tiles[pos] in this._thermoCodes;
};

/**
 * Event handler that gets passed the position of the tile that caused the event.
 * May return `true` to prevent other handlers from being executed.
 *
 * @typedef {function(GUIp.common.islandsMap.Vec, !Event): (boolean|undefined)} GUIp.common.islandsMap.ViewCallback
 */

/**
 * @class
 * @classdesc Represents an <svg> and maps DOM nodes to their positions and vice versa.
 * @param {!SVGElement} svg
 * @param {number} scale
 */
ui_imap.View = function(svg, scale) {
	/** @type {!SVGElement} */
	this.svg = svg;
	/** @type {?SVGElement} */
	this.root = null;
	/** @type {number} */
	this.scale = scale;
	/**
	 * Event listeners registered on the SVG node.
	 *
	 * @type {!Object<string, {
	 *     listener: function(!Event),
	 *     keys: !Array<string>,
	 *     callbacks: !Array<GUIp.common.islandsMap.ViewCallback>
	 * }>}
	 */
	this.handlers = Object.create(null);
	/** @type {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} */
	this.nodes = {};
	/** @type {!WeakMap<!SVGElement, GUIp.common.islandsMap.Vec>} */
	this.positions = new WeakMap;
};

ui_imap.View.prototype = {
	constructor: ui_imap.View,

	/**
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {!SVGElement} node
	 */
	addNode: function(pos, node) {
		this.nodes[pos] = node;
		this.positions.set(node, pos);
	},

	/**
	 * @private
	 * @param {!Array<GUIp.common.islandsMap.ViewCallback>} callbacks
	 * @param {!Event} ev
	 */
	_dispatchEvent: function(callbacks, ev) {
		for (var node = ev.target; node && node !== this.svg; node = node.parentNode) {
			var pos = this.positions.get(node);
			if (pos != null) {
				for (var i = 0, len = callbacks.length; i < len; i++) {
					if (GUIp.common.try2(callbacks[i], pos, ev)) {
						break;
					}
				}
				return;
			}
		}
	},

	/**
	 * @param {string} key
	 * @param {string} eventType
	 * @param {GUIp.common.islandsMap.ViewCallback} callback
	 * @param {boolean} [priority]
	 */
	register: function(key, eventType, callback, priority) {
		var h = this.handlers[eventType],
			callbacks, listener;
		if (h) {
			if (priority) {
				h.keys.unshift(key); // we have only a few listeners for each event so linear-time `unshift` is feasible
				h.callbacks.unshift(callback);
			} else {
				h.keys.push(key);
				h.callbacks.push(callback);
			}
		} else {
			callbacks = [callback];
			listener = this._dispatchEvent.bind(this, callbacks);
			this.handlers[eventType] = {listener: listener, keys: [key], callbacks: callbacks};
			this.svg.addEventListener(eventType, listener, true);
		}
	},

	/**
	 * @param {string} key
	 * @param {string} eventType
	 */
	unregister: function(key, eventType) {
		var h = this.handlers[eventType],
			i = 0;
		if (h && (i = h.keys.indexOf(key)) !== -1) {
			h.keys.copyWithin(i, i + 1);
			h.callbacks.copyWithin(i, i + 1);
			h.keys.length = --h.callbacks.length;
		}
	},

	unregisterAll: function() {
		var types = Object.keys(this.handlers),
			type = '';
		for (var i = 0, len = types.length; i < len; i++) {
			type = types[i];
			this.svg.removeEventListener(type, this.handlers[type].listener, true);
		}
		this.handlers = Object.create(null);
	}
};
