/**
 * A vector on a hexagonal grid, packed into a 16-bit integral number: 0x0000RRQQ.
 *
 * @typedef {number} GUIp.common.islandsMap.Vec
 */

// look, I have a picture for you!
/*
	 |
	-0------>  X
	 |------->  Q
	 | \
	 |  \
	 |   \
	 |    ┘
	 v     R

	 Y
*/

/** @namespace */
ui_imap.vec = {
	/**
	 * @param {number} q
	 * @param {number} r
	 * @returns {GUIp.common.islandsMap.Vec}
	 */
	make: function(q, r) {
		return (q & 0xFF) | (r & 0xFF) << 8;
	},

	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} scale
	 * @returns {GUIp.common.islandsMap.Vec}
	 */
	fromCartesian: function(x, y, scale) {
		var t = y / (3 * scale);
		return this.make(Math.round(x / (Math.sqrt(3) * scale) - t), Math.round(t * 2));
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} vec
	 * @param {number} scale
	 * @returns {!Array<number>} A 2-element array, namely, [x, y].
	 */
	toCartesian: function(vec, scale) {
		// make use of sign-propagating right shift
		var q = vec << 24 >> 24,
			r = vec << 16 >> 24;
		return [(q + r * 0.5) * Math.sqrt(3) * scale, r * 1.5 * scale];
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} a
	 * @param {GUIp.common.islandsMap.Vec} b
	 * @returns {GUIp.common.islandsMap.Vec}
	 */
	add: function(a, b) {
		return ((a + b) & 0xFF) | ((a + (b & 0xFF00)) & 0xFF00);
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} a
	 * @param {GUIp.common.islandsMap.Vec} b
	 * @returns {GUIp.common.islandsMap.Vec}
	 */
	sub: function(a, b) {
		return ((a - b) & 0xFF) | ((a - (b & 0xFF00)) & 0xFF00);
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} vec
	 * @param {number} n
	 * @returns {GUIp.common.islandsMap.Vec}
	 */
	mul: function(vec, n) {
		return (vec << 24) * n >>> 24 | ((vec & 0xFF00) << 16) * n >>> 16;
	},

	/**
	 * Compare two vectors as (r, q) pairs.
	 *
	 * @param {GUIp.common.islandsMap.Vec} a
	 * @param {GUIp.common.islandsMap.Vec} b
	 * @returns {number}
	 */
	cmp: function(a, b) {
		return (
			((a + 0x80) & 0xFF) | ((a + 0x8000) & 0xFF00)
		) - (
			((b + 0x80) & 0xFF) | ((b + 0x8000) & 0xFF00)
		);
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} vec
	 * @returns {number}
	 */
	len: function(vec) {
		var q = vec << 24 >> 24,
			r = vec << 16 >> 24,
			result = Math.abs(q + r);
		if ((q ^ r) < 0) { // if their signs are opposite
			return result + Math.min(Math.abs(q), Math.abs(r));
		}
		return result;
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} a
	 * @param {GUIp.common.islandsMap.Vec} b
	 * @returns {number}
	 */
	dist: function(a, b) {
		return this.len(this.sub(a, b));
	},

	/**
	 * @param {GUIp.common.islandsMap.Vec} arrow
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @returns {boolean}
	 */
	inArrowSector: function(arrow, pos) {
		if (!pos) return false;
		var q = pos << 24 >> 24,
			r = pos << 16 >> 24;
		switch (arrow) {
			case 0x0001: /*E*/   return r <= q && r << 1 >= -q;
			case 0x00FF: /*W*/   return r >= q && r << 1 <= -q;
			case 0x0100: /*SSE*/ return r >= q && r >= -q << 1;
			case 0xFF00: /*NNW*/ return r <= q && r <= -q << 1;
			case 0xFF01: /*NNE*/ return r >= -q << 1 && r << 1 <= -q;
			case 0x01FF: /*SSW*/ return r <= -q << 1 && r << 1 >= -q;
			case 0xFE01: /*N*/   return r < 0 && r === -q << 1;
			case 0x02FF: /*S*/   return r > 0 && r === -q << 1;
			default:             return false;
		}
	},

	/**
	 * @private
	 * @type {!Array<?Array<GUIp.common.islandsMap.Vec>>}
	 */
	_cache: [[0x0]],

	/**
	 * Generate an array of all vectors whose length is exactly n. The array is sorted according to cmp.
	 * These arrays are cached, so do not mutate them.
	 *
	 * @param {number} n
	 * @returns {!Array<GUIp.common.islandsMap.Vec>}
	 */
	ofLen: function(n) {
		var result = this._cache[n],
			i = 0;
		if (result) return result;
		for (i = this._cache.length; i < n; i++) {
			this._cache[i] = null;
		}
		result = [
			this.make(0, -n),
			this.make(0, n),
			this.make(-n, 0),
			this.make(n, 0),
			this.make(-n, n),
			this.make(n, -n)
		];
		for (i = 1; i < n; i++) {
			result.push(
				this.make(-i, i - n),
				this.make(-i, n),
				this.make(i, -n),
				this.make(i, n - i),
				this.make(-n, i),
				this.make(n, -i)
			);
		}
		return (this._cache[n] = result.sort(this.cmp));
	}
};
