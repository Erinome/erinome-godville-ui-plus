/**
 * Model transformers.
 *
 * @alias GUIp.common.islandsMap.mtrans
 * @namespace
 */
ui_imap.mtrans = {};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 */
ui_imap.mtrans.guessPort = function(model) {
	model.port = 0x0 in model.tiles ? 0x0 : 0x8080;
};

ui_imap.mtrans._processTile = function(model, pos, code) {
	var dist = 0;
	switch (code) {
		// ordered by popularity
		case 0x20: /* */ break;
		case 0x2C: case 0x3B: /*,;*/ model.reefs.push(pos); break;
		case 0x49: /*I*/ model.visited.push(pos); break;
		case 0x23: case 0x24: /*#$*/
			dist = ui_imap.vec.len(pos);
			if (!model.borderRadius || dist < model.borderRadius) {
				model.borderRadius = dist;
			}
			break;
		case 0x42: /*B*/ model.roamingBeasties.push(pos); break;
		case 0x62: /*b*/ model.beasties.push(pos); break;
		case 0x40: /*@*/ model.whirlpools.push(pos); break;
		case 0x4D: case 0x66: case 0x46: case 0x47: case 0x67: /*MfFGg*/ model.treasures.push(pos); break;
		case 0x74: /*t*/ model.thermos.push(new ui_imap.Thermo(pos, 0, 3)); break;
		case 0x79: /*y*/ model.thermos.push(new ui_imap.Thermo(pos, 3, 5)); break;
		case 0x75: /*u*/ model.thermos.push(new ui_imap.Thermo(pos, 5, 7)); break;
		case 0x6F: /*o*/ model.thermos.push(new ui_imap.Thermo(pos, 7, 9)); break;
		case 0x5B: /*[*/ model.thermos.push(new ui_imap.Thermo(pos, 9, 11)); break;
		case 0x5D: /*]*/ model.thermos.push(new ui_imap.Thermo(pos, 11)); break;
		case 0x71: /*q*/ model.arrows.push({pos: pos, dir: 0xFF00}); break;
		case 0x61: /*a*/ model.arrows.push({pos: pos, dir: 0x00FF}); break;
		case 0x7A: /*z*/ model.arrows.push({pos: pos, dir: 0x01FF}); break;
		case 0x65: /*e*/ model.arrows.push({pos: pos, dir: 0xFF01}); break;
		case 0x64: /*d*/ model.arrows.push({pos: pos, dir: 0x0001}); break;
		case 0x63: /*c*/ model.arrows.push({pos: pos, dir: 0x0100}); break;
		case 0x70: /*p*/ model.port = pos; break;
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {number} code
 */
ui_imap.mtrans.addTile = function(model, pos, code) {
	model.tiles[pos] = code;
	// update map radius
	var dist = ui_imap.vec.len(pos);
	if (dist > model.radius) {
		model.radius = dist;
	}
	// update `nonBorderRadius`
	if (!model.isFoggedAt(pos) && model.isNonBorderAt(pos) && dist > model.nonBorderRadius) {
		model.nonBorderRadius = dist;
	}
	// add the tile to a group of points of interest if it is one of those
	var groupIndex = (model.poiGroupIndexAt[pos] + 1) | 0; // for type stability
	if (groupIndex) {
		for (var i = model.poiGroups.length; i < groupIndex; i++) {
			model.poiGroups[i] = [];
		}
		model.poiGroups[groupIndex - 1].push(pos);
	}
	// do various processing depending on its type
	this._processTile(model, pos, code);
};

/**
 * Migrate some special tiles.
 *
 * @private
 * @param {!Array<GUIp.common.islandsMap.Vec>} oldArr
 * @param {!Array<GUIp.common.islandsMap.Vec>} newArr
 * @param {!Object<GUIp.common.islandsMap.Vec, number>} newTiles
 */
ui_imap.mtrans._migrateTiles = function(oldArr, newArr, newTiles) {
	var newPositions = GUIp.common.makeHashSet(newArr),
		pos = 0x0;
	for (var i = 0, len = oldArr.length; i < len; i++) {
		pos = oldArr[i];
		if (!(pos in newPositions) && pos in newTiles) {
			newArr.push(pos);
		}
	}
};

/**
 * @private
 * @param {!Array<{pos: GUIp.common.islandsMap.Vec}>} oldArr
 * @param {!Array<{pos: GUIp.common.islandsMap.Vec}>} newArr
 * @param {!Object<GUIp.common.islandsMap.Vec, number>} newTiles
 */
ui_imap.mtrans._migrateObjs = function(oldArr, newArr, newTiles) {
	var newPositions = GUIp.common.makeHashSet(newArr.map(function(o) { return o.pos; })),
		pos = 0x0,
		o;
	for (var i = 0, len = oldArr.length; i < len; i++) {
		o = oldArr[i];
		pos = o.pos;
		if (!(pos in newPositions) && pos in newTiles) {
			newArr.push(o); // these objects are immutable and thus can be shared
		}
	}
};

/**
 * Migrate objects shadowed by arks.
 *
 * @private
 * @param {!GUIp.common.islandsMap.Model} oldModel
 * @param {!GUIp.common.islandsMap.Model} newModel
 */
ui_imap.mtrans._migrateShadowed = function(oldModel, newModel) {
	var arkPos = 0x0,
		code = 0x0;
	for (var i = 0, len = newModel.arks.length; i < len; i++) {
		arkPos = newModel.arks[i];
		if (newModel.tiles[arkPos] === 0x20 && // 0x20 means we have nothing below this ark at the moment
			(code = oldModel.tiles[arkPos]) && (code | 0x20) !== 0x62 /*Bb*/ && !oldModel.isFoggedAt(arkPos)
		) {
			newModel.tiles[arkPos] = code;
			this._processTile(newModel, arkPos, code);
		}
	}
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 */
ui_imap.mtrans._migratePOIs = function(from, to) {
	var poiGroups = [], i, j, len, jlen, group, pos, target;
	for (i = 0, len = from.poiGroups.length; i < len; i++) {
		group = from.poiGroups[i];
		var newGroup = poiGroups[i] = [];
		for (j = 0, jlen = group.length; j < jlen; j++) {
			pos = group[j];
			if (pos in to.tiles) {
				to.poiGroupIndexAt[pos] = i; // unmerge POIs of the same color but possibly different groups
				newGroup.push(pos);
			}
		}
	}
	// move newly created points of interest to new groups, even if they have an already known color
	for (i = 0, len = to.poiGroups.length; i < len; i++) {
		group = to.poiGroups[i];
		target = null;
		var targetIndex = poiGroups.length;
		for (j = 0, jlen = group.length; j < jlen; j++) {
			pos = group[j];
			if (pos in from.poiGroupIndexAt) continue;
			// that point has just been created
			to.poiGroupIndexAt[pos] = targetIndex;
			if (target) {
				target.push(pos);
			} else {
				poiGroups[targetIndex] = target = [pos];
			}
		}
	}
	to.poiGroups = poiGroups;
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {GUIp.common.islandsMap.Vec} Vector from `pos` to the enemy.
 */
ui_imap.mtrans._findSingleEnemyNearby = function(model, pos) {
	var result = 0x0,
		deltas = ui_imap.vec.ofLen(1),
		delta = 0x0,
		i = 0,
		len = 0;
	// check for beasties
	for (i = 0; i < 6; i++) {
		delta = deltas[i];
		if ((model.tiles[ui_imap.vec.add(pos, delta)] | 0x20) === 0x62 /*Bb*/) {
			if (result) return 0x8080;
			result = delta;
		}
	}
	// check for other arks
	for (i = 0, len = model.arks.length; i < len; i++) {
		delta = model.arks[i];
		if (delta !== 0x8080 && ui_imap.vec.len((delta = ui_imap.vec.sub(delta, pos))) === 1) {
			if (result) return 0x8080;
			result = delta;
		}
	}
	return result;
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {boolean}
 */
ui_imap.mtrans._hasJustExploredIslandNearby = function(from, to, pos) {
	var deltas = ui_imap.vec.ofLen(1),
		adj = 0x0,
		newCode = 0x0;
	for (var i = 0; i < 6; i++) {
		adj = ui_imap.vec.add(pos, deltas[i]);
		newCode = to.tiles[adj];
		if (newCode !== from.tiles[adj]) {
			switch (newCode) {
				// I][ouytMG
				case 0x49: case 0x5D: case 0x5B: case 0x6F: case 0x75: case 0x79: case 0x74: case 0x4D: case 0x47:
					return true;
			}
		}
	}
	return false;
};

/**
 * @private
 * @param {!Array<GUIp.common.islandsMap.Vec>} whirlpools
 * @param {GUIp.common.islandsMap.Vec} pos
 * @returns {GUIp.common.islandsMap.Vec} Vector from the whirlpool to `pos`.
 */
ui_imap.mtrans._findWhirlpoolNearby = function(whirlpools, pos) {
	var delta = 0x0;
	for (var i = 0, len = whirlpools.length; i < len; i++) {
		if (ui_imap.vec.len((delta = ui_imap.vec.sub(pos, whirlpools[i]))) === 1) {
			return delta;
		}
	}
	return 0x0;
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 * @param {GUIp.common.islandsMap.Vec} oldPos
 * @param {GUIp.common.islandsMap.Vec} newPos
 * @returns {GUIp.common.islandsMap.Vec}
 */
ui_imap.mtrans._guessArkDirection = function(from, to, oldPos, newPos) {
	var delta = 0x0;
	if (oldPos === newPos) {
		// the ark stayed at the same place
		if ((delta = this._findSingleEnemyNearby(from, oldPos)) &&
			delta !== 0x8080 &&
			!this._findSingleEnemyNearby(to, oldPos) &&
			!this._hasJustExploredIslandNearby(from, to, oldPos)
		) {
			switch (to.tiles[ui_imap.vec.add(oldPos, delta)]) {
				// check if the beastie dropped an arrow
				case 0x71: /*q*/ return 0xFF00;
				case 0x61: /*a*/ return 0x00FF;
				case 0x7A: /*z*/ return 0x01FF;
				case 0x65: /*e*/ return 0xFF01;
				case 0x64: /*d*/ return 0x0001;
				case 0x63: /*c*/ return 0x0100;
			}
			return delta;
		}
	} else if (ui_imap.vec.len((delta = ui_imap.vec.sub(newPos, oldPos))) === 1) {
		// the ark moved normally
		return delta;
	} else if (this._findWhirlpoolNearby(from.whirlpools, oldPos)) {
		// the ark went through a pair of whirlpools
		return this._findWhirlpoolNearby(to.whirlpools, newPos);
	}
	return 0x0;
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 */
ui_imap.mtrans._guessArkDirections = function(from, to) {
	var forward = from.step <= to.step,
		oldArk = 0x0,
		newArk = 0x0;
	for (var i = 0, len = to.arks.length; i < len; i++) {
		if ((oldArk = from.arks[i]) !== 0x8080 && (newArk = to.arks[i]) !== 0x8080) {
			to.arkDirections[i] = forward ? (
				this._guessArkDirection(from, to, oldArk, newArk)
			) : this._guessArkDirection(to, from, newArk, oldArk);
		}
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 */
ui_imap.mtrans.migrate = function(from, to) {
	this._migrateTiles(from.visited, to.visited, to.tiles); // necessary for multipass seas
	this._migrateShadowed(from, to); // must be called prior to migrating treasures and arrows
	this._migrateTiles(from.treasures, to.treasures, to.tiles);
	this._migrateTiles(from.reefs, to.reefs, to.tiles);
	this._migrateObjs(from.arrows, to.arrows, to.tiles);
	this._migratePOIs(from, to);
	// we do not attempt to migrate thermo hints.
	// the only way we can lose a hint is when new one is placed right onto the old one (yeah, multipass seas
	// can be surprising sometimes), in which case we don't care about the old one anyway.
	this._guessArkDirections(from, to);
	if (to.thermos.length) {
		// exclude thermo hints from visited so that they are not rendered semi-transparent
		var thPositions = GUIp.common.makeHashSet(to.thermos.map(function(th) { return th.pos; }));
		GUIp.common.filterInPlace(to.visited, function(pos) { return !(pos in thPositions); });
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {number} radius
 * @param {boolean} fill
 * @param {?{oldRadius: number, added: !Array<GUIp.common.islandsMap.Vec>}} rollbackInfo
 * @returns {{oldRadius: number, added: !Array<GUIp.common.islandsMap.Vec>}}
 */
ui_imap.mtrans.expand = function(model, radius, fill, rollbackInfo) {
	var i = 0,
		j = 0,
		len = 0,
		to = 0,
		pos = 0x0,
		added, vectors;
	if (rollbackInfo) {
		added = rollbackInfo.added;
	} else {
		rollbackInfo = {oldRadius: model.radius, added: (added = [])};
	}
	if (fill) {
		for (i = -radius; i <= radius; i++) {
			for (j = -radius - Math.min(i, 0), to = radius - Math.max(i, 0); j <= to; j++) {
				pos = ui_imap.vec.make(i, j);
				if (!(pos in model.tiles)) {
					// we bypass `addTile` here because it will not do anything else
					model.tiles[pos] = 0x3F; // ?
					added.push(pos);
				}
			}
		}
	} else {
		vectors = ui_imap.vec.ofLen(radius);
		for (i = 0, len = vectors.length; i < len; i++) {
			pos = vectors[i];
			if (!(pos in model.tiles)) {
				// we bypass `addTile` here because it will not do anything else
				model.tiles[pos] = 0x3F; // ?
				added.push(pos);
			}
		}
	}

	if (radius > model.radius) {
		model.radius = radius;
	}
	if (model.port === 0x8080) {
		this.guessPort(model);
	}
	return rollbackInfo;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {{oldRadius: number, added: !Array<GUIp.common.islandsMap.Vec>}} rollbackInfo
 */
ui_imap.mtrans.unexpand = function(model, rollbackInfo) {
	for (var i = 0, len = rollbackInfo.added.length; i < len; i++) {
		delete model.tiles[rollbackInfo.added[i]];
	}
	model.radius = rollbackInfo.oldRadius;
	if (model.port !== 0x8080 && !(model.port in model.tiles)) {
		this.guessPort(model);
	}
};
