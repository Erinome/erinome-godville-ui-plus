/**
 * @class
 */
ui_imap.vtrans.Ruler = function() {
	/**
	 * @readonly
	 * @type {boolean}
	 */
	this.active = false;
	/** @type {!Array<function(boolean)>} */
	this.onstatechange = [];
	this._uniqueID = 'ruler' + Math.random();
	this._model = null;
	this._view = null;
	this._initialized = false; // for lazy initialization
	this._preventClick = false;
	/**
	 * @private
	 * @type {GUIp.common.islandsMap.Vec}
	 */
	this._anchor = 0x8080;
	/**
	 * @private
	 * @type {GUIp.common.islandsMap.Vec}
	 */
	this._hovered = 0x8080;
	/**
	 * @private
	 * @type {!Array<GUIp.common.islandsMap.Vec>}
	 */
	this._watched = [];
	/**
	 * @private
	 * @type {?SVGElement}
	 */
	this._line = null;
	/**
	 * @private
	 * @type {!Array<!SVGElement>}
	 */
	this._tooltips = [];

	this._onOver  = this._onOver.bind(this);
	this._onDown  = this._onDown.bind(this);
	this._onClick = this._onClick.bind(this);
	this._onLeave = GUIp.common.try2.bind(this, this._onLeave);
};

ui_imap.vtrans.Ruler.prototype = {
	constructor: ui_imap.vtrans.Ruler,

	/**
	 * @private
	 * @param {string} tagName
	 * @param {!Object<string, string>} attrs
	 * @param {?Array<!SVGElement>} [children]
	 * @returns {!SVGElement}
	 */
	_create: function(tagName, attrs, children) {
		var node = document.createElementNS('http://www.w3.org/2000/svg', tagName),
			keys = Object.keys(attrs),
			i = 0,
			len = 0,
			key = '';
		for (i = 0, len = keys.length; i < len; i++) {
			key = keys[i];
			node.setAttribute(key, attrs[key]);
		}
		if (children) {
			for (i = 0, len = children.length; i < len; i++) {
				node.appendChild(children[i]);
			}
		}
		return node;
	},

	_createElements: function() {
		var fragment = document.createDocumentFragment();

		// in SVG, order of declaration matters: the latter elements are rendered
		// above the former ones, so inject our code at the very end
		fragment.appendChild(this._create('defs', {class: 'e_ruler_defs'}, [
			this._create('marker', {
				id: 'e_ruler_arrow',
				markerWidth:  '10',
				markerHeight: '10',
				refX: '5',
				refY: '3',
				orient: 'auto',
				markerUnits: 'strokeWidth'
			}, [
				this._create('path', {d: 'M0,0 L0,6 L9,3 z'})
			])
		]));

		fragment.appendChild((this._line = this._create('line', {
			id: 'e_ruler',
			class: 'hidden',
			'marker-end': 'url(#e_ruler_arrow)'
		})));

		var len = this._watched.length + 1;
		for (var i = 0; i < len; i++) {
			fragment.appendChild((this._tooltips[i] = this._create('g', {class: 'e_ruler_tooltip hidden'}, [
				this._create('ellipse', {cx: '7', cy: '-4', rx: '9', ry: '7'}),
				this._create('text', {x: '7'})
			])));
		}
		this._tooltips.length = len;
		this._tooltips[0].classList.add('primary'); // for styling customization

		this._view.root.appendChild(fragment);
	},

	/**
	 * @private
	 * @param {?Element} node
	 */
	_remove: function(node) {
		var parent;
		if (node && (parent = node.parentNode)) {
			parent.removeChild(node);
		}
	},

	_removeElements: function() {
		var marker = document.getElementById('e_ruler_arrow'); // faster than looking for .e_ruler_defs
		if (marker) {
			this._remove(marker.parentNode);
		}
		this._remove(this._line);
		this._line = null;
		this._tooltips.forEach(this._remove);
		this._tooltips.length = 0;
	},

	/**
	 * @private
	 * @param {number} which
	 * @param {GUIp.common.islandsMap.Vec} where
	 */
	_raiseTooltip: function(which, where) {
		var tooltip = this._tooltips[which],
			text = tooltip.lastChild,
			scale = this._view.scale,
			xy = ui_imap.vec.toCartesian(where, scale);
		if (text) {
			text.textContent = ui_imap.vec.dist(this._anchor, where);
		}
		tooltip.setAttribute('transform', 'translate(' + (xy[0] + 0.4 * scale) + ',' + (xy[1] - 0.4 * scale) + ')');
		tooltip.classList.remove('hidden');
	},

	_raiseSecondaryTooltips: function() {
		for (var i = 0, len = this._watched.length; i < len; i++) {
			if (this._watched[i] === this._anchor) {
				this._tooltips[i + 1].classList.add('hidden');
			} else {
				this._raiseTooltip(i + 1, this._watched[i]);
			}
		}
	},

	_registerAnchor: function() {
		var xy = ui_imap.vec.toCartesian(this._anchor, this._view.scale);
		this._view.nodes[this._anchor].classList.add('e_ruler_anchor');
		this._line.setAttribute('x1', xy[0]);
		this._line.setAttribute('y1', xy[1]);
	},

	_unregisterAnchor: function() {
		this._view.nodes[this._anchor].classList.remove('e_ruler_anchor');
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.Vec} where
	 */
	_setAnchor: function(where) {
		this._anchor = where;
		this._registerAnchor();
		this._raiseSecondaryTooltips();
	},

	_unsetAnchor: function() {
		this._unregisterAnchor();
		this._anchor = 0x8080;
		// hide secondary tooltips
		for (var i = 1, len = this._tooltips.length; i < len; i++) {
			this._tooltips[i].classList.add('hidden');
		}
	},

	_showHoverEmulation: function() {
		this._view.nodes[this._hovered].classList.add('hovered');
	},

	_hideHoverEmulation: function() {
		this._view.nodes[this._hovered].classList.remove('hovered');
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.Vec} where
	 */
	_setHovered: function(where) {
		if (this._hovered !== 0x8080) {
			this._hideHoverEmulation();
		}
		this._hovered = where;
	},

	_hideArrow: function() {
		this._line.classList.add('hidden');
		this._tooltips[0].classList.add('hidden');
		if (this._hovered !== 0x8080) {
			this._hideHoverEmulation();
		}
	},

	_updateArrow: function() {
		if (this._hovered === this._anchor) {
			this._hideArrow();
			return;
		}
		var xy = ui_imap.vec.toCartesian(this._hovered, this._view.scale);
		this._line.setAttribute('x2', xy[0]);
		this._line.setAttribute('y2', xy[1]);
		this._line.classList.remove('hidden');
		// show the primary tooltip unless there is a secondary one above that tile
		if (this._watched.includes(this._hovered)) {
			this._tooltips[0].classList.add('hidden');
		} else {
			this._raiseTooltip(0, this._hovered);
		}
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.Vec} pos
	 */
	_onOver: function(pos) {
		// on touch devices mouseover event happens simultaneously with mousedown
		if (GUIp.common.isTouching) return;
		this._setHovered(pos);
		if (this.active && this._anchor !== 0x8080) {
			this._updateArrow();
		}
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {!Event} ev
	 */
	_onDown: function(pos, ev) {
		if (ev.button) return; // ignore everything but left (primary) button
		this._preventClick = this.active;
		if (!this.active) return;

		if (this._anchor === 0x8080) {
			this._setAnchor(pos);
			this._hovered = pos;
		} else if (pos === this._anchor) {
			// disable ruler mode when clicking on the anchor
			this._unsetAnchor();
			this.toggle();
		} else if (GUIp.common.isTouching) {
			// touch devices will require two clicks to show the ruler between the two selected tiles
			if (pos !== this._hovered) {
				this._setHovered(pos);
				this._updateArrow();
				this._showHoverEmulation();
			} else {
				this._hideArrow();
				this._unregisterAnchor();
				this._setAnchor(pos);
			}
		} else {
			this._hideArrow();
			this._unsetAnchor();
		}
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {!Event} ev
	 * @returns {boolean}
	 */
	_onClick: function(pos, ev) {
		if (!this._preventClick) return false;
		this._preventClick = false;
		ev.stopPropagation();
		return true; // stop immediate propagation
	},

	_onLeave: function() {
		// on touch devices we will not hide the ruler when clicking outside the map
		if (!GUIp.common.isTouching) {
			this._hideArrow();
			this._hovered = 0x8080;
		}
	},

	_init: function() {
		if (this._initialized || (!this.active && this._anchor === 0x8080)) {
			return;
		}

		this._view.register(this._uniqueID, 'mouseover', this._onOver);
		this._view.register(this._uniqueID, 'mousedown', this._onDown);
		this._view.register(this._uniqueID, 'click', this._onClick, true);
		this._view.svg.addEventListener('mouseleave', this._onLeave);

		this._watched = this._model.arks.filter(function(ark) { return ark !== 0x8080; });
		this._createElements();
		if (this._anchor !== 0x8080) {
			if (this._anchor in this._model.tiles) {
				this._registerAnchor();
				this._raiseSecondaryTooltips();
			} else {
				this._anchor = 0x8080;
			}
		}
		if (!(this._hovered in this._model.tiles)) {
			this._hovered = 0x8080;
		}

		this._initialized = true;
	},

	_onActivate: function() {
		this._view.svg.classList.add('e_no_double_tap');
		if (this._anchor !== 0x8080 && this._hovered !== 0x8080) {
			this._updateArrow();
			if (this._hovered !== this._anchor && GUIp.common.isTouching) {
				this._showHoverEmulation();
			}
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		if (!view.root) {
			GUIp.common.error('ruler is disabled: cannot find the root <g>');
			this._model = this._view = null;
			return;
		}
		this._model = model;
		this._view = view;
		this._initialized = false;
		this._init();
		if (this.active) {
			this._onActivate();
		}
	},

	unbind: function() {
		if (!this._model) return;

		this._view.unregister(this._uniqueID, 'mouseover');
		this._view.unregister(this._uniqueID, 'mousedown');
		this._view.unregister(this._uniqueID, 'click');
		this._view.svg.removeEventListener('mouseleave', this._onLeave);
		this._view.svg.classList.remove('e_no_double_tap');

		if (this._anchor !== 0x8080) {
			this._unregisterAnchor();
		}
		this._removeElements();
		this._model = this._view = null;
	},

	toggle: function() {
		if (!this._model) return;

		this.active = !this.active;
		this._init();
		if (this.active) {
			this._onActivate();
		} else {
			this._hideArrow();
			this._view.svg.classList.remove('e_no_double_tap');
		}

		for (var i = 0, len = this.onstatechange.length; i < len; i++) {
			this.onstatechange[i](this.active);
		}
	}
};
