/**
 * @interface GUIp.common.islandsMap.vtrans.IHintDrawer
 */
/**
 * Positions of hints that can be drawn by this object.
 * Passing arbitrary positions, not included in this array, to draw and undraw is forbidden.
 *
 * @readonly
 * @member {!Array<GUIp.common.islandsMap.Vec>} GUIp.common.islandsMap.vtrans.IHintDrawer#processable
 */
/**
 * @function GUIp.common.islandsMap.vtrans.IHintDrawer#draw
 * @param {GUIp.common.islandsMap.Vec} pos
 */
/**
 * @function GUIp.common.islandsMap.vtrans.IHintDrawer#undraw
 * @param {GUIp.common.islandsMap.Vec} pos
 */
/**
 * Undraw everything and remove utility elements from the SVG (if any).
 * It is forbidden to use a drawer after the disposal.
 *
 * @function GUIp.common.islandsMap.vtrans.IHintDrawer#dispose
 */

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {GUIp.common.islandsMap.Vec} center
 * @returns {!Array<GUIp.common.islandsMap.Vec>}
 */
ui_imap.vtrans._genAxes = function(model, center) {
	var result = [center], pos, delta, deltas = ui_imap.vec.ofLen(1);
	for (var i = 0; i < 6; i++) {
		delta = deltas[i];
		pos = center;
		for (var j = model.radius; j; j--) {
			pos = ui_imap.vec.add(pos, delta);
			if (model.isNonBorderAt(pos)) {
				result.push(pos);
			}
		}
	}
	return result;
};

/**
 * @private
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {!Array<GUIp.common.islandsMap.Vec>} redThermoPositions
 * @param {!Array<number>} redThermoRadiuses
 * @returns {boolean}
 */
ui_imap.vtrans._shouldMarkTileWithRed = function(pos, redThermoPositions, redThermoRadiuses) {
	var deltas = ui_imap.vec.ofLen(1),
		redThermosLen = redThermoPositions.length,
		adjPos = 0x0;
adjLoop:
	for (var i = 0; i < 6; i++) {
		adjPos = ui_imap.vec.add(pos, deltas[i]);
		for (var j = 0; j < redThermosLen; j++) {
			if (ui_imap.vec.dist(adjPos, redThermoPositions[j]) <= redThermoRadiuses[j]) {
				continue adjLoop;
			}
		}
		return true; // there is an adjacent tile not covered by any red hint
	}
	return false;
};

/**
 * @class
 * @implements {GUIp.common.islandsMap.vtrans.IHintDrawer}
 * @classdesc Draws hints by adding a CSS class to their tiles.
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 */
ui_imap.vtrans.TileMarkingHintDrawer = function(model, view) {
	this.processable = [];
	this._nodes = view.nodes;
	this._associated = {}; // !Object<GUIp.common.islandsMap.Vec, [string, !Array<GUIp.common.islandsMap.Vec>]>
	/**
	 * @private
	 * @type {!Object<string, !Object<GUIp.common.islandsMap.Vec, number>>}
	 */
	this._refs = {e_hint_p: {}, e_hint: {}, e_hint_r: {}};
	this._processPort(model);
	this._processThermos(model);
	this._processArrows(model);
};

ui_imap.vtrans.TileMarkingHintDrawer.prototype = {
	constructor: ui_imap.vtrans.TileMarkingHintDrawer,

	// this: GUIp.common.islandsMap.Vec
	_allows: function(thermo) { return thermo.allows(this); },

	_isAppropriate: function(model, pos, thermos) {
		return model.isNonBorderAt(pos) && thermos.every(this._allows, pos);
	},

	_processPort: function(model) {
		var portPos = model.port;
		if (portPos === 0x8080) return;
		this.processable.push(portPos);
		this._associated[portPos] = ['e_hint_p', ui_imap.vtrans._genAxes(model, portPos)];
	},

	/**
	 * @private
	 * @param {!Array<GUIp.common.islandsMap.Vec>} positions
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.Thermo} thermo
	 * @param {number} radius
	 * @param {!Array<!GUIp.common.islandsMap.Thermo>} overlapping
	 */
	_addGreenThermoPositions: function(positions, model, thermo, radius, overlapping) {
		var vectors = ui_imap.vec.ofLen(radius),
			base = thermo.pos,
			pos = 0x0;
		for (var i = 0, len = vectors.length; i < len; i++) {
			pos = ui_imap.vec.add(base, vectors[i]);
			if (this._isAppropriate(model, pos, overlapping)) {
				positions.push(pos);
			}
		}
	},

	/**
	 * @private
	 * @param {!Array<GUIp.common.islandsMap.Vec>} positions
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.Thermo} thermo
	 * @param {!Array<GUIp.common.islandsMap.Vec>} redThermoPositions
	 * @param {!Array<number>} redThermoRadiuses
	 */
	_addRedThermoPositions: function(positions, model, thermo, redThermoPositions, redThermoRadiuses) {
		var vectors = ui_imap.vec.ofLen(thermo.no),
			base = thermo.pos,
			pos = 0x0;
		for (var i = 0, len = vectors.length; i < len; i++) {
			pos = ui_imap.vec.add(base, vectors[i]);
			if (model.isNonBorderAt(pos) &&
				ui_imap.vtrans._shouldMarkTileWithRed(pos, redThermoPositions, redThermoRadiuses)
			) {
				positions.push(pos);
			}
		}
	},

	_processThermos: function(model) {
		var overlapping = [],
			olen = 0,
			j = 0,
			thermo, positions, another, redThermoPositions, redThermoRadiuses;
		for (var i = 0, len = model.thermos.length; i < len; i++) {
			thermo = model.thermos[i];
			this.processable.push(thermo.pos);
			this._associated[thermo.pos] = [thermo.yes ? 'e_hint' : 'e_hint_r', (positions = [])];

			if (thermo.yes) {
				olen = 0;
				for (j = 0; j < len; j++) {
					another = model.thermos[j];
					if (thermo.crosses(another)) {
						overlapping[olen++] = another;
					}
				}
				overlapping.length = olen;
				for (olen = thermo.yes; olen > thermo.no; olen--) {
					this._addGreenThermoPositions(positions, model, thermo, olen, overlapping);
				}
			} else {
				if (!redThermoPositions) {
					redThermoPositions = [];
					redThermoRadiuses = [];
					for (j = 0; j < len; j++) {
						another = model.thermos[j];
						if (!another.yes) {
							redThermoPositions.push(another.pos);
							redThermoRadiuses.push(another.no);
						}
					}
				}
				this._addRedThermoPositions(positions, model, thermo, redThermoPositions, redThermoRadiuses);
			}
		}
	},

	_processArrows: function(model) {
		var thermosLen = model.thermos.length,
			deltas = ui_imap.vec.ofLen(1),
			tile, pos, j, k, klen;
		for (var i = 0, len = model.arrows.length; i < len; i++) {
			var positions = [], used = {}, arrow = model.arrows[i];
			this.processable.push(arrow.pos);
			this._associated[arrow.pos] = ['e_hint', positions];

			var initial = ui_imap.vec.add(arrow.pos, arrow.dir);
			if (this._isAppropriate(model, initial, model.thermos)) {
				positions[0] = initial;
				used[initial] = true;
			}
			for (j = 0; j < 6; j++) {
				var adj = deltas[j];
				// check if this is adjacent to arrow's direction
				if (ui_imap.vec.dist(arrow.dir, adj) !== 1) {
					continue;
				}
				pos = initial;
				// diameter (doubled radius) is the longest chord, so use it as an upper bound
				for (k = model.radius * 2 + 1; k; k--) {
					// (k & 0x1) === 1 during the first iteration
					pos = ui_imap.vec.add(pos, k & 0x1 ? adj : arrow.dir);
					if (this._isAppropriate(model, pos, model.thermos)) {
						positions.push(pos);
						used[pos] = true;
					}
				}
			}
			// mark tiles around thermo hints
			for (j = 0; j < thermosLen; j++) {
				var vectors = ui_imap.vec.ofLen(model.thermos[j].no + 1);
				initial = model.thermos[j].pos;
				for (k = 0, klen = vectors.length; k < klen; k++) {
					pos = ui_imap.vec.add(initial, vectors[k]);
					if (ui_imap.vec.inArrowSector(arrow.dir, ui_imap.vec.sub(pos, arrow.pos)) &&
						!(pos in used) && this._isAppropriate(model, pos, model.thermos)
					) {
						positions.push(pos);
						used[pos] = true;
					}
				}
			}
		}
	},

	draw: function(pos) {
		var pair = this._associated[pos],
			cls = pair[0],
			positions = pair[1],
			refs = this._refs[cls];
		for (var i = 0, len = positions.length; i < len; i++) {
			pos = positions[i];
			if ((refs[pos] = refs[pos] + 1 || 1) === 1) {
				this._nodes[pos].classList.add(cls);
			}
		}
	},

	undraw: function(pos) {
		var pair = this._associated[pos],
			cls = pair[0],
			positions = pair[1],
			refs = this._refs[cls];
		for (var i = 0, len = positions.length; i < len; i++) {
			pos = positions[i];
			if (!--refs[pos]) {
				this._nodes[pos].classList.remove(cls);
			}
		}
	},

	dispose: function() {
		var pairs = Object.values(this._associated),
			cls = '',
			pair, positions;
		for (var i = 0, len = pairs.length; i < len; i++) {
			pair = pairs[i];
			cls = pair[0];
			positions = pair[1];
			for (var j = 0, jlen = positions.length; j < jlen; j++) {
				this._nodes[positions[j]].classList.remove(cls);
			}
		}
	}
};

/**
 * @class
 * @implements {GUIp.common.islandsMap.vtrans.IHintDrawer}
 * @classdesc Draws hints by adding a CSS class to their tiles. Hints' forms depend on which other hints are activated.
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 */
ui_imap.vtrans.TileMarkingOldHintDrawer = function(model, view) {
	this.processable = [];
	this._nodes = view.nodes;
	/**
	 * @private
	 * @type {!Object<GUIp.common.islandsMap.Vec, {type: number, radius: number, positions: !Array<GUIp.common.islandsMap.Vec>}>}
	 */
	this._associated = {};
	/**
	 * @private
	 * @type {!Array<GUIp.common.islandsMap.Vec>}
	 */
	this._active = [];
	/**
	 * @private
	 * @type {!Array<GUIp.common.islandsMap.Vec>}
	 */
	this._activeRed = [];
	/**
	 * A dictionary of masks. Multiple bits can be set while _redraw is in progress.
	 *
	 * 0x1 - a tile is green.
	 * 0x2 - a tile is red.
	 *
	 * @private
	 * @type {!Object<GUIp.common.islandsMap.Vec, number>}
	 */
	this._tileState = {};
	this._processPort(model);
	this._processThermos(model);
	this._processArrows(model);
};

ui_imap.vtrans.TileMarkingOldHintDrawer.prototype = {
	constructor: ui_imap.vtrans.TileMarkingOldHintDrawer,

	_processPort: function(model) {
		var portPos = model.port;
		if (portPos === 0x8080) return;
		this.processable.push(portPos);
		this._associated[portPos] = {type: 0, radius: -1, positions: ui_imap.vtrans._genAxes(model, portPos)};
	},

	_processThermos: function(model) {
		var pos, radius, to;
		for (var i = 0, len = model.thermos.length; i < len; i++) {
			var positions = [], thermo = model.thermos[i];
			this.processable.push(thermo.pos);
			this._associated[thermo.pos] = {type: thermo.yes ? 1 : 2, radius: thermo.no, positions: positions};

			if (thermo.yes) {
				radius = thermo.no + 1;
				to = thermo.yes;
			} else {
				radius = to = thermo.no;
			}
			for (; radius <= to; radius++) {
				var vectors = ui_imap.vec.ofLen(radius);
				for (var j = 0, jlen = vectors.length; j < jlen; j++) {
					pos = ui_imap.vec.add(thermo.pos, vectors[j]);
					if (model.isNonBorderAt(pos)) {
						positions.push(pos);
					}
				}
			}
		}
	},

	_processArrows: function(model) {
		var deltas = ui_imap.vec.ofLen(1), pos, j, k;
		for (var i = 0, len = model.arrows.length; i < len; i++) {
			var positions = [], arrow = model.arrows[i];
			this.processable.push(arrow.pos);
			this._associated[arrow.pos] = {type: 3, radius: -1, positions: positions};

			var initial = ui_imap.vec.add(arrow.pos, arrow.dir);
			if (initial in model.tiles) { // definitely not a border
				positions[0] = initial;
			}
			for (j = 0; j < 6; j++) {
				var adj = deltas[j];
				// check if this is adjacent to arrow's direction
				if (ui_imap.vec.dist(arrow.dir, adj) !== 1) {
					continue;
				}
				pos = initial;
				// diameter (doubled radius) is the longest chord, so use it as an upper bound
				for (k = model.radius * 2 + 1; k; k--) {
					// (k & 0x1) === 1 during the first iteration
					pos = ui_imap.vec.add(pos, k & 0x1 ? adj : arrow.dir);
					if (model.isNonBorderAt(pos)) {
						positions.push(pos);
					}
				}
			}
		}
	},

	/**
	 * @private
	 * @param {!GUIp.common.islandsMap.vtrans.TileMarkingOldHintDrawer} self
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @returns {number}
	 */
	_getMaskForGreen: function(self, pos) {
		var active = self._active,
			associated = self._associated,
			thermoPos = 0x0;
		for (var i = 0, len = active.length; i < len; i++) {
			thermoPos = active[i];
			if (ui_imap.vec.dist(pos, thermoPos) <= associated[thermoPos].radius) {
				return 0x0;
			}
		}
		return 0x1;
	},

	/**
	 * @private
	 * @param {!GUIp.common.islandsMap.vtrans.TileMarkingOldHintDrawer} self
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {!Array<number>} activeRedThermoRadiuses
	 * @returns {number}
	 */
	_getMaskForRed: function(self, pos, activeRedThermoRadiuses) {
		return +ui_imap.vtrans._shouldMarkTileWithRed(pos, self._activeRed, activeRedThermoRadiuses) && 0x2;
	},

	/**
	 * @private
	 * @this {!Object<GUIp.common.islandsMap.Vec, {radius: number}>}
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @returns {number}
	 */
	_getRadiusAt: function(pos) { return this[pos].radius; },

	_redraw: function() {
		var mask = 0x0,
			cls = '',
			pos = 0x0,
			hint, getMask, activeRedThermoRadiuses, positions;

		for (var i = 0, len = this._active.length; i < len; i++) {
			hint = this._associated[this._active[i]];
			if (hint.type === 2) {
				// red thermo
				mask = 0x2;
				getMask = this._getMaskForRed;
				cls = 'e_hint_r';
				if (!activeRedThermoRadiuses) {
					activeRedThermoRadiuses = this._activeRed.map(this._getRadiusAt, this._associated);
				}
			} else {
				// green thermo or arrow
				mask = 0x1;
				getMask = this._getMaskForGreen;
				cls = 'e_hint';
			}

			// iterate through every tile of every active hint
			positions = hint.positions;
			for (var j = 0, jlen = positions.length; j < jlen; j++) {
				pos = positions[j];
				// mark the tile unless it is inside some other active thermo
				if ((this._tileState[pos] & mask) !== getMask(this, pos, activeRedThermoRadiuses)) {
					this._nodes[pos].classList.toggle(cls);
					this._tileState[pos] ^= mask;
				}
			}
		}
	},

	/**
	 * @private
	 * @param {function(this: DOMTokenList, string)} f
	 * @param {string} cls
	 * @param {!Array<GUIp.common.islandsMap.Vec>} positions
	 */
	_modifyNodes: function(f, cls, positions) {
		for (var i = 0, len = positions.length; i < len; i++) {
			f.call(this._nodes[positions[i]].classList, cls);
		}
	},

	draw: function(pos) {
		var hint = this._associated[pos];
		if (hint.type) {
			// thermo or arrow
			if (this._active.includes(pos)) {
				return; // already drawn
			}
			this._active.push(pos);
			if (hint.type === 2) {
				// red thermo
				this._activeRed.push(pos);
			}
			this._redraw();
		} else {
			// port
			this._modifyNodes(DOMTokenList.prototype.add, 'e_hint_p', hint.positions);
		}
	},

	undraw: function(pos) {
		var hint = this._associated[pos];
		if (hint.type) {
			// thermo or arrow
			if (!GUIp.common.linearRemove(this._active, pos)) {
				return; // not drawn
			}
			var mask = 0x1, cls = 'e_hint';
			if (hint.type === 2) {
				// red thermo
				mask = 0x2;
				cls = 'e_hint_r';
				GUIp.common.linearRemove(this._activeRed, pos);
			}
			for (var i = 0, len = hint.positions.length; i < len; i++) {
				pos = hint.positions[i];
				if (this._tileState[pos] & mask) {
					this._nodes[pos].classList.remove(cls);
					this._tileState[pos] ^= mask;
				}
			}
			this._redraw();
		} else {
			// port
			this._modifyNodes(DOMTokenList.prototype.remove, 'e_hint_p', hint.positions);
		}
	},

	dispose: function() {
		var port = Object.values(this._associated).find(function(hint) { return !hint.type; });
		if (port) {
			this._modifyNodes(DOMTokenList.prototype.remove, 'e_hint_p', port.positions);
		}
		var positions = Object.keys(this._tileState);
		for (var i = 0, len = positions.length; i < len; i++) {
			var pos = positions[i], mask = this._tileState[pos];
			if (mask & 0x1) {
				this._nodes[pos].classList.remove('e_hint');
			} else if (mask) {
				this._nodes[pos].classList.remove('e_hint_r');
			}
		}
	}
};

/**
 * @class
 * @implements {GUIp.common.islandsMap.vtrans.IHintDrawer}
 * @classdesc Draws hints by creating <line>s, <polyline>s, or <polygon>s.
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 * @param {{whirlpoolZoneRadius: number}} options
 */
ui_imap.vtrans.PolylinearHintDrawer = function(model, view, options) {
	var i, len;
	this.processable = [];
	/**
	 * @private
	 * @type {!Object<GUIp.common.islandsMap.Vec, !SVGElement>}
	 */
	this._associated = {};
	if (!view.root) {
		GUIp.common.error('polylinear hints are disabled: cannot find the root <g>');
		return;
	}
	if (model.port !== 0x8080) {
		this._associate(view, model.port, 'e_hint_t5', this._create6Cross(model, view, model.port));
	}
	for (i = 0, len = model.whirlpools.length; i < len; i++) {
		var whp = model.whirlpools[i];
		this._associate(view, whp, 'e_hint_t2',
			this._createInnerContour(view, whp, options.whirlpoolZoneRadius));
	}
	for (i = 0, len = model.thermos.length; i < len; i++) {
		var thermo = model.thermos[i], contour, cls;
		if (thermo.yes) {
			contour = this._createOuterContour(view, thermo.pos, thermo.yes);
			cls = 'e_hint_t1';
		} else {
			contour = this._createInnerContour(view, thermo.pos, thermo.no)
			cls = 'e_hint_t3';
		}
		this._associate(view, thermo.pos, cls, contour);
	}
	for (i = 0, len = model.arrows.length; i < len; i++) {
		var arrow = model.arrows[i];
		this._associate(view, arrow.pos, 'e_hint_t4', this._createSector(model, view, arrow.pos, arrow.dir));
	}
};

ui_imap.vtrans.PolylinearHintDrawer.prototype = {
	constructor: ui_imap.vtrans.PolylinearHintDrawer,

	// %.1f,%.1f
	_formatXY: function(x, y) {
		return x.toFixed(1) + ',' + y.toFixed(1);
	},

	_pFormatXY: function(xy) {
		return this._formatXY(xy[0], xy[1]);
	},

	_createElement: function(tagName) {
		return document.createElementNS('http://www.w3.org/2000/svg', tagName);
	},

	_createContainer: function(view, pos) {
		var g = this._createElement('g');
		g.setAttribute('transform', 'translate(' + this._pFormatXY(ui_imap.vec.toCartesian(pos, view.scale)) + ')');
		return g;
	},

	_create6Cross: function(model, view, pos) {
		var g = this._createContainer(view, pos),
			deltas = ui_imap.vec.ofLen(1),
			radius = Math.max(model.radius + 1, 25) * view.scale,
			xy;

		for (var i = 0; i < 3; i++) {
			var line = this._createElement('line');

			xy = ui_imap.vec.toCartesian(deltas[i], radius);
			line.setAttribute('x1', xy[0].toFixed(1));
			line.setAttribute('y1', xy[1].toFixed(1));

			xy = ui_imap.vec.toCartesian(deltas[5 - i], radius);
			line.setAttribute('x2', xy[0].toFixed(1));
			line.setAttribute('y2', xy[1].toFixed(1));

			g.appendChild(line);
		}

		return g;
	},

	_vecOrder: [0, 1, 3, 5, 4, 2],

	/**
	 * @private
	 * @param {!GUIp.common.islandsMap.View} view
	 * @param {string} type
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {function(!Array<GUIp.common.islandsMap.Vec>, !Array<string>)} builder
	 * @returns {!SVGElement}
	 */
	_createContour: function(view, type, pos, builder) {
		var deltas = ui_imap.vec.ofLen(1),
			points = [];
		builder.call(this, deltas, points);

		var g = this._createContainer(view, pos),
			poly = this._createElement(type);
		// poly.setAttribute('fill', 'none'); // used to flicker without that for some reason, but does not now
		poly.setAttribute('points', points.join(' '));
		g.appendChild(poly);
		return g;
	},

	_createInnerContour: function(view, pos, radius) {
		radius = (radius + 1 / 3) * view.scale;
		return this._createContour(view, 'polygon', pos, function(deltas, points) {
			for (var i = 0; i < 6; i++) {
				points.push(this._pFormatXY(ui_imap.vec.toCartesian(deltas[this._vecOrder[i]], radius)));
			}
		});
	},

	_createOuterContour: function(view, pos, radius) {
		return this._createContour(view, 'polygon', pos, function(deltas, points) {
			var prev = deltas[this._vecOrder[4]],
				cur  = deltas[this._vecOrder[5]],
				prevCornerXY = ui_imap.vec.toCartesian(ui_imap.vec.add(prev, cur), view.scale / 3);

			for (var i = 0; i < 6; i++) {
				var next = deltas[this._vecOrder[i]],
					xy       = ui_imap.vec.toCartesian(cur, radius * view.scale),
					cornerXY = ui_imap.vec.toCartesian(ui_imap.vec.add(cur, next), view.scale / 3);
				points.push(
					this._formatXY(xy[0] + prevCornerXY[0], xy[1] + prevCornerXY[1]),
					this._formatXY(xy[0] + cornerXY[0],     xy[1] + cornerXY[1])
				);

				prev = cur;
				cur = next;
				prevCornerXY = cornerXY;
			}
		});
	},

	_createSector: function(model, view, pos, dir) {
		return this._createContour(view, 'polyline', pos, function(deltas, points) {
			var coeff = Math.max(model.radius, 24) * 3;
			for (var i = 0; i < 6; i++) {
				var delta = deltas[i];
				// check whether this is adjacent to arrow's direction
				if (ui_imap.vec.dist(dir, delta) !== 1) {
					continue;
				}
				var xy = ui_imap.vec.toCartesian(ui_imap.vec.add(dir, delta), view.scale / 3),
					near = this._pFormatXY(xy),
					far = this._formatXY(xy[0] * coeff, xy[1] * coeff);
				if (!points.length) {
					points.push(far, near);
				} else {
					points.push(near, far);
					break;
				}
			}
		});
	},

	/**
	 * @private
	 * @param {GUIp.common.islandsMap.View} view
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {string} cls
	 * @param {!SVGElement} node
	 */
	_associate: function(view, pos, cls, node) {
		node.setAttribute('class', cls + ' epl e_hint hidden');
		view.root.appendChild(node);
		this.processable.push(pos);
		this._associated[pos] = node;
	},

	draw: function(pos) {
		this._associated[pos].classList.remove('hidden');
	},

	undraw: function(pos) {
		this._associated[pos].classList.add('hidden');
	},

	dispose: function() {
		var nodes = Object.values(this._associated),
			g, parent;
		for (var i = 0, len = nodes.length; i < len; i++) {
			g = nodes[i];
			if ((parent = g.parentNode)) {
				parent.removeChild(g);
			}
		}
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 * @param {string} type
 * @param {!Object} [options]
 * @returns {!GUIp.common.islandsMap.vtrans.IHintDrawer}
 */
ui_imap.vtrans.createHintDrawer = function(model, view, type, options) {
	switch (type) {
		case 'tileMarking':    return new this.TileMarkingHintDrawer(model, view);
		case 'tileMarkingOld': return new this.TileMarkingOldHintDrawer(model, view);
		case 'polylinear':     return new this.PolylinearHintDrawer(model, view, options);

		default: throw new Error("unknown drawer type '" + type + "'");
	}
};
