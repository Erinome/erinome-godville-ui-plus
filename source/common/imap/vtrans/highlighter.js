/**
 * @class
 * @param {string} className
 * @param {function(!GUIp.common.islandsMap.Model): !Array<!Array<GUIp.common.islandsMap.Vec>>} getGroups
 */
ui_imap.vtrans.TileGroupHighlighter = function(className, getGroups) {
	this._cls = className;
	this._getGroups = getGroups;
	this._uniqueID = 'highlighter' + Math.random();
	/**
	 * @private
	 * @type {!Object<GUIp.common.islandsMap.Vec, number>}
	 */
	this._indexOf = {};
	/**
	 * @private
	 * @type {!Array<!Array<!DOMTokenList>>}
	 */
	this._clsGroups = [];
	this._hovered = -1;
	this._onOver = this._onOver.bind(this);
	this._onOut  = this._onOut.bind(this);
};

ui_imap.vtrans.TileGroupHighlighter.prototype = {
	constructor: ui_imap.vtrans.TileGroupHighlighter,

	_toggle: function(enable) {
		var group = this._clsGroups[this._hovered];
		for (var i = 0, len = group.length; i < len; i++) {
			group[i].toggle(this._cls, enable);
		}
	},

	_onOver: function(pos) {
		this._onOut(); // we might skip a mouseout event in case the SVG gets replaced
		var index = this._indexOf[pos];
		if (index != null) {
			this._hovered = index;
			this._toggle(true);
		}
	},

	_onOut: function() {
		if (this._hovered !== -1) {
			this._toggle(false);
			this._hovered = -1;
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		// precalc _indexOf and _clsGroups
		this._indexOf = {};
		var groups = this._getGroups(model);
		for (var groupIndex = 0, glen = this._clsGroups.length = groups.length; groupIndex < glen; groupIndex++) {
			var classes = [], group = groups[groupIndex];
			for (var i = 0, len = group.length; i < len; i++) {
				var pos = group[i];
				this._indexOf[pos] = groupIndex;
				classes[i] = view.nodes[pos].classList;
			}
			this._clsGroups[groupIndex] = classes;
		}

		if (this._hovered !== -1) {
			if (this._hovered < this._clsGroups.length) {
				this._toggle(true);
			} else {
				this._hovered = -1;
			}
		}
		view.register(this._uniqueID, 'mouseover', this._onOver);
		view.register(this._uniqueID, 'mouseout', this._onOut);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		view.unregister(this._uniqueID, 'mouseover');
		view.unregister(this._uniqueID, 'mouseout');
		if (this._hovered !== -1 && this._clsGroups.length) {
			this._toggle(false);
		}
		// this._indexOf = {};
		this._clsGroups.length = 0;
	}
};
