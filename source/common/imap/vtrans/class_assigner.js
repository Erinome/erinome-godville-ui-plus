/**
 * @class
 * @param {string} cls
 * @param {function(!GUIp.common.islandsMap.Model): !Array<GUIp.common.islandsMap.Vec>} getPositions
 */
ui_imap.vtrans.ClassAssigner = function(cls, getPositions) {
	this._cls = cls;
	this._getPositions = getPositions;
};

ui_imap.vtrans.ClassAssigner.prototype = {
	constructor: ui_imap.vtrans.ClassAssigner,

	_apply: function(model, nodes, newState) {
		var positions = this._getPositions(model);
		for (var i = 0, len = positions.length; i < len; i++) {
			nodes[positions[i]].classList.toggle(this._cls, newState);
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		this._apply(model, view.nodes, true);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._apply(model, view.nodes, false);
	}
};
