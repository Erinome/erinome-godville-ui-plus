/** @namespace */
ui_imap.observer = {};

ui_imap.observer._isRelevantNode = function(node) {
	var classes = node.classList;
	if (classes && (classes.contains('e_hint') || classes.contains('e_ruler_tooltip') || classes.contains('e_ruler_defs'))) {
		// ignore our own hints and ruler
		return false;
	}
	// the ruler has much stuff to ignore
	return node.id !== 'e_ruler' && node.nodeName.toLowerCase() !== '#text';
};

ui_imap.observer._isRelevantMutation = function(mutation) {
	if (mutation.target.classList.contains('dir_resp')) {
		// ignore native direction notice
		return false;
	}
	return Array.prototype.some.call(mutation.addedNodes, ui_imap.observer._isRelevantNode);
};

/**
 * @param {function()} callback
 * @param {*} [thisArg]
 * @returns {!MutationObserver}
 */
ui_imap.observer.create = function(callback, thisArg) {
	return GUIp.common.newMutationObserver(function(mutations, observer) {
		if (mutations.some(ui_imap.observer._isRelevantMutation)) {
			try {
				callback.call(thisArg);
			} finally {
				observer.takeRecords();
			}
		}
	});
};
