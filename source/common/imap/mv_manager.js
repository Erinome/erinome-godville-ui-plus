/**
 * @interface GUIp.common.islandsMap.IMVManager
 */
/**
 * @property {?GUIp.common.islandsMap.Model} GUIp.common.islandsMap.IMVManager#model
 * @readonly
 */
/**
 * @property {?GUIp.common.islandsMap.View} GUIp.common.islandsMap.IMVManager#view
 */
/**
 * @function GUIp.common.islandsMap.IMVManager#replaceModelAndView
 * @param {!Element} svg
 * @param {number} step
 */
/**
 * @function GUIp.common.islandsMap.IMVManager#replaceView
 * @param {!Element} svg
 */
/**
 * @function GUIp.common.islandsMap.IMVManager#expandMap
 * @param {boolean} fill
 * @returns {function(function(!GUIp.common.islandsMap.conv.RawModel))}
 */
/**
 * @function GUIp.common.islandsMap.IMVManager#unexpandMap
 * @returns {function(function(!GUIp.common.islandsMap.conv.RawModel))}
 */

/**
 * @class
 * @implements {GUIp.common.islandsMap.IMVManager}
 * @param {?GUIp.common.islandsMap.Model} initialModel
 */
ui_imap.MigratingMVManager = function(initialModel) {
	this.model = initialModel;
	this.view = null;
	/** @type {!Object<string, boolean>} */
	this.conditions = {};
	this._expansionRollbackInfo = null;
};

ui_imap.MigratingMVManager.prototype = {
	constructor: ui_imap.MigratingMVManager,

	replaceModelAndView: function(svg, step) {
		var mv = ui_imap.domParsers.mvFromSVG(svg, step, this.conditions);
		if (this.model) {
			ui_imap.mtrans.migrate(this.model, mv.model);
		}
		this.model = mv.model;
		this.view = mv.view;
	},

	replaceView: function(svg) {
		this.view = ui_imap.domParsers.vFromSVG(svg);
	},

	_createRedrawCaller: function() {
		var model = this.model,
			rawModel;
		return function(redraw) {
			if (!rawModel) rawModel = ui_imap.conv.encode(model);
			redraw(rawModel);
		};
	},

	expandMap: function(fill) {
		this._expansionRollbackInfo = ui_imap.mtrans.expand(
			this.model,
			ui_imap.defaults.predictBorderRadius(this.model, this.conditions),
			fill,
			this._expansionRollbackInfo
		);
		return this._createRedrawCaller();
	},

	unexpandMap: function() {
		ui_imap.mtrans.unexpand(this.model, this._expansionRollbackInfo);
		this._expansionRollbackInfo = null;
		return this._createRedrawCaller();
	}
};
