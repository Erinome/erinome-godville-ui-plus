/**
 * @alias GUIp.common.expr
 * @namespace
 */
var ui_cexpr = GUIp.common.expr = {};

/**
 * @readonly
 * @type {?function(string): {type: string}}
 */
ui_cexpr.jsep = null;

//! include './parser.js';
//! include './gv_api.js';

/**
 * @param {function(string): {type: string}} jsep
 */
ui_cexpr.init = function(jsep) {
	ui_cexpr.jsep = jsep;
	jsep.addBinaryOp('~', 7);
	jsep.addBinaryOp('~*', 7);
	jsep.addBinaryOp('!~', 7);
	jsep.addBinaryOp('!~*', 7);
	jsep.addLiteral('gv', ui_cexpr.gvAPIObject);
	jsep.addLiteral('Infinity', Infinity);
	jsep.addLiteral('Math', Math);
	jsep.addLiteral('RegExp', RegExp);
	jsep.addLiteral('String', String);
	jsep.addLiteral('Array', Array);
};
