/**
 * @private
 * @param {!Array<{type: string}>} items
 */
ui_cexpr._transformArray = function(items) {
	for (var i = 0, len = items.length; i < len; i++) {
		items[i] = ui_cexpr._transformAST(items[i], false);
	}
};

/**
 * @private
 * @param {{type: string}} node
 * @param {boolean} detectGV
 * @returns {{type: string}}
 */
ui_cexpr._transformMember = function(node, detectGV) {
	var prop = node.property;
	if (node.computed) {
		prop = ui_cexpr._transformAST(prop, false);
	} else if (prop.type !== 'Identifier') {
		// a bug in JSEP
		prop = {type: 'Identifier', name: prop.raw || 'this'};
	}

	var obj = node.object = ui_cexpr._transformAST(node.object, false),
		valueType = typeof obj.value;
	if (/*obj.type === 'Literal' &&*/ (valueType === 'object' || valueType === 'function')) {
		// check for typos in properties at compile-time
		if (!node.computed && !(obj.value && prop.name in obj.value)) {
			throw new Error('unknown variable "' + obj.raw + '.' + prop.name + '"');
		}
		if (detectGV && obj.type === 'E_GVLiteral') {
			return {
				type: 'E_GVExpression',
				property: node.computed ? prop : {
					type: 'Literal',
					value: prop.name,
					raw: '"' + prop.name + '"' // no need to escape
				}
			};
		}
	}

	node.property = prop;
	return node;
};

ui_cexpr._transformers = {
	Literal: function(node) {
		if (node.raw === 'gv') {
			node.type = 'E_GVLiteral';
		}
		return node;
	},

	Identifier: function(node) {
		// unknown identifiers are treated as (unquoted) strings
		return {
			type: 'Literal',
			value: node.name,
			raw: '"' + node.name + '"' // no need to escape
		};
	},

	ArrayExpression: function(node) {
		ui_cexpr._transformArray(node.elements);
		return node;
	},

	ConditionalExpression: function(node, inBooleanContext) {
		if (!node.test) {
			// I hope this is not assumed to be a feature by JSEP guys
			throw new Error('"ConditionalExpression" without a test');
		}
		node.test       = ui_cexpr._transformAST(node.test, true);
		node.consequent = ui_cexpr._transformAST(node.consequent, inBooleanContext);
		node.alternate  = ui_cexpr._transformAST(node.alternate, inBooleanContext);
		return node;
	},

	LogicalExpression: function(node, inBooleanContext) {
		if (!node.left) {
			// same hell as with ConditionalExpression
			throw new Error('"LogicalExpression" without left child');
		}
		node.left  = ui_cexpr._transformAST(node.left, inBooleanContext);
		node.right = ui_cexpr._transformAST(node.right, inBooleanContext);
		return node;
	},

	UnaryExpression: function(node) {
		if (!node.argument) {
			// same hell as with ConditionalExpression
			throw new Error('"UnaryExpression" without an argument');
		}
		node.argument = ui_cexpr._transformAST(node.argument, node.operator === '!' && node.prefix);
		return node;
	},

	BinaryExpression: function(node, inBooleanContext) {
		if (!node.left) {
			// same hell as with ConditionalExpression
			throw new Error('"BinaryExpression" without left child');
		}
		var op = node.operator,
			lhs = ui_cexpr._transformAST(node.left, false),
			rhs = ui_cexpr._transformAST(node.right, false),
			insensitive, negated;
		if ((negated = (insensitive = op === '!~*') || op === '!~')) {
			// a !~ b desugars to !(a ~ b)
			inBooleanContext = true;
		} else if (!(insensitive = op === '~*') && op !== '~') {
			// generic binary operator
			node.left = lhs;
			node.right = rhs;
			return node;
		}

		// regex operator
		return {
			type: 'E_MatchExpression',
			text: lhs,
			pattern: rhs,
			insensitive: insensitive,
			negated: negated,
			testOnly: inBooleanContext
		};
	},

	MemberExpression: function(node) {
		return ui_cexpr._transformMember(node, true);
	},

	CallExpression: function(node) {
		// we do not want to transform gv.inventoryHasType("coolstory-box") into
		// gvCache("inventoryHasType")("coolstory-box") since: 1. it's pointless, and 2. it loses "this"
		node.callee = (node.callee.type === 'MemberExpression' ? ui_cexpr._transformMember : ui_cexpr._transformAST)(
			node.callee, false
		);
		ui_cexpr._transformArray(node.arguments);
		return node;
	}
};

/**
 * @private
 * @param {{type: string}} node
 * @param {boolean} [inBooleanContext]
 * @returns {{type: string}}
 */
ui_cexpr._transformAST = function(node, inBooleanContext) {
	var f = ui_cexpr._transformers[node.type];
	if (!f) {
		throw new Error('"' + node.type + '" is unsupported');
	}
	return f(node, inBooleanContext);
};

/**
 * @param {string} text
 * @param {boolean} [inBooleanContext]
 * @returns {{type: string}}
 */
ui_cexpr.parse = function(text, inBooleanContext) {
	return ui_cexpr._transformAST(ui_cexpr.jsep(text), !!inBooleanContext);
};

ui_cexpr._getFirstChar = function(m0) { return m0[0]; };

/**
 * @private
 * @param {string} fragment
 * @returns {string}
 */
ui_cexpr._processTextFragment = function(fragment) {
	var regex = /\bgv\.(\w+)/g, m;
	while ((m = regex.exec(fragment))) {
		if (!(m[1] in ui_cexpr.gvAPIObject)) {
			throw new Error('unknown variable "' + m[0] + '"');
		}
	}
	return fragment.replace(/\{\{|\}\}/g, ui_cexpr._getFirstChar);
};

/**
 * @param {string} text
 * @returns {!Array<(string|{type: string})>}
 */
ui_cexpr.parseEmbedded = function(text) {
	var result = [], processedTill = 0, searchFrom = 0, oBrace, cBrace, expr;
	while ((oBrace = text.indexOf('{', searchFrom)) >= 0) {
		if (text[oBrace + 1] === '{') {
			// doubling a brace escapes it
			searchFrom = oBrace + 2;
			continue;
		}
		// yes, we grab the first encountered brace, so one cannot have them inside the code.
		// since current version of jsep supports neither \xXX nor \uXXXX escapes,
		// String.fromCharCode(123) and String.fromCharCode(125) have to be used instead
		// if you need them for some reason
		cBrace = text.indexOf('}', oBrace + 1);
		if (cBrace < 0) break;
		oBrace = text.lastIndexOf('{', cBrace - 1); // take the closest one if there are multiple braces in a row

		// process the part we've skipped over
		if (processedTill !== oBrace) {
			result.push(ui_cexpr._processTextFragment(text.slice(processedTill, oBrace)));
		}
		processedTill = searchFrom = cBrace + 1;

		// process the braced part
		expr = text.slice(oBrace + 1, cBrace).trim();
		result.push((/^gv\.\w+$/.test(expr) ? ui_cexpr._processTextFragment : ui_cexpr.parse)(expr));
	}
	// process the rest of the string
	if (processedTill !== text.length) {
		result.push(ui_cexpr._processTextFragment(text.slice(processedTill)));
	}
	return result;
};
