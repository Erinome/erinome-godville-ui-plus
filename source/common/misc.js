/**
 * @readonly
 * @type {boolean}
 */
GUIp.common.isAndroid = worker.navigator.userAgent.toLowerCase().indexOf('android') >= 0; // cannot use .includes here

/**
 * true if the last recently used input device was a touch screen.
 * Supposed to be used in mouse event handlers.
 *
 * @readonly
 * @type {boolean}
 */
GUIp.common.isTouching = false;

(function() {
	var moves = 0;

	var onMouseMove = function() {
		// on touch devices, mousemove is not fired when dragging
		// therefore, if we have two successive moves (without intervening touchend), then it's a real mouse
		if (++moves >= 2) {
			GUIp.common.isTouching = false;
			worker.removeEventListener('mousemove', onMouseMove, true);
		}
	};

	worker.addEventListener('mousemove', onMouseMove, true);
	worker.addEventListener('touchend', function onTouchEnd() {
		GUIp.common.isTouching = true;
		if (moves >= 2) {
			worker.addEventListener('mousemove', onMouseMove, true);
		}
		moves = 0;
	}, true);
})();

/**
 * @param {string} url
 * @returns {string}
 */
GUIp.common.resolveURL = function(url) {
	if (/^\w*:/.test(url)) {
		return url;
	} else if (url[0] !== '/') {
		if (worker.location.pathname.slice(-1) !== '/') {
			url = '/' + url;
		}
		return worker.location.protocol + '//' + worker.location.host + worker.location.pathname + url;
	} else if (url[1] !== '/') {
		return worker.location.protocol + '//' + worker.location.host + url;
	} else {
		return worker.location.protocol + url;
	}
};

GUIp.common.addCSSFromURL = function(href, id) {
	if (!href) {
		return;
	}
	document.head.insertAdjacentHTML('beforeend', '<link id="' + id + '" type="text/css" href="' + href + '" rel="stylesheet" media="screen" />');
};

GUIp.common.addCSSFromString = function(text, id) {
	if (!id) {
		id = 'guip_user_css';
	}
	if (!document.getElementById(id)) {
		document.head.insertAdjacentHTML('beforeend', '<style id="' + id + '"></style>');
	}
	document.getElementById(id).textContent = text;
};

/**
 * @param {string} text
 * @returns {*}
 */
GUIp.common.parseJSON = function(text) {
	try {
		return JSON.parse(text);
	} catch (e) {
		return null;
	}
};

/**
 * Shuffle the array in place.
 *
 * @param {!Array} arr
 * @returns {!Array}
 */
GUIp.common.shuffleArray = function(arr) {
	// walk forward for a bit better cache locality
	for (var i = 0, n = arr.length; n > 1; i++, n--) {
		var j = i + Math.floor(Math.random() * n), x = arr[j];
		arr[j] = arr[i];
		arr[i] = x;
	}
	return arr;
};

/**
 * Rearrange elements in the array such that all elements x for which pred(x) is true
 * come before all elements y for which pred(y) is false.
 *
 * @param {!Array<*>} arr
 * @param {function(*): boolean} pred
 * @param {*} [thisArg]
 * @returns {number} Number of elements for which pred(x) is true.
 */
GUIp.common.partitionArray = function(arr, pred, thisArg) {
	var l = 0, r = arr.length - 1;
	while (true) {
		while (l <= r && pred.call(thisArg, arr[l])) {
			l++;
		}
		while (l < r && !pred.call(thisArg, arr[r])) {
			r--;
		}
		if (l >= r) {
			return l;
		}
		var t = arr[l];
		arr[l++] = arr[r];
		arr[r--] = t;
	}
};

/**
 * @param {!Array<*>} arr
 * @param {function(*, number, !Array<*>): boolean} pred
 * @param {*} [thisArg]
 * @returns {!Array<*>}
 */
GUIp.common.filterInPlace = function(arr, pred, thisArg) {
	var i, len = arr.length;
	for (i = 0; i < len && pred.call(thisArg, arr[i], i, arr); i++) { }
	var j = i;
	while (++i < len) {
		var x = arr[i];
		if (pred.call(thisArg, x, i, arr)) {
			arr[j++] = x;
		}
	}
	arr.length = j;
	return arr;
};

/**
 * dest[pos:] = src
 *
 * @param {!Array} dest
 * @param {number} pos
 * @param {!Array} src
 */
GUIp.common.replaceArrayTail = function(dest, pos, src) {
	for (var i = 0, len = src.length; i < len; i++) {
		dest[pos++] = src[i];
	}
	dest.length = pos;
};

/**
 * Search for a first occurence of an element in the array and, if found, remove it. Complexity is O(n).
 * Relative order of the remaining elements is unspecified.
 *
 * @param {!Array<*>} arr
 * @param {*} x
 * @returns {boolean} true iff existed.
 */
GUIp.common.linearRemove = function(arr, x) {
	var i = arr.indexOf(x);
	if (i < 0) return false;
	var last = arr.length - 1;
	arr[i] = arr[last];
	arr.length = last;
	return true;
};

/**
 * Search for a first occurence of an element in the array and, if found, remove it. Complexity is O(n).
 *
 * @param {!Array<*>} arr
 * @param {*} x
 * @returns {boolean} true iff existed.
 */
GUIp.common.linearRemoveStable = function(arr, x) {
	var i = arr.indexOf(x);
	if (i < 0) return false;
	arr.copyWithin(i, i + 1);
	arr.length--;
	return true;
};

/**
 * @param {*} smth
 * @returns {boolean}
 */
GUIp.common.isIntegralArray = function(smth) {
	return Array.isArray(smth) && smth.every(Number.isSafeInteger); // assume smth.every === Array.prototype.every
};

/**
 * @param {!Array} a
 * @param {!Array} b
 * @returns {boolean}
 */
GUIp.common.areArraysEqual = function(a, b) {
	var len = a.length;
	if (len !== b.length) return false;
	for (var i = 0; i < len; i++) {
		if (a[i] !== b[i]) return false;
	}
	return true;
};

/**
 * Essentially the reverse of Object.keys.
 *
 * @param {!Array<*>} items
 * @param {?Object} [result]
 * @returns {!Object<*, boolean>}
 */
GUIp.common.makeHashSet = function(items, result) {
	result = result || Object.create(null);
	for (var i = 0, len = items.length; i < len; i++) {
		result[items[i]] = true;
	}
	return result;
};

/**
 * Kuhn's (a.k.a. Hungarian) algorithm.
 *
 * @param {!Array<!Array<number>>} g - The first part of the graph represented as an adjacency list.
 * @param {number} k - Size of the second part of the graph.
 * @returns {number}
 */
GUIp.common.findBipartiteMatching = function(g, k) {
	var n = g.length;
	if (!n) return 0;
	if (n > 0xFFFF) {
		throw new Error('too large graph for bipartite matching: n = ' + n + ', k = ' + k);
	}
	var match = new Uint16Array(k),
		used = new Uint8Array(n);
	match.fill(0xFFFF);

	var runKuhn = function(cur) {
		used[cur] = 1;
		var adj = g[cur];
		for (var i = 0, len = adj.length; i < len; i++) {
			var next = adj[i],
				next2 = match[next];
			if (next2 === 0xFFFF || (!used[next2] && runKuhn(next2))) {
				match[next] = cur;
				return 1;
			}
		}
		return 0;
	};

	var naivelyUsed = new Uint8Array(n),
		resultSize = 0,
		cur, i, len;
	// find a suboptimal matching using simple greedy algorithm
	for (cur = 0; cur < n; cur++) {
		var adj = g[cur];
		for (i = 0, len = adj.length; i < len; i++) {
			var next = adj[i];
			if (match[next] === 0xFFFF) {
				match[next] = cur;
				naivelyUsed[cur] = 1;
				resultSize++;
				break;
			}
		}
	}
	// improve it
	for (cur = 0; cur < n; cur++) {
		if (!naivelyUsed[cur]) {
			used.fill(0);
			resultSize += runKuhn(cur);
		}
	}
	return resultSize;
};

/**
 * Attempt to change (internal) name of the function. The new name is respected by Chrome's stacktraces,
 * however in other browsers it is not. This is a no-op in Opera since it is impossible to rename a function there.
 *
 * @param {string} newName
 * @param {!Function} func
 * @returns {!Function}
 */
GUIp.common.namedFunction = function(newName, func) {
	try {
		Object.defineProperty(func, 'name', {configurable: true, value: newName});
	} catch (e) { /* Function::name is non-configurable in Opera */ }
	return func;
};

/**
 * @param {!Object} obj
 * @param {string} prop
 * @param {function(): *} calc
 * @returns {!Object}
 */
GUIp.common.defineCachedProperty = function(obj, prop, calc) {
	return Object.defineProperty(obj, prop, {configurable: true, enumerable: true, get: function() {
		var value = calc.call(this);
		Object.defineProperty(this, prop, {configurable: true, enumerable: true, value: value});
		return value;
	}});
};

/**
 * @param {string} text
 * @returns {string}
 */
GUIp.common.escapeHTML = function(text) {
	return (
		text
		.replace(/&/g, '&amp;') // this one must be called first
		.replace(/"/g, '&#34;') // shorter than &quot;
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
	);
};

/**
 * @param {string} text
 * @returns {string}
 */
GUIp.common.escapeRegex = function(text) {
	return text.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
};

/**
 * @param {!RegExp} regex
 * @param {string} text
 * @param {*} [fallback]
 * @returns {(string|*)}
 */
GUIp.common.matchRegex = function(regex, text, fallback) {
	var m = regex.exec(text);
	return m ? m[1] : fallback;
};

/**
 * @param {?Array<(string|undefined)>} m
 * @returns {?string}
 */
GUIp.common.findNonEmptyCapture = function(m) {
	var s;
	if (m) {
		for (var i = 1, len = m.length; i < len; i++) {
			if ((s = m[i])) return s;
		}
	}
	return null;
};

/**
 * @class
 * @template T
 * @param {function(): T} f
 */
GUIp.common.FuncCache = function(f) {
	this._f = f;
	this._cached = undefined;
};

GUIp.common.FuncCache.prototype = {
	constructor: GUIp.common.FuncCache,

	/**
	 * @returns {T}
	 */
	get: function() {
		return this._cached !== undefined ? this._cached : (this._cached = this._f());
	},

	invalidate: function() {
		this._cached = undefined;
	}
};

/**
 * @template T
 * @param {number} delay
 * @param {function(T)} f
 * @returns {function(T)}
 */
GUIp.common.debounce = function(delay, f) {
	var timer = 0, execute = function(arg) {
		timer = 0;
		f(arg);
	};
	return function(arg) {
		if (timer) clearTimeout(timer);
		timer = GUIp.common.setTimeout(execute, delay, arg);
	};
};

/**
 * @template T
 * @param {number} delay
 * @param {function(T)} f
 * @returns {function(T)}
 */
GUIp.common.throttle = function(delay, f) {
	var timer = 0,
		execute = function() {
			var arg = savedArg;
			timer = 0;
			savedArg = execute;
			if (arg !== execute) f(arg);
		},
		savedArg = execute; // it is impossible to pass this reference as an argument from the outside
	return function(arg) {
		if (timer) {
			savedArg = arg;
		} else {
			timer = GUIp.common.setTimeout(execute, delay);
			f(arg);
		}
	};
};

/**
 * @function
 * @param {!Error} e
 */
// log by default. may be overridden
GUIp.common.onUnhandledException = GUIp.common.error;

/**
 * @param {!Function} func
 * @param {...*} [args]
 * @returns {*}
 */
GUIp.common.try = function(func) {
	try {
		return func.apply(this, Array.prototype.slice.call(arguments, 1));
	} catch (e) {
		GUIp.common.onUnhandledException(e);
	}
};

/**
 * @param {function(*, *): *} func
 * @param {*} [arg1]
 * @param {*} [arg2]
 * @returns {*}
 */
// special-cased for speed
GUIp.common.try2 = function(func, arg1, arg2) {
	try {
		return func.call(this, arg1, arg2);
	} catch (e) {
		GUIp.common.onUnhandledException(e);
	}
};

/**
 * @param {!Function} callback
 * @param {number} delay
 * @param {...*} [args]
 * @returns {number}
 */
GUIp.common.setTimeout = function(callback, delay, arg1, arg2) {
	var len = arguments.length;
	if (len <= 4) {
		return worker.setTimeout(GUIp.common.try2, delay, callback, arg1, arg2);
	}
	var args = [GUIp.common.try, delay, callback, arg1, arg2];
	for (var i = 4; i < len; i++) {
		args[i + 1] = arguments[i];
	}
	return worker.setTimeout.apply(null, args);
};

/**
 * @param {!Function} callback
 * @param {number} period
 * @param {...*} [args]
 * @returns {number}
 */
GUIp.common.setInterval = function(callback, period, arg1, arg2) {
	var len = arguments.length;
	if (len <= 4) {
		return worker.setInterval(GUIp.common.try2, period, callback, arg1, arg2);
	}
	var args = [GUIp.common.try, period, callback, arg1, arg2];
	for (var i = 4; i < len; i++) {
		args[i + 1] = arguments[i];
	}
	return worker.setInterval.apply(null, args);
};

/**
 * @param {function(!Array<!MutationRecord>, !MutationObserver)} callback
 * @returns {!MutationObserver}
 */
GUIp.common.newMutationObserver = function(callback) {
	return new MutationObserver(GUIp.common.try2.bind(null, callback));
};

/**
 * @param {!EventTarget} target
 * @param {string} type
 * @param {function(!Event)} listener
 * @param {(boolean|!Object)} [options]
 */
GUIp.common.addListener = function(target, type, listener, options) {
	target.addEventListener(type, GUIp.common.namedFunction('on' + type, function(ev) {
		GUIp.common.try2.call(this, listener, ev);
	}), options);
};

/**
 * @param {string} s
 * @returns {!GUIp.OMultiMap}
 */
GUIp.common.parseQueryString = function(s) {
	var q = new GUIp.OMultiMap,
		kv = '',
		eqPos = 0,
		kvPairs;
	if (!s || s === '?') return q;
	if (s[0] !== '?') {
		throw new Error('invalid query string: "' + s + '"');
	}
	kvPairs = s.split('&');
	for (var i = 0, len = kvPairs.length; i < len; i++) {
		kv = kvPairs[i];
		eqPos = kv.indexOf('=');
		if (eqPos >= 0) {
			q.add(decodeURIComponent(kv.slice(!i, eqPos)), decodeURIComponent(kv.slice(eqPos + 1)));
		} else {
			q.add(decodeURIComponent(i ? kv : kv.slice(1)), '');
		}
	}
	return q;
};

/**
 * @private
 * @this {!Array<string>}
 * @param {!Array<string>} values
 * @param {string} key
 */
GUIp.common._stringifyQueryString = function(values, key) {
	key = encodeURIComponent(key) + '=';
	for (var i = 0, len = values.length; i < len; i++) {
		this.push(key + encodeURIComponent(values[i]));
	}
};

/**
 * @param {!GUIp.OMultiMap} q
 * @returns {string}
 */
GUIp.common.stringifyQueryString = function(q) {
	var result = [];
	q.forEach(GUIp.common._stringifyQueryString, result);
	return result.length ? '?' + result.join('&') : '';
};

GUIp.common.setCurrentGodname = function(gn) {
	var a, names = localStorage.getItem('eGUI_CurrentUser');
	names = names ? names.split('|') : [];
	if (names[0] === gn) {
		return;
	}
	if ((a = names.indexOf(gn)) > -1) {
		names.splice(a,1);
	}
	names.unshift(gn);
	localStorage.setItem('eGUI_CurrentUser',names.slice(0,5).join('|'));
};

GUIp.common.getCurrentGodname = function(customDomain) {
	var a, b, sorted, names = (localStorage.getItem('eGUI_CurrentUser') || '').split('|');
	if (a = (document.getElementById('menu_top') || '').textContent) {
		sorted = names.slice().sort(function(a,b) { return b.length - a.length });
		if ((b = new RegExp(sorted.join('|')).exec(a))) {
			return b[0];
		}
	}
	return names[0] || GUIp.common.getGodnameFromCookies() || (!customDomain && GUIp.common.warn('username cannot be detected!'), '_unknown_');
};

GUIp.common.getGodnameFromCookies = function() {
	var result = '',
		ca = worker.document.cookie.split(';');
	for (var i = 0, len = ca.length; i < len; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.slice(1);
		}
		if (c.startsWith('gn=')) {
			result = c.slice('gn='.length);
			try {
				result = JSON.parse(decodeURIComponent(result)).replace(/\+/g,' ');
				if (localStorage.getItem('eGUI_' + result + ':ForumSubscriptions') === null) {
					result = '';
				}
			} catch (e) {
				result = '';
			}
			break;
		}
	}
	return result;
};

GUIp.common.forceDesktopPage = function() {
	if (GUIp.common.isAndroid) {
		worker.document.cookie = 'm_f=1;path=/;max-age=31536000';
		worker.document.cookie = 'm_pp=1;path=/;max-age=31536000';
		worker.document.cookie = 'm_fl=1;path=/;max-age=31536000';
		worker.document.cookie = 'm_n=1;path=/;max-age=31536000';
		var lnk = document.querySelector('a.dsk_mob, .m_switch a:not(#to_top)');
		if (lnk && lnk.href && lnk.href.includes('/to_desktop')) {
			lnk.click();
			return;
		}
		if (lnk = document.querySelector('.lastduelpl_f.to_desktop > a.ui-link')) {
			lnk.click();
			return;
		}
	}
};

/**
 * @param {string} path
 * @param {?function(): boolean} [guard]
 * @param {?function()} [onload]
 * @param {?function()} [onerror]
 */
GUIp.common.loadDomainScript = function(path, guard, onload, onerror) {
	if (guard && guard()) {
		// already satisfied
		if (onload) onload();
	} else {
		var s = document.createElement('script');
		s.src = '/javascripts/' + path;
		if (onload || onerror) {
			GUIp.common.addListener(s, 'load', function() {
				(!guard || guard() ? onload : onerror)();
			});
			GUIp.common.addListener(s, 'error', onerror);
		}
		document.head.appendChild(s);
	}
};

/**
 * @param {{type: string}} msg
 */
GUIp.common.postErinomeMessage = function(msg) {
	worker.postMessage({erinomeMessage: msg}, '*');
};

GUIp.common.getXHR = function(path, success_callback, fail_callback) {
	if (worker.GUIp_browser !== 'Opera') {
		GUIp.common.extensionXHR(path, 'GET', null, null, success_callback, fail_callback);
	} else {
		GUIp.common.processXHR(path, 'GET', null, null, success_callback, fail_callback);
	}
};
GUIp.common.postXHR = function(path, postdata, encoding, success_callback, fail_callback) {
	if (worker.GUIp_browser !== 'Opera') {
		GUIp.common.extensionXHR(path, 'POST', postdata, encoding, success_callback, fail_callback);
	} else {
		GUIp.common.processXHR(path, 'POST', postdata, encoding, success_callback, fail_callback);
	}
};
GUIp.common.processXHR = function() {
	//! include 'mixins/xhr.js';

	// this: function(!Object)
	var handler = function(xhr) {
		xhr.lastModified = xhr.getResponseHeader('Last-Modified');
		GUIp.common.try2(this, xhr);
	};

	return function(path, method, data, encoding, onSuccess, onFailure) {
		processXHR(path, method, data, encoding, onSuccess && handler.bind(onSuccess), onFailure && handler.bind(onFailure));
	};
}();
GUIp.common.extensionXHRCallbacks = {};
GUIp.common.extensionXHR = function(path, method, data, encoding, success_callback, fail_callback) {
	var scid, fcid;
	if (success_callback) {
		scid = Math.random().toString();
		GUIp.common.extensionXHRCallbacks[scid] = success_callback;
	}
	if (fail_callback) {
		fcid = Math.random().toString();
		GUIp.common.extensionXHRCallbacks[fcid] = fail_callback;
	}
	GUIp.common.postErinomeMessage({type:'webxhr',url:path,method:method,data:data,encoding:encoding,scid:scid,fcid:fcid});
};
GUIp.common.addListener(worker, 'message', function onWebXHRResponse(event) {
	var msg = event.data;
	if (!msg || !(msg = msg.erinomeMessage) || msg.type !== 'webxhrResponse') {
		return;
	}
	var callback = GUIp.common.extensionXHRCallbacks[msg.cid];
	delete GUIp.common.extensionXHRCallbacks[msg.scid];
	delete GUIp.common.extensionXHRCallbacks[msg.fcid];
	if (callback) callback(msg.xhr);
});

/**
 * @param {string} path
 * @param {?function(!Object)} [onsuccess]
 * @param {?function(!Object)} [onfailure]
 */
GUIp.common.getDomainXHR = function(path, onsuccess, onfailure) {
	GUIp.common.getXHR(worker.location.protocol + '//' + worker.location.host + path, onsuccess, onfailure);
};

/**
 * @param {string} logID
 * @param {?function(!Object)} [onsuccess]
 * @param {?function(!Object)} [onfailure]
 */
GUIp.common.requestLog = function(logID, onsuccess, onfailure) {
	GUIp.common.getDomainXHR('/duels/log/' + logID, onsuccess, onfailure);
};

/**
 * @param {string} sound
 */
GUIp.common.playSound = function(sound) {
	if (!sound.startsWith('data:') || worker.GUIp_browser === 'Opera') {
		try {
			var res, fmt = worker.GUIp_browser === 'Opera' ? 'wav' : 'mp3';
			switch (sound) {
				case 'msg':   sound = '/sounds/msg.' + fmt;       break;
				case 'spar':  sound = '/sounds/challenge.' + fmt; break;
				case 'arena': sound = '/sounds/arena.' + fmt;     break;
			}
			res = new Audio(sound).play();
			if (res) {
				res.catch(GUIp.common.warn);
			}
		} catch (e) { GUIp.common.error(e); }
	} else {
		GUIp.common.postErinomeMessage({type:'playsound',content:sound});
	}
};

/**
 * @param {string} theme
 */
GUIp.common.exposeThemeName = function(theme) {
	document.body.className = document.body.className.replace(/\bth_\w+/g, '').trim() + ' ' + theme;
};

/**
 * @param {number} date
 * @returns {number}
 */
GUIp.common.getDungeonVersion = function(date) {
	return GUIp_locale === 'ru' && date >= 1608296400e3 ||		// 18 Dec 2020, 13:00 UTC
		GUIp_locale === 'en' && date >= 1617508800e3 ? 2 : 1;	// 04 Apr 2021, 04:00 UTC
};

/**
 * @param {number} arrowCode
 * @param {number} dy
 * @param {number} dx
 * @param {number} version
 * @returns {boolean}
 */
GUIp.common.checkDungeonArrow = function(arrowCode, dy, dx, version) {
	var diag = true,
		step = 0;
	// mirror coordinates so that we have to deal only with E and SE arrows
	switch (arrowCode) {
		case 0x2197: /*↗*/ dy = -dy; break;
		case 0x2196: /*↖*/ dy = -dy; // break;
		case 0x2199: /*↙*/ dx = -dx; break;
		case 0x2190: /*←*/ dx = -dx; // break;
		case 0x2192: /*→*/ diag = false; break;
		case 0x2191: /*↑*/ step = -dy; dy = dx; dx = step; diag = false; break;
		case 0x2193: /*↓*/ step =  dy; dy = dx; dx = step; diag = false; break;
		case 0x2560: /*╠*/ return dx > 0;
		case 0x2563: /*╣*/ return dx < 0;
		case 0x2566: /*╦*/ return dy > 0;
		case 0x2569: /*╩*/ return dy < 0;
	}
	if (dx <= 0) return false;
	step = version === 2 ? 2.414 : 5;
	return dx <= (dy >= 0 ? dy : -dy) * step ? diag && dy >= 0 && dy <= dx * step : !diag;
};

GUIp.common.improveMap = function(map, isCGM, version, maxStep) {
	var i, j, ik, jk, len, chronolen = +worker.Object.keys(this.chronicles).reverse()[0],
		$boxML = document.querySelectorAll(map + ' .dml'),
		$boxMC = document.querySelectorAll(map + ' .dmc'),
		kRow = $boxML.length,
		kColumn = $boxML[0] && $boxML[0].children.length,
		MaxMap = 0,       // count of pointers of any type
		MaxMapThermo = 0, // count of thermo pointers
		MapData = {
			cells: {},
			kColumn: kColumn,
			kRow: kRow
		};
	if (!$boxML.length) {
		return;
	}
	if (!version) version = 2;
	var chamber = '✖',
		staircase = '◿';
	for (i = 1, len = maxStep || chronolen; i < len; i++) {
		if (this.chronicles[i].chamber) {
			chamber = '';
		} else if (this.chronicles[i].staircase) {
			staircase = '';
		}
		if (!chamber && !staircase) break;
	}
	// build map info
	for (ik = -1; ik <= kRow; ik++) {
		MapData.cells[ik] = {};
		for (jk = -1; jk <= kColumn; jk++) {
			if (ik < 0 || jk < 0 || ik === kRow || jk === kColumn) {
				MapData.cells[ik][jk] = {
					explored: false,
					wall: false,
					unknown: true,
					ptl: -1
				};
			} else {
				MapData.cells[ik][jk] = {
					explored: !((new worker.RegExp('[#?!⚠' + chamber + staircase + ']')).test($boxMC[ik * kColumn + jk].textContent)),
					wall: $boxMC[ik * kColumn + jk].textContent.includes('#'),
					unknown: $boxMC[ik * kColumn + jk].textContent.includes('?'),
					ptl: /[?!@⚠]/.test($boxMC[ik * kColumn + jk].textContent) ? 0 : -1
				};
			}
		}
	}
	// remove unknown marks from cells located near explored ones
	for (ik = 0; ik < kRow; ik++) {
		for (jk = 0; jk < kColumn; jk++) {
			if (MapData.cells[ik][jk].explored) {
				for (i = -1; i <= 1; i++) {
					for (j = -1; j <= 1; j++) {
						if (MapData.cells[ik+i][jk+j] && (!isCGM || !i || !j)) { MapData.cells[ik+i][jk+j].unknown = false; }
					}
				}
			}
		}
	}
	for (var si = 0; si < kRow; si++) {
		for (var sj = 0; sj < kColumn; sj++) {
			var ij, ttl = '',
				code = 0,
				cell = $boxMC[si * kColumn + sj],
				pointer = cell.textContent.trim(),
				chronopointers = cell.dataset.pointers;
			if (pointer === '🚪') {
				// replace exit sign. probably it should be done somewhere else
				cell.classList.add('map_exit_pos_' + worker.GUIp_locale);
			} else if (pointer === '?') {
				// assign different classes to differently unexplored cells
				cell.classList.add(MapData.cells[si][sj].unknown ? 'absolutelyUnknown' : 'notAWall');
			} else if (pointer === '@') {
				// check if current position has some directions in chronicle
				cell.classList.add('map_pos');
				if (chronopointers) {
					chronopointers = chronopointers.split(' ');
					for (i = 0, len = chronopointers.length; i < len; i++) {
						switch (chronopointers[i]) {
							case 'north_side': ttl += '╩'; break;
							case 'east_side':  ttl += '╠'; break;
							case 'south_side': ttl += '╦'; break;
							case 'west_side':  ttl += '╣'; break;
							case 'north_east': ttl += '↗'; break;
							case 'north_west': ttl += '↖'; break;
							case 'south_east': ttl += '↘'; break;
							case 'south_west': ttl += '↙'; break;
							case 'north':      ttl += '↑'; break;
							case 'east':       ttl += '→'; break;
							case 'south':      ttl += '↓'; break;
							case 'west':       ttl += '←'; break;
							case 'freezing': ttl += '✵'; break;
							case 'cold':     ttl += '❄'; break;
							case 'mild':     ttl += '☁'; break;
							case 'warm':     ttl += '♨'; break;
							case 'hot':      ttl += '☀'; break;
							case 'burning':  ttl += '✺'; break;
						}
					}
					GUIp.common.debug('current position has pointers:', ttl);
				}
			}
			if (/[←→↓↑↙↘↖↗⌊⌋⌈⌉∨<∧>╠╣╦╩]/.test(pointer + ttl)) {
				cell.classList.add('pointerMarker');
				if (cell.classList.contains('disabledPointer')) {
					continue;
				}
				MaxMap++;
				// get directions from the arrows themselves, not relying on parsed chronicles
				if (!ttl.length) {
					switch (pointer) {
						case '⌊': ttl = '↑→'; break;
						case '⌋': ttl = '↑←'; break;
						case '⌈': ttl = '↓→'; break;
						case '⌉': ttl = '↓←'; break;
						case '∨': ttl = '↖↗'; break;
						case '<': ttl = '↗↘'; break;
						case '∧': ttl = '↙↘'; break;
						case '>': ttl = '↖↙'; break;
						default: ttl = pointer; break;
					}
				}
				for (ij = 0, len = ttl.length; ij < len; ij++) {
					if ('→←↓↑↘↙↖↗╠╣╦╩'.includes(ttl[ij])) {
						code = ttl.charCodeAt(ij);
						for (ik = 0; ik < kRow; ik++) {
							for (jk = 0; jk < kColumn; jk++) {
								if (GUIp.common.checkDungeonArrow(code, ik - si, jk - sj, version) &&
									MapData.cells[ik][jk].ptl >= 0
								) {
									MapData.cells[ik][jk].ptl += 1024;
								}
							}
						}
					}
				}
			}
			if (/[✺☀♨☁❄✵]/.test(pointer + ttl)) {
				cell.classList.add('pointerMarker');
				if (cell.classList.contains('disabledPointer')) {
					continue;
				}
				MaxMapThermo++;
				// if we're standing on the pointer - use parsed value from chronicle
				if (ttl.length) {
					pointer = ttl;
				}
				var ThermoMinStep = 0; // minimum steps to the treasury
				var ThermoMaxStep = 0; // maximum steps to the treasury
				switch (pointer) {
					case '✺': ThermoMinStep = 1; ThermoMaxStep = 2; break;    // ✺ - очень горячо(1-2)
					case '☀': ThermoMinStep = 3; ThermoMaxStep = 5; break;    // ☀ - горячо(3-5)
					case '♨': ThermoMinStep = 6; ThermoMaxStep = 9; break;    // ♨ - тепло(6-9)
					case '☁': ThermoMinStep = 10; ThermoMaxStep = 13; break;  // ☁ - свежо(10-13)
					case '❄': ThermoMinStep = 14; ThermoMaxStep = 18; break;  // ❄ - холодно(14-18)
					case '✵': ThermoMinStep = 19; ThermoMaxStep = 100; break; // ✵ - очень холодно(19)
				}
				MapData.scanList = [];
				MapData.minStep = ThermoMinStep;
				MapData.maxStep = ThermoMaxStep;
				for (ik in MapData.cells) {
					for (jk in MapData.cells[ik]) {
						MapData.cells[ik][jk].step = NaN;
						MapData.cells[ik][jk].realstep = NaN;
					}
				}
				GUIp.common.mapIteration(MapData, si, sj, 0, false);
				for (ik = ((si - ThermoMaxStep) > 0 ? si - ThermoMaxStep : 0); ik <= ((si + ThermoMaxStep) < kRow ? si + ThermoMaxStep : kRow - 1); ik++) {
					for (jk = ((sj - ThermoMaxStep) > 0 ? sj - ThermoMaxStep : 0); jk <= ((sj + ThermoMaxStep) < kColumn ? sj + ThermoMaxStep : kColumn - 1); jk++) {
						if (MapData.cells[ik][jk].step >= ThermoMinStep && MapData.cells[ik][jk].step <= ThermoMaxStep) {
							if (MapData.cells[ik][jk].ptl >= 0) {
								MapData.cells[ik][jk].ptl+=128;
							}
						} else if (MapData.cells[ik][jk].step < ThermoMinStep && (!MapData.cells[ik][jk].realstep || MapData.cells[ik][jk].realstep >= ThermoMinStep)) {
							if (MapData.cells[ik][jk].ptl >= 0) {
								MapData.cells[ik][jk].ptl++;
							}
						}
					}
				}
			}
		}
	}
	if (MaxMap !== 0 || MaxMapThermo !== 0) {
		for (i = 0; i < kRow; i++) {
			for (j = 0; j < kColumn; j++) {
				if (MapData.cells[i][j].ptl === 1024*MaxMap + 128*MaxMapThermo) {
					$boxMC[i * kColumn + j].classList.add('pointerMatched');
				} else {
					for (ik = 0; ik < MaxMapThermo; ik++) {
						if (MapData.cells[i][j].ptl === 1024*MaxMap + 128*ik + (MaxMapThermo - ik)) {
							$boxMC[i * kColumn + j].classList.add('pointerMatchedThermo');
						}
					}
				}
			}
		}
	}
};

GUIp.common.mapIteration = function(MapData, iPointer, jPointer, step, specway) {
	if (++step > MapData.maxStep) {
		return;
	}
	if (MapData.cells[iPointer][jPointer].unknown) {
		specway = true;
	}
	for (var iStep = -1; iStep <= 1; iStep++) {
		for (var jStep = -1; jStep <= 1; jStep++) {
			if (iStep !== jStep && (iStep === 0 || jStep === 0)) {
				var iNext = iPointer + iStep,
					jNext = jPointer + jStep;
				if (iNext >= -1 && iNext <= MapData.kRow && jNext >= -1 && jNext <= MapData.kColumn) {
					if (MapData.cells[iNext][jNext] && !MapData.cells[iNext][jNext].wall) {
						var proceed = false;
						if (!specway && (!MapData.cells[iNext][jNext].realstep || MapData.cells[iNext][jNext].realstep > step)) {
							proceed = true;
							MapData.cells[iNext][jNext].realstep = step;
						}
						if (!MapData.cells[iNext][jNext].step || MapData.cells[iNext][jNext].step > step) {
							proceed = true;
							MapData.cells[iNext][jNext].step = step;
						}
						if (proceed) {
							GUIp.common.mapIteration(MapData, iNext, jNext, step, specway);
						}
					}
				}
			}
		}
	}
};

GUIp.common.describeCell = function(currentCell, stepNum, stepMax, stepData, trapMoveLossCount, wormholeSource) {
	var mark_no, marks_length, steptext, lasttext, titlemod, titletext;
	if (!wormholeSource) {
		for (mark_no = 0, marks_length = stepData.marks.length; mark_no < marks_length; mark_no++) {
			currentCell.classList.add(stepData.marks[mark_no]);
		}
	} else {
		currentCell.classList.remove('trapUnknown');
		currentCell.classList.add('wormhole');
	}
	if (stepData.pointers.length && !currentCell.title.startsWith('[' + worker.GUIp_i18n.map_pointer)) {
		titletext = currentCell.title.replace(/treasure is (on .*? side|in .*? directions?|within .*? steps!?|quite far)|сокровище (в .*? половине|в .*? направлени.|в двух шагах!|не дальше .*? шагов|где-то далеко)\n?/i,'');
		currentCell.title = '[' + worker.GUIp_i18n.map_pointer + ': ' + worker.GUIp_i18n[stepData.pointers[0]] + (stepData.pointers[1] ? worker.GUIp_i18n.or + worker.GUIp_i18n[stepData.pointers[1]] : '') + ']' + (titletext ? (titletext.startsWith(', ') ? '' : '\n') + titletext : '');
	}
	steptext = GUIp.common.splitSentences(stepData.text);
	if (stepNum === 1) {
		steptext = stepData.text.split('\n');
		if (steptext.length > 2) {
			steptext = [steptext.slice(1,-1).join('\n')];
		} else {
			steptext = [steptext[0]];
		}
	} else if (stepNum === stepMax) {
		steptext = steptext.slice(1);
	} else if (stepData.marks.includes('boss')) {
		steptext = steptext.slice(1, -2);
	} else if (stepData.marks.includes('trapMoveLoss') || trapMoveLossCount) {
		if (!trapMoveLossCount) {
			steptext = steptext.slice(1);
			trapMoveLossCount++;
		} else {
			steptext = steptext.slice(0, -1);
			trapMoveLossCount = 0;
		}
	} else if (stepData.marks.includes('staircase')) {
		steptext = steptext.slice(0, 1);
	} else {
		steptext = steptext.length > 2 ? steptext.slice(1, -1) : (steptext = GUIp.common.splitSentences(stepData.text,3), steptext.length > 2 ? steptext.slice(1, -1) : steptext.slice(0, -1));
	}
	steptext = steptext.join('').trim();
	if (currentCell.title.length) {
		titlemod = false;
		titletext = currentCell.title.split('\n');
		for (var i = 0, len = titletext.length; i < len; i++) {
			lasttext = /^(.*?) : (.*?)$/.exec(titletext[i]);
			if (lasttext && lasttext[2] === steptext) {
				if (lasttext[1] !== '#'+stepNum) {
					titletext[i] = lasttext[1] + ', #' + stepNum + ' : ' + steptext;
				}
				titlemod = true;
				break;
			}
		}
		if (!titlemod) {
			titletext.push('#' + stepNum + ' : ' + steptext);
		}
		currentCell.title = titletext.join('\n');
	} else {
		currentCell.title = '#' + stepNum + ' : ' + steptext;
	}
	return trapMoveLossCount;
};

// parsing dungeons!
GUIp.common.dungeonPhrases = [
	'bossHint','boss','bonusGodpower','bonusHealth','trapUnknown','trapTrophy','trapGold','trapLowDamage',
	'trapModerateDamage','trapMoveLoss','jumpingDungeon','treasureChest','pointerMarker','sideMarker','longJump',
	'deadEnd','directionless','vault','staircaseHint','staircase',
	'custom','discard' // these must be the last
];
// regexes here are case-sensitive to reduce false positives
GUIp.common.pointerRegExp = /[^\wА-ЯЁа-яё]((?:север|юг)о-(?:восток|запад)|север|восток|юг|запад|(?:очень )?(?:холодно|горячо)|свежо|тепло|(?:nor|sou)th-(?:ea|we)st|north|east|south|west|(?:very )?(?:cold|hot)|freezing|mild|warm|burning)/g;
GUIp.common.sideRegExp = /[^\wА-ЯЁа-яё](северн|восточн|южн|западн|north|east|south|west)/g;
GUIp.common.extraDiscardRegExp = /[]/g;

GUIp.common.parseDungeonPhrases = function(callback_success,callback_failure,timeout) {
	var i, j, len, category, phrases;
	if (timeout === null) {
		return;
	} else if (timeout) {
		worker.clearTimeout(timeout);
	}
	// prepare regular expressions
	for (i = 0, j = 0, len = GUIp.common.dungeonPhrases.length; i < len; i++) {
		category = GUIp.common.dungeonPhrases[i];
		if ((phrases = localStorage.getItem('LogDB:' + category + 'Phrases'))) {
			GUIp.common[category + 'RegExp'] = new RegExp(phrases, category === 'discard' ? 'g' : '');
			j++;
		}
	}
	if (j === len) {
		if (callback_success) {
			callback_success();
		}
	} else {
		// reschedule database update in 10 seconds
		if (+localStorage.getItem('LogDB:lastSerial') !== 0) {
			localStorage.setItem('LogDB:lastUpdate', Date.now() - 6*60*60*1000 + 10*1000);
			localStorage.setItem('LogDB:lastSerial', 0);
			GUIp.common.warn('not enough categories detected in phrases database, please retry in 10 seconds.');
		} else {
			GUIp.common.warn('not enough categories detected in phrases database.');
		}
		// clean partially loaded regexps
		for (i = 0, len = GUIp.common.dungeonPhrases.length; i < len; i++) {
			delete GUIp.common[GUIp.common.dungeonPhrases[i] + 'RegExp'];
		}
		if (callback_failure) {
			callback_failure();
		}
	}
};
GUIp.common.getDungeonPhrases = function(callback_success,callback_failure) {
	if (+localStorage.getItem('LogDB:lastUpdate') < (Date.now() - 6*60*60*1000)) {
		var timeout, customChronicler = localStorage.getItem('LogDB:dungeonPhrasesURL') || '';
		timeout = GUIp.common.setTimeout(function() { GUIp.common.parseDungeonPhrases(callback_success,callback_failure); timeout = null; },2e3);
		GUIp.common.getXHR(customChronicler.length >= 3 ? customChronicler : 'https://eximido.github.io/gvdb/dungeondb2_' + worker.GUIp_locale + '.json', function(xhr) {
			var i, j, len, response, category;
			try {
				if (xhr.lastModified && localStorage.getItem('LogDB:lastSerial') === xhr.lastModified) {
					localStorage.setItem('LogDB:lastUpdate', Date.now());
					GUIp.common.info('dungeon phrases DB is up to date');
					GUIp.common.parseDungeonPhrases(callback_success,callback_failure,timeout);
					return;
				}
				response = JSON.parse(xhr.responseText);
				if (response.status !== 'success') {
					throw 'request was unsuccessful';
				}
				for (i = 0, j = 0, len = GUIp.common.dungeonPhrases.length; i < len; i++) {
					category = GUIp.common.dungeonPhrases[i];
					if (response[category]) {
						localStorage.setItem('LogDB:' + category + 'Phrases', response[category]);
						j++;
					}
				}
			} catch (e) {
				GUIp.common.error('unexpected response to dungeon phrases update request:', e);
			}
			if (j === GUIp.common.dungeonPhrases.length) {
				localStorage.setItem('LogDB:lastUpdate', Date.now());
				localStorage.setItem('LogDB:lastSerial', xhr.lastModified);
			} else {
				GUIp.common.error(
					'not enough data to update phrases database (parsed', j, 'of', GUIp.common.dungeonPhrases.length, 'sections)');
			}
			GUIp.common.parseDungeonPhrases(callback_success,callback_failure,timeout);
		}, function() {
			GUIp.common.parseDungeonPhrases(callback_success,callback_failure,timeout);
		});
		return;
	}
	GUIp.common.parseDungeonPhrases(callback_success,callback_failure);
};

GUIp.common.setExtraDiscardData = function(texts) {
	GUIp.common.extraDiscardRegExp = texts.length ? (
		new RegExp('(^|[^\\wА-ЯЁа-яё])(?:' +
			texts.map(GUIp.common.escapeRegex).sort(function(a, b) { return b.length - a.length; }).join('|') +
		')(?![\\wА-ЯЁа-яё])', 'g')
	) : /[]/g;
};

GUIp.common.sanitizeChronicleText = function(text) {
	// we replace these fragments with underscores since some patterns in the Phrases DB rely on the fact
	// that heroes' names cannot be empty
	return text.replace(GUIp.common.discardRegExp, '_').replace(GUIp.common.extraDiscardRegExp, '$1_');
};

GUIp.common.bossAbilitiesList = {
	'золотоносный':'auriferous','глушащий':'deafening','лучезарный':'enlightened','взрывной':'explosive','неверующий':'faithless',
	'мощный':'hulking','паразитирующий':'leeching','бойкий':'nimble','ушастый':'overhearing','тащащий':'pickpocketing',
	'спешащий':'scurrying','творящий':'skilled','драпающий':'sneaky','транжирящий':'squandering','зовущий':'summoning',
	'пробивающий':'sweeping','мутирующий':'mutating','ломающий':'chipping','мнимый':'illusive','крепчающий':'escalating'
};

/**
 * @param {string} msg
 * @returns {number}
 */
GUIp.common.parseGatheredSoul = function(text) {
	var result, regex;
	regex = new RegExp('(' + worker.GUIp_i18n.gathered_soul_types.join('|') + ')' + (worker.GUIp_locale === 'en' ? ' soul' : 'ую душ(онк)?у'));
	if (result = regex.exec(text)) {
		return worker.GUIp_i18n.gathered_soul_types.indexOf(result[1]) + 1;
	}
	return 0;
};

GUIp.common.updateGatheredSouls = function(storage, date, origin, kind) {
	var list = JSON.parse(storage.get('LastGatheredSouls')) || [];
	for (var i = 0, len = list.length; i < len; i++) {
		if (Math.abs(date - list[i].date) < 180e3 && list[i].origin === origin && list[i].kind === kind) {
			// if we're trying to insert a same originated soul of the same kind within the 3 minute window,
			// then most likely it is a dupe
			return;
		}
	}
	list.unshift({date: date, origin: origin, kind: kind});
	list.sort(function(a, b) {
		return b.date - a.date;
	}).splice(5);
	storage.set('LastGatheredSouls', JSON.stringify(list));
};

/**
 * @param {string} msg
 * @param {string} heroName
 * @param {!Array<string>} otherNames
 * @param {function(string): number} check
 * @returns {number}
 */
GUIp.common.parseDungeonResultFromStep = function(msg, heroName, otherNames, check) {
	var res, pos, endPos = 0;
	// we need to replace character names in msg with something neutral, so in case we have
	// one name being a substring of another one the processing won't get confused
	// and we have to do it starting with the longest one due to the same reason
	otherNames.sort(function(a, b) { return b.length - a.length; });
	for (var i = 0, len = otherNames.length; i < len; i++) {
		pos = '%' + i + '%';
		if (otherNames[i] === heroName) {
			heroName = pos;
		}
		msg = msg.replace(new RegExp(otherNames[i],'g'),pos);
		otherNames[i] = pos;
	}
	while ((pos = msg.indexOf(heroName, endPos)) >= 0) {
		pos += heroName.length;
		endPos = Math.min.apply(null, otherNames.map(function(name) {
			var i = msg.indexOf(name, pos);
			return i >= 0 ? i : msg.length;
		}));
		if ((res = check(msg.slice(pos, endPos)))) {
			return res;
		}
	}
	return 0;
};

GUIp.common.parseChronicles = function(page, steps, options) {
	var i, len, lastNotParsed, ch, line, masterName, step = 1, time = '', texts = [], infls = [],
		chronicles = Array.from(page.querySelectorAll('#last_items_arena .new_line .text_content')),
		n_down = (page.querySelector('#last_items_arena .block_h a, #last_items_arena .afl a:not(#fight_log_capt)') || {}).textContent === '▲';
	if (n_down) {
		chronicles.reverse();
	}
	for (i = 0, len = chronicles.length; i < len; i++) {
		lastNotParsed = true;
		ch = chronicles[i];
		line = ch.textContent.trim();
		if (ch.className.includes('infl')) {
			infls.push(line.replace(/[\t\n\ ]+/g,' '));
		} else {
			texts.push(line);
		}
		if (ch.previousElementSibling && chronicles[i].previousElementSibling.className.includes('d_capt')) {
			time = chronicles[i].previousElementSibling.firstChild.textContent.trim() || time; // timestamp is in the first text node of `d_capt`
		}
		if ((i || options.compat) &&
			(n_down ? (
				chronicles[i + 1] && chronicles[i + 1].parentNode.style.borderBottomWidth
			) : ch.parentNode.style.borderBottomWidth) === '1px'
		) {
			GUIp.common.parseSingleChronicle.call(this, texts, infls, time, step, true);
			lastNotParsed = false;
			time = '';
			texts = [];
			infls = [];
			step++;
		}
		if (!ch.className.includes('infl')) {
			if (/voice from above announced that all bosses in|голос откуда-то сверху сообщил, что ни единого живого босса/.test(line)) {
				ch.innerHTML = ch.innerHTML.replace(/A pleasant voice from above announced that all bosses in this dungeon have perished and wished the intruders to burn in hell\.|Приятный голос откуда-то сверху сообщил, что ни единого живого босса (?:в этом подземелье|здесь) не осталось, и пожелал виновникам гореть в аду\./, '<strong>$&</strong>');
			} else if (options.putDMLink &&
				ch.parentNode.classList.contains('d_imp') &&
				!ch.getElementsByTagName('a')[0] &&
				(masterName = GUIp.common.findNonEmptyCapture(GUIp.common.customRegExp.exec(line)))
			) {
				ch.innerHTML = ch.innerHTML.replace(masterName,
					'<a href="' + (options.gvURL || '') + '/gods/' + encodeURIComponent(masterName) +
					'" target="_blank">' + masterName + '</a>'
				);
				GUIp.common.addListener(ch.getElementsByTagName('a')[0], 'click', function(ev) {
					// do not select this step in the replay when we click on the link
					if (!ev.button) ev.stopPropagation();
				});
			}
			if (line.includes('🎄')) {
				ch.innerHTML = ch.innerHTML.replace(/🎄(?!<)/g,
					'<span class="e_emoji e_emoji_xmas_tree' +
						(GUIp.common.renderTester.testChar('🎄') ? '' : ' eguip_font') +
					'">🎄</span>'
				);
			}
		}
	}
	if (lastNotParsed) {
		GUIp.common.parseSingleChronicle.call(this, texts, infls, time, step, true);
	}
	if (steps !== Object.keys(this.chronicles).length) {
		GUIp.common.warn('invalid number of steps detected! (have ' + Object.keys(this.chronicles).length + ' of ' + steps + ')');
	}
};

GUIp.common.parseSingleChronicle = function(texts, infls, time, step, trusted) {
	// keep the old time field since trusted data from chronicle log has time at the start of a step and we need to know it at the end
	if (this.chronicles[step] && trusted && this.chronicles[step].time) {
		time = this.chronicles[step].time;
	}
	if (!this.chronicles[step] || trusted) {
		// we may have step numbers messed up in live chronicles right after finishing the boss fight,
		// so we can only rely on trusted data from the chronicle log, thus allow it to completely overwrite any step
		this.chronicles[step] = { direction: null, marks: [], pointers: [], jumping: false, directionless: false, wormhole: false, wormholedst: null, text: texts.join('\n').replace('&nbsp;',' '), infls: infls, time: time };
	} else {
		// and we are not interested in rescanning fully processed entries multiple times
		return;
	}
	if (step <= 1) {
		return;
	}
	var i, len, j, len2, directionRegExp = /[^\wА-ЯЁа-яё-](север|восток|юг|запад|north|east|south|west)(?:[аеу]|wards?|ern|bound)?(?![\wА-ЯЁа-яё-])/,
		chronicle = this.chronicles[step],
		category, rx, direction;
	for (j = 0, len2 = texts.length; j < len2; j++) {
		texts[j] = GUIp.common.sanitizeChronicleText(texts[j]);
		// (-2) because of `custom` and `discard`
		for (i = 0, len = GUIp.common.dungeonPhrases.length - 2; i < len; i++) {
			category = GUIp.common.dungeonPhrases[i];
			rx = GUIp.common[category + 'RegExp'];
			if (rx && rx.test(texts[j]) && !chronicle.marks.includes(category)) {
				chronicle.marks.push(category);
			}
		}
		var stepSentences = GUIp.common.splitSentences(texts[j],3);
		if (!chronicle.marks.includes('staircase') && (direction = directionRegExp.exec(stepSentences[0]))) {
			chronicle.direction = direction[1];
		}
		chronicle.wormhole = chronicle.wormhole || GUIp.common.longJumpRegExp.test(texts[j]);
		chronicle.directionless = chronicle.directionless || (!chronicle.direction && GUIp.common.directionlessRegExp.test(stepSentences[0]));
		chronicle.jumping = chronicle.jumping || GUIp.common.jumpingDungeonRegExp.test(stepSentences[0]);
		if (chronicle.jumping && this.dungeonExtras && this.dungeonExtras.type === 'mystery' && step > this.dungeonExtras.typeStep) {
			this.parseDungeonExtras([], 'jumping', step);
		}
		var sideMarkersMatched = false;
		if (GUIp.common.pointerMarkerRegExp.test(texts[j]) || (sideMarkersMatched = GUIp.common.sideMarkerRegExp.test(texts[j]))) {
			var middle = stepSentences.length > 2 ? stepSentences.slice(1,-1).join(' ') : stepSentences[1];
			var pointer, pointers = middle.match(sideMarkersMatched ? GUIp.common.sideRegExp : GUIp.common.pointerRegExp);
			if (pointers && sideMarkersMatched) {
				switch (pointers[0].slice(1)) {
				case 'северн':
				case 'north': pointer = 'north_side'; break;
				case 'восточн':
				case 'east': pointer = 'east_side'; break;
				case 'южн':
				case 'south': pointer = 'south_side'; break;
				case 'западн':
				case 'west': pointer = 'west_side'; break;
				}
				if (pointer && !chronicle.pointers.includes(pointer)) {
					chronicle.pointers.push(pointer);
				}
			} else if (pointers && !sideMarkersMatched) {
				// check for arrow(s) first
				for (i = 0, len = pointers.length; i < len; i++) {
					pointer = null;
					switch (pointers[i].slice(1)) {
					case 'северо-восток':
					case 'north-east': pointer = 'north_east'; break;
					case 'северо-запад':
					case 'north-west': pointer = 'north_west'; break;
					case 'юго-восток':
					case 'south-east': pointer = 'south_east'; break;
					case 'юго-запад':
					case 'south-west': pointer = 'south_west'; break;
					case 'север':
					case 'north': pointer = 'north'; break;
					case 'восток':
					case 'east': pointer = 'east'; break;
					case 'юг':
					case 'south': pointer = 'south'; break;
					case 'запад':
					case 'west': pointer = 'west'; break;
					}
					if (pointer && !chronicle.pointers.includes(pointer)) {
						chronicle.pointers.push(pointer);
					}
				}
				// if nothing was found, check for thermo-hints
				if (!chronicle.pointers.length)
				for (i = 0, len = pointers.length; i < len; i++) {
					pointer = null;
					switch (pointers[i].slice(1)) {
					case 'очень холодно':
					case 'very cold':
					case 'freezing': pointer = 'freezing'; break;
					case 'холодно':
					case 'cold': pointer = 'cold'; break;
					case 'свежо':
					case 'mild': pointer = 'mild'; break;
					case 'тепло':
					case 'warm': pointer = 'warm'; break;
					case 'горячо':
					case 'hot': pointer = 'hot'; break;
					case 'очень горячо':
					case 'very hot':
					case 'burning': pointer = 'burning'; break;
					}
					if (pointer && !chronicle.pointers.includes(pointer)) {
						chronicle.pointers.push(pointer);
						// if there's anything left in pointers here then we most likely captured some superfluous word(s)
						// in this case we can only hope that the already captured word was the correct one (or rewrite phrases database at some point)
						break;
					}
				}
			}
		}
	}
};

GUIp.common.calculateDirectionlessMove = function(target, initCoords, initStep) {
	var i, len, j, coords = { x: initCoords.x, y: initCoords.y },
		dmap = document.querySelectorAll(target + ' .dml'),
		heroesCoords = GUIp.common.calculateXY(GUIp.common.getOwnCell()),
		steps = this.dmapMaxStep ? this.dmapMaxStep() : Object.keys(this.chronicles).length,
		directionless = 0,
		directionlessSteps = [],
		whstep = -1,
		ststep = -1,
		transitions = null;
	GUIp.common.debug(
		'going to calculate directionless moves from step #' + initStep + ' at [' + coords.y + ',' + coords.x + ']');
	for (i = initStep; i <= steps; i++) {
		if (this.chronicles[i].directionless) {
			directionlessSteps.push(i);
		}
		if (this.chronicles[i].wormhole && this.chronicles[i].wormholedst === null) {
			whstep = i;
			break;
		}
		if (this.chronicles[i].marks.includes('staircase')) {
			ststep = i;
			transitions = false;
			for (j = i + 1; j <= steps; j++) {
				if (this.chronicles[j].marks.includes('staircase')) {
					transitions = !transitions; // if this is true in the end, then we have returned to the same map again where we were at the initStep
				}
			}
			break;
		}
		GUIp.common.moveCoords(coords, this.chronicles[i]);
	}
	// we've moved to another floor and never returned, no reason for guessing directions here (this actually should be skipped already)
	if (transitions === false) {
		GUIp.common.warn('directionless combo is requested for wrong floor!',initStep);
		return {};
	}
	var variations = GUIp.common.getAllRPerms('nesw'.split(''),directionlessSteps.length),
		formatResult = function(combo) {
			var result = {};
			for (i = 0, len = combo.length; i < len; i++) {
				result[directionlessSteps[i]] = combo[i];
			}
			return result;
		};
	for (i = 0, len = variations.length; i < len; i++) {
		coords = { x: initCoords.x, y: initCoords.y };
		directionless = 0;
		for (j = initStep; j <= steps; j++) {
			if (this.chronicles[j].directionless) {
				GUIp.common.moveCoords(coords, { direction: this.corrections[variations[i][directionless]] });
				directionless++;
			} else {
				GUIp.common.moveCoords(coords, this.chronicles[j]);
			}
			if (!dmap[coords.y] || !dmap[coords.y].children[coords.x] || /[#!?]/.test(dmap[coords.y].children[coords.x].textContent)) {
				break;
			}
			if (whstep === j && (/[~@]/.test(dmap[coords.y].children[coords.x].textContent) || dmap[coords.y].children[coords.x].classList.contains('wormhole'))) {
				GUIp.common.debug('found result + wh:', variations[i].join());
				return formatResult(variations[i]);
			}
			if (ststep === j && /[◿@]/.test(dmap[coords.y].children[coords.x].textContent)) {
				GUIp.common.debug('found result + st:', variations[i].join());
				return formatResult(variations[i]);
			}
		}
		if (heroesCoords.x - coords.x === 0 && heroesCoords.y - coords.y === 0) {
			GUIp.common.debug('found result:', variations[i].join());
			return formatResult(variations[i]);
		}
	}
	GUIp.common.error('directionless combo not found!');
	return {};
};

GUIp.common.calculateWormholeMove = function(target, initCoords, initStep) {
	var result = GUIp.common.calculateWormholeMoveSub.call(this, target, initCoords, initStep, true);
	if (!result.wm) {
		GUIp.common.debug('retrying with wcheck disabled');
		result = GUIp.common.calculateWormholeMoveSub.call(this, target, initCoords, initStep, false);
	}
	if (result.wm) {
		GUIp.common.debug('found possible targets: ' + JSON.stringify(result));
	} else {
		GUIp.common.error('wormhole destination not found!');
	}
	return result;
};

GUIp.common.calculateWormholeMoveSub = function(target, initCoords, initStep, checkWalls, jumpList) {
	var i, j, m, n, noTML, result, subresult, corrections = {}, coords = { x: initCoords.x, y: initCoords.y },
		dmap = document.querySelectorAll(target + ' .dml'),
		heroesCoords = GUIp.common.calculateXY(GUIp.common.getOwnCell()),
		steps = this.dmapMaxStep ? this.dmapMaxStep() : Object.keys(this.chronicles).length,
		transitions = null;
	jumpList = jumpList ? jumpList.split(',') : [];
	jumpList.push(coords.x+':'+coords.y);
	GUIp.common.debug(
		'going to calculate wormhole jump target from step #' + initStep + ' at [' + initCoords.y + ',' + initCoords.x + ']');
	for (i = initStep+1; i <= steps; i++) {
		if (this.chronicles[i].marks.includes('staircase')) {
			transitions = false;
			for (j = i + 1; j <= steps; j++) {
				if (this.chronicles[j].marks.includes('staircase')) {
					transitions = !transitions; // if this is true in the end, then we have returned to the same map again where we were at the initStep
				}
			}
			break;
		}
	}
	// we've moved to another floor and never returned, no reason for guessing destinations here
	if (transitions === false) {
		return {};
	}
	for (m = -8; m <= 8; m++) {
		loopX:
		for (n = -8; n <= 8; n++) {
			corrections = {};
			if (Math.abs(m) + Math.abs(n) < 2 || Math.abs(m) + Math.abs(n) > 10) {
				continue loopX;
			}
			coords.x = initCoords.x + n;
			coords.y = initCoords.y + m;
			if (initStep === steps && coords.y === heroesCoords.y && coords.x === heroesCoords.x) {
				result = {wm: {}, dm: corrections};
				result.wm[initStep] = [m,n];
				return result;
			}
			if (!dmap[coords.y] || !dmap[coords.y].children[coords.x] || /[#!?]/.test(dmap[coords.y].children[coords.x].textContent)) {
				continue loopX;
			}
			if (checkWalls && !GUIp.common.checkWalls(dmap, coords, this.chronicles, initStep)) {
				continue loopX;
			}
			noTML = 1;
			for (i = initStep+1; i <= steps; i++) {
				if (!this.chronicles[i].directionless) {
					GUIp.common.moveCoords(coords, this.chronicles[i]);
				} else {
					if (!corrections[i]) {
						Object.assign(corrections, GUIp.common.calculateDirectionlessMove.call(this, target, coords, i));
						if (!corrections[i]) {
							continue loopX;
						}
					}
					GUIp.common.moveCoords(coords, {direction: this.corrections[corrections[i]]});
				}
				if (i === steps && coords.y === heroesCoords.y && coords.x === heroesCoords.x) {
					result = {wm: {}, dm: corrections};
					result.wm[initStep] = [m,n];
					return result;
				}
				if (!dmap[coords.y] || !dmap[coords.y].children[coords.x] || /[#!?]/.test(dmap[coords.y].children[coords.x].textContent)) {
					continue loopX;
				}
				if (this.chronicles[i].marks.includes('boss') && !(/[💀@]/.test(dmap[coords.y].children[coords.x].textContent) || dmap[coords.y].children[coords.x].classList.contains('boss'))) {
					continue loopX;
				}
				if (this.chronicles[i].marks.includes('staircase') && /[◿@]/.test(dmap[coords.y].children[coords.x].textContent)) {
					result = {wm: {}, dm: corrections};
					result.wm[initStep] = [m,n];
					return result;
				}
				if (this.chronicles[i].wormhole) {
					if (!(/[~@]/.test(dmap[coords.y].children[coords.x].textContent) || dmap[coords.y].children[coords.x].classList.contains('wormhole')) || jumpList.includes(coords.x+':'+coords.y)) {
						continue loopX;
					}
					subresult = GUIp.common.calculateWormholeMoveSub.call(this, target, {y: coords.y, x: coords.x}, i, checkWalls, jumpList.join(','));
					if (subresult.wm) {
						result = {wm: {}, dm: corrections};
						result.wm[initStep] = [m,n];
						Object.assign(result.wm,subresult.wm);
						Object.assign(result.dm,subresult.dm);
						return result;
					} else {
						continue loopX;
					}
				}
				if (this.chronicles[i].marks.includes('trapMoveLoss')) {
					noTML^=1;
				}
				if (checkWalls && noTML && !GUIp.common.checkWalls(dmap, coords, this.chronicles, i)) {
					continue loopX;
				}
			}
		}
	}
	return {};
};

GUIp.common.calcBlocked = function(chronicles, step) {
	var lastSentence, steptext = GUIp.common.sanitizeChronicleText(chronicles[step].text);
	lastSentence = (GUIp.common.splitSentences(steptext,3).slice(-1))[0];
	if (!lastSentence) {
		return 0;
	}
	if (GUIp.common.deadEndRegExp.test(lastSentence)) {
		lastSentence = ' ';
		if (chronicles[step+1] && chronicles[step+1].direction && !chronicles[step+1].jumping) {
			lastSentence += chronicles[step+1].direction;
		} else if (chronicles[step].direction && !chronicles[step].jumping) {
			switch (chronicles[step].direction) {
				case 'север':
				case 'north': lastSentence += 'south'; break;
				case 'восток':
				case 'east': lastSentence += 'west'; break;
				case 'юг':
				case 'south': lastSentence += 'north'; break;
				case 'запад':
				case 'west': lastSentence += 'east'; break;
			}
		}
	}
	var blocked = 0;
	if (/[^\wА-ЯЁа-яё-](?:север|восток|юг|запад|north|east|south|west)/.test(lastSentence) || lastSentence.includes('первый взгляд идти совершенно некуда.')) {
		blocked = 15;
		if (/[^\wА-ЯЁа-яё-](?:север|north)/.test(lastSentence)) {
			blocked -= 1;
		}
		if (/[^\wА-ЯЁа-яё-](?:юг|south)/.test(lastSentence)) {
			blocked -= 2;
		}
		if (/[^\wА-ЯЁа-яё-](?:запад|west)/.test(lastSentence)) {
			blocked -= 4;
		}
		if (/[^\wА-ЯЁа-яё-](?:восток|east)/.test(lastSentence)) {
			blocked -= 8;
		}
	}
	return blocked;
};

GUIp.common.checkWalls = function(dmap, initCoords, chronicles, step) {
	if (!dmap[initCoords.y - 1] || !dmap[initCoords.y + 1] || !dmap[initCoords.y - 1].children[initCoords.x] || !dmap[initCoords.y + 1].children[initCoords.x] || !dmap[initCoords.y].children[initCoords.x - 1] || !dmap[initCoords.y].children[initCoords.x + 1]) {
		return false;
	}
	var blocked = GUIp.common.calcBlocked(chronicles, step);
	if (!(blocked & 0x01) === dmap[initCoords.y - 1].children[initCoords.x].textContent.includes('#') ||
		!(blocked & 0x02) === dmap[initCoords.y + 1].children[initCoords.x].textContent.includes('#') ||
		!(blocked & 0x04) === dmap[initCoords.y].children[initCoords.x - 1].textContent.includes('#') ||
		!(blocked & 0x08) === dmap[initCoords.y].children[initCoords.x + 1].textContent.includes('#')) {
		return false;
	}
	return true;
};

GUIp.common.effectiveGodvoiceDirection = function(chronicles,step,isDetector) {
	if (!chronicles[step]) {
		return null;
	}
	var max, voice, key, keys, direction, directions = [0,0,0,0,0,0],
		vRegExp = worker.GUIp_locale === 'ru' ? /«(.*(?:север|юг|запад|восток|вниз|спуск|вверх|наверх|подним|лестниц).*)»/i : /“(.*(?:north|south|west|east|down|up).*)”/i,
		dRegExp = /север|юг|запад|восток|вниз|спуск|вверх|наверх|подним|лестниц|north|south|west|east|down|up/gi;
	var caser = function(str) {
		switch (str) {
			case 'север' :
			case 'north' : return 0;
			case 'юг'    :
			case 'south' : return 1;
			case 'запад' :
			case 'west'  : return 2;
			case 'восток':
			case 'east'  : return 3;
			case 'вниз'  :
			case 'спуск' :
			case 'down'  : return 4;
			case 'подним':
			case 'вверх' :
			case 'наверх':
			case 'лестниц':
			case 'up'    : return 5;
		}
		return null;
	};
	var replaceSpecDirs = function(str) {
		var dict = {
			север:  'норд',
			восток: 'ост',
			юг:    'зюйд',
			запад: 'вест'
		};
		// these "special" directions can be used only as distinct words,
		// but \b metacharacter doesn't work with non-latin, thus... we have this:
		Object.keys(dict).forEach(function(dir) {
			str = str.replace(new RegExp('(^|[!"#$%&\'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~ ])' + dict[dir] + 'у?($|[!"#$%&\'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~ ])','i'), '$1' + dir + '$2');
		});
		return str;
	};
	for (var i = 0, len = chronicles[step].infls.length; i < len; i++) {
		voice = replaceSpecDirs(chronicles[step].infls[i]);
		direction = vRegExp.exec(voice);
		// this is an arbitrary godvoice without any special words - skipping
		if (!direction) {
			continue;
		}
		// this is a directional godvoice, but we're interested only in cases where exactly one direction is specified
		if ((direction = direction[1].match(dRegExp)).length > 1 && (keys = Object.keys(GUIp.common.makeHashSet(direction))).length > 1) {
			// and there are several non-contradictory cellar-related special words that are allowed to be used simultaneously, in this case we shouldn't skip them
			if (![['вниз','спуск'],['вверх','наверх','подним','лестниц']].some(function(a) {
				return keys.every(function(b) {
					return a.includes(b);
				});
			})) {
				continue;
			}
		}
		if (isDetector) return true;
		if ((key = caser(direction[0])) !== null) {
			directions[key]++;
		}
	};
	if (isDetector) return false;
	max = Math.max.apply(null, directions);
	if (max < 1) {
		return null;
	}
	if (directions.filter(function(count) { return count === max; }).length > 1 && chronicles[step-1]) {
		if ((key = caser(chronicles[step-1].direction)) !== null) {
			directions[key]++;
		}
		max = Math.max.apply(null, directions);
	}
	if (directions.filter(function(count) { return count === max; }).length === 1) {
		switch (directions.findIndex(function(count) { return count === max; })) {
			case 0: return 'north';
			case 1: return 'south';
			case 2: return 'west';
			case 3: return 'east';
		}
	}
	return null;
};

GUIp.common.markGuidedSteps = function(step,isJumping,mapCells,coords) {
	if (this.chronicles[step] && (!this.chronicles[step].dGV || this.dungeonGuidedSteps[step] === 1 && isJumping && coords)) {
		// mark this step as visited so we won't be checking the same step again and again
		this.chronicles[step].dGV = true;
		// check whether there was a navigational godvoice at all
		if (!!GUIp.common.effectiveGodvoiceDirection(this.chronicles,step,true)) {
			// if it was, jumping dungeons may require additional checks
			if (isJumping && !this.chronicles[step].jumping) {
				if (!coords) {
					this.dungeonGuidedSteps[step] = 1; // this may be considered wrong for jumping dungeons in some cases, so we'd better recheck if we had a chance
				} else {
					var ncoords = {};
					switch (GUIp.common.effectiveGodvoiceDirection(this.chronicles,step)) {
						case 'north': ncoords = {y:coords.y-1, x:coords.x}; break;
						case 'south': ncoords = {y:coords.y+1, x:coords.x}; break;
						case 'west':  ncoords = {y:coords.y, x:coords.x-1}; break;
						case 'east':  ncoords = {y:coords.y, x:coords.x+1}; break;
					}
					// todo: if there were godvoices, but effective result was completely undefined or lead into a wall by some other indirect way, and the party didn't jump, should that count for guided steps or not?
					// in this code it would count step as a guided if certain direction was unknown, and won't count as a guided if direction was known and defined by the motion vector. probably it's wrong
					if (!(mapCells[ncoords.y] && mapCells[ncoords.y].children[ncoords.x] && mapCells[ncoords.y].children[ncoords.x].textContent.trim() === '#')) {
						this.dungeonGuidedSteps[step] = 2; // generally the evidence says that asking the party to jump over a wall won't count as a guided step if the attempt was unsuccessful. that's weird, but that's just it.
					}
				}
			} else {
				this.dungeonGuidedSteps[step] = 2;
			}
		}
	}
};

GUIp.common.moveCoords = function(coords, chronicle, direction) {
	direction = direction || 1;
	if (chronicle.direction) {
		var step = chronicle.jumping ? 2 : 1;
		step *= direction;
		switch (chronicle.direction) {
		case 'север':
		case 'north': coords.y -= step; break;
		case 'восток':
		case 'east': coords.x += step; break;
		case 'юг':
		case 'south': coords.y += step; break;
		case 'запад':
		case 'west': coords.x -= step; break;
		}
	}
};

GUIp.common.getOwnCell = function() {
	var cells = document.querySelectorAll('.dml .dmc');
	for (var i = 0, len = cells.length; i < len; i++) {
		if (cells[i].textContent.trim() === '@') {
			return cells[i];
		}
	}
	return null;
}

GUIp.common._isDungeonCellar = {
	state: null,
	step: -1,
	parse: function(step) {
		this.step = step;
		var fname = document.querySelector('#map .block_content > div > div, #ar_name, #hero2 .block div + div, .e_m_dmap > div');
		if (!fname) {
			this.state = false;
			return;
		}
		if (fname.textContent.includes(worker.GUIp_i18n.map_cellar_substr)) {
			this.state = true;
			return;
		}
		// currently there's a bug (?) in custom dungeons where a custom basement map
		// doesn't have a label marking it being a basement, and this breaks everything to hell.
		// so below is our attempt to workaround this until it's fixed in game (if ever)
		// first, let's quickly see if we have an exit mark on the map
		var cells = document.querySelectorAll('.dml .dmc');
		for (var i = 0, len = cells.length; i < len; i++) {
			if (/[ВE]|🚪/.test(cells[i].textContent.trim())) {
				// if we do then it isn't a basement, obviously
				this.state = false;
				return false;
			}
		}
		// second, check if this dungeon is custom by parsing text from its 1st step
		var chronicles = (GUIp.improver || GUIp.log || '').chronicles;
		if (chronicles && chronicles[1] && GUIp.common.customRegExp.exec(chronicles[1].text)) {
			// and if it is, iterate through all steps up to current and count the number of traversions
			var j = 0;
			for (var i = 1; i <= step; i++) {
				if (chronicles[i] && chronicles[i].marks.includes('staircase')) {
					j++;
				}
			}
			// if it is odd, we're in a basement
			this.state = !!(j % 2);
			return;
		}
		// and if we're here, it's either the first floor or our chronicle parsing has failed
		this.state = false;
	}
};

GUIp.common.isDungeonCellar = function() {
	// use cache to reparse chronicles only once per step
	var step = (GUIp.log && GUIp.log.dmapMaxStep()) || (GUIp.stats && GUIp.stats.currentStep()) || 0;
	if (GUIp.common._isDungeonCellar.step !== step) {
		GUIp.common._isDungeonCellar.parse(step);
	}
	return GUIp.common._isDungeonCellar.state;
}

GUIp.common.calculateXY = function(cell) {
	var coords = {};
	if (cell) {
		coords.x = GUIp.common.getNodeIndex(cell);
		coords.y = GUIp.common.getNodeIndex(cell.parentNode);
	}
	return coords;
};

GUIp.common.calculateExitXY = function(downstairs) {
	var exit_coords = { x: null, y: null },
		cells = document.querySelectorAll('.dml .dmc'),
		exit_mark = downstairs || GUIp.common.isDungeonCellar() ? /◿/ :/[ВE]|🚪/;
	for (var i = 0, len = cells.length; i < len; i++) {
		if (exit_mark.test(cells[i].textContent)) {
			exit_coords = GUIp.common.calculateXY(cells[i]);
			break;
		}
	}
	if (exit_coords.x === null) {
		if (GUIp.common.getOwnCell()) {
			exit_coords = GUIp.common.calculateXY(GUIp.common.getOwnCell());
		}
	}
	return exit_coords;
};

GUIp.common.calculateSpecOffset = function(type) {
	var sign,
		exit_coords = GUIp.common.calculateExitXY(),
		spec_coords = { x: null, y: null },
		spec_offset = { x: null, y: null },
		cells = document.querySelectorAll('.dml .dmc');
	switch (type) {
		case 'nook':
			sign = '✖';
			break;
		case 'stairs':
			sign = '◿';
			break;
	}
	if (!sign) {
		return spec_offset;
	}
	for (var i = 0, len = cells.length; i < len; i++) {
		if (cells[i].textContent.trim().includes(sign)) {
			spec_coords = GUIp.common.calculateXY(cells[i]);
			break;
		}
	}
	if (spec_coords.x !== null && spec_coords.x !== null) {
		spec_offset = { x: spec_coords.x - exit_coords.x, y: spec_coords.y - exit_coords.y };
	}
	return spec_offset;
};

GUIp.common.getNodeIndex = function(node) {
	var i = 0;
	while ((node = node.previousElementSibling)) {
		i++;
	}
	return i;
};

GUIp.common.getRPerms = function(array, size, initialStuff, output) {
	if (initialStuff.length >= size) {
		output.push(initialStuff);
	} else {
		for (var i = 0; i < array.length; ++i) {
			GUIp.common.getRPerms(array, size, initialStuff.concat(array[i]), output);
		}
	}
};

GUIp.common.getAllRPerms = function(array, size) {
	var output = [];
	GUIp.common.getRPerms(array, size, [], output);
	return output;
};

GUIp.common.splitSentences = function(text, expectedMinimalLength) {
	var result = GUIp.common.splitSentencesInt(text);
	if (expectedMinimalLength && result.length < expectedMinimalLength) {
		return GUIp.common.splitSentencesInt(text,true)
	}
	return result;
}

GUIp.common.splitSentencesInt = function(text, splitQuoted) {
	var letter, buffer = '',
		end = false,
		nested = false,
		colond = false,
		result = [];
	for (var i = 0, len = text.length; i < len; i++) {
		letter = text[i];
		switch (letter) {
			case '“':
			case '«':
				nested = true;
				buffer += letter;
				break;
			case '”':
			case '»':
				if (colond || (splitQuoted && result.length > 0)) {
					end = true;
				}
				nested = false;
				buffer += letter;
				break;
			case '.':
			case '!':
			case '?':
				if (!nested) {
					end = true;
				}
				buffer += letter;
				break;
			case ':':
				colond = true;
				buffer += letter;
				break;
			case '\n':
			case ' ':
				if (end && buffer.slice(-3) === '...') {
					end = false;
				}
				if (end) {
					result.push(buffer + letter);
					buffer = '';
					end = false;
					nested = false;
					colond = false;
				} else if (buffer.length > 0) {
					buffer += letter;
				}
				break;
			default:
				buffer += letter;
		}
	}
	if (buffer.length) {
		result.push(buffer);
	}
	if (!result.length) {
		result.push(text);
	}
	return result;
};

/**
 * @param {!Date} date - Date to be modified.
 * @param {number} hours
 * @param {number} minutes
 * @param {?string} [ampm]
 * @returns {number} Numeric representation of the updated date.
 */
GUIp.common.setTime = function(date, hours, minutes, ampm) {
	if (hours === 12) {
		if (ampm === 'AM') {
			hours = 0;
		}
	} else if (ampm === 'PM') {
		hours += 12;
	}
	return date.setHours(hours, minutes, 0, 0);
};

/**
 * @param {string} s
 * @returns {!Date}
 */
GUIp.common.parseDateTime = function(s) {
	var tz = 0,
		year = 0,
		m, date;
	if ((m = /(\d+)\.(\d+)\.(\d+)\.?\s+(\d+):(\d+)\s*(?:([+-])(\d+):(\d+))?/.exec(s))) {
		if (m[6]) {
			tz = +m[7] * 60 + +m[8];
			if (m[6] === '+') tz = -tz;
			tz -= new Date().getTimezoneOffset();
		}
		year = +m[3];
		return new Date(year >= 1000 ? year : year + 2000, +m[2] - 1, +m[1], +m[4], +m[5] + tz);
	}
	if ((m = /(\d+)\/(\d+)\/(\d+)\s+(\d+):(\d+)\s*(?:([AP]M)\s*)?(?:([+-])(\d+):(\d+))?/i.exec(s))) {
		if (m[7]) {
			tz = +m[8] * 60 + +m[9];
			if (m[7] === '+') tz = -tz;
			tz -= new Date().getTimezoneOffset();
		}
		year = +m[3];
		date = new Date(year >= 1000 ? year : year + 2000, +m[1] - 1, +m[2]);
		GUIp.common.setTime(date, +m[4], +m[5] + tz, (m[6] || '').toUpperCase());
		return date;
	}
	return new Date(NaN);
};

GUIp.common.formatTime = function(date, dtype) {
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	if (dtype === 'fakejson') {
		return ((new Date(date - date.getTimezoneOffset() * 60000)).toJSON() || '-').slice(0,19) +
			(date.getTimezoneOffset() < 0 ? '+' : '-') +
			('0' + Math.floor(Math.abs(date.getTimezoneOffset()) / 60)).slice(-2) + ':' +
			('0' + Math.floor(Math.abs(date.getTimezoneOffset()) % 60)).slice(-2);
	} else if (dtype === 'forum') {
		var offset = (Date.now() - date) / 60000;
		if (offset < 60) {
			if (offset < 1) {
				offset++;
			}
			return Math.ceil(offset) + ' ' + (offset > 1 ? worker.GUIp_i18n.format_time_minutes : worker.GUIp_i18n.format_time_minute) + ' ' + worker.GUIp_i18n.format_time_ago;
		} else if (offset < 1440) {
			return Math.ceil(offset / 60) + ' ' + (offset / 60 > 1 ? worker.GUIp_i18n.format_time_hours : worker.GUIp_i18n.format_time_hour) + ' ' + worker.GUIp_i18n.format_time_ago;
		} else {
			if (worker.GUIp_locale === 'ru') {
				if (offset < 2880) {
					return 'вчера';
				} else if (offset < 4320) {
					return '2 дня назад';
				} else {
					return ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
				}
			} else {
				if (offset < 2880) {
					return '1 day ago';
				} else if (offset < 4320) {
					return '2 days ago';
				} else {
					var hpost = false,
						hours = date.getHours();
					if (hours > 11) {
						hours -= 12;
						hpost = true;
					}
					if (hours < 1) {
						hours = 12;
					}
					return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear() + ' ' + hours + ':' + date.getMinutes() + (hpost ? 'pm' : 'am');
				}
			}
		}
	} else if (dtype === 'logger') {
		if (worker.GUIp_locale === 'ru') {
			return GUIp.common.formatTime(date,'simpledate');
		} else {
			return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear()
		}
	} else if (dtype === 'westerndate') {
		return ('0' + (date.getMonth() + 1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + '/' + ('' + date.getFullYear()).slice(-2);
	} else if (dtype === 'simpledate') {
		return ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
	} else if (dtype === 'simpletime') {
		return ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2);
	} else if (dtype === 'simpledatetime') {
		return GUIp.common.formatTime(date,'simpletime') + (+date - Date.now() > 82800e3 /* 1 day minus 1 hour */ ? ', ' + GUIp.common.formatTime(date,'simpledate') : '');
	} else if (dtype === 'fulltime') {
		return ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
	} else if (dtype === 'remaining') {
		if (date < 0) date = 0;
		var hrem = Math.floor(date/60), mrem = Math.floor(date%60);
		return (hrem < 10 ? '0' : '') + hrem + ':' + (mrem < 10 ? '0' : '') + mrem;
	} else {
		return ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() + ', ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
	}
};

GUIp.common.setPageBackground = function(background) {
	if (background === 'cloud') {
		if (worker.GUIp_browser !== 'Opera') {
			document.body.style.backgroundImage = 'url(' + worker.GUIp_getResource('images/background.jpg') + ')';
		} else {
			worker.GUIp_getResource('images/background.jpg',document.body);
		}
	} else {
		document.body.style.backgroundImage = background ? 'url(' + background + ')' : '';
	}
};

GUIp.common.cleanComboData = function(str) {
	return str.toLowerCase().replace(/[^a-zа-яё]+/g, '');
};

GUIp.common.checkExprData = function(str) {
	try {
		GUIp.common.expr.parse(str, true);
	} catch (e) {
		worker.alert(worker.GUIp_i18n.custom_informers_error + ':\n' + str + '\n\n' + e);
		return false;
	}
	return true;
};

GUIp.common.checkExprTitle = function(str) {
	try {
		GUIp.common.expr.parseEmbedded(str);
	} catch (e) {
		worker.alert(worker.GUIp_i18n.custom_informers_error + ':\n' + str + '\n\n' + e + (
			str.includes('{') ? GUIp_i18n.custom_informers_braces_note : ''
		));
		return false;
	}
	return true;
};

/**
 * @param {string} type
 * @returns {number}
 */
GUIp.common.parseInformerType = function(type) {
	var mask = 0x0,
		bit = 0x0;
	for (var i = 0, len = type.length; i < len; i++) {
		switch (type.charCodeAt(i) | 0x20) {
			case 115: /*s*/ bit = 0x1; break;
			case 108: /*l*/ bit = 0x10; break;
			case 100: /*d*/ bit = 0x20; break;
			case 97:  /*a*/ bit = 0x40; break;
			case 114: /*r*/ bit = 0x80; break;
			// codes above once were part of public API; should not change them
			case 113: /*q*/ bit = 0x100; break;
			case 119: /*w*/ bit = 0x200; break;
			case 110: /*n*/ bit = 0x400; break;
			// 0x800 is reserved for disabling informers in the shop
			case 105: /*i*/ bit = 0x1000; break;
			case 122: /*z*/ bit = 0x2000; break;
			default: mask |= 0x80000000; continue;
		}
		mask |= mask & bit ? 0x80000000 : bit;
	}
	return mask;
};

/**
 * @param {string} type
 * @returns {(string|number)}
 */
GUIp.common.sanitizeInformerType = function(type) {
	var oldMask = 0x0,
		mask = 0x0;
	if (!type) return 0;
	oldMask = +type || GUIp.common.parseInformerType(type);
	mask = oldMask & 0x3FF1; // drop unused bits
	// correct meaningless type values to something working
	if (mask & 0x481) { // R|N|S
		mask |= 0x10; // L
	} else if (mask & 0x300 && !(mask & 0xF0)) { // (Q|W) & !(L|D|A|R)
		mask |= 0x30; // LD
	}
	if ((mask & 0xC0) === 0xC0) { // AR
		mask ^= 0x40; // !A
	}
	if ((mask & 0x300) === 0x300) { // QW
		mask ^= 0x100; // !Q
	}
	if (!mask || mask & 0x800) return mask; // unrepresentable as an alphabetic sequence
	if (mask === oldMask && +type) return +type;
	type = mask & 0x10 ? 'L' : '';
	if (mask & 0x20)  type += 'D';
	if (mask & 0x40)  type += 'A';
	if (mask & 0x80)  type += 'R';
	if (mask & 0x400) type += 'N';
	if (mask & 0x1)   type += 'S';
	if (mask & 0x100) type += 'Q';
	if (mask & 0x200) type += 'W';
	if (mask & 0x1000) type += 'I';
	if (mask & 0x2000) type += 'Z';
	return type;
};

GUIp.common.createLightbox = function(lbType,storage,def,callback,ev) {
	var inheight, inwidth, sortable,
		lightbox = document.createElement("div"),
		dimmer = document.createElement("div");

	if (ev) ev.preventDefault();
	lightbox.id = 'optlightbox';
	lightbox.className = 'e_bl_cell block';
	dimmer.id = 'optdimmer';

	lightbox.innerHTML = '		<div id="lightbox_title" style="font-weight: bold;"></div>' +
'		<div class="bl_content" style="text-align: center;">' +
'			<div id="lightbox_desc" class="e_new_line"></div>' +
'			<div id="lightbox_table" class="e_new_line" >' +
'			</div><div>' +
'			<input id="lightbox_add" class="input_btn" type="button" value="' + worker.GUIp_i18n.lb_add + '">' +
'			<input id="lightbox_save" class="input_btn" type="submit" value="' + worker.GUIp_i18n.lb_save + '" disabled>' +
'			<input id="lightbox_reset" class="input_btn" type="button" value="' + worker.GUIp_i18n.lb_reset + '" disabled>' +
'			<input id="lightbox_import" class="input_btn" type="button" value="' + worker.GUIp_i18n.import + '">' +
'			<input id="lightbox_export" class="input_btn" type="button" value="' + worker.GUIp_i18n.export + '" disabled>' +
'			<input id="lightbox_close" class="input_btn" type="button" value="' + worker.GUIp_i18n.lb_close + '">' +
'			</div><div id="lightbox_ieblock" class="hidden"><span id="lightbox_iedesc"></span> <input id="lightbox_iefield" type="text" size="30" value=""> <input id="lightbox_iebutton" type="button" value="OK"></div>' +
'		</div>';

	document.body.appendChild(lightbox);
	document.body.appendChild(dimmer);

	var reloadSortables = function() {
		if (sortable) {
			sortable.destroy();
		}
		sortable = GUIp.common.sortable({els: '.lightbox_row', onDrop: function() { document.getElementById('lightbox_save').disabled = false; }});
	};

	var prepareLightboxInputValue = function(value) {
		return value ? ('' + value).replace(/&/g, '&amp;').replace(/'/g, '&apos;').replace(/"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;') : '';
	};

	var addLightboxRow = function(lbType, lbData, wantedPositioning) {
		var i, len, inputs, handler, lbRow, lbTable = document.getElementById('lightbox_table');
		lbRow = '<div class="lightbox_row"><span class="e_dragging" draggable="true">☰</span>';
		switch (lbType) {
			case 'ally_blacklist':
				lbRow += '<input type="checkbox"' + (lbData.q ? '' : ' checked') + ' title="' + worker.GUIp_i18n.lb_enable + '"> <input type="text" style="width: 22%;" value="' + prepareLightboxInputValue(lbData.n) + '"> <input type="text" style="width: 32%;" value="' + prepareLightboxInputValue(lbData.r) + '"> <input type="text" style="width: 17%;" value="' + prepareLightboxInputValue(lbData.s) + '">';
				break;
			case 'custom_craft':
				lbRow += '<input type="checkbox"' + (lbData.q ? '' : ' checked') + ' title="' + worker.GUIp_i18n.lb_enable + '"> <input type="text" style="width: 13%;" value="' + prepareLightboxInputValue(lbData.t) + '"> <input type="text" style="width: 28%;" value="' + prepareLightboxInputValue(lbData.d) + '"> <input type="text" style="width: 13%;" value="' + prepareLightboxInputValue(lbData.l) + '"> <input type="text" style="width: 13%;" value="' + prepareLightboxInputValue(lbData.g) + '">';
				break;
			case 'custom_informers':
				lbRow += '<input type="checkbox"' + (lbData.q ? '' : ' checked') + ' title="' + worker.GUIp_i18n.lb_enable + '"> <input type="text" style="width: 14%;" value="' + prepareLightboxInputValue(lbData.title) + '"> <textarea rows="1" class="taci" style="width: 50%;">' + prepareLightboxInputValue(lbData.expr) + '</textarea> <input type="text" style="width: 7%;" value="' + prepareLightboxInputValue(lbData.type) + '">';
				break;
			default:
				lbRow += '<input type="text" size="30" value="' + (typeof lbData === 'object' ? '' : prepareLightboxInputValue(lbData)) + '">';
				break;
		}
		lbRow += ' <input type="button" value="[x]"></div>';
		lbTable.insertAdjacentHTML('beforeend',lbRow);
		handler = function() {
			removeLightboxRow(this);
			changedLightbox();
		};
		inputs = lbTable.querySelectorAll('input[type="button"]:not(.improved)');
		for (i = 0, len = inputs.length; i < len; i++) {
			GUIp.common.addListener(inputs[i], 'click', handler);
			inputs[i].classList.add('improved');
		}
		inputs = lbTable.querySelectorAll('input:not(.improved), textarea:not(.improved)');
		for (i = 0, len = inputs.length; i < len; i++) {
			GUIp.common.addListener(inputs[i], 'change', changedLightbox);
			GUIp.common.addListener(inputs[i], 'input', changedLightbox);
			if (inputs[i].classList.contains('taci')) {
				GUIp.common.addListener(inputs[i], 'input', function(e) { autoresizeTextarea(e.target); });
			}
			inputs[i].classList.add('improved');
		}
	};

	var removeLightboxRow = function(input) {
		var lbTable = document.getElementById('lightbox_table');
		lbTable.removeChild(input.parentNode);
		reloadSortables();
	};

	var setLightboxTA = function(lbType, lbData) {
		var i, len, last, lbTable = document.getElementById('lightbox_table');
		while (last = lbTable.lastChild) {
			lbTable.removeChild(last);
		}
		for (i = 0, len = lbData.length; i < len; i++) {
			addLightboxRow(lbType, lbData[i]);
		}
		if (len === 0) {
			addLightboxRow(lbType, {});
		}
		reloadSortables();
		// resizing should be deferred until setting the dimensions for the lightbox table itself are done
		GUIp.common.setTimeout(function() {
			Array.from(lbTable.querySelectorAll('.taci')).forEach(function (e) {
				autoresizeTextarea(e);
			});
		}, 0);
	};

	var loadLightbox = function(lbType) {
		var lbData = storage.get('CustomWords:' + lbType);
		if (lbData) {
			try {
				lbData = JSON.parse(lbData);
			} catch (e) {
				lbData = [];
			}
			setLightboxTA(lbType,lbData);
			document.getElementById('lightbox_reset').disabled = false;
			document.getElementById('lightbox_export').disabled = false;
		} else {
			setLightboxTA(lbType,def[lbType] || []);
		}
	};

	var checkForDupes = function(parsed,key,val) {
		if (parsed.some(function(a) { return (key ? a[key] : a) === val})) {
			worker.alert(GUIp_i18n.fmt('lb_save_duplicate',val))
			return 1;
		}
		return 0;
	};

	var saveLightbox = function(lbType) {
		var i, len, items, parsed = [], lbItems, lbTable = document.getElementById('lightbox_table');
		lbItems = lbTable.querySelectorAll('input[type="checkbox"],input[type="text"],textarea');
		for (i = 0, len = lbItems.length; i < len; i++) {
			switch (lbType) {
				case 'ally_blacklist':
					items = [lbItems[i+1].value,lbItems[i+2].value,lbItems[i+3].value].map(function(s) { return s.trim(); });
					if (items[0].length) {
						if (checkForDupes(parsed,'n',items[0])) {
							return;
						}
						parsed.push({q: !lbItems[i].checked, n: items[0], r: (items[1] || ''), s: (items[2] || '')});
					}
					i += 3;
					break;
				case 'custom_craft':
					items = [lbItems[i+1].value,lbItems[i+2].value,lbItems[i+3].value,lbItems[i+4].value].map(function(s) { return s.toLowerCase().trim(); });
					if (items[0].length && items[1].length && GUIp.common.cleanComboData(items[2]) && GUIp.common.cleanComboData(items[3])) {
						parsed.push({q: !lbItems[i].checked, i: i, t: items[0], d: items[1], l: GUIp.common.cleanComboData(items[2]), g: GUIp.common.cleanComboData(items[3])});
					}
					i += 4;
					break;
				case 'custom_informers':
					items = [lbItems[i+1].value,lbItems[i+2].value,lbItems[i+3].value].map(function(s) { return s.trim(); });
					if (items[0].length && items[1].length) {
						if (lbItems[i].checked && !(GUIp.common.checkExprData(items[1]) && GUIp.common.checkExprTitle(items[0]))) {
							return;
						}
						if (checkForDupes(parsed,'title',items[0])) {
							return;
						}
						parsed.push({
							q: !lbItems[i].checked,
							title: items[0],
							expr: items[1],
							type: GUIp.common.sanitizeInformerType(items[2])
						});
					}
					i += 3;
					break;
				default:
					if ((items = lbItems[i].value.trim())) {
						if (checkForDupes(parsed,null,items)) {
							return;
						}
						parsed.push(items);
					}
			}
		}
		if (parsed.length) {
			storage.set('CustomWords:' + lbType, JSON.stringify(parsed));
			setLightboxTA(lbType,parsed);
			document.getElementById('lightbox_save').disabled = true;
			document.getElementById('lightbox_export').disabled = false;
			if (callback) { callback(); }
		} else {
			resetLightbox(lbType,true); // TODO: show error of some kind instead of silently resetting
		}
	};

	var resetLightbox = function(lbType,forced) {
		if (!forced && !worker.confirm(worker.GUIp_i18n.lb_reset_confirm)) {
			return;
		}
		setLightboxTA(lbType,def[lbType] || []);
		storage.remove('CustomWords:' + lbType);
		document.getElementById('lightbox_save').disabled = true;
		document.getElementById('lightbox_reset').disabled = true;
		document.getElementById('lightbox_export').disabled = true;
		if (callback) { callback(); }
	};

	var importLightboxData = function(lbType) {
		document.getElementById('lightbox_iefield').value = '';
		document.getElementById('lightbox_iedesc').textContent = worker.GUIp_i18n.import_prompt;
		document.getElementById('lightbox_iebutton').classList.remove('hidden');
		document.getElementById('lightbox_ieblock').classList.remove('hidden');
		lightbox.classList.add('expanded');
	};

	var importLightboxDataProcess = function(lbType) {
		var line = document.getElementById('lightbox_iefield').value;
		try {
			if (!line || !JSON.parse(line)) {
				throw '';
			}
			storage.set('CustomWords:' + lbType, line);
			setLightboxTA(lbType,JSON.parse(line));
			document.getElementById('lightbox_save').disabled = false;
			worker.alert(worker.GUIp_i18n.import_success);
			document.getElementById('lightbox_ieblock').classList.add('hidden');
			lightbox.classList.remove('expanded');
		} catch (e) {
			worker.alert(worker.GUIp_i18n.import_fail);
		}
	};

	var exportLightboxData = function(lbType) {
		var line = storage.get('CustomWords:' + lbType);
		if (line && JSON.parse(line)) {
			document.getElementById('lightbox_iefield').value = line;
			document.getElementById('lightbox_iedesc').textContent = worker.GUIp_i18n.export_prompt;
			document.getElementById('lightbox_iebutton').classList.add('hidden');
			document.getElementById('lightbox_ieblock').classList.remove('hidden');
			lightbox.classList.add('expanded');
		}
	};

	var changedLightbox = function() {
		var node;
		// in Chrome, it is possible to change <input>'s text via Ctrl+Z even if it is detached from the DOM root,
		// so we don't know if the lightbox is still opened
		if ((node = document.getElementById('lightbox_save'))) {
			node.disabled = false;
		}
		if ((node = document.getElementById('lightbox_reset'))) {
			node.disabled = false;
		}
	};

	var autoresizeTextarea = function(e) {
		e.style.height = 'auto';
		e.style.height = (e.scrollHeight + (e.offsetHeight - e.clientHeight)) + 'px';
	}

	document.getElementById('lightbox_title').textContent = worker.GUIp_i18n['lb_' + lbType + '_title'];
	document.getElementById('lightbox_desc').innerHTML = worker.GUIp_i18n['lb_' + lbType + '_desc'];

	loadLightbox(lbType);

	inheight = Math.max(worker.innerHeight * 0.7,400);
	inwidth = (lbType === 'custom_informers' ? 800 : 600);

	lightbox.style.width = inwidth + 'px';
	lightbox.style.height = inheight + 'px';

	lightbox.style.visibility = 'visible';
	lightbox.style.left = worker.innerWidth/2 - (inwidth / 2) + 'px';
	lightbox.style.top = worker.innerHeight/2 - (inheight / 2) + worker.scrollY + 'px';

	document.getElementById('lightbox_table').style.overflowY = 'scroll';
	document.getElementById('lightbox_table').style.height = (inheight - 125) + 'px';

	var scrollLightbox = GUIp.common.try2.bind(null, function() {
		lightbox.style.left = worker.innerWidth/2 - (inwidth / 2) + 'px';
		lightbox.style.top = worker.innerHeight/2 - (inheight / 2) + worker.scrollY + 'px';
	});
	var destroyLightbox = function() {
		document.body.removeChild(dimmer);
		document.body.removeChild(lightbox);
		document.removeEventListener('scroll', scrollLightbox);
	};
	document.addEventListener('scroll', scrollLightbox);
	GUIp.common.addListener(document.getElementById('lightbox_iebutton'), 'click', importLightboxDataProcess.bind(null, lbType));
	GUIp.common.addListener(document.getElementById('lightbox_add'), 'click', function() {
		addLightboxRow(lbType,{},true);
		reloadSortables();
	});
	GUIp.common.addListener(document.getElementById('lightbox_save'), 'click', saveLightbox.bind(null,lbType));
	GUIp.common.addListener(document.getElementById('lightbox_reset'), 'click', resetLightbox.bind(null,lbType,false));
	GUIp.common.addListener(document.getElementById('lightbox_import'), 'click', importLightboxData.bind(null,lbType));
	GUIp.common.addListener(document.getElementById('lightbox_export'), 'click', exportLightboxData.bind(null,lbType));
	GUIp.common.addListener(document.getElementById('lightbox_close'), 'click', destroyLightbox);
	GUIp.common.addListener(dimmer, 'click', destroyLightbox);
};

GUIp.common.sortable = function(options) {
	var dragEl, type, sortables, overClass, movingClass;
	function handleDragStart(e) {
		e.dataTransfer.effectAllowed = 'move';
		dragEl = this;
		// this/e.target is the source node.
		this.classList.add(movingClass);
		e.dataTransfer.setDragImage(this, 0, 0);
		e.dataTransfer.setData('text/html', this.innerHTML);
		options.onDragStart && options.onDragStart(e);
	}
	function handleDragOver(e) {
		if (e.preventDefault) {
			e.preventDefault(); // Allows us to drop.
		}
		e.dataTransfer.dropEffect = 'move';
		options.onDragOver && options.onDragOver(e);
		return false;
	}
	function handleDragEnter() {
		this.classList.add(overClass);
		options.onDragEnter && options.onDragEnter(e);
	}
	function handleDragLeave() {
		// this/e.target is previous target element.
		this.classList.remove(overClass);
		options.onDragLeave && options.onDragLeave(e);
	}
	function handleDrop(e) {
		var dropParent, dropIndex, dragIndex;
		// this/e.target is current target element.
		if (e.stopPropagation) {
			e.stopPropagation(); // stops the browser from redirecting.
		}
		if (dragEl !== this) {
			dragEl.innerHTML = this.innerHTML;
			this.innerHTML = e.dataTransfer.getData('text/html');
			options.onDrop && options.onDrop(e);
		}
		dragEl = null;
		return false;
	}
	function handleDragEnd() {
		for (var i = 0, len = sortables.length; i < len; i++) {
			sortables[i].classList.remove(overClass, movingClass);
		}
		options.onDragEnd && options.onDragEnd(e);
	}
	function destroy() {
		for (var i = 0, len = sortables.length; i < len; i++) {
			modifyListeners(sortables[i], false, true);
		}
		sortables = null;
		dragEl = null;
	}
	function modifyListeners(el, isAdd, flag) {
		var addOrRemove = isAdd ? 'add' : 'remove';
		el[addOrRemove + 'EventListener']('dragstart', handleDragStart);
		el[addOrRemove + 'EventListener']('dragenter', handleDragEnter);
		el[addOrRemove + 'EventListener']('dragover', handleDragOver);
		el[addOrRemove + 'EventListener']('dragleave', handleDragLeave);
		if (flag) {
			el[addOrRemove + 'EventListener']('drop', handleDrop);
			el[addOrRemove + 'EventListener']('dragend', handleDragEnd);
		}
	}
	function init() {
		sortables = Array.from(document.querySelectorAll(options.els));
		type = options.type || 'insert'; // insert or swap
		overClass = options.overClass || 'sortable-over';
		movingClass = options.movingClass || 'sortable-moving';
		for (var i = 0, len = sortables.length; i < len; i++) {
			modifyListeners(sortables[i], true, true);
		}
	}
	init();
	return {
		destroy: destroy
	};
};

GUIp.common.extrapolate = function(evidence,sample) {
	if (evidence[sample] !== undefined) { return evidence[sample]; }
	var closeMin = -Infinity, closeMax = Infinity, max = -Infinity, preMax = -Infinity, min = Infinity, preMin = Infinity, key;
	// find min and max
	for (key in evidence) {
		if (key < sample && key > closeMin) { closeMin = key; }
		if (key > sample && key < closeMax) { closeMax = key; }
		if (key > max) { preMax = max; max = key; }
		if (key < min) { preMin = min; min = key; }
		if (key < preMin && key > min) { preMin = key; }
	}
	// this is redefined if we want to extrapolate near the ends of the evidence set
	var baseValueIndex = closeMin;
	if (closeMax === Infinity) { closeMax = max; closeMin = preMax; baseValueIndex = max; }
	if (closeMin === -Infinity) { closeMax = preMin; closeMin = min; baseValueIndex = min; }
	var delta = closeMax - closeMin;
	var valDelta = evidence[closeMax] - evidence[closeMin];
	var deltaLength = (sample - baseValueIndex);
	return evidence[baseValueIndex] + (deltaLength * (valDelta / delta));
};

GUIp.common.forecastRegexes = {
	accu70: /распаковывается в 70% праны|accumulator charge restores 70%/,
	gvroads: /все дороги ведут в Годвилль|all roads lead to Godville/,
	hearing: /улучшат слышимость гласов героями|внимательно прислушиваются к гласам|с большим успехом общаться с героями|гласы с небес слышны лучше|с большей охотой реагируют на божьи гласы|heroes are a little more likely to hear and react|voices more likely to be heard|able to hear their deities|more likely to listen to their deities/,
	unhearing: /причиной искажения некоторых гласов|decrease godvoice hearing/,
	epic: /получить незапланированное эпическое|an unexpected epic quest/,
	fame: /произвести хорошее впечатление на горожан|добавить известности гильдиям|increase guild fame in towns/,
	melting: /Переплавить монеты в золотые кирпичи|выплавка кирпичей божественными влияниями|melting a golden brick with a god influence|melting gold coins into bricks/,
	retirement: /храмовладельцев хотя бы сегодня отказаться от пьянок|put more money in savings/,
	noconversion: /отрицательно влияют на свойства храмов|atmosphere is negatively affecting temples/,
	notraining: /услуги по прокачиванию умений сегодня не предоставляются|coaches went into the astral plane/,
	nopotions: /отказываются продавать лечебные снадобья|decided to stop selling healing potions|(clear|empty) their shelves of potions/,
	noequip: /магазинах снаряжения сегодня пусто|торговые лавки без нового снаряжения|left all equipment shops|without new equipment/,
	noguildhealing: /лекари не оказывают премиальных услуг за статус|No special treatment is provided to guild/,
	norstraders: /героям никто не облегчит их ношу|No one can ease the heroes. burden in the fields today|roadside traders are closed/,
	prayer: /эффективность молитв в храмах сегодня резко возрастет|increase the efficiency of all temple prayer/,
	longquests: /усложняет выполнение взятых сегодня заданий|complications in progressing with quests/,
	longauras: /ауры действуют на них гораздо дольше|increase duration of all auras|auras to stick onto the skin for longer/,
	shortauras: /ауры будут сдуваться|all auras to be half as long/,
	undead: /монстров возможно их воскрешение|monsters could self-resurrect/,
	corovans: /предрекает героям более частые встречи с вооруж.нными бандами|повышенной активности разбойничьих группировок/,
	goldmonsters: /монстра хоть что-нибудь да найдется|monsters should have something valuable/,
	goldbosses: /боссы сегодня носят заметно больше наличности|bosses to carry more gold/,
	easybosses: /Выкопать босса сегодня куда легче|underground monster lair today|likely to meet underground boss|left underground bosses sleep-deprived/,
	badtraders: /риск быть обманутым при продаже|likely to be fooled during a sale/,
	skills: /Умения сегодня используются заметно чаще|favors all skilled heroes|practicing their special skills/,
	arena: /уменьшает стоимость отправки на дуэль|magnetic cloud above the arena/,
	personality: /влияния богов сильнее склоняют чашу характера|влияния бога меняют характер героя сильнее|deeper personality changes/,
	itemloss: /массовой потере зрения и трофеев|sun could cause mass loss of|temporary loss of artifacts/,
	easyitems: /активируемые трофеи могут обходиться вдвое дешевле|half as much godpower for activation/,
	cheapitems: /продать их можно лишь за сущие|artifacts, greatly decreasing/,
	pricyitems: /трофеи сегодня можно продать в..ое дороже|activatable artifacts are worth twice/,
	bingo: /награда за бинго сегодня в разы больше обычной|Bingo game today is much higher/,
	fishing: /день застав(?:и|ляе)т героев садиться с удочкой почаще|are more likely to fish today|excellent fishing conditions/,
	resting: /отлеживания под деревом резко возросла|under a tree will be much more/,
	sidejobs: /(?:шанс получить|могут дать) незапланированный подряд|Unplanned side.jobs are possible|provide unscheduled side jobs/,
	tribbles: /наткнуться на триббла сегодня выше|пушисты и зубасты|Meeting a tribble is more likely/,
	sleeping: /герои с удовольствием спят чаще|heroes will happily sleep a bit more/,
	fastsell: /распродают трофеи гораздо быстрее|sell items much faster/,
	pointfall: /точки на картах возникают ближе и чаще|close and frequent spread of POIs/,
	lazypets: /не будут использовать свои способности|pets a day off, so they won.t use/,
	signs: /возможностей подать знак сегодня будет больше|increase in godly signs|more opportunities for sending god signs/,
	specbosses: /боссы будут иметь эту способность|calls for mild weather with a storm of|so many bosses are wielding/,
	hidden: /можно только гадать, в чем именно он|в каждом прогнозе должна быть загадка|уточнять его содержимое астрологи отказываются|прогноз написан невидимыми|поля этой газеты слишком малы|astrologers refused to disclose it|can only guess about/
};

GUIp.common.parseForecasts = function(input) {
	var pos, forecast = [];
	for (var key in GUIp.common.forecastRegexes) {
		if ((pos = input.search(GUIp.common.forecastRegexes[key])) >= 0) {
			forecast.push({pos: pos, key: key});
		}
	}
	return forecast.sort(function(a, b) { return a.pos - b.pos; }).map(function(a) { return a.key; });
};

GUIp.common.dmapExclCache = {};
GUIp.common.dmapExcl = function() {
	var isMapInCellar = GUIp.common.isDungeonCellar(),
		cellarShift = isMapInCellar ? 100 : 0,
		exitPos = GUIp.common.calculateExitXY(isMapInCellar),
		mapCells = document.querySelectorAll('.dml'),
		mapCache = GUIp.common.dmapExclCache;
	var ki, kj, kcontent;
	for (var i = 0, len = mapCells.length; i < len; i++) {
		for (var j = 0, len2 = mapCells[i].children.length; j < len2; j++) {
			// pos relative to exit
			ki = i - exitPos.y + cellarShift;
			kj = j - exitPos.x + cellarShift;
			// current cell content
			kcontent = mapCells[i].children[j].textContent.trim();
			if (kcontent === '!') {
				if (!mapCache[ki]) mapCache[ki] = {};
				mapCache[ki][kj] = true;
			} else if (mapCache[ki] && mapCache[ki][kj]) {
				if (kcontent === '?') {
					mapCells[i].children[j].textContent = '!';
					mapCells[i].children[j].classList.add('restoredExcl');
					mapCells[i].children[j].classList.remove('notAWall');
				} else {
					delete mapCache[ki][kj];
				}
			}
		}
	}
};

GUIp.common.dmapDisabledPointersCache = [];
GUIp.common.dmapDisabledPointersMark = function() {
	var x, y, isMapInCellar = GUIp.common.isDungeonCellar(),
		cellarShift = isMapInCellar ? 100 : 0,
		exitPos = GUIp.common.calculateExitXY(isMapInCellar),
		mapCells = document.querySelectorAll('.dml'),
		mapCache = GUIp.common.dmapDisabledPointersCache;
	// remove every existing .disabledPointer and its cell title additions, if any
	Array.from(document.querySelectorAll('.dml .disabledPointer')).forEach(function(cell) {
		cell.classList.remove('disabledPointer');
		cell.title = cell.title.replace(new RegExp('(\\[' + worker.GUIp_i18n.map_pointer + ': .*?\\]) \\(' + worker.GUIp_i18n.map_pointer_disabled + '\\)'),'$1');
	});
	// mark everything again, basically after map was rebuilt or when a pointer was switched
	mapCache.forEach(function(pair) {
		x = pair[0] + exitPos.x - cellarShift;
		y = pair[1] + exitPos.y - cellarShift;
		if (mapCells[y] && mapCells[y].children[x]) {
			mapCells[y].children[x].classList.add('disabledPointer');
			// for now add disablance notices only for pointers that are already properly titled
			if (mapCells[y].children[x].title.includes('[' + worker.GUIp_i18n.map_pointer + ':')) {
				mapCells[y].children[x].title = mapCells[y].children[x].title.replace(new RegExp('(\\[' + worker.GUIp_i18n.map_pointer + ': .*?\\])'),'$1 (' + worker.GUIp_i18n.map_pointer_disabled + ')');
			}
		}
	});
};

GUIp.common.dmapDisabledPointersSwitch = function(cell, saveCallback, highlightCallback) {
	var idx, isMapInCellar = GUIp.common.isDungeonCellar(),
		cellarShift = isMapInCellar ? 100 : 0,
		coords = GUIp.common.calculateXY(cell),
		exit_coords = GUIp.common.calculateExitXY(isMapInCellar),
		mapDistinctCells = document.querySelectorAll('.dmc'),
		mapCache = GUIp.common.dmapDisabledPointersCache;
	// we need relative coords
	coords.x = coords.x - exit_coords.x + cellarShift;
	coords.y = coords.y - exit_coords.y + cellarShift;
	// basically this we want to "toggle" this pointer
	if ((idx = mapCache.findIndex(function(pair) {
			if (pair[0] === coords.x && pair[1] === coords.y) {
				return true;
			}
			return false;
	})) >= 0) {
		mapCache.splice(idx, 1);
	} else {
		mapCache.push([coords.x, coords.y]);
	}
	// save whatever the result was
	saveCallback();
	// then apply updated .disabledPointer classes
	GUIp.common.dmapDisabledPointersMark();
	// and finally redraw matched cells again
	Array.from(mapDistinctCells).forEach(function(cell) {
		cell.classList.remove('pointerMatched');
		cell.classList.remove('pointerMatchedThermo');
	});
	highlightCallback();
};

GUIp.common.dmapDisabledPointersBind = function(saveCallback, highlightCallback) {
	if (GUIp.common.dmapDisabledPointersCache.length) {
		GUIp.common.dmapDisabledPointersMark();
	}
	// we want to bind this to the whole dungeon map container as this way we will be able to prevent events to propagate in all browsers,
	// while per-cell bindings seems to allow preventing propagation in chrome only
	var node = document.getElementsByClassName('dml')[0];
	if (node && (node = node.parentNode) && !node.dataset.ePointerSwitcher) {
		node.dataset.ePointerSwitcher = true;
		var tmout, target, switched = false,
			process = function(e) {
				// fixup for new nested cells in dungeon map - we need to support both new and old markup
				target = e.target.classList.contains('dmc') ? e.target : e.target.parentNode;
				// we're obviously interested in those cells that are pointer markers only
				if (target.classList.contains('pointerMarker') || /[←→↓↑↙↘↖↗⌊⌋⌈⌉∨<∧>╠╣╦╩✺☀♨☁❄✵]/.test(target.textContent)) {
					tmout = GUIp.common.setTimeout(function() {
						switched = true;
						GUIp.common.dmapDisabledPointersSwitch(target, saveCallback, highlightCallback);
						// if we got here on touch devices, it means we should also have a tooltip already popped up
						// so politely ask it to remove itself
						GUIp.common.tooltips.hide();
					},12e2);
				}
			},
			cancel = function() {
				worker.clearTimeout(tmout);
			};
		GUIp.common.addListener(node, 'mousedown', function(e) {
			// we're interested only in left mouse button,
			// and touch devices doesn't seem to be able to produce prolonged "click" emulation
			if (e.button !== 0 || GUIp.common.isTouching) { return; }
			process(e);
		}, true);
		GUIp.common.addListener(node, 'click', function(e) {
			// we don't want to process other click handlers when switching pointers,
			// but we do want them to act normally in all other cases
			if (switched) {
				switched = false;
				e.stopPropagation();
			}
			cancel();
		}, true);
		GUIp.common.addListener(node, 'touchstart', process, true);
		GUIp.common.addListener(node, 'touchend', cancel);
		GUIp.common.addListener(node, 'touchmove', cancel);
	}
};

// fixme: for now this simply moves cell titles from sub elements to their parents to restore the old markup the UI dungeon code historically relies on
GUIp.common.dmapTitlesFixup = function(mapCells) {
	Array.from(mapCells).forEach(function(row) {
		Array.from(row.children).forEach(function(cell) {
			if (cell.firstElementChild && cell.firstElementChild.title) {
				if (!cell.title) {
					cell.title = cell.firstElementChild.title;
				}
				cell.firstElementChild.removeAttribute('title');
			}
		});
	});
};

GUIp.common.dmapCoords = function() {
	var exitPos = GUIp.common.calculateExitXY(),
		mapCells = document.querySelectorAll('.dml');
	var ki, kj, kcontent;
	for (var i = 0, len = mapCells.length; i < len; i++) {
		for (var j = 0, len2 = mapCells[i].children.length; j < len2; j++) {
			// pos relative to exit
			ki = i - exitPos.y;
			kj = j - exitPos.x;
			// write coords to title
			kcontent = kj+';\xA0'+(-ki); // nbsp
			if (!mapCells[i].children[j].title.includes('[' + kcontent + ']')) {
				mapCells[i].children[j].title = '[' + kcontent + '] ' + (mapCells[i].children[j].title[0] === '#' ? '\n' : '') + mapCells[i].children[j].title;
			}
		}
	}
};

GUIp.common.dmapDimensions = function() {
	var rows, cols, mapCells = document.querySelectorAll('.dml');
	if (!mapCells.length) return '[0×0]';
	var checkForEdge = function(cells) {
		return cells.some(function(a) { return !(a.classList.contains('dmw') || a.classList.contains('absolutelyUnknown')) });
	};
	rows = mapCells.length;
	cols = mapCells[0].children.length;
	if (checkForEdge(Array.from(mapCells[0].children))) rows++;
	if (checkForEdge(Array.from(mapCells[mapCells.length-1].children))) rows++;
	if (checkForEdge(Array.from(mapCells, function(a) { return a.children[0]; }))) cols++;
	if (checkForEdge(Array.from(mapCells, function(a) { return a.children[a.children.length - 1]; }))) cols++;
	return '['+cols+'×'+rows+']';
};

/**
 * @param {!Array<{q: boolean, n: string}>} marks
 * @returns {!Object<string, {q: boolean, n: string}>}
 */
GUIp.common.preprocessPlayerBlacklist = function(marks) {
	var result = Object.create(null),
		mark;
	for (var i = 0, len = marks.length; i < len; i++) {
		mark = marks[i];
		if (!mark.q) {
			result[mark.n] = mark;
		}
	}
	return result;
};

/**
 * @param {!NodeList} players
 * @param {!Object<string, {r: string, s: string}>} marks
 */
GUIp.common.markBlacklistedPlayers = function(players, marks) {
	var playerName = '',
		player, mark;
	for (var i = 0, len = players.length; i < len; i++) {
		player = players[i];
		playerName = player.textContent;
		if (player.tagName !== 'A') {
			playerName = (/\((.*)\)/.exec(playerName) || [, playerName])[1];
		}
		if ((mark = marks[playerName])) {
			if (!player.classList.contains('e_player_marked')) {
				player.classList.add('e_player_marked');
				// save old title and style
				player.dataset.etitle = player.title;
				player.dataset.ecss = player.style.cssText;
			}
			player.title = GUIp_i18n.player_marked + (mark.r ? ':\n' + mark.r : '');
			player.style.cssText = player.dataset.ecss + (mark.s ? ' ' + mark.s : '');
		} else if (player.classList.contains('e_player_marked')) {
			player.classList.remove('e_player_marked');
			player.title = player.dataset.etitle || '';
			player.style.cssText = player.dataset.ecss || '';
		}
	}
};

GUIp.common.cleanupLogStorage = function() {
	if (localStorage.getItem('LogDB:cleanupDate') === GUIp.common.formatTime(new Date(),'logger')) {
		return;
	}
	// make a list of logs marked as a current one for whatever account
	var logID, i = 0, exemptLogs = [],
		patternCurrent = /eGUI_[^:]+:Log:current$/,
		patternLogs = /eGUI_[^:]+:Log:([^:]+):/;
	for (var key in localStorage) {
		if (patternCurrent.test(key)) {
			exemptLogs.push(localStorage.getItem(key));
		}
	}
	for (var key in localStorage) {
		if ((logID = key.match(patternLogs)) && !exemptLogs.includes(logID[1])) {
			i++;
			localStorage.removeItem(key);
		}
	}
	localStorage.setItem('LogDB:cleanupDate',GUIp.common.formatTime(new Date(),'logger'));
	GUIp.common.debug('old log keys cleaned up:', i);
};

GUIp.common.erinome_url = 'https://gv.erinome.net';
GUIp.common.erinome_url_en = 'https://gvg.erinome.net';
