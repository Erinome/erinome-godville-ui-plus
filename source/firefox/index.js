(function() {
'use strict';

var self = require('sdk/self'),
	window = require('sdk/window/utils').getMostRecentBrowserWindow(),
	pageMod = require('sdk/page-mod').PageMod;

pageMod({
	include: /https?:\/\/(?:(?:b\.)?godville\.net|gdvl\.tk|gvg?\.erinome\.net|godvillegame\.com)\/+(?:superhero|user\/+(?:profile|rk_success)|forums|news|gods\/+(?!api\/)|hero\/+last_fight|(?:reporter\/+)?duels\/+log\/).*/,
	contentScriptFile: self.data.url('main.js'),
	contentScriptWhen: 'ready',
	onAttach: function(worker) {
		// var sendResponse = worker.port.emit.bind(worker.port, 'backgroundScriptResponse');
		worker.port.on('contentScriptMsg', function(msg) {
			switch (msg.type) {
				case 'playsound':
					try {
						new window.Audio(msg.content).play();
					} catch (e) { }
					break;
			}
		});
	}
});

})();
