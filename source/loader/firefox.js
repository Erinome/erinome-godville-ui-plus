(function(window) {
'use strict';

//! include './utils.js';
//! include './xhrs.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = 'resource://godville-ui-plus-at-erinome-dot-net/content/';
// localStorage.setItem('eGUI_prefix', prefix);

modules.MODULES.browser = {src: 'guip_firefox.js'};

var messageSender = self.port.emit.bind(self.port, 'contentScriptMsg');
// self.port.on('backgroundScriptResponse', utils.postErinomeMessageTo.bind(null, '*'));

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr: xhrs.processWebXHR,
	playsound: messageSender
}));
initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix), function loadFont() { });

})(this);
