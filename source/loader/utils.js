/** @namespace */
var utils = {};

/**
 * @param {string} targetOrigin
 * @param {{type: string}} msg
 */
utils.postErinomeMessageTo = function(targetOrigin, msg) {
	window.postMessage({erinomeMessage: msg}, targetOrigin);
};
