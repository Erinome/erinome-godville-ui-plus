/** @namespace */
var initializer = {};

initializer.ROOT_MODULES = [['superhero'], ['options'], ['forum'], ['log']];

initializer.STYLE_SHEETS = [
	['common.css', 'superhero.css'],
	['common.css', 'options.css'],
	['common.css', 'forum.css'],
	['common.css', 'superhero.css']
];

/**
 * @param {string} pathname
 * @returns {number}
 */
initializer.detectPageType = function(pathname) {
	if (/^\/+superhero/.test(pathname)) {
		return 0;
	} else if (/^\/+user\/+(?:profile|rk_success)/.test(pathname)) {
		return 1;
	} else if (/^\/+(?:forums\/+(?:show(?:_topic)?\/+\d+|subs|last_posts|last_subs|posts_by_god)|forums$|news$|gods\/+(?!api\/)|hero\/+last_fight)/.test(pathname)) {
		return 2;
	} else if (/^(?:\/+reporter)?\/+duels\/+log\//.test(pathname)) {
		return 3;
	}
	return -1;
};

/**
 * @param {function(string, function())} moduleLoader
 * @param {function(string)} styleSheetLoader
 * @param {function(string, string)} fontLoader
 */
initializer.init = function(moduleLoader, styleSheetLoader, fontLoader) {
	var host = window.location.hostname;
	if (host === 'godville.net' || host === 'b.godville.net' || host === 'gdvl.tk' || host === 'gv.erinome.net') {
		modules.MODULES.i18n = modules.MODULES.phrasesRu;
	} else if (host === 'godvillegame.com' || host === 'gvg.erinome.net') {
		modules.MODULES.i18n = modules.MODULES.phrasesEn;
	} else {
		return;
	}
	var page = initializer.detectPageType(window.location.pathname);
	if (page === -1) return;
	fontLoader('eGUIp', 'eGUIp.otf');
	initializer.STYLE_SHEETS[page].forEach(styleSheetLoader);
	modules.load(initializer.ROOT_MODULES[page], moduleLoader);
};

/**
 * @param {function(!Object, string)} listener
 */
initializer.registerErinomeListener = function(listener) {
	window.addEventListener('message', function onMessage(ev) {
		var msg = ev.data;
		if (msg && (msg = msg.erinomeMessage)) {
			try {
				listener(msg, ev.origin);
			} catch (e) {
				console.error('[eGUI+] error:', e);
			}
		}
	});
};

/**
 * @param {!Object<string, function(!Object, function(!Object))>} callbacks
 * @returns {function(!Object, string)}
 */
initializer.createErinomeListener = function(callbacks) {
	return function(msg, origin) {
		var type = msg.type, callback;
		if (type === 'moduleInitialized') {
			modules.onInit.call(modules.MODULES[msg.which]);
		} else if ((callback = callbacks[type])) {
			callback(msg, utils.postErinomeMessageTo.bind(null, origin));
		}
	};
};
