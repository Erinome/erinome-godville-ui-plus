(function(window) {
'use strict';

//! include './utils.js';
//! include './modules.js';
//! include './initializer.js';

/**
 * @param {string} tag
 * @param {string} type
 * @param {(string|number)} id
 * @param {string} text
 */
var injectResource = function(tag, type, id, text) {
	var elem = document.createElement(tag);
	elem.type = type;
	elem.id = 'godville-ui-plus-' + id;
	elem.textContent = text;
	document.head.appendChild(elem);
};

/**
 * @param {(string|number)} id
 * @param {string} fontFamily
 * @param {string} fontData
 */
var injectFontFaceStyle = function(id, fontFamily, fontData) {
	injectResource('style', 'text/css', id,
		'@font-face { font-family: "' + fontFamily + '"; src: url("' + fontData + '"); }'
	);
};

/**
 * @this {function(this: !FileReader, string)}
 * @param {string} path
 * @param {function(string)} inject
 * @param {?function()} [onload]
 */
var loadResource = function(path, inject, onload) {
	var file = opera.extension.getFile('/content/' + path);
	if (file) {
		var reader = new FileReader;
		reader.onload = function() {
			inject(reader.result);
			if (onload) {
				onload();
			}
		};
		this.call(reader, file);
	}
};

var loadTextResource = loadResource.bind(FileReader.prototype.readAsText);
var loadBinResource  = loadResource.bind(FileReader.prototype.readAsDataURL);

/**
 * @returns {function(string, function())}
 */
var createModuleLoader = function() {
	var serial = 0;
	return function(src, onload) {
		loadTextResource(src, injectResource.bind(null, 'script', 'text/javascript', serial++), onload);
	};
};

/**
 * @returns {function(string)}
 */
var createStyleSheetLoader = function() {
	var serial = 0;
	return function(href) {
		loadTextResource(href, injectResource.bind(null, 'style', 'text/css', 'css-' + serial++));
	};
};

/**
 * @returns {function(string, string)}
 */
var createFontLoader = function() {
	var serial = 0;
	return function(fontFamily, src) {
		loadBinResource(src, injectFontFaceStyle.bind(null, 'font-' + serial++, fontFamily));
	};
};

modules.MODULES.browser  = {src: 'guip_opera.js'};
modules.MODULES.array    = {src: 'Array.js'};
modules.MODULES.domTokLs = {src: 'DOMTokenList.js'};
modules.MODULES.number   = {src: 'Number.js'};
modules.MODULES.object   = {src: 'Object.js'};
modules.MODULES.string   = {src: 'String.js'};

modules.MODULES.phrasesRu.deps.push('array', 'domTokLs', 'number', 'object', 'string');
modules.MODULES.phrasesEn.deps.push('array', 'domTokLs', 'number', 'object', 'string');

if (/(b\.)?godville\.net|godvillegame\.com|gdvl\.tk|gvg?\.erinome\.net/.test(window.location.host)) {
	window.opera.defineMagicFunction('GUIp_getResourceInternal', function(real, thisObject, url) {
		return opera.extension.getFile('/content/' + url);
	});
	var doInit = function() {
		window.removeEventListener('DOMContentLoaded', doInit);
		// hack to remove embedded youtube video since it breaks the login page badly
		var ytf = document.querySelector('iframe[src*="youtube.com"]');
		if (ytf) {
			ytf.parentNode.removeChild(ytf);
		}
		// initialize normally
		initializer.registerErinomeListener(initializer.createErinomeListener({}));
		initializer.init(createModuleLoader(), createStyleSheetLoader(), createFontLoader());
	};
	if (document.readyState === 'loading') {
		window.addEventListener('DOMContentLoaded', doInit);
	} else {
		doInit();
	}
	// attempt to load required polyfills on all pages since it's impossible properly open even the login page without these
	var polyLoader = function(event) {
		window.opera.removeEventListener('BeforeExternalScript', polyLoader, false);
		var loader = document.createElement('script');
		loader.id = 'eri_poly_loader';
		loader.textContent = '(function() { var scripts = [\'WeakMap.js\',\'MutationObserver.js\'];' +
			'scripts.forEach(function (fname) {' +
				'var request = new XMLHttpRequest();' +
				'request.open("GET", "https://bitbucket.org/Erinome/erinome-godville-ui-plus/raw/master/source/vendor/" + fname, false); request.send(null);' + /* make this BLOCK */
				'if (request.status === 200) { var script = document.createElement(\'script\'); script.textContent = request.responseText; document.head.insertBefore(script,document.getElementById(\'eri_poly_loader\')); }' +
			'}); })();';
		document.head.insertBefore(loader,event.element);
	}
	// we must use this special event to hook before any external script runs (some of them fail otherwise)
	window.opera.addEventListener('BeforeExternalScript', polyLoader, false);
}

})(window); // note: this !== window in Opera
