(function(window) {
'use strict';

//! include './utils.js';
//! include './xhrs.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = localStorage.eGUI_prefix = (chrome.extension.getURL || chrome.runtime.getURL)('');

modules.MODULES.browser = {src: 'guip_chrome.js'};

// Chrome 49 (the last available for XP) doesn't natively support Object.values
if (!Object.values) {
	modules.MODULES.object = {src: 'Object.js'};
	modules.MODULES.phrasesRu.deps.push('object');
	modules.MODULES.phrasesEn.deps.push('object');
}

var port = null;
var mv3 = chrome.runtime.getManifest().manifest_version === 3;

var sendMessage = function(msg) {
	// with mv3 we can't properly use ports as of chrome 126,
	// they get disconnected without triggering onDisconnect most of the time
	if (mv3) {
		// send an one-time message instead
		chrome.runtime.sendMessage(msg);
		return;
	}
	if (!port) {
		port = chrome.runtime.connect();
		port.onMessage.addListener(utils.postErinomeMessageTo.bind(null, '*'));
	}
	port.postMessage(msg);
};

// accept one-time messages sent back from the mv3 service worker
if (mv3) {
	chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
		// internal messages don't have tab property in sender object
		if (!sender.tab) {
			utils.postErinomeMessageTo('*',msg);
		}
	});
}

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr: xhrs.processWebXHR,
	playsound: sendMessage,
	makefocus: sendMessage,
	notify: sendMessage,
	notifyHide: sendMessage
}));
initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix), dom.createFontLoader(prefix));

})(this);
