(function() {
'use strict';

chrome.runtime.onMessage.addListener(msg => {
	if ('playsound' in msg) {
		try {
			new Audio(msg.playsound).play();
		} catch (e) { }
	}
});

})();
