import datetime
import os.path
import re

from waflib           import Build
from waflib.Configure import conf


APPNAME = "godville-ui-plus"
with open("current_version", encoding="utf-8-sig") as f:
	VERSION = f.read().strip()


class Browser:
	def __init__(self, name, archive_suffix, flat_archive, script_dir, content_dir, http_name):
		self.name         = name
		self.dest_dir     = name
		self.full_name    = "%s-%s" % (APPNAME, name)
		self.archive_name = "%s-%s" % (self.full_name, archive_suffix)
		self.archive_root = "" if flat_archive else self.full_name
		self.script_dir   = os.path.join(self.dest_dir, script_dir)  # *.js
		self.content_dir  = os.path.join(self.dest_dir, content_dir) # *.css, *.png, etc.
		self.http_name    = http_name


BROWSERS = [
	Browser("chrome", "legacy.zip", False, "", "", "chromebin"),
	Browser("chrome-mv3", "latest.zip", False, "", "", "chromemv3bin"),
	Browser("firefox", "legacy.xpi", True, "content", "content", "firefoxbin"),
	Browser("firefox-we", "latest-unsigned.xpi", True, "", "", "firefoxwebin"),
	Browser("opera", "latest.oex", True, "content", "content", "operabin"),
]

CHROME, CHROME_MV3, FIREFOX, FIREFOX_WE, OPERA = BROWSERS

VARIABLE_RX = re.compile(r"@([A-Z_][A-Z0-9_]*)@")

VARIABLES = {
	"VERSION": VERSION,
}

CONFIG = ["packaging.template.ini", "packaging.ini"]


def options(opt):
	opt.load("""
		dyncp  fontforge  font_subset  http_post  js_preproc  sassc  subst_all  zip
	""", tooldir="waftools")
	opt.add_option("--font", action="store_true", default=False, help="rebuild custom font")


def configure(cnf):
	cnf.load("""
		dyncp  fontforge  font_subset  http_post  js_preproc  sassc  subst_all  zip
	""", tooldir="waftools")

	cnf.env.SASS_OPTIONS = {"output_style": "compact"} # For `libsass`.
	cnf.env.append_value("SASS_FLAGS", "--style=compact") # For `sassc`.


def build(bld):
	bld.post_mode = Build.POST_AT_ONCE # Generate tasks from every group before starting to run any.

	source_dir = bld.path.find_dir("source")
	binary_patterns = "**/*.png"
	for browser in BROWSERS:
		browser_dir = source_dir.find_dir(browser.name)
		bld.subst_all(
			source=browser_dir.ant_glob("**", excl=binary_patterns),
			strip=browser_dir,
			target=browser.dest_dir,
			encoding="utf-8",
			re_m4=VARIABLE_RX,
			**VARIABLES
		)
		bld.copy_all(
			source=browser_dir.ant_glob(binary_patterns),
			strip=browser_dir,
			target=browser.dest_dir,
		)

	scripts = source_dir.ant_glob("""
		common/**/*.js  forum/**/*.js  loader/**/*.js  log/**/*.js  mixins/**/*.js
		superhero/**/*.js  *.js
	""")
	substituted = [node.change_ext(".subst" + node.suffix()) for node in scripts]
	bld(
		features="subst",
		source=scripts,
		target=substituted,
		encoding="utf-8",
		re_m4=VARIABLE_RX,
		**VARIABLES
	)
	bld(
		features="js_preproc",
		source=substituted,
		target=[node.change_ext(".proc" + node.suffix()) for node in substituted],
		root_dir=source_dir.get_bld(),
		find_node=lambda p: source_dir.find_or_declare("%s.subst.proc%s" % os.path.splitext(p)),
	)

	script_dirs  = [browser.script_dir  for browser in BROWSERS]
	content_dirs = [browser.content_dir for browser in BROWSERS]

	bld(
		features="copy",
		source="source/loader/chrome.subst.proc.js",
		target="%s/loader.js" % CHROME.script_dir,
	)
	bld(
		features="copy",
		source="source/loader/chrome.subst.proc.js",
		target="%s/loader.js" % CHROME_MV3.script_dir,
	)
	bld(
		features="copy",
		source="source/loader/firefox.subst.proc.js",
		target="%s/data/main.js" % FIREFOX.dest_dir,
	)
	bld(
		features="copy",
		source="source/loader/firefox_we.subst.proc.js",
		target="%s/loader.js" % FIREFOX_WE.script_dir,
	)
	bld(
		features="copy",
		source="source/loader/opera.subst.proc.js",
		target="%s/includes/loader.js" % OPERA.dest_dir,
	)

	for module in ("common", "forum", "log", "superhero"):
		bld(
			features="copy",
			source="source/%s/main.subst.proc.js" % module,
			target=["%s/%s.js" % (prefix, module) for prefix in script_dirs],
		)
	for node in source_dir.ant_glob("*.js", excl="omap.js"):
		bld(
			features="copy",
			source=node.change_ext(".subst.proc" + node.suffix()),
			target=[os.path.join(prefix, node.name) for prefix in script_dirs],
		)
	bld(
		features="copy",
		source="source/omap.subst.proc.js",
		target=["%s/omap.js" % browser.script_dir for browser in BROWSERS if browser is not OPERA],
	)

	opera_polyfills = [
		"source/vendor/%s.js" % f
		for f in ("Array", "DOMTokenList", "MutationObserver", "Number", "Object", "String", "WeakMap")
	]
	bld.copy_all(
		source=bld.path.ant_glob("source/vendor/*.js", excl=opera_polyfills),
		strip="source/vendor",
		target=script_dirs,
	)
	bld.copy_all(
		source=opera_polyfills,
		strip="source/vendor",
		target=OPERA.script_dir,
	)
	bld.copy_all(
		source="source/vendor/Object.js",
		strip="source/vendor",
		target=CHROME.script_dir,
	)

	style_sheets = source_dir.ant_glob("*.sass")
	bld(
		source=style_sheets,
		options=bld.env.SASS_OPTIONS,
	)
	bld.copy_all(
		source=[node.change_ext(".css") for node in style_sheets],
		strip=source_dir.get_bld(),
		target=content_dirs,
	)

	if bld.options.font:
		bld(
			features="font_subset",
			source="fonts/DejaVuSans.ttf",
			target="fonts/DejaVuSans.sub.ttf",
			text="☠⚐⏎",
		)
		bld(
			features="font_subset",
			source="fonts/DejaVuSerif.ttf",
			target="fonts/DejaVuSerif.sub.ttf",
			text="⤑",
		)
		bld(
			features="font_subset",
			source="fonts/Symbola.otf",
			target="fonts/Symbola.sub.otf",
			text="📏🍴♂♀💰📦🐾🗳🎄",
			unicodes=[
				0xFE0E,  # Variation Selector-15
				0xFE0F,  # Variation Selector-16
				0x1F9B4, # Bone
			],
		)
		bld(
			features="fontforge",
			# The order of source fonts matters.
			source="""
				fonts/merge.pe
				fonts/DejaVuSerif.sub.ttf  fonts/DejaVuSans.sub.ttf  fonts/Symbola.sub.otf
			""",
			target=bld.srcnode.make_node("fonts/eGUIp.otf"),
		)
	bld.copy_all(
		source=bld.srcnode.make_node("fonts/eGUIp.otf"),
		# Old Firefoxes prohibit injecting fonts into the page due to CSP.
		target=[browser.content_dir for browser in BROWSERS if browser is not FIREFOX],
		strip="fonts",
	)

	bld.copy_all(
		source=bld.path.ant_glob("images/**"),
		target=content_dirs,
	)
	bld.subst_all(
		# `glob` to force files from the source directory to be taken, not from the build one.
		bld.path.ant_glob("update.rdf  updates.json"),
		encoding="utf-8",
		re_m4=VARIABLE_RX,
		**VARIABLES
	)
	bld.copy_all(
		source="LICENSE",
		target=[browser.dest_dir for browser in BROWSERS],
	)

	bld.add_group() # Complete previous tasks before running the following ones.

	for browser in BROWSERS:
		bld(
			features="zip",
			source=bld.bldnode.make_node(browser.dest_dir),
			target=browser.archive_name,
			prefix=browser.archive_root,
		)

	# Firefox WE extensions should have a timestamp in their file names.
	now = datetime.datetime.now()
	bld(
		features="dyncp",
		source=FIREFOX_WE.archive_name,
		target="%s-latest-%s.xpi" % (FIREFOX_WE.full_name, now.strftime("%Y%m%d%H%M%S")),
		# Delete outdated archives.
		outdated=bld.bldnode.ant_glob(
			"%s-latest*.xpi" % FIREFOX_WE.full_name,
			excl=FIREFOX_WE.archive_name, # Be sure to leave the primary one untouched.
			# Without this parameter, Waf forgets nodes that do not exist now but are to be created
			# in the current build. This disables caching when `build` is called the second time
			# after `clean`. (That's not what we want.)
			remove=False,
		),
		# Firefox blocks extension files being used, so they could be impossible to remove.
		suppress_errors=True,
	)


class UploadContext(Build.BuildContext):
	"""sends archives to a remote host"""

	cmd = fun = "upload"


def upload(upl):
	import configparser

	config = configparser.ConfigParser()
	config.read(CONFIG, encoding="utf-8-sig")
	url = config["Upload"]["URL"]

	files = [
		("updaterdf",   upl.bldnode.find_node("update.rdf"),   "application/xml"),
		("updatesjson", upl.bldnode.find_node("updates.json"), "application/json"),
	]
	for browser in BROWSERS:
		if browser is not FIREFOX_WE:
			path = browser.archive_name
		else:
			path = "%s-latest.xpi" % FIREFOX_WE.full_name # Must be signed and placed manually.
		node = upl.bldnode.find_node(path)
		if node is None:
			upl.fatal("Cannot find %r" % upl.bldnode.make_node(path).srcpath())
		files.append((browser.http_name, node, "application/zip"))

	upl(
		features="http_post",
		url=url,
		files=files,
		data={"version": VERSION},
	)
